package com.im.api.validation;


import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.OrderSearchType;
import com.im.imonline.api.business.OrderSearchRequestDataConfig;
import com.im.imonline.api.constants.OrderSearchAPIConstants_New;

/**
 * @author usumas00
 * OrderSearchElasticAPIValidation
 * Order Search Automation -- Validation Class skeleton
 */
public class OrderSearchAPIValidation {
	Logger logger = Logger.getLogger("OrderSearchAPIValidation"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	HashMap<String, String> hmKeywordName = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	OrderSearchRequestDataConfig objConfig = null;

	public OrderSearchAPIValidation(APIAssertion pElem, OrderSearchRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		hmTestData=objConfig.gethmTestData();
	}



	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
		OrderSearchType[] listForValidation=objConfig.getListSetConfig();
		performResponseCodeValidation(objJSONUtil);
		if(objConfig.getListSetConfig() != null) {
			for(OrderSearchType attribute : listForValidation) {
				switch(attribute) {
					case ORDER_NUMBER:
						performValidationForOrderNumber(objJSONUtil);
					break;
					case SKU:
						performValidationForIngramPartNumber(objJSONUtil);
					break;
					case VPN:
						performValidationForVendorPartNumber(objJSONUtil);
					break;
					case RESELLER_NUMBER:
						performValidationForResellerNumber(objJSONUtil);
					break;
					case CUSTOMER_ORDER_NUMBER:
						performValidationForCustomerOrderNumber(objJSONUtil);
					break;
					case VENDOR_NAME:
						performValidationForVendorName(objJSONUtil);
					break;
					case ENDUSERPONUM:
						performValidationForEndUserPO(objJSONUtil);
					break;
					case SERIAL:
						performValidationForSerial(objJSONUtil);
						break;
					case UPC:
						performValidationForUPC(objJSONUtil);
						break;
					case BID:
						performValidationForBid(objJSONUtil);
						break;
					case CPN:
						performValidationForCPN(objJSONUtil);
						break;
					case INVOICENUMBER:
						performValidationForInvoiceNumber(objJSONUtil);
						break;
					case ORDER_DATE_RANGE:
						performValidationForOrderDate(objJSONUtil);
						break;
					case SHIP_DATE_RANGE:
						performValidationForShipDate(objJSONUtil);
						break;
					case BACKORDER_SAP:
						performValidationForBackOrderKeyForSAP(objJSONUtil);
						break;
					case STATUS_INPROGRESS:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.InProgress);
						break;
					case STATUS_PARTIALLYPROCESSED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.PartiallyProcessed);
						break;
					case STATUS_SHIPPED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.Shipped);
						break;
					case STATUS_PARTIALLYINVOICED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.PartiallyInvoiced);
						break;
					case STATUS_INVOICED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.Invoiced);
						break;
					case STATUS_ONHOLD:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.OnHold);
						break;
					case STATUS_CUSTOMERHOLD:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.CustomerHold);
						break;
					case STATUS_VOIDED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.Voided);
						break;
					case STATUS_BACKORDER_IMPLUSE:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.Backorder_IMPluse);
						break;
					case STATUS_ORDERREVIEW:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.OrderReview);
						break;
					case STATUS_CREDITCHECK:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.CreditCheck);
						break;
					case STATUS_PARTIALHOLD:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.PartialHold);
						break;
					case STATUS_PARTIALLYREJECTED:
						performValidationForOrderStatus(objJSONUtil, OrderSearchAPIConstants_New.PartiallyRejected);
						break;
				default:
					break;

				}//end of switch
			}//end of for
		} else
			objAPIAssertion.setExtentInfo("No Validation is asked to perform the Search Type is empty");
	}
	catch(Exception e) {
		String sErrMessage="FAIL: performValidationforGivenConfig() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
		logger.fatal(sErrMessage);       
		objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
	}
	}



	private void performValidationForOrderStatus(JsonUtil objJSONUtil,String orderStatus) throws Exception {
		objAPIAssertion.setExtentInfo("Picking the values from response");
		List<String> lstOrderstatus = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathStatus);
		objAPIAssertion.assertFalse(lstOrderstatus.size()==0, "List isn't empty");
		
		objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
		boolean bCheck = Util.compareListValuesWithExpectedValue(lstOrderstatus, orderStatus);
		objAPIAssertion.assertTrue(bCheck, "Validation of Invoice Number Expected ["+lstOrderstatus+"] Actual "+orderStatus);

	}



	private void performValidationForBackOrderKeyForSAP(JsonUtil objJSONUtil) throws Exception {
		boolean bCheck=true;
		objAPIAssertion.setExtentInfo("Picking the values from response");
		List<String> lstBackOrderedValue = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathBackOrdered);
		objAPIAssertion.assertFalse(lstBackOrderedValue.size()==0, "List isn't empty");
		String BackOrderedValue_A="A";
		String BackOrderedValue_B="B";

		objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
		for(String strActual : lstBackOrderedValue) {
			//String sActualwithSpace=strActual+"";
				if(!(strActual.equalsIgnoreCase(BackOrderedValue_B)||strActual.equalsIgnoreCase(BackOrderedValue_A))) {
					bCheck=false;
				break;
			}}

		
		objAPIAssertion.assertTrue(bCheck, "Validation of Back ORdered Orders For SAP nations");

	}



	private void performValidationForShipDate(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String>hmRequestMap =objConfig.gethmTestData();
			String sExpectedToDate=hmRequestMap.get(OrderSearchAPIConstants_New.ShipDateRange).substring(26, 36);
			String sExpectedFromDate=hmRequestMap.get(OrderSearchAPIConstants_New.ShipDateRange).substring(8, 18);
			objAPIAssertion.setExtentInfo("Expected Ship Date range Picked is from: "+sExpectedFromDate+" to: "+sExpectedToDate);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualShipDate = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathShipDateRange);
			objAPIAssertion.assertFalse(lstActualShipDate.size()==0,"List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= DateTimeUtil.verifyTheListOfDateFitBetweenTheMentionedRange(lstActualShipDate, sExpectedFromDate, sExpectedToDate,"yyyy-MM-dd");
			objAPIAssertion.assertTrue(bCheck,  "Validation of Ship Date Expected <i>"+sExpectedFromDate +" to "+sExpectedToDate+"</i> Actual ["+lstActualShipDate+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForShipDate() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForOrderDate(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String>hmRequestMap =objConfig.gethmTestData();
			String sExpectedToDate=hmRequestMap.get(OrderSearchAPIConstants_New.OrderDateRange).substring(26, 36);
			String sExpectedFromDate=hmRequestMap.get(OrderSearchAPIConstants_New.OrderDateRange).substring(8, 18);
			objAPIAssertion.setExtentInfo("Expected Order Date range Picked is from: "+sExpectedFromDate+" to: "+sExpectedToDate);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualOrderCreateDate = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathOrderCreateDate);
			objAPIAssertion.assertFalse(lstActualOrderCreateDate.size()==0,"List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= DateTimeUtil.verifyGivenDateFallsBetween2DatesInMentionedFormat(lstActualOrderCreateDate, sExpectedFromDate, sExpectedToDate,"yyyy-MM-dd");
			objAPIAssertion.assertTrue(bCheck,  "Validation of Order Date Expected ["+sExpectedFromDate +" to "+sExpectedToDate+"] Actual ["+lstActualOrderCreateDate+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForOrderDate() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForInvoiceNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedInvoiceNumber = hmRequestMap.get(OrderSearchAPIConstants_New.InvoiceNumber).replaceAll("\"", "");
			objAPIAssertion.setExtentInfo("Expected Invoice# is "+sExpectedInvoiceNumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualInvoiceNumber = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathInvoiceNumber);
			objAPIAssertion.assertFalse(lstActualInvoiceNumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualInvoiceNumber, sExpectedInvoiceNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Invoice Number Expected ["+sExpectedInvoiceNumber+"] Actual"+lstActualInvoiceNumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForInvoiceNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForCPN(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedCPN = hmRequestMap.get(OrderSearchAPIConstants_New.CPN);
			objAPIAssertion.setExtentInfo("Expected CPN# is "+sExpectedCPN);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualCPN = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCPN);
			objAPIAssertion.assertFalse(lstActualCPN.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualCPN, sExpectedCPN);
			objAPIAssertion.assertTrue(bCheck, "Validation of CPN Number Expected ["+sExpectedCPN+"] Actual"+lstActualCPN+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForCPN() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForBid(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedBid = hmRequestMap.get(OrderSearchAPIConstants_New.Bid);
			objAPIAssertion.setExtentInfo("Expected Bid# is "+sExpectedBid);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualBid = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathBid);
			objAPIAssertion.assertFalse(lstActualBid.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualBid, sExpectedBid);
			objAPIAssertion.assertTrue(bCheck, "Validation of Bid Number Expected ["+sExpectedBid+"] Actual"+lstActualBid+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForBid() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForUPC(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedUPC = hmRequestMap.get(OrderSearchAPIConstants_New.UPCNumber);
			objAPIAssertion.setExtentInfo("Expected UPC# is "+sExpectedUPC);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualUPC = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathUPCNumber);
			objAPIAssertion.assertFalse(lstActualUPC.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualUPC, sExpectedUPC);
			objAPIAssertion.assertTrue(bCheck, "Validation of UPC Number Expected ["+sExpectedUPC+"] Actual"+lstActualUPC+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForUPC() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForSerial(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedSerial = hmRequestMap.get(OrderSearchAPIConstants_New.SerialNumber);
			objAPIAssertion.setExtentInfo("Expected SKU# is "+sExpectedSerial);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualSerial = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSerialNumber);
			objAPIAssertion.assertFalse(lstActualSerial.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualSerial, sExpectedSerial);
			objAPIAssertion.assertTrue(bCheck, "Validation of Serial Number Expected ["+sExpectedSerial+"] Actual"+lstActualSerial+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForSerial() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForEndUserPO(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedEndUserPONumber = hmRequestMap.get(OrderSearchAPIConstants_New.EndUserPO).replaceAll("\"", "");
			objAPIAssertion.setExtentInfo("Expected End User PO# is "+sExpectedEndUserPONumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualEndUserPONumber = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathEndUserPO);
			objAPIAssertion.assertFalse(lstActualEndUserPONumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualEndUserPONumber, sExpectedEndUserPONumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of End User PO Number Expected ["+sExpectedEndUserPONumber+"] Actual"+lstActualEndUserPONumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForEndUserPO() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForVendorName(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedVendorName = hmRequestMap.get(OrderSearchAPIConstants_New.VendorName);
			objAPIAssertion.setExtentInfo("Expected Vendor is "+sExpectedVendorName);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualVendorName= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVendorName);
			objAPIAssertion.assertFalse(lstActualVendorName.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualVendorName, sExpectedVendorName);
			objAPIAssertion.assertTrue(bCheck, "Validation of Vendor Name Expected ["+sExpectedVendorName+"] Actual"+lstActualVendorName+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForVendorName() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForCustomerOrderNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedCustomerOrderNumber = hmRequestMap.get(OrderSearchAPIConstants_New.Customer_Order_Number).replaceAll("\"", "");
			objAPIAssertion.setExtentInfo("Expected ResellerPO# is "+sExpectedCustomerOrderNumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualCustomerOrderNumber = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCustomerOrderNumber);
			objAPIAssertion.assertFalse(lstActualCustomerOrderNumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualCustomerOrderNumber, sExpectedCustomerOrderNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Customer Number Expected ["+sExpectedCustomerOrderNumber+"] Actual"+lstActualCustomerOrderNumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForCustomerOrderNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForResellerNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedCustomerNumber = hmRequestMap.get(OrderSearchAPIConstants_New.ResellerNumber).replaceAll("\"", "");
			objAPIAssertion.setExtentInfo("Expected Customer number# is "+sExpectedCustomerNumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualCustomerNumber = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathResellerNumber);
			objAPIAssertion.assertFalse(lstActualCustomerNumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualCustomerNumber, sExpectedCustomerNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Customer Number Expected ["+sExpectedCustomerNumber+"] Actual"+lstActualCustomerNumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForResellerNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForOrderNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedOrderNumber = hmRequestMap.get(OrderSearchAPIConstants_New.OrderNumbers).replaceAll("\"", "");
			objAPIAssertion.setExtentInfo("Expected Order# is "+sExpectedOrderNumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualOrderNumber = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathOrderNumber);
			objAPIAssertion.assertFalse(lstActualOrderNumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualOrderNumber, sExpectedOrderNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Order Number Expected ["+sExpectedOrderNumber+"] Actual"+lstActualOrderNumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForOrderNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	private void performValidationForIngramPartNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedSKU = hmRequestMap.get(OrderSearchAPIConstants_New.IngramPartNumber);
			objAPIAssertion.setExtentInfo("Expected SKU# is "+sExpectedSKU);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualSKU = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU);
			objAPIAssertion.assertFalse(lstActualSKU.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualSKU, sExpectedSKU);
			objAPIAssertion.assertTrue(bCheck, "Validation of Ingram Part Number Expected ["+sExpectedSKU+"] Actual"+lstActualSKU+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForIngramPartNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

	private void performValidationForVendorPartNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedVPN = hmRequestMap.get(OrderSearchAPIConstants_New.VendorPartNumber);
			objAPIAssertion.setExtentInfo("Expected SKU# is "+sExpectedVPN);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualVPN = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVPN);
			objAPIAssertion.assertFalse(lstActualVPN.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualVPN, sExpectedVPN);
			objAPIAssertion.assertTrue(bCheck, "Validation of Ingram Part Number Expected ["+sExpectedVPN+"] Actual"+lstActualVPN+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForVendorPartNumber() method of OrderSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

}
