package com.im.api.validation;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.business.HermesE2ERegister;
import com.im.imonline.api.business.OrderUnit;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;



public class OrderSimulateAPIValidation extends CommonValidation{
	Logger logger = Logger.getLogger("OrderSimulateAPIValidation"); 
	HashMap<String, String> hmTestData = null, hmConfig = null;
	HashMap<String, String> expectedReqSkuData = new HashMap<String, String>();
	String sRequestBody=null, sReqSku =null; 
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objConfig = null;
	E2EHermesAPITestDataUtil objTestDataUtil=null;
	HermesE2ERegister hermesOption;


	public OrderSimulateAPIValidation(APIAssertion pElem, HermesE2EFlowRequestConfig objConfig2) {
		super(pElem);
		objAPIAssertion=pElem;
		objConfig=objConfig2;
		hmTestData=objConfig.getHmTestData();
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			performResponseCodeAndStatusCodeValidation(objJSONUtil,hmTestData.get(HermesAPIConstants.APITEMPLATE));		
			hermesOption=objConfig.getHermesRegisterForTestCase();
			performResponseOrderNumberValidation(objJSONUtil);
			objTestDataUtil=objConfig.getObjTestDataUtil();
			
			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{				
				//String Xpathvalue ="$..lines[?(@.partnumber=='"+oOU.getSKU()+"' && @.globallinenumber=='"+oOU.getGlobalLineNumber()+ "')]";
				String Xpathvalue ="$..lines[?(@.globallinenumber=~ /.*"+ oOU.getGlobalLineNumber()+ "/i)]";
				List<Object> listDetailsResData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
				HashMap<String, Object> hmpDetailsResData=(HashMap<String, Object>)listDetailsResData.get(0);

				objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.GLOBALDKUID),(String)oOU.getGlobalskuid(),"OrderSimulate Response: Item globalskuid of SKU: "+(String)oOU.getSKU());
				performQuantityValidation(hmpDetailsResData,oOU);
			}
			
		} catch (Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of OrderSimulateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performUnitpriceValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {		
		try {
			String uPrice=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.UNITPRODUCTPRICE));
			double unitPr=Double.parseDouble(uPrice);
			objAPIAssertion.assertEquals(unitPr,Double.parseDouble(oOU.getUnitPrice()), "OrderSimulate Response: Item UnitPrice for SKU["+(String)oOU.getSKU()+"]");
			} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performUnitpriceValidation() method of OrderSimulateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performQuantityValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			String quan=String.valueOf(hmpDetailsResData.get("requestedquantity"));
			double qty=Double.parseDouble(quan);
			objAPIAssertion.assertEquals(qty,oOU.getQuantity(), "OrderSimulate Response: Item Quantity for SKU["+(String)oOU.getSKU()+"]");
		} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performQuantityValidation() method of OrderSimulateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performResponseOrderNumberValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sOrderNumber = objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderquoteresponse.ordernumber");
			objAPIAssertion.assertHardTrue(sOrderNumber!=null, "OrderSimulate Response: Order number not null Validation for Order Simulate API Response");

			String sOrderSuffix = objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderquoteresponse.orderdistribution.ordersuffix");
			objAPIAssertion.assertHardTrue(sOrderSuffix!=null, "OrderSimulate Response: Order Suffix not null Validation for Order Simulate API Response");

			objAPIAssertion.setExtentInfo("OrderSimulate Response: The Order Number is: "+sOrderNumber+"-"+sOrderSuffix);
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseOrderNumberValidation() method of OrderSimulateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

}
