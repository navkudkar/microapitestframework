package com.im.api.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.aventstack.extentreports.Status;
import com.im.api.core.common.Constants;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIAssertion;

public class VPNPricingAPIValidation 
{
	HashMap<String, String> hmTestData = new HashMap<String,String>();
	APIAssertion objAPIAssertion;

	public VPNPricingAPIValidation(HashMap<String, String> phmTestData, APIAssertion pAPIAssertion) 
	{
		
		hmTestData=phmTestData;
		objAPIAssertion = pAPIAssertion;
		
	}
	
	public void ErrorCheckForVPNPricing(List<HashMap<String, String>> lstGetVPNRes) throws Exception 
	{
		
		for(HashMap<String, String> objHM : lstGetVPNRes)
		{		
			Set<String> objKeySet = objHM.keySet();
			for(String strKey : objKeySet)
		    {
			   String sExpectedError = hmTestData.get(strKey);
			   String sActualError = objHM.get(strKey);
			   
			   if(!sExpectedError.equalsIgnoreCase(Constants.BLANKVALUE) & null!=sActualError)
				   objAPIAssertion.assertEquals(sActualError, sExpectedError, "Error Message check for["+strKey+"]");
			   else
				   ExtentTestManager.log(Status.INFO,"Error Code does not exist" ,hmTestData.get(Constants.ExcelHeaderRunConfig));
		    }
		}
	}
}
	


