package com.im.api.validation;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.IntermediateResults;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.business.HermesE2ERegister;
import com.im.imonline.api.business.OrderUnit;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;

public class OrderCreateAPIValidation extends CommonValidation{
	Logger logger = Logger.getLogger("OrderCreateAPIValidation"); 
	HashMap<String, String> hmTestData = null, hmConfig = null;
	HashMap<String, String> expectedReqSkuData = new HashMap<String, String>();
	String sRequestBody=null, sReqSku =null; 
	FlagOptions_Hermes flagType = null;
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objConfig = null;
	E2EHermesAPITestDataUtil objTestDataUtil=null;
	private HermesE2ERegister hermesOption;


	public OrderCreateAPIValidation(APIAssertion pElem, HermesE2EFlowRequestConfig objConfig2) {
		super(pElem);
		objAPIAssertion=pElem;
		objConfig=objConfig2;
		hmTestData=objConfig.getHmTestData();
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			performResponseCodeAndStatusCodeValidation(objJSONUtil,hmTestData.get(HermesAPIConstants.APITEMPLATE));	
			hermesOption=objConfig.getHermesRegisterForTestCase();
			objTestDataUtil=objConfig.getObjTestDataUtil();
			performResponseOrderNumberValidation(objJSONUtil);

			//save freight amount for further validation
			objTestDataUtil.setIntermediateTestData(IntermediateResults.FREIGHTAMOUNT, 
					objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.freightamount"));

			//calculate OrderSubtotal Value and store in intermediate data for further validation
			//TODO: check with Hermes team for logic
			boolean flagFrAmtConsideredInSubTotalForCountry=hmTestData.get("RunConfig").equals("CA")?true:false;
			double frAmtForSubTotal=!flagFrAmtConsideredInSubTotalForCountry?Double.parseDouble(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.freightamount")):0.0;

			DecimalFormat df = new DecimalFormat("0.00");
			String sSubTotal=String.valueOf(df.format(Double.parseDouble(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.totalorderamount"))
					-Double.parseDouble(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.taxamount"))
					-frAmtForSubTotal)
					);
			objTestDataUtil.setIntermediateTestData(IntermediateResults.ORDERSUBTOTALVALUE, sSubTotal);

			if(!hermesOption.getScenarioForHoldOrder().equals("NONE"))
				objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.holdreasoncode"),"IM",
						"OrderCreate Response: Hold Order Parameter - holdreasoncode");

			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				String Xpathvalue;
				if(!objTestDataUtil.getCountryGroups().isSAP())
					Xpathvalue ="$..lines[?(@.globallinenumber=~ /.*"+ oOU.getGlobalLineNumber()+ "/i)]";
				else
					Xpathvalue ="$..lines[?(@.linenumber=~ /.*00"+ oOU.getGlobalLineNumber()+ "0/i)]";
				List<Object> listDetailsResData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
				HashMap<String, Object> hmpDetailsResData=(HashMap<String, Object>)listDetailsResData.get(0);
				if(hermesOption.getListOfProducts().length==1)
				{
					oOU.setOrderSubTotalForSKU(objTestDataUtil.getIntermediateTestData(IntermediateResults.ORDERSUBTOTALVALUE.toString()));
					oOU.setFreightAmountForSKU(objTestDataUtil.getIntermediateTestData(IntermediateResults.FREIGHTAMOUNT.toString()));	
				}
				else
				{
					oOU.setOrderSubTotalForSKU(hmpDetailsResData.get(HermesAPIConstants.NETAMOUNT).toString());
					oOU.setFreightAmountForSKU(hmpDetailsResData.get(HermesAPIConstants.FREIGHTAMOUNT).toString());
				}
				//order details not retrieved for SAP countries other than CL CO PE				
				if(!objTestDataUtil.getCountryGroups().isSAP() || hmTestData.get("RunConfig").matches("CO|PE|CL"))
				{
					objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.GLOBALDKUID),(String)oOU.getGlobalskuid(),"OrderCreate Response: Item globalskuid of SKU: "+(String)oOU.getSKU());
					performQuantityValidation(hmpDetailsResData,oOU);
				}
				
				if(!objTestDataUtil.getCountryGroups().isSAP())
				{
					if(!oOU.getProdcode().equals(FlagOptions_Hermes.DOWNLOADABLE) && !oOU.getProdcode().equals(FlagOptions_Hermes.DIRECTSHIP))
						objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.CARRIERCODE),(String)oOU.getCarriercode(),"OrderCreate Response: Item carriercode for SKU: "+(String)oOU.getSKU());

					objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.WAREHOUSEID),(String)oOU.getWarehouse(),"OrderCreate Response: Item warehouseid for SKU: "+(String)oOU.getSKU());
					performErpordernumberNotNullValidation(objJSONUtil,hmpDetailsResData,oOU);
				}
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	private void performErpordernumberNotNullValidation(JsonUtil objJSONUtil, HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			if(objTestDataUtil.getCountryGroups().isSAP())
				oOU.setErpOrderNumber((String)hmpDetailsResData.get(HermesAPIConstants.ERPORDERNUMBER));
			else
				oOU.setErpOrderNumber(objJSONUtil.getActualValueFromJSONResponseWithoutModify(HermesAPIConstants.XPATHINVOICINGSYSTEMORDERID));
			objAPIAssertion.assertHardTrue(oOU.getErpordernumber()!=null,"Item erpordernumber Not null for SKU: "+(String)oOU.getSKU());
			objAPIAssertion.setExtentInfo("Ingram Order number for sku: "+oOU.getSKU()+" "+(String)hmpDetailsResData.get(HermesAPIConstants.ERPORDERNUMBER));
		} catch (Exception e) {
			String sErrMessage="FAIL: performErpordernumberNotNullValidation() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performUnitpriceValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {		
		try {
			String uPrice=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.REQUESTEDUNITPRICE));
			double unitPr=Double.parseDouble(uPrice);
			objAPIAssertion.assertEquals(unitPr,Double.parseDouble(oOU.getUnitPrice()), "OrderCreate Response: Item UnitPrice for SKU["+(String)oOU.getSKU()+"]");
			uPrice=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.UNITPRODUCTPRICE));
			oOU.setUnitPrice(uPrice);
			} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performUnitpriceValidation() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performQuantityValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			String quan=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.REQUESTEDQUANTITY));
			double qty=Double.parseDouble(quan);
			objAPIAssertion.assertEquals(qty,oOU.getQuantity(), "OrderCreate Response: Item Quantity for SKU["+(String)oOU.getSKU()+"]");
			} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performQuantityValidation() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performResponseOrderNumberValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sOrderNumber = objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.customerponumber");
			objAPIAssertion.assertHardTrue(sOrderNumber!=null, "customerponumber not null Validation for Order Create API Response for: "+sOrderNumber);

			sOrderNumber = objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordercreateresponse.invoicingsystemorderid");
			objAPIAssertion.assertHardTrue(sOrderNumber!=null, "invoicingsystemorderid not null Validation for Order Create API Response for: "+sOrderNumber);

		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseOrderNumberValidation() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

}
