package com.im.api.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.business.HermesE2ERegister;
import com.im.imonline.api.business.OrderUnit;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;

/**
 * @author Vinita Lakhwani
 * HermesPnAAPIValidation
 * Hermes PnA -- Validation Class skeleton
 */
public class HermesPnAAPIValidation extends CommonValidation {
	Logger logger = Logger.getLogger("HermesPnAAPIValidation"); 
	HashMap<String, String> hmTestData = new HashMap<String, String>(), hmConfig = null;
	HashMap<String, String> expectedReqSkuData = new HashMap<String, String>();
	String sRequestBody=null, sReqSku =null; 
	FlagOptions_ProductSummary flagType = null;
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objConfig = null;
	E2EHermesAPITestDataUtil objTestDataUtil=null;
	HermesE2ERegister hermesOption=null;


	public HermesPnAAPIValidation(APIAssertion pElem, HermesE2EFlowRequestConfig objConfig2) {
		super(pElem);
		objAPIAssertion=pElem;
		objConfig=objConfig2;
		hmTestData=objConfig.getHmTestData();
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			List<FlagOptions_Hermes> flgOptLst=new ArrayList<FlagOptions_Hermes>();
			FlagOptions_Hermes[] flgoptArr= {FlagOptions_Hermes.QUANTITYBREAK,FlagOptions_Hermes.WARRANTY, FlagOptions_Hermes.JTYPE, FlagOptions_Hermes.DIRECTSHIP_2};
			flgOptLst=Arrays.asList(flgoptArr);
			performResponseCodeAndStatusCodeValidation(objJSONUtil,hmTestData.get(HermesAPIConstants.APITEMPLATE));

			objTestDataUtil=objConfig.getObjTestDataUtil();
			hermesOption=objConfig.getHermesRegisterForTestCase();
			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				String sSKU=oOU.getSKU().replaceFirst("^0+(?!$)", "");
				
				String Xpathvalue ="$..details[?(@.ingrampartnumber=='"+sSKU+"')]";
				List<Object> listDetailsResData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
				HashMap<String, Object> hmpDetailsResData=(HashMap<String, Object>)listDetailsResData.get(0);

				if(!flgOptLst.contains(oOU.getProdcode()))
				{
					oOU.setVpn(objTestDataUtil.getMapContentsOfProductSummaryData().get(oOU.getSKU()).get(HermesAPIConstants.MANUFACTURERPARTNUMBER));
					oOU.setGlobalskuid(objTestDataUtil.getMapContentsOfProductSummaryData().get(oOU.getSKU()).get(HermesAPIConstants.GLOBALMATERIAL));

					if(!objJSONUtil.getActualValueFromJSONResponseWithoutModify(HermesAPIConstants.XPATHSTATUSCODE).equals("200"))
						objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.ITEMSTATUS),"SUCCESS","Hermes P&A: Item Status Validation "+(String)oOU.getSKU());

					if(!objTestDataUtil.getCountryGroups().isSAP())
						objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.MANFACTURERPARTNUMBER),(String)oOU.getVpn(),"Hermes P&A: Item Manfacturerpartnumber of SKU: "+(String)oOU.getSKU());
					else
						objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.VENDORPARTNUMBER),(String)oOU.getVpn(),"Hermes P&A: Item Manfacturerpartnumber of SKU: "+(String)oOU.getSKU());
					
					String sGlobalID=(String)oOU.getGlobalskuid();
					sGlobalID=sGlobalID.substring(0, 5)+sSKU;		
					objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.GLOBALDKUID),sGlobalID,"Hermes P&A: Item Globalskuid of SKU: "+(String)oOU.getSKU());
					
					performQuantityValidation(hmpDetailsResData,oOU);
				}
				
				//this is required for the SKUs with product code warranty, qtybreak, jtype
				switch(oOU.getProdcode())
				{
				case WARRANTY:
				case QUANTITYBREAK:
				case ACOP_QUANTITYBREAK:
				case JTYPE:
				if(!objTestDataUtil.getCountryGroups().isSAP())
					oOU.setVpn((String)hmpDetailsResData.get(HermesAPIConstants.MANFACTURERPARTNUMBER));
				else
					oOU.setVpn((String)hmpDetailsResData.get(HermesAPIConstants.VENDORPARTNUMBER));
				oOU.setVendorCode((String)hmpDetailsResData.get(HermesAPIConstants.VENDORNUMBER));
				oOU.setGlobalskuid((String)hmpDetailsResData.get(HermesAPIConstants.GLOBALDKUID));
				break;
				
				default:
					break;
				}
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of HermesPnAAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performQuantityValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			String quan=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.QUANTITY));
			double qty=Double.parseDouble(quan);
			objAPIAssertion.assertEquals(qty,oOU.getQuantity(), "Hermes P&A: Item Quantity for SKU["+(String)oOU.getSKU()+"]");
			} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performQuantityValidation() method of HermesPnAAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

}
