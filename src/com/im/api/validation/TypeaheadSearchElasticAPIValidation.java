package com.im.api.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.SearchParam;
import com.im.imonline.api.business.TypeaheadSearchRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;

/**
 * @author Vinita Lakhwani
 * TypeaheadSearchElasticAPIValidation
 * Elastic Typeahead Search Automation -- Validation Class skeleton
 */
public class TypeaheadSearchElasticAPIValidation {
	Logger logger = Logger.getLogger("TypeaheadSearchElasticAPIValidation"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	SearchParam searchParam = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	TypeaheadSearchRequestDataConfig objConfig = null;
	
	public TypeaheadSearchElasticAPIValidation(APIAssertion pElem, TypeaheadSearchRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		hmTestData=objConfig.gethmTestData();
		searchParam=objConfig.getSearchConfig();
	}

	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of TypeaheadSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	/*private void performResKeySuggestionValidation(JsonUtil objJSONUtil) throws Exception {
		boolean matchKeyword=false;
		List<String> lstAct=new ArrayList<String>();
		String[] sActArr;
		try {
			List<Object> lstActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathTypeaheadResKeys);
			for(Object oAct : lstActual)
				lstAct.add(oAct.toString());

			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.QUERYSTRING);
			for(String sAct : lstAct)
			{
				sActArr= sAct.split(",");
				for(String sAct1 : sActArr)
					matchKeyword=sAct1.toLowerCase().contains(sExpected.toLowerCase());
				objAPIAssertion.assertTrue(matchKeyword, "typeahead suggestion key check . Expected : ["+sExpected+ "] contained in each of Actual : "+sAct);
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResKeySuggestionValidation() method of TypeaheadSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}*/
	
	public static synchronized <E> boolean compareListValuesWithExpValueMoreThanOneIgnoreSpclCharactersAndCase(List<E> lstActual, String expectedvalue) {

		boolean bResult=false;	
		Pattern special = Pattern.compile ("[./-]");
		Matcher hasSpclChars;
		for(E strActual : lstActual) {
			String sActual = strActual+"";
			hasSpclChars=special.matcher(sActual);
			if (hasSpclChars.find())
			{
				sActual=sActual.replaceAll("[./-]", " ");
				sActual=sActual.trim().replaceAll("  +", " ");
			}
			if(sActual.toLowerCase().contains(expectedvalue.toLowerCase())) {
				bResult=true;
				break;
			}}
		return bResult;	
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		boolean flag =false,flag1=true;
		List<String> lstAct=new ArrayList<String>();
		try {
			performResponseCodeValidation(objJSONUtil);

			if(objConfig.getSearchConfig()!=null)
			{
				String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.QUERYSTRING);

				List<List<Object>> lstActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathTypeaheadResKeys);
				for(List<Object> oAct : lstActual)
					for(Object obj : oAct)
						lstAct.add(obj.toString().toLowerCase());

				switch(objConfig.getSearchConfig())
				{
				case CATEGORY:
				case SUBCATEGORY:
				case PRODUCTTYPE:
				case VENDORNAME:
				case PRODUCTFAMILY:
					objAPIAssertion.assertHardTrue(lstAct.size()>0, "Validation: Typeahead Search String : ["+sExpected+"] fetch keys in response: \n");
					flag = lstAct.stream().allMatch(s -> s.contains(sExpected.toLowerCase().trim()));
					objAPIAssertion.assertTrue(flag, "Validation of Typeahead Search String : ["+sExpected+"] presence in each of the received keys in response: \n"+lstAct);
					break;
					
				case VENDOR_PRODTYPE:
				case VENDOR_PRODUCTFAMILY:
				case PRODTYPE_PRODUCTFAMILY:
				case VENDOR_PRODTYPE_PRODUCTFAMILY:
					objAPIAssertion.assertHardTrue(lstAct.size()>0, "Validation: Typeahead Search String : ["+sExpected+"] fetch keys in response: \n");
					objAPIAssertion.assertTrue(compareListValuesWithExpValueMoreThanOneIgnoreSpclCharactersAndCase(lstAct, sExpected),
							"Presence of Search String ["+sExpected+"] in at least one of the Typeahead response keys : "+lstAct);
					String[] arrExp=sExpected.split(" ");
					for(String sExp : arrExp)
					{
						flag = lstAct.stream().allMatch(s -> s.contains(sExp.toLowerCase().trim())); 
						flag1 = flag1 && flag;
					}	
					objAPIAssertion.assertTrue(flag1, "Validation of all parts of Typeahead Search String : ["+sExpected+"] presence in each of the received keys in response: \n"+lstAct);
					break;

				case INVALID:
					objAPIAssertion.assertHardTrue(lstAct.size()<=0, "Typeahead Invalid Search String : ["+sExpected+"] fetches empty response as expected\n");
					break;
				}
			} 
			else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of TypeaheadSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
}
