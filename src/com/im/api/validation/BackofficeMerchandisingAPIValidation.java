package com.im.api.validation;

import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_EXACT_LOCATION_FLAG;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_ALL;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_EXACT;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_PHRASE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_STATUS;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_TYPE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_SUB_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants;
import com.im.imonline.api.backoffice.business.BackOfficeRuleCreateRegister;
import com.jayway.jsonpath.JsonPath;



/**
 * @author Paresh Save
 * BackofficeMerchandisingAPIValidation
 * Backoffice Merchandising Automation -- Validation Class skeleton
 */
public class BackofficeMerchandisingAPIValidation {
	Logger logger = Logger.getLogger("BackofficeMerchandisingAPIValidation"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmTestFormMapData = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	BackOfficeRuleCreateRegister objBackofficeReg;
	String jsonResponse = null;
	List<HashMap<String, String>> lstAllSourceDataFromResponse=null;
	private List<String> lstDefaultValidationCheck = Arrays.asList("Name","PlacementType","PlacementZone","IsActive","Title","NewWindowFlg","AddSecurityToken");

	public BackofficeMerchandisingAPIValidation(APIAssertion pElem, BackOfficeRuleCreateRegister objBackofficeReg, HashMap<String, String> pExcelHMap) throws Exception {
		objAPIAssertion=pElem;
		this.objBackofficeReg=objBackofficeReg;	
		hmTestData=pExcelHMap;	
		hmTestFormMapData=(HashMap<String, String>) objBackofficeReg.getFormDataRequest();		
	}

	public void performRuleValidation(JsonUtil objJSONUtil) throws Exception {
		
		try {
			String sRuleID = objBackofficeReg.getRuleID();
			System.out.println("Rule id :     "+sRuleID);
			String xPathRule ="$..hits[?(@._id=='"+sRuleID.trim()+"')]._source";
			jsonResponse = objJSONUtil.getResponse().asString();
			lstAllSourceDataFromResponse = JsonPath.read(jsonResponse, xPathRule);
			boolean ruleDisplay = !(lstAllSourceDataFromResponse.isEmpty()||lstAllSourceDataFromResponse==null);
			if(objBackofficeReg.getRuleDisplayed()) {	
				if(!ruleDisplay) ruleDisplay = retryLogic(objJSONUtil);					
				objAPIAssertion.assertHardTrue(ruleDisplay, "Verify Rule is present in Product Placement API response.");					
				validateResponseFields(lstAllSourceDataFromResponse.get(0));	
			}else {
				String sMessage = "Verify Rule should not present in Product Placement API response for Expire End Date Rule / Deactive Rule";
				String sMessageExcatLocSenario = "Verify Rule should not present in Product Placement API response for Exact Location True when Location does not matched in Request ";
				String sDisMessage = objBackofficeReg.getExactLocNegative() ? sMessageExcatLocSenario : sMessage ;
				objAPIAssertion.assertHardTrue(!ruleDisplay,sDisMessage);	
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performRuleValidation() method of BackofficeMerchandisingAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	
	public boolean retryLogic(JsonUtil objJSONUtil) throws Exception {
		boolean ruleDisplay = false;
		try {
			int cnt =1;		
			while(cnt<=5) {
				Thread.sleep(3000);
				objJSONUtil.postRequest();
				String sRuleID = objBackofficeReg.getRuleID();
				System.out.println("Rule id :     "+sRuleID);
				String xPathRule ="$..hits[?(@._id=='"+sRuleID.trim()+"')]._source";
				jsonResponse = objJSONUtil.getResponse().asString();
				lstAllSourceDataFromResponse = JsonPath.read(jsonResponse, xPathRule);
				ruleDisplay = !(lstAllSourceDataFromResponse.isEmpty()||lstAllSourceDataFromResponse==null);
				if(ruleDisplay) {
					System.out.println("Rule is getting displayed after retry Count:     ---   : "+cnt);
					objAPIAssertion.setExtentInfo("Rule is getting displayed after retry Count:     ---   : "+cnt);
					break;
				}else {
					cnt = cnt+1 ;
				}
						
			}
			if(!ruleDisplay)
				objAPIAssertion.setExtentInfo("Rule is not getting displayed. Response :     ---   : "+jsonResponse);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performRuleValidation() method of BackofficeMerchandisingAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		return ruleDisplay;
	}

	private void validateResponseFields(HashMap<String, String> lstAllSourceDataFromResponse) throws Exception {		
		try {
			validateResponseFieldsUsingArray(lstDefaultValidationCheck,lstAllSourceDataFromResponse);
			if(objBackofficeReg.getFlagForProductSpecificTypePlacement())   validateProductSelection();
			validateRuleLocations();
			validateImageTypePlacement(lstAllSourceDataFromResponse);
			validateFieldsLevelValue(lstAllSourceDataFromResponse);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: validateResponseFields() method of BackofficeMerchandisingAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	private void validateProductSelection() throws Exception {
		if(objBackofficeReg.getProductSelection().equals(BackOfficeMerchandisingAPIConstants.PRODUCT_SELECTION_BY_SPECIFIC)) {
			validateProductSelectionSKUSpecific();
		}else {
			int missmatched = 0;
			HashMap<String, String> actProdCat = new HashMap<String,String>();
			String sProductSelection[] =objBackofficeReg.getProductSelectionValues().split(Constants.SEPARATOR_BY_COMMA);	
			int size = sProductSelection.length;
			String xpathProdQry = "$..hits[?(@._id=='"+objBackofficeReg.getRuleID().trim()+"')]._source.productquery";
			List<List<Map<String,String>>> lstHmp = JsonPath.read(jsonResponse, xpathProdQry);
			objAPIAssertion.assertTrue(lstHmp.get(0).size()==size, "Validation for Response field : Product Selection Count");
			for(int i =0 ; i<lstHmp.get(0).size();i++) {
				String Xpathname ="$..hits[?(@._id=='"+objBackofficeReg.getRuleID().trim()+"')]._source.productquery["+i+"].refinementname";
				String Xpathvalue ="$..hits[?(@._id=='"+objBackofficeReg.getRuleID().trim()+"')]._source.productquery["+i+"].refinementvalue";
				List<String> sName = JsonPath.read(jsonResponse, Xpathname);
				List<String> sValue = JsonPath.read(jsonResponse, Xpathvalue);
				actProdCat.put(sName.get(0), sValue.get(0));
				
				boolean flagProductSelectionMatch = Arrays.asList(sProductSelection).stream().anyMatch(s1->s1.contains(sName.get(0))&&s1.contains(actProdCat.get(sName.get(0))));
				if(!flagProductSelectionMatch) {
					String sActual = removeSpecialChar(sProductSelection[i].split(":")[1].trim());
					String sExpected = removeSpecialChar(actProdCat.get(sName.get(0)));					
					if(!sActual.equalsIgnoreCase(sExpected)) {
						objAPIAssertion.assertTrue(false, "Validation for Response field : Product Selection Value Missmatch Actual Name : "+sName+" & Value : "+sValue+
							"Expected List  : "+ Arrays.toString(sProductSelection));
						missmatched = missmatched+1;
					}
				}
			}	
			if(missmatched==0)
				objAPIAssertion.assertTrue(true, "Validation for Response field : Product Selection Value Actual List : "+actProdCat+
					"Expected List  : "+ Arrays.toString(sProductSelection));
		}
	}
	
	private String removeSpecialChar(String sActual) throws Exception {
		 sActual=sActual.replaceAll("[^a-zA-Z0-9]", " ");
         sActual=sActual.trim().replaceAll("  +", " ");
         return sActual;
	}
	
	
	private void validateProductSelectionSKUSpecific() throws Exception {		
		String sProductSelection[] =objBackofficeReg.getProductSelectionValues().split(Constants.SEPARATOR_BY_COMMA);	
		int size = sProductSelection.length;
		String xpathProdQry = "$..hits[?(@._id=='"+objBackofficeReg.getRuleID().trim()+"')]._source.productrecords..sku";
		List<String> lstHmp = JsonPath.read(jsonResponse, xpathProdQry);
		objAPIAssertion.assertTrue(lstHmp.size()==size, "Validation for Response field : Product Selection SKU Specific Count");
		List<String> lstMissMatched = lstHmp.stream().filter(sr->!Arrays.asList(sProductSelection).contains(sr)).collect(Collectors.toList());
		if(lstMissMatched.size()!=0)
			objAPIAssertion.assertTrue(lstMissMatched.size()==0, "Validation for Response field : Rule Product Selection Value Missmatch Actual : "+Arrays.asList(sProductSelection)+" & Expected : "+lstHmp);	
		else
			objAPIAssertion.assertTrue(true, "Validation for Response field : Rule Product Selection Value Actual : "+Arrays.asList(sProductSelection)+" & Expected : "+lstHmp);	
	}
	
	private void validateRuleLocations() throws Exception {		
		String sLocationSelection[] =objBackofficeReg.getLocation().split(Constants.SEPARATOR_BY_SEMICOLON);		
		int size = sLocationSelection.length;
		String xpathProdQry = "$..hits[?(@._id=='"+objBackofficeReg.getRuleID().trim()+"')]._source.placementlocations";
		List<List<Map<String,String>>> lstHmp = JsonPath.read(jsonResponse, xpathProdQry);
		if(Arrays.asList(sLocationSelection).contains(BackOfficeMerchandisingAPIConstants.LOC_GLOBAL)) {
			objAPIAssertion.assertTrue(lstHmp.isEmpty(), "Validation for Response field : Location should be Global");
		}else {			
			objAPIAssertion.assertTrue(lstHmp.get(0).size()==size, "Validation for Response field : Rule Location Count");
			int cnt = 0;
			for	(Map<String,String> sMap : lstHmp.get(0)) {
				Map<String,String> sExpectedMap = getExpectedMapLocations(sLocationSelection[cnt]);
				System.out.println();
				boolean flagMapCompare = sMap.entrySet().stream()
						.allMatch(e -> String.valueOf(e.getValue()).equals(sExpectedMap.get(e.getKey())));
				objAPIAssertion.assertTrue(flagMapCompare, "Validation for Response field : Rule Location Value Actual : "+sMap+" & Expected : "+sExpectedMap);
				cnt++;
			}	
		}
		
	}
	
	private Map<String,String> getExpectedMapLocations(String sLocation) {
		String sKeywordvalue = null;
		int cntRefinement=0;
		String[] strList=sLocation.split(Constants.SEPARATOR_BY_COMMA);
		Map<String,String> formattedMap = new HashMap<String,String>();
	
		for(String sKeywordFull:strList) {
			String sKeyword = sKeywordFull.split(Constants.Separator)[0].trim();
			String svalue = sKeywordFull.split(Constants.Separator)[1].trim();
			switch (sKeyword) {
			case LOC_KEYWORD         : sKeywordvalue = svalue;
									   break;	
			case LOC_KEYWORD_SKU     : sKeywordvalue = svalue;
									   break;		
			case LOC_CATEGORY        : formattedMap.put(LOC_CATEGORY,svalue);
									   formattedMap.put("refinement_count",String.valueOf(cntRefinement=cntRefinement+1));
			  						   break;						
			case LOC_MATCHMODE   : 	   formattedMap.putAll(getExpectedMatchedMode(sKeywordFull,sKeywordvalue,cntRefinement));
			  						   break;			
			case LOC_SUB_CATEGORY    : formattedMap.put(LOC_SUB_CATEGORY,svalue);
									   formattedMap.put("refinement_count",String.valueOf(cntRefinement=cntRefinement+1));
			   						   break;				
			case LOC_PRODUCT_TYPE    : formattedMap.put(LOC_PRODUCT_TYPE,svalue);
									   formattedMap.put("refinement_count",String.valueOf(cntRefinement=cntRefinement+1));
									   break;										   
			case LOC_VENDOR_NAME     : formattedMap.put(LOC_VENDOR_NAME,svalue);
									   formattedMap.put("refinement_count",String.valueOf(cntRefinement=cntRefinement+1));
			 						   break;
			case LOC_PRODUCT_STATUS  : formattedMap.put(LOC_PRODUCT_STATUS,svalue);
									   formattedMap.put("refinement_count",String.valueOf(cntRefinement=cntRefinement+1));
			   						   break;
			case LOC_EXACT_LOCATION_FLAG  : formattedMap.put("exactlocationflg","true");
									   break;	
			default                  : System.out.println("Invalid location type!"+sKeyword); 

			}

		}
		System.out.println(formattedMap);	
		return formattedMap;
		
	}
	
	private Map<String,String> getExpectedMatchedMode(String sKeyword, String sKeywordvalue, int cntRefinement) {
			Map<String, String> hsmap = new HashMap<String,String>();
			switch (sKeyword) {
			case LOC_MATCHMODE_ALL   : hsmap.put("ust_match_all",sKeywordvalue);
									   hsmap.put("refinement_count",String.valueOf(cntRefinement));
			  						   break;
			case LOC_MATCHMODE_EXACT : hsmap.put("ust_match_exact",sKeywordvalue);
									   hsmap.put("refinement_count",String.valueOf(cntRefinement));
									   break;
			case LOC_MATCHMODE_PHRASE: hsmap.put("ust_match_phrase",sKeywordvalue);
									   hsmap.put("refinement_count",String.valueOf(cntRefinement));
									   break;			
			default                  : System.out.println("Invalid location type!"+sKeyword); 

			}	

		return hsmap;
		
	}

	private String getFormattedExpectedValue(String sField) throws Exception {		
		String sValue=null;			
		switch (sField) {
			case "PlacementType" :
				sValue=objBackofficeReg.getPlacementType();
				break;
			case "PlacementZone"  :
				sValue=objBackofficeReg.getPageZone();
				break;			
			default:sValue=String.valueOf(hmTestFormMapData.get(sField));
		}			
		return sValue;
	}
	
	private void validateImageTypePlacement(HashMap<String, String> lstAllSourceDataFromResponse) throws Exception {
		List<String> lstValidationCheck = null;
		switch (objBackofficeReg.getPlacementTypeEnum()) {
			case FEATURED_CATEGORIES :
				lstValidationCheck = Arrays.asList("ImageUrl");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case AD_223_WIDTH :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case AD_497_WIDTH :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;
			case BACKGROUND_ADVERT :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case TEXTADS :
				lstValidationCheck = Arrays.asList("TitleUrl","BodyText","LinkText","LinkUrl");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case SKYSCRAPPER_BANNER :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText","TitleUrl");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;
			case BOTTOM_BANNER :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;
			case TOP_BANNER :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;
			case MIDDLE_BANNER :
				lstValidationCheck = Arrays.asList("ImageUrl","TargetUrl","HoverText");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;
			default:break;
		}			
	}
	
	private void validateFieldsLevelValue(HashMap<String, String> lstAllSourceDataFromResponse) throws Exception {
		List<String> lstValidationCheck = null;
		if(objBackofficeReg.getFieldsLevelFlag()) {
		switch (objBackofficeReg.getFieldsLevelFlagName()) {
			case BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME :
				lstValidationCheck = Arrays.asList("Vendor");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case BackOfficeMerchandisingAPIConstants.LOC_CATEGORY :
				lstValidationCheck = Arrays.asList("Category1");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;	
			case BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU  :
				lstValidationCheck = Arrays.asList("Sku");
				validateResponseFieldsUsingArray(lstValidationCheck,lstAllSourceDataFromResponse);
				break;			
			default:break;
		}	
		}
	}
	
	private void validateResponseFieldsUsingArray(List<String> lstValidationCheck, HashMap<String, String> lstAllSourceDataFromResponse) throws Exception {
		for(String sField : lstValidationCheck ) {
				String sExpected = getFormattedExpectedValue(sField);
				String sActual = String.valueOf(lstAllSourceDataFromResponse.get(sField.toLowerCase()));
				objAPIAssertion.assertEquals(sActual, sExpected, " Response Field : "+sField);
		}	
	}
	
	
	
}
