package com.im.api.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.ProductSearchRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductSearchTestDataUtil;

/**
 * @author Vinita Lakhwani
 * ProductSearchElasticAPIValidation
 * Elastic Product Search Automation -- Validation Class skeleton
 */
public class ProductSearchElasticAPIValidation {
	Logger logger = Logger.getLogger("ProductSearchElasticAPIValidation"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	HashMap<String, String> hmKeywordName = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	ProductSearchRequestDataConfig objConfig = null;
	private String keywordConfig=null;

	public ProductSearchElasticAPIValidation(APIAssertion pElem, ProductSearchRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		keywordConfig=objConfig.getKeywordConfig();
		hmTestData=objConfig.gethmTestData();
		hmKeywordName=objConfig.getDataMap();
	}

	private void performResKeywordTextValidation(JsonUtil objJSONUtil) throws Exception {
		boolean matchKeyword=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathKeywordText);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
			matchKeyword=sActual.contains(sExpected);
			objAPIAssertion.assertTrue(matchKeyword, "Response 'Did you mean'- text check . Expected : ["+sExpected+ "] and Actual : "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResKeywordTextValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performResCatDescValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			if(objConfig.getSingleSKUSpecificDataMap()!=null && 
					!(objConfig.getSingleSKUSpecificDataMap().get(ProductSearchElasticAPIConstants.CAT1DESC).equalsIgnoreCase(ProductSearchElasticAPIConstants.NULLCONSTANT))) 
			{
				List<String> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathCatDesc);
				if(sActual.size()>0)
				{
					String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_CATEGORY);
					allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
					objAPIAssertion.assertTrue(allMatch, "Response 'Category description' Validation. Expected : ["+sExpected+ "] and Actual : "+sActual);
				}else
					objAPIAssertion.setHardAssert_TCFailsAndStops("No Products retrieved in the response");
			}else {
				objAPIAssertion.setExtentSkip("Data not captured for the test case");	
			}
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResCatDescValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performResSubCatDescValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			if(objConfig.getSingleSKUSpecificDataMap()!=null && 
					!(objConfig.getSingleSKUSpecificDataMap().get(ProductSearchElasticAPIConstants.CAT2DESC).equalsIgnoreCase(ProductSearchElasticAPIConstants.NULLCONSTANT))) 
			{
				List<String> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathSubCatDesc);
				if(sActual.size()>0)
				{
					String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SUBCATEGORY);
					allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
					objAPIAssertion.assertTrue(allMatch, "Response 'Sub-Category description' Validation. Expected : ["+sExpected+"] and Actual : "+sActual);
				}else
					objAPIAssertion.setHardAssert_TCFailsAndStops("No Products retrieved in the response");
			}else {
				objAPIAssertion.setExtentSkip("Data not captured for the test case");	
			}
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResSubCatDescValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
	}


	private void performResVendorNameValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			if(objConfig.getSingleSKUSpecificDataMap()!=null && 
					!(objConfig.getSingleSKUSpecificDataMap().get(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME).equalsIgnoreCase(ProductSearchElasticAPIConstants.NULLCONSTANT))) 
			{
				List<String> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathVendorName);	
				String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME);
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(allMatch, "Response 'Vendor Name' Validation. Expected : ["+sExpected+"] and Actual : "+sActual);
			}else {
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResVendorNameValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

	private void performResProdTypeCat3Validation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			if(objConfig.getSingleSKUSpecificDataMap()!=null && 
					!(objConfig.getSingleSKUSpecificDataMap().get(ProductSearchElasticAPIConstants.CAT3DESC).equalsIgnoreCase(ProductSearchElasticAPIConstants.NULLCONSTANT))) 
			{
				List<String> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathprodTypeCatDesc);	
				String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_PRODTYPE);
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(allMatch, "Response 'cat3Desc/ProductType requested' Validation. Expected : ["+sExpected+"] and Actual : "+sActual);
			}else {
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResProdTypeCat3Validation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}
	
	private void performResSortingValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		List<String> sActual= null;
		List<String> sExpected=null;
		List<Object> oActual = new ArrayList<Object>();
		List<Double> dActual = new ArrayList<Double>();
		List<Integer> iActual = new ArrayList<Integer>();
		List<? extends Number> dExpected = new ArrayList<Number>();

		try {
			String sortBy=hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY);
			String sortOrder=hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION);

			if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
			{
				switch(sortBy) {
				case ProductSearchElasticAPIConstants.SORTBY_VENDORNAME:
					sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathVendorName);
					sExpected=Util.getSortedListInRequiredOrder(sActual, sortOrder);
					allMatch=sActual.equals(sExpected);
					objAPIAssertion.assertTrue(allMatch, "Validation of Response Sorting based on "+sortBy+" in "+sortOrder+" order : Actual: ["+sActual.toString()+ "]");
					break;

				case ProductSearchElasticAPIConstants.SORTBY_VPN:
					sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathVendorPartNumber);
					sExpected=Util.getSortedListInRequiredOrder(sActual, sortOrder);
					allMatch=sActual.equals(sExpected);
					objAPIAssertion.assertTrue(allMatch, "Validation of Response Sorting based on "+sortBy+" in "+sortOrder+" order : Actual: ["+sActual.toString()+ "]");
					break;

				case ProductSearchElasticAPIConstants.DEALERPRICE:
					oActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathDealerPrice);
					if(oActual.size()>0)
					{
					for (Object s: oActual) { 
						if(s != null)
							dActual.add(Double.valueOf(s.toString()));
					}
					dExpected=Util.getSortedNumberListInRequiredOrder(dActual, sortOrder);
					allMatch=dActual.equals(dExpected);
					objAPIAssertion.assertTrue(allMatch, "Validation of Response Sorting based on "+sortBy+" in "+sortOrder+" order : Actual: ["+oActual.toString()+ "]");
					}
					else
						objAPIAssertion.setHardAssert_TCFailsAndStops("No Products retrieved in the response");
					break;

				case ProductSearchElasticAPIConstants.SORTBY_STOCK:
					oActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathAggAvailableQty);
					if(oActual.size()>0)
					{
						boolean nonNullElemExist= false;
						for (Object s: oActual) { 
							if (s != null) 
							{nonNullElemExist = true;
							break;}
						}
						if(!nonNullElemExist)
							objAPIAssertion.setExtentInfo("Validation of Response Sorting not done on "+sortBy+" in "+sortOrder+" order as: Actual list: ["+oActual.toString()+ "]");
						else {						
							for (Object s: oActual) { iActual.add(Integer.valueOf(s.toString()));}
							dExpected=Util.getSortedNumberListInRequiredOrder(iActual, sortOrder);
							allMatch=iActual.equals(dExpected);
							objAPIAssertion.assertTrue(allMatch, "Validation of Response Sorting based on "+sortBy+" in "+sortOrder+" order : Actual: ["+oActual.toString()+ "]");
						}
					}
					else
						objAPIAssertion.setHardAssert_TCFailsAndStops("No Products retrieved in the response");
					break;					
				}
			}else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");		
		}
		catch(Exception e) {
			e.printStackTrace();
			String sErrMessage="FAIL: performResSortingValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
	}

	private void performResPriceRangeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			List<String> sLstActual = objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathDealerPrice);
			HashMap<String, String> pDataMap=objConfig.getDataMap();
			if(pDataMap.get(ProductSearchElasticAPIConstants.PRICERANGESEARCH).equals(""+true))
			{
				float sMinValue = Float.valueOf(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_MINPRICE));
				float sMaxValue = Float.valueOf(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_MAXPRICE));
				if(sLstActual.isEmpty())
					throw new Exception("ERROR: Null values received in the function performResPriceRangeValidation of ProductSearchElasticAPIValidation");

				objAPIAssertion.assertTrue(Util.checkValuesWithinGivenRange(sLstActual, sMinValue, sMaxValue), "Validation of Response for price range. Actual : ["+sLstActual+ "] and Expected in Range :["+sMinValue+"] to ["+sMaxValue+"]");
			}
			else
				objAPIAssertion.setExtentInfo("Validation of Response for price range not performed as dealer price null.");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResPriceRangeValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}    
	}

	private void performResProdStatusValidation(JsonUtil objJSONUtil) throws Exception {
		List<String> sActual=null;
		List<Integer> iActual=null;
		String sExpected = "true";
		boolean matchKeyword=false;
		try {
			HashMap<String, String> pDataMap=objConfig.getDataMap();
			if(pDataMap.get(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG).equals(""+true))
			{
			switch(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS))	{
			case ProductSearchElasticAPIConstants.FLAG_INSTOCK:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathInStockStatus);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Instock Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				
				//this code will execute if stock range search with min qty was performed
				if(objConfig.getListSetConfig().contains(ProductSearchElasticAPIConstants.EXCELHEADER_MINSTOCK)) {
				iActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathAggAvailableQty);
				 
				for (int i: iActual)
			        if(i< ProductSearchElasticAPIConstants.MINSTOCKQTY)
			        	matchKeyword=false;
			        else 
			        	matchKeyword=true;
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Stock' Validation .Actual["+iActual+"] Expected to be greater than ["+ProductSearchElasticAPIConstants.MINSTOCKQTY+"]");
				}
				break;

			case ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathInStockOrOrderFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Instock or Order Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_NEW:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathNewProductFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'New Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_ESD:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathESDFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'ESD Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				
				//sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathEndUserRequiredFlag);
				//matchKeyword = Util.compareListOfValuesWithExpectedValue(sActual, sExpected);
				//objAPIAssertion.assertTrue(matchKeyword, "Response 'End User Required Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathDirectshipFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Directship Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathHeavyWeightFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Heavy Weight Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathPurchaseToAllFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Purchase To All Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathMatchedQueries);
				sExpected = ProductSearchElasticAPIConstants.AUTHORIZEDTOPURCHASE;
				matchKeyword = Util.compareListOfValuesWithExpectedValueMoreThanOneIgnoreCase(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Authorized To Purchase Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_BUNDLE:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathBomFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Bundle Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			case ProductSearchElasticAPIConstants.FLAG_REFURBISHED:
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathRefurbishedFlag);
				matchKeyword = Util.compareListValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Refurbished Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
				break;

			default:
				break;
			}	
			} else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
			
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResProdStatusValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
	}

	//no data for any country so couldn't develop
	private void performResVendorSpclBidsFlagValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			//TODO Method Body
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResVendorSpclBidsFlagValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	

	}

	//Logic to be confirmed as logic different for different ID's like IMproductSearchV8,IMproductSearchV7,IMproductSearchV6...
	private void performResQtyBreakFlagValidation(JsonUtil objJSONUtil) throws Exception {
		String sExpected = "true";
		boolean matchKeyword=false;
		List<String> sActual=null;
		String xpath=null;
		HashMap<String, String> reqMap=objConfig.gethmTestData();
		try {
			HashMap<String, String> pDataMap=objConfig.getDataMap();
			if(pDataMap.get(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG).equals(""+true))	{
				if(!reqMap.get(ProductSearchElasticAPIConstants.ID).equals("IMproductSearchV8") &&
						!reqMap.get(ProductSearchElasticAPIConstants.ID).equals("IMproductSearchV7"))
					xpath=ProductSearchElasticAPIConstants.xpathQuantityBreakFlag;
				else
					xpath=reqMap.get("RunConfig").equals("UK")?ProductSearchElasticAPIConstants.xpathQuantityBreakFlag:ProductSearchElasticAPIConstants.xpathAcopQuantityBreakFlag;
				
				sActual= objJSONUtil.getListofStringsFromJSONResponse(xpath);
				matchKeyword = Util.compareListOfValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Quantity Break Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
			}else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");			
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResQtyBreakFlagValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	

	}

	
	//Logic to be confirmed as logic different for different ID's like IMproductSearchV8,IMproductSearchV7,IMproductSearchV6...
	private void performResPromotionFlagValidation(JsonUtil objJSONUtil) throws Exception {
		String sExpected = "true";
		boolean matchKeyword=false;
		List<String> sActual=null;
		String xpath=null;
		HashMap<String, String> reqMap=objConfig.gethmTestData();
		try {
			HashMap<String, String> pDataMap=objConfig.getDataMap();
			if(pDataMap.get(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG).equals(""+true))	{
				if(!reqMap.get(ProductSearchElasticAPIConstants.ID).equals("IMproductSearchV8") &&
						!reqMap.get(ProductSearchElasticAPIConstants.ID).equals("IMproductSearchV7"))
					xpath=ProductSearchElasticAPIConstants.xpathPromotionflag;
				else
					xpath=reqMap.get("RunConfig").equals("UK")?ProductSearchElasticAPIConstants.xpathPromotionflag:ProductSearchElasticAPIConstants.xpathAcopPromotionflag;
				
				sActual= objJSONUtil.getListofStringsFromJSONResponse(xpath);
				matchKeyword = Util.compareListOfValuesWithExpectedValue(sActual, sExpected);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Promotion Flag' Validation .Actual["+sActual+"] Expected["+sExpected+"]");
			} else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");			
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResPromotionFlagValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	

	}

	private void performValidationForViewAlloption(JsonUtil objJSONUtil) throws Exception {
		try {

			objAPIAssertion.assertHardTrue((objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)<=6),"Response received within 6 secs i.e. in "+objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)+" secs");

			String prodCount=objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING);
			objAPIAssertion.assertHardTrue(Integer.parseInt(prodCount)>0, "No of products retrieved is greater than 0 : "+prodCount);

			String totalCount=objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits);
			objAPIAssertion.assertHardTrue(Integer.parseInt(totalCount)>0, "No of total hits is greater than 0 : "+totalCount);

		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForViewAlloption() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			Set<String> listForValidation=objConfig.getListSetConfig();
			performResponseCodeValidation(objJSONUtil);
			performRelevanceValidation(objJSONUtil);
			if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				for(String attribute : listForValidation) {
					switch(attribute) {

					case ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS:
						//if(objConfig.get)
						performResProductFieldValidationForKeywordSearch(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD:
						performResKeywordTextValidation(objJSONUtil);
						performResProductFieldValidationForKeywordSearch(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_CATEGORY:
						performResCatDescValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_SUBCATEGORY:
						performResSubCatDescValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME:
						performResVendorNameValidation(objJSONUtil);
						break;
						
					case ProductSearchElasticAPIConstants.EXCELHEADER_PRODTYPE:
						performResProdTypeCat3Validation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_VIEWALL:
						performValidationForViewAlloption(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY:
						performResSortingValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_PRICERANGESEARCH:
						performResPriceRangeValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS:
						performResProdStatusValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS:
						performResVendorSpclBidsFlagValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.FLAG_QTYBREAK:
						performResQtyBreakFlagValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.FLAG_PROMOTION:
						performResPromotionFlagValidation(objJSONUtil);
						break;

					case ProductSearchElasticAPIConstants.PAGESIZE:
						performPageSizeValidation(objJSONUtil);
						break;
					
					case ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE:
						if(objConfig.getDataMap().get(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE)!=null) {
						String totalCount=objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits);
						objAPIAssertion.assertHardTrue(Integer.parseInt(totalCount)>0, "No of total hits is greater than 0, and Total Count getting as : "+totalCount);
						}else{
							objAPIAssertion.setExtentSkip("Data not captured for the test case");
						}break;
					}//end of switch
				}//end of for
			} else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performRelevanceValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		List<Object> oActual = new ArrayList<Object>();
		List<Double> dActual = new ArrayList<Double>();
		List<? extends Number> dExpected = new ArrayList<Number>();
		try
		{
			if(!objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits)
					.equalsIgnoreCase((ProductSearchElasticAPIConstants.NULLCONSTANT))) {
				oActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathIndexScore);
				for (Object s: oActual) {
					if(s != null)
						dActual.add(Double.valueOf(s.toString()));
				}
				if(dActual.size()>1 && !dActual.isEmpty() 
						&& objConfig.getDataMap().get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY)==null){
					dExpected=Util.getSortedNumberListInRequiredOrder(dActual, "desc");
					allMatch=dActual.equals(dExpected);
					objAPIAssertion.assertTrue(allMatch, "Validation of Response Relevance based on  Score. Actual: ["+oActual.toString()+ "]");   
				}else {					
					objAPIAssertion.setExtentInfo("Sorting is set for other than Relevance");
				}
			}
			else
				objAPIAssertion.setExtentInfo("No products retrieved in response.");
		}catch (Exception e) {
			String sErrMessage="FAIL: performRelevanceValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);      
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performPageSizeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			
			if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				List<HashMap<String, String>> resHitsMap=objJSONUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathTotalNoRecordsInTheResponse);
				if(resHitsMap!=null)
				{
					int actualPageSize = resHitsMap.size();
					String iExpectedPageSize = hmTestData.get(ProductSearchElasticAPIConstants.PAGESIZE);
					boolean result = actualPageSize <= Integer.valueOf(iExpectedPageSize);
					objAPIAssertion.assertTrue(result, "Page Size validation Actual Page Count ["+actualPageSize+"] should be less than or equal to Expected Page Count ["+iExpectedPageSize+"]");
				}
				else
					objAPIAssertion.setHardAssert_TCFailsAndStops("No Products retrieved in the response");
			}else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
			
		} catch (Exception e) {
			String sErrMessage="FAIL: performPageSizeValidation() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

	private void performResProductFieldValidationForKeywordSearch(JsonUtil objJSONUtil) throws Exception {
		ProductSearchTestDataUtil objTestDataUtil=objConfig.getProductSearchTestDataUtil();
		List<String> sActual=null;
		String[] sExpected=null;
		String sExpect=null;
		boolean matchKeyword=false;
		try {
			switch(keywordConfig)
			{
			case ProductSearchElasticAPIConstants.KEYWORD_GENERIC:	
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_SKU:
				if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathMaterial);	
				sExpect =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD).replaceFirst("^0+(?!$)", "");
				matchKeyword=sActual.get(0).contains(sExpect);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Material'  Validation : Expected ["+sExpect+ "] and Actual "+sActual);
				}else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_SKU:
				if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) 
				{
					sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathMaterial);	
					if(sActual.size()>0)
					{
						objAPIAssertion.assertTrue(sActual.size()>=2, "Response hits size Validation : Expected greater than or equal to ["+2+ "] and Actual "+sActual.size());
						sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS).split("\",\"");
						if(sExpected!=null)
							for(String sExp : sExpected)
							{
								matchKeyword=sActual.contains(sExp);
								objAPIAssertion.assertTrue(matchKeyword, "Response 'Material'  Validation : Expected ["+sExp+ "] and Actual "+sActual);
							}
						else
							objAPIAssertion.setExtentSkip("Data not captured for the test case");
					}else
						objAPIAssertion.setExtentSkip("Data not captured for the test case");
				}
				else
					objAPIAssertion.setHardAssert_TCFailsAndStops("No products retrieved in the response");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_VPN:
				if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathVendorPartNumber);
				if(sActual.size()>0)
				{
				sExpect =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				matchKeyword=sActual.contains(sExpect);
				objAPIAssertion.assertTrue(matchKeyword, "Response 'Manufacturerpartnumber'  Validation : Expected ["+sExpect+ "] and Actual "+sActual);
				}
				else
					objAPIAssertion.setHardAssert_TCFailsAndStops("No products retrieved in the response");
				}else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_VPN:
				if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathVendorPartNumber);
					objAPIAssertion.assertTrue(sActual.size()>=2, "Response hits size Validation : Expected greater than or equal to ["+2+ "] and Actual "+sActual.size());
				sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS).split("\",\"");
				if(sExpected!=null)
					for(String sExp : sExpected) {
						matchKeyword=sActual.contains(sExp);
						objAPIAssertion.assertTrue(matchKeyword, "Response 'Manufacturerpartnumber'  Validation : Expected ["+sExp+ "] and Actual "+sActual);
					}
				else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				}
				else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_UPC_EAN:
				if(objTestDataUtil.getListOfEAN().size()>0)
				{
					sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathUpcEan);
					if(sActual.size()>0)
					{
						sExpect =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
						matchKeyword=sActual.contains(sExpect);
						objAPIAssertion.assertTrue(matchKeyword, "Response 'upcean'  Validation : Expected ["+sExpect+ "] and Actual "+sActual);
					}else
						objAPIAssertion.setHardAssert_TCFailsAndStops("No products retrieved in the response");

				}else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_UPC_EAN:
				if(objTestDataUtil.getListOfEAN().size()>1)
				{
					sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathUpcEan);
					if(sActual.size()>0)
					{
						sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS).split("\",\"");
						for(String sExp : sExpected) {
							matchKeyword=sActual.contains(sExp);
							objAPIAssertion.assertTrue(matchKeyword,"Response 'upcean' Validation : Expected ["+sExp+ "] and Actual "+sActual);
						}
					}else
						objAPIAssertion.setHardAssert_TCFailsAndStops("No products retrieved in the response");
				}
				else
					objAPIAssertion.setExtentSkip("Data not captured for the test case");
				break;

			case ProductSearchElasticAPIConstants.KEYWORD_LONGDESC1:
                if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
                sActual = objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathLongDesc);
                List<String> sActualWithoutQuotes=new ArrayList<String>();
                
                for( int i=0; i<sActual.size();i++)
                	if(sActual.get(i).contains("\""))
                		sActualWithoutQuotes.add(sActual.get(i).replaceAll("\"", ""));
                	else
                		sActualWithoutQuotes.add(sActual.get(i));
                
                sExpect = hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
                matchKeyword=Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualWithoutQuotes, sExpect);
                objAPIAssertion.assertTrue(matchKeyword, "Response 'LongDescription' Validation. Expected : ["+sExpect+"] and Actual : "+sActual);
                }else
                    objAPIAssertion.setExtentSkip("Data not captured for the test case");
                break; 

            case ProductSearchElasticAPIConstants.KEYWORD_INVALID:
                performResTotalHitsValidationForInvalidKeywordSearch(objJSONUtil);
                break;

            case ProductSearchElasticAPIConstants.KEYWORD_AUTOCORRECTERRONEOUS:
            	if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
            		String sAct = objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathAutoCorrectKeywordText);    
            		matchKeyword=sAct.equalsIgnoreCase(ProductSearchElasticAPIConstants.VENDORNM_MICROSOFT);
            		objAPIAssertion.assertTrue(matchKeyword, "Auto-Corrected Keyword Name Validation : "+sAct);
               	}else
                    objAPIAssertion.setExtentSkip("Data not captured for the test case");
                break;

            case ProductSearchElasticAPIConstants.KEYWORD_INCOMPLETE:
                if(!objConfig.getProductSearchTestDataUtil().getTotalProducts().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
                sActual = objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathMaterial);    
                sExpect = hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
                matchKeyword = Util.compareListOfValuesWithExpectedValueMoreThanOne(sActual, sExpect);
                objAPIAssertion.assertTrue(matchKeyword, "Response 'Incomplete SKU' Validation. Expected : ["+sExpect+"] and Actual : "+sActual);
                }else
                    objAPIAssertion.setExtentSkip("Data not captured for the test case");
                break; 

            default:
                break;
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResProductFieldValidationForKeywordSearch() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

	//TODO need to make it generic. Should connect with Prashant N/Vinita - By Chandra.
	private void performResTotalHitsValidationForInvalidKeywordSearch(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits);	
			String sExpected =  "0";
			objAPIAssertion.assertTrue(sActual.equals(sExpected), "Response 'Total Hits'. Expected : ["+sExpected+ "] and Actual : "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResTotalHitsValidationForInvalidKeywordSearch() method of ProductSearchElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
}
