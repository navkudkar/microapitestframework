package com.im.api.validation;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.im.api.core.business.APIEnumerations.ConnectToDatabase;
import com.im.api.core.business.ExecuteSQLscript;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductDetails;
import com.im.imonline.api.business.ProductDetailsRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.jayway.jsonpath.JsonPath;

public class ProductDetailsElasticAPIValidation {

	private Logger logger = Logger.getLogger("ProductDetailsElasticAPIValidation"); 
	private HashMap<String, String> hmTestData = null;
	private FlagOptions_ProductDetails flagName = null;
	private APIAssertion objAPIAssertion = null;
	private String xpathRule=null, sJsonResponse = null; 
	private ProductDetailsRequestDataConfig objConfig = null;
	private List<HashMap<Object, Object>> lstProductSearchData = null;
	private Set<String> setCrossSellData = null;
	private List<List<HashMap<Object, Object>>> lstProdSpecsData = null;
	private List<String> lstComponentData = null, sMatchedQueries = null;
	private Multimap<Object, Object> mapTechSpecsData = null;
	private HashMap<String, String> mapMarketingInformationData = null;
	private static final String lstOfCountriesApplicableForPIM = "US|AT|DE|FR";
	ExecuteSQLscript objES = null;

	public ProductDetailsElasticAPIValidation(APIAssertion pElem, ProductDetailsRequestDataConfig pobjConfig) throws SQLException {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		hmTestData=objConfig.gethmTestData();
		flagName=objConfig.getFlagName();
		
	}

	private void performResponseCodeValidation(JsonUtil objJsonUtil) throws Exception {
		try {
			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	public void performValidationforGivenConfig(JsonUtil objJsonUtil) throws Exception {
		objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
		String sSKUNumber = null, sTrue = "true", sActual = null, sExpected = null, sCountryCode = null;
		boolean bStatus;
		int iOnOrderQty;
		String xPathPDetails = null;
		try {
			sCountryCode = objConfig.getProductDetailsTestDataUtil().getRequestData().get(Constants.ExcelHeaderRunConfig);
			performResponseCodeValidation(objJsonUtil);
			sJsonResponse = objJsonUtil.getResponse().asString();
			xPathPDetails ="$..hits[?(@._index=~/^.*products.*$/)]._source";
			lstProductSearchData = JsonPath.read(sJsonResponse, xPathPDetails);
			xPathPDetails ="$..hits[?(@._index=~/^.*pdetails.*$/)]._source.prod_spec";
			lstProdSpecsData = JsonPath.read(sJsonResponse, xPathPDetails);
			xPathPDetails = "$..matched_queries[0]";
			sMatchedQueries = JsonPath.read(sJsonResponse, xPathPDetails);

			sSKUNumber = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.MATERIAL).toString();

			switch(flagName) {
			case ACCESSORIES:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "hasaccessory should be 'true'");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_HASCROSSSELL).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "hascrosssell should be 'true'");
				
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode)) {
					xpathRule = "$..hits[?(@._index=~/^.*pdetails.*$/)]..cross_sell[?(@.contenttype=='CrossSell')].additionalmaterial";
					List<String> lstCrossSellData = JsonPath.read(sJsonResponse, xpathRule);
					System.out.println("Cross Sell Data : "+lstCrossSellData);
					setCrossSellData = new HashSet<String>(lstCrossSellData);
					objAPIAssertion.assertTrue(setCrossSellData.equals(objConfig.getProductDetailsTestDataUtil().getAccessoriesSKU(sSKUNumber, "C", objES).get("childmaterial")), "Validation for Child Materials of Accessories SKU.Actual["+setCrossSellData+"] Expected["+objConfig.getProductDetailsTestDataUtil().getAccessoriesSKU(sSKUNumber, "C", objES).get("childmaterial")+"]");
				}
				else
					objAPIAssertion.setExtentInfo("Accessories Data not configured for the country ["+sCountryCode+"] in PIM Tool!!!");
				break;

			case ACOP_PROMOTION:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "acoppromotionflag should be 'true'");
				break;

			case ACOP_QUANTITY_BREAK:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "acopquantitybreakflag should be 'true'");
				break;

			case ACOP_SPECIAL_PRICING:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "acopwebdiscountflag should be 'true'");
				break;

			case ACOP_WEB_ONLY_PRICE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "acopwebonlypriceflag should be 'true'");
				break;

			case AVAILABLE_IN_BUNDLE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_COMPONENT).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "componentflag should be 'true'");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.PARTOFBUNDLE).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getPartOfBundleSKUForComponentSKU().get(sSKUNumber);
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.PARTOFBUNDLE);
				objAPIAssertion.assertEquals(sActual, sExpected, "partofbundle should contain Parent SKU Number");
				break;

			case BACK_ORDER:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).toString();
				objAPIAssertion.assertEquals(sActual, "Y", "backorderswitch flag should be 'Y'");
				break;

			case BLOW_OUT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_BLOWOUT).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "blowoutflag should be 'true'");
				break;

			case BULK_FREIGHT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "oversizeflag should be 'true'");
				break;

			case BUNDLE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "bomflag should be 'true'");

				xpathRule = "$..kits.component";
				lstComponentData = JsonPath.read(sJsonResponse, xpathRule);
				objAPIAssertion.assertTrue(lstComponentData.equals(objConfig.getProductDetailsTestDataUtil().getComponentDataForBundleSKU().get(sSKUNumber)), "Validation for Component SKU's of Bundle SKU.Actual["+lstComponentData+"] Expected["+objConfig.getProductDetailsTestDataUtil().getComponentDataForBundleSKU().get(sSKUNumber)+"]");
				break;

			case CLEARANCE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_CLEARANCE).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "clearanceflag should be 'true'");
				
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getClearanceFlagData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE_DESC).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE_DESC).toString();
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE_DESC).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "clearance value description");

				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getClearanceFlagData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE).toString();
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "clearance value");
				break;

			case DIRECTSHIP:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "directshipflag should be 'true'");
				break;

			case DISCONTINUED:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "discontinuedflag should be 'true'");
				break;

			case DOWNLOADABLE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ESD).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "isesdproductflag on Product Details API Response");
				break;

			case END_USER:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "enduserrequiredflag on Product Details API Response");
				break;

			case FREE_ITEM:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "shipalongflagdim should be 'true'");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.SHIP_ALONG_EXPIRED_DATE).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getFreeItemData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_EXPIRED_DATE).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_EXPIRED_DATE).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "Ship Along Expiry Date");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getFreeItemData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION);
				objAPIAssertion.assertEquals(sActual, sExpected, "Ship Along Part Description");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MATERIAL).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getFreeItemData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MATERIAL).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MATERIAL).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "Ship Along Material");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.SHIP_ALONG_FREE_QTY).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getFreeItemData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_FREE_QTY).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_FREE_QTY).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "Ship Along Free Quantity");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MIN_QTY).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getFreeItemData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MIN_QTY).toString();
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.SHIP_ALONG_MIN_QTY).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "Ship Along Minimum Quantity");

				objAPIAssertion.assertTrue(sMatchedQueries.contains(ProductSearchElasticAPIConstants.SHIPALONG), "Validation for presence of shipalong in Matched_Queries ["+sActual+"]");
				break;

			case HEAVY_WEIGHT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "heavyweightflag should be 'true'");
				break;

			case INSTOCK:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "instock flag should be 'true'");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "instockororder flag should be 'true'");
				break;

			case INSTOCK_OR_ORDER:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "instockororder flag should be 'true'");

				iOnOrderQty = (Integer) lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.ONORDERQTY);
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).toString();

				if(sActual.equals(sTrue))
					bStatus = iOnOrderQty>=0;
				else
					bStatus = iOnOrderQty>=0;

				objAPIAssertion.assertTrue(bStatus, "Validation for OnOrderQty ["+iOnOrderQty+"]");
				break;

			case LICENCE_PRODUCT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "licenseproductsflag should be 'true'");
				break;

			case LTL:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_LTL).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "ltlflag should be 'true'");
				break;

			case NEW:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_NEW).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "newproductflag should be 'true'");
				break;

			case NO_RETURNS:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "noreturnsflag shoul be 'true'");
				break;

			case NOT_AUTHORIZED:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_NOTAUTHORIZED).toString();
				objAPIAssertion.assertEquals(sActual, "false", "purchasetoall flag should be 'false'");
				break;

			case NOT_ORDERABLE_ONLINE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).toString();
				objAPIAssertion.assertEquals(sActual, "N", "backorderswitch should be 'N' for Not_Orderable_Online product");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).toString();
				objAPIAssertion.assertEquals(sActual, "false", "Return Limitations should be 'false' for Not_Orderable_Online product");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "directshipflag should be 'true' for Not_Orderable_Online product");
				break;

			case OUT_OF_STOCK:
				String sFalse = "false";
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).toString();
				objAPIAssertion.assertEquals(sActual, sFalse, "instock flag should be 'false'");

				iOnOrderQty = (Integer)lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.ONORDERQTY);
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).toString();

				if(sActual.equals(sFalse))
					bStatus = iOnOrderQty==0;
				else
					bStatus = iOnOrderQty>=0;

				objAPIAssertion.assertTrue(bStatus, "Validation for OnOrderQty ["+iOnOrderQty+"]");
				break;

			case PROMOTION:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "promotionflag should be 'true'");
				break;

			case QUANTITY_BREAK:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "quantitybreakflag should be 'true'");

				objAPIAssertion.assertTrue(sMatchedQueries.contains(ProductSearchElasticAPIConstants.AGGREGATION_QTYBREAK), "Validation for presence of quantitybreak in Matched_Queries ["+sActual+"]");
				break;

			case REFURBISHED:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "refurbishedflag should be 'true'");
				break;

			case REPLACEMENT_PRODUCT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).toString();
				objAPIAssertion.assertEquals(sActual, "1", "subrule field should be 1");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getSubMaterialData().get(sSKUNumber);
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL);
				objAPIAssertion.assertEquals(sActual, sExpected, "presence of Replaced Product SKU Number");
				break;

			case RETURN_LIMITATIONS:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "returnlimitations flag should be 'true'");
				break;

			case SHIP_FROM_PARTNER:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "endlesssaisleflag should be 'true'");
				break;

			case SIMILAR_PRODUCTS:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getSimilarProductsData().get(sSKUNumber);
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS);
				objAPIAssertion.assertEquals(sActual, sExpected, "Similar Products");
				break;

			case SPECIAL_BIDS:
				objAPIAssertion.assertTrue(sMatchedQueries.contains("specialBid"), "Validation for presence of specialBid in Matched_Queries ["+sActual+"]");
				break;

			case SPECIAL_PRICING:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getPromoEndDateforSpecialPricingSKU().get(sSKUNumber);
				else	
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING);
				objAPIAssertion.assertEquals(sActual, sExpected, "promoenddt value for Special Pricing Product");
				break;

			case SUGGESTED_PRODUCT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).toString();
				objAPIAssertion.assertEquals(sActual, "2", "subrule field should be 1");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL).toString();
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode))
					sExpected = objConfig.getProductDetailsTestDataUtil().getSubMaterialData().get(sSKUNumber);
				else
					sExpected = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber).get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL);
				objAPIAssertion.assertEquals(sActual, sExpected, "presence of Suggested Product SKU Number");
				break;

			case WARRANTIES:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_WARRANTIES).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "haswarranty should be 'true'");

				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_HASCROSSSELL).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "hascrosssell should be 'true'");
				
				if(lstOfCountriesApplicableForPIM.contains(sCountryCode)) {
					xpathRule = "$..hits[?(@._index=~/^.*pdetails.*$/)]..cross_sell[?(@.contenttype=='W')].additionalmaterial";
					List<String> lstCrossSellData = JsonPath.read(sJsonResponse, xpathRule);
					System.out.println("Cross Sell Data : "+lstCrossSellData);
					setCrossSellData = new HashSet<String>(lstCrossSellData);
					objAPIAssertion.assertTrue(setCrossSellData.equals(objConfig.getProductDetailsTestDataUtil().getAccessoriesSKU(sSKUNumber, "W", objES).get("childmaterial")), "Validation for Child Materials of Warranties SKU.Actual["+setCrossSellData+"] Expected["+objConfig.getProductDetailsTestDataUtil().getAccessoriesSKU(sSKUNumber, "W", objES).get("childmaterial")+"]");
				}
				else
					objAPIAssertion.setExtentInfo("Warranties Data not configured for the country ["+sCountryCode+"] in PIM Tool!!!");break;

			case WARRANTY_PRODUCT:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "warrantyproduct flag should be 'true'");
				break;

			case WEB_ONLY_PRICE:
				sActual = lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE).toString();
				objAPIAssertion.assertEquals(sActual, sTrue, "webdiscountflag should be 'true'");

				objAPIAssertion.assertTrue(sMatchedQueries.contains(ProductSearchElasticAPIConstants.WEBONLYPRICE), "Validation for presence of webonlyprice in Matched_Queries ["+sActual+"]");
				break;

			default:
				break;
			}
			if(lstOfCountriesApplicableForPIM.contains(sCountryCode)) {
				
				getTechSpecsAndMarketingInformationData(lstProdSpecsData.get(0));
				performValidationForTechnicalSpecificationAccordion(sSKUNumber);
				performValidationForMarketingInformation(sSKUNumber);
				performValidationForImgaeData(sSKUNumber);
				performValidationForRichMediaData(sSKUNumber);
			
			} 
			else {
				performValidationForBasicContentsProductDetailsSearch(sSKUNumber, lstProductSearchData.get(0));
				if(!hmTestData.get("customerKey").equals("000001"))
					performValidationForAdditionalInformationAccordion(sSKUNumber, lstProductSearchData.get(0));
			}
				
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			objES.closeConnection();
		}
	}

	private void getTechSpecsAndMarketingInformationData(List<HashMap<Object, Object>> lstProdSpecsData) throws Exception {
		mapTechSpecsData = HashMultimap.create();
		mapMarketingInformationData = new HashMap<String, String>();
		try {
			for(HashMap<Object, Object> spec : lstProdSpecsData) {
				if(spec.get(ProductSearchElasticAPIConstants.ProdSpecs_HeaderName).toString().contains("General") && spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeName).toString().contains("Marketing") ||
						spec.get(ProductSearchElasticAPIConstants.ProdSpecs_HeaderName).toString().contains("Information") && spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeName).toString().contains("Marketing")	)
					mapMarketingInformationData.put(spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeName).toString().trim(), spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeValue).toString().trim());
				else 
					mapTechSpecsData.put(spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeName).toString().trim(), spec.get(ProductSearchElasticAPIConstants.ProdSpecs_AttributeValue).toString().trim());
			}
		} catch(Exception e) {
			String sErrMessage="FAIL: getTechSpecsAndMarketingInformationData() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	//TODO due to collision, below method is failing for AT.
	//Because of collision, ordering is not followed.
	private void performValidationForTechnicalSpecificationAccordion(String sSKUNumber) throws Exception {
		Multimap<String, String> mapExpectedTechSpecsData;
		boolean bFlag = false;
		try {
			mapExpectedTechSpecsData = objConfig.getProductDetailsTestDataUtil().getTechSpecData(sSKUNumber, objES); 
			objAPIAssertion.assertTrue(mapTechSpecsData.entries().containsAll(mapExpectedTechSpecsData.entries()), "Validation for Technical Specifications Data.Actual["+mapTechSpecsData+"] Excpected["+mapExpectedTechSpecsData+"]");
			for(Map.Entry<Object, Object> mapObj : mapTechSpecsData.entries()) {
				if(!mapExpectedTechSpecsData.containsEntry(mapObj.getKey(), mapObj.getValue())) {
					bFlag = false;
					break;
				} 
				else
					bFlag = true;
			}
			objAPIAssertion.assertTrue(bFlag, "Validation for Technical Specifications Data.Actual["+mapTechSpecsData+"] Excpected["+mapExpectedTechSpecsData+"]");
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForTechnicalSpecificationAccordion() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	//TODO even though both values are same, this method is failing - AT.
	private void performValidationForMarketingInformation(String sSKUNumber) throws Exception {
		Map<String, String> mapExpectedMarketingInfoData;
		try {
			mapExpectedMarketingInfoData = objConfig.getProductDetailsTestDataUtil().getMarketingInformation(sSKUNumber, objES);
			objAPIAssertion.assertTrue(mapExpectedMarketingInfoData.equals(mapMarketingInformationData), "Validation for Marketing Information Data.Actual["+mapMarketingInformationData+"] Expected["+mapExpectedMarketingInfoData+"]");
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForMarketingInformation() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	private void performValidationForImgaeData(String sSKUNumber) throws Exception {
		List<Map<String, String>> lstExpectedImageData;
		List<String> lstImageLowData = new ArrayList<String>();
		List<String> lstImageMidData = new ArrayList<String>();
		List<String> lstImageHighData = new ArrayList<String>();
		List<String> lstActualImageData = new ArrayList<String>();
		//boolean bFlag = false;
		try {
			lstExpectedImageData = objConfig.getProductDetailsTestDataUtil().getImageData(sSKUNumber, objES);
			for(Map<String, String> mapObj : lstExpectedImageData) {
				lstImageLowData.add(mapObj.get("imagelow"));
				lstImageMidData.add(mapObj.get("imagemid"));
				lstImageHighData.add(mapObj.get("imagehigh"));
			}
			
			Collections.sort(lstImageLowData);
			Collections.sort(lstImageMidData);
			Collections.sort(lstImageHighData);
			
			lstActualImageData = Arrays.asList((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGELOW).toString().split(",")));
			Collections.sort(lstActualImageData);
			objAPIAssertion.assertTrue(lstActualImageData.equals(lstImageLowData), "Validation for Image Low data.Actual["+lstActualImageData+"] Expected["+lstImageLowData+"]");
			
			lstActualImageData = Arrays.asList((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEMID).toString().split(",")));
			Collections.sort(lstActualImageData);
			objAPIAssertion.assertTrue(lstActualImageData.equals(lstImageMidData), "Validation for Image Mid data.Actual["+lstActualImageData+"] Expected["+lstImageMidData+"]");
			
			lstActualImageData = Arrays.asList((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEHIGH).toString().split(",")));
			Collections.sort(lstActualImageData);
			objAPIAssertion.assertTrue(lstActualImageData.equals(lstImageHighData), "Validation for Image High data.Actual["+lstActualImageData+"] Expected["+lstImageHighData+"]");
			
//			for(String lstObj : lstImageLowData) {
//				if((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGELOW).toString()).contains(lstObj.toString()))
//					bFlag = true;
//				else {
//					bFlag = false;
//					break;
//				}				
//			}
//			objAPIAssertion.assertTrue(bFlag, "Validation for Image Low data.Actual["+lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGELOW)+"] Expected["+lstImageLowData+"]");
//			
//			bFlag = false;
//			for(String lstObj : lstImageMidData) {
//				if((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEMID).toString()).contains(lstObj.toString()))
//					bFlag = true;
//				else {
//					bFlag = false;
//					break;
//				}				
//			}
//			objAPIAssertion.assertTrue(bFlag, "Validation for Image Mid data.Actual["+lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEMID)+"] Expected["+lstImageMidData+"]");
//			
//			bFlag = false;
//			for(String lstObj : lstImageHighData) {
//				if((lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEHIGH).toString()).contains(lstObj.toString()))
//					bFlag = true;
//				else {
//					bFlag = false;
//					break;
//				}				
//			}
//			objAPIAssertion.assertTrue(bFlag, "Validation for Image High data.Actual["+lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.IMAGEHIGH)+"] Expected["+lstImageHighData+"]");
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForImgaeData() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	private void performValidationForRichMediaData(String sSKUNumber) throws Exception {
		List<Map<String, String>> lstExpectedRichMediaData = null;
		String sBrochureURL = null, sPowerPointURL = null, sVideoURL = null;
		try {
			lstExpectedRichMediaData = objConfig.getProductDetailsTestDataUtil().getRichMediaData(sSKUNumber, objES);
			for(Map<String, String> mapObj : lstExpectedRichMediaData) {
				if(mapObj.get("description").equals("Brochure"))
					sBrochureURL = mapObj.get("imagelow")+"~+~BROCHURE";
				else if(mapObj.get("description").equals("Manual"))
					sPowerPointURL = mapObj.get("imagelow")+"~+~MANUAL";
				else
					sVideoURL = mapObj.get("imagelow")+"~+~VIDEO";
			}
			if(null != sBrochureURL)
				objAPIAssertion.assertEquals(lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.BROCHURE_URL).toString(), sBrochureURL, "Validation for Brochure URL");
			if(null != sPowerPointURL)
				objAPIAssertion.assertEquals(lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.POWER_POINT_URL).toString(), sPowerPointURL, "Validation for PowerPoint URL");
			if(null != sVideoURL)
				objAPIAssertion.assertEquals(lstProductSearchData.get(0).get(ProductSearchElasticAPIConstants.VIDEO_URL).toString(), sVideoURL, "Validation for Video URL");
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForRichMediaData() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performValidationForAdditionalInformationAccordion(String sSKUNumber, HashMap<Object, Object> mapResponseData) throws Exception {
		Map<String, String> mapAdditionalInformationAccordionData = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber);
		String sActual = null, sExpected = null;
		try {
			if(mapAdditionalInformationAccordionData.containsKey(ProductSearchElasticAPIConstants.HEIGHT) && null != mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.HEIGHT)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.HEIGHT).toString();
				sExpected = mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.HEIGHT).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Additional Information Accordion - 'Height' Attribute in Product Details API Response");
			}

			if(mapAdditionalInformationAccordionData.containsKey(ProductSearchElasticAPIConstants.LENGTH) && null != mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.LENGTH)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.LENGTH).toString();
				sExpected = mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.LENGTH).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Additional Information Accordion - 'Length' Attribute in Product Details API Response");
			}

			if(mapAdditionalInformationAccordionData.containsKey(ProductSearchElasticAPIConstants.WIDTH) && null != mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.WIDTH)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.WIDTH).toString();
				sExpected = mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.WIDTH).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Additional Information Accordion - 'Width' Attribute in Product Details API Response");
			}

			if(mapAdditionalInformationAccordionData.containsKey(ProductSearchElasticAPIConstants.PALLETQTY) && null != mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.PALLETQTY)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.PALLETQTY).toString();
				sExpected = mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.PALLETQTY).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Additional Information Accordion - 'Pallet Quantity' Attribute in Product Details API Response");
			}

			if(mapAdditionalInformationAccordionData.containsKey(ProductSearchElasticAPIConstants.SHIPPINGWEIGHT) && null != mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.SHIPPINGWEIGHT)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.SHIPPINGWEIGHT).toString();
				sExpected = mapAdditionalInformationAccordionData.get(ProductSearchElasticAPIConstants.SHIPPINGWEIGHT).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Additional Information Accordion - 'Shipping Weight' Attribute in Product Details API Response");
			}
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForAdditionalInformationAccordion() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performValidationForBasicContentsProductDetailsSearch(String sSKUNumber, HashMap<Object, Object> mapResponseData) throws Exception {
		Map<String, String> mapBasicContentsOfProductDetailsData = objConfig.getProductDetailsTestDataUtil().getBasicContentsProductDetailsData().get(sSKUNumber);
		String sActual = null, sExpected = null;
		try {
			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.CAT1DESC) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT1DESC)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.CAT1DESC).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT1DESC).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Category name in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.CAT2DESC) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT2DESC)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.CAT2DESC).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT2DESC).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Sub-Category name in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.CAT3DESC) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT3DESC)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.CAT3DESC).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.CAT3DESC).toString();
				objAPIAssertion.assertEquals(sActual, sExpected, "Product Type name in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.VENDORNAME) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.VENDORNAME)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.VENDORNAME).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.VENDORNAME).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Vendor name in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.TRIMMED_MATERIAL) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.TRIMMED_MATERIAL)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.TRIMMED_MATERIAL).toString();
				sExpected = sSKUNumber;	
				objAPIAssertion.assertTrue(sExpected.contains(sActual), "Validation for SKU Number in Product Details API Response. Actual["+sActual+"] Expected["+sExpected+"]");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "VPN Number in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.UPCEAN) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.UPCEAN)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.UPCEAN).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.UPCEAN).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "UPC/EAN Number in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.LONGDESC1) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.LONGDESC1)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.LONGDESC1).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.LONGDESC1).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Product Description in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.MEDIUMDESC) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.MEDIUMDESC)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.MEDIUMDESC).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.MEDIUMDESC).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Medium Description in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.IMAGELOW) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGELOW)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.IMAGELOW).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGELOW).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Image low in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.IMAGEMID) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGEMID)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.IMAGEMID).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGEMID).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Image mid in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.IMAGEHIGH) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGEHIGH)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.IMAGEHIGH).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.IMAGEHIGH).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "Image high in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.BROCHURE_URL) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.BROCHURE_URL)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.BROCHURE_URL).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.BROCHURE_URL).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "brochure url in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.POWER_POINT_URL) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.POWER_POINT_URL)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.POWER_POINT_URL).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.POWER_POINT_URL).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "power point url in Product Details API Response");
			}

			if(mapBasicContentsOfProductDetailsData.containsKey(ProductSearchElasticAPIConstants.VIDEO_URL) && null != mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.VIDEO_URL)) {
				sActual = mapResponseData.get(ProductSearchElasticAPIConstants.VIDEO_URL).toString();
				sExpected = mapBasicContentsOfProductDetailsData.get(ProductSearchElasticAPIConstants.VIDEO_URL).toString();		
				objAPIAssertion.assertEquals(sActual, sExpected, "video url in Product Details API Response");
			}
		} catch(Exception e) {
			String sErrMessage="FAIL: performValidationForBasicContentsProductDetailsSearch() method of ProductDetailsElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
}
