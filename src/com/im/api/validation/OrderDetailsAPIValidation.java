package com.im.api.validation;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.CheckOutOptions;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.business.APIEnumerations.IntermediateResults;
import com.im.imonline.api.business.APIEnumerations.WarehouseTypes;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.business.HermesE2ERegister;
import com.im.imonline.api.business.OrderUnit;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;
import com.im.wrapper.testdata.ShipToAddressData;

import net.minidev.json.JSONArray;

public class OrderDetailsAPIValidation extends CommonValidation{
	Logger logger = Logger.getLogger("OrderDetailsAPIValidation"); 
	HashMap<String, String> hmTestData = null, hmConfig = null;
	HashMap<String, String> expectedReqSkuData = new HashMap<String, String>();
	String sRequestBody=null, sReqSku =null; 
	FlagOptions_ProductSummary flagType = null;
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objConfig = null;
	E2EHermesAPITestDataUtil objTestDataUtil=null;
	private HermesE2ERegister hermesOption;


	public OrderDetailsAPIValidation(APIAssertion pElem, HermesE2EFlowRequestConfig objConfig2) {
		super(pElem);
		objAPIAssertion=pElem;
		objConfig=objConfig2;
		hmTestData=objConfig.getHmTestData();
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			performResponseCodeAndStatusCodeValidation(objJSONUtil,hmTestData.get(HermesAPIConstants.APITEMPLATE));		
			hermesOption=objConfig.getHermesRegisterForTestCase();
			objTestDataUtil=objConfig.getObjTestDataUtil();	
			objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.customerordernumber"),
					objConfig.getObjTestDataUtil().getIntermediateTestData(IntermediateResults.CUSTOMERPONUMBER.toString()), "OrderDetails Response: customerordernumber field Validation for serviceresponse of Order Details call");
			
			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				if(hmTestData.get(HermesAPIConstants.ORDERNUMBER).equals((String)oOU.getErpordernumber()))
				{
					String Xpathvalue ="$..lines[?(@.partnumber=='"+oOU.getSKU()+"' && @.globallinenumber=='"+oOU.getGlobalLineNumber()+ "')]";
					List<Object> listDetailsResData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
					HashMap<String, Object> hmpDetailsResData=(HashMap<String, Object>)listDetailsResData.get(0);

					objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.ERPORDERNUMBER),(String)oOU.getErpordernumber(),"OrderDetails Response: Item erpordernumber for SKU: "+(String)oOU.getSKU());
					objAPIAssertion.assertEquals((String)hmpDetailsResData.get(HermesAPIConstants.MANUFACTURERPARTNUMBER),(String)oOU.getVpn(),"OrderDetails Response: Item manufacturerpartnumber of SKU: "+(String)oOU.getSKU());
					
					//TODO: check logic with hermes team
					if(!hermesOption.getWarehouseType().equals(WarehouseTypes.SPLIT.toString())
							&& hermesOption.getScenarioForHoldOrder().equals("NONE"))
					{
						objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.freightamount"),oOU.getFreightAmountForSKU(), "OrderDetails Response: freightamount field Validation for serviceresponse of Order Details call");
						objAPIAssertion.assertEquals(Double.parseDouble(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.ordersubtotal")),Double.parseDouble(oOU.getOrderSubTotalForSKU()), "OrderDetails Response: ordersubtotal field Validation for serviceresponse of Order Details call");
					}
					performUnitPriceValidation(hmpDetailsResData,oOU);
					performRequestedQtyValidation(hmpDetailsResData,oOU);
					
					if(hermesOption.getScenarioForHoldOrder().equals("NONE"))
						performShipfromwarehouseidAndCarrierValidation(hmpDetailsResData,oOU);
				}
			}
			
			Set<CheckOutOptions> keySet = hermesOption.getKeySetForConfigurations();
			for(CheckOutOptions option:keySet) {
				switch(option) {
				case BASKET_NOTE:
					performBasketNotesValidation(objJSONUtil);
					break;
				case SHIPTO_BILLTO_ADDRESS: 
					performShiptoAddrValidation(objJSONUtil,hermesOption.getShippingAddressType());
				break;
				case LINE_NOTES: 
					performLineNotesValidation(objJSONUtil);
					break;
				case DELIVERY_SERVICE_TYPE: 
					//validated at order unit level
				break;
				case ENDUSER_PONUMBER: 
					performEUPOValidation(objJSONUtil);
					break;
				case PAYMENT_TYPE: 
					performPaymentTypeConfigValidation(objJSONUtil,hermesOption.getPaymentType());				
				break;
				case WAREHOUSE_TYPE: 
					//validated at order unit level
				break;
				case HOLD_ORDER_TYPE: 
					if(!hermesOption.getScenarioForHoldOrder().equals("NONE"))
					{
						objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.lines[0].linestatus"),"Customer Hold(IM)",
													"OrderDetails Response: Hold Order Parameter - linestatus");
						objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.orderstatus"),"Customer Hold(IM)",
								"OrderDetails Response: Hold Order Parameter - orderstatus");					
					}
				break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

	private void performEUPOValidation(JsonUtil objJSONUtil) {
		try {
			String euPoNum=hermesOption.getEUPONumber();
			if(!euPoNum.equals("")) {
			List<Object> euPoNumList=objJSONUtil.getResponseFromJayway(objJSONUtil, "$..enduserponumber");
			objAPIAssertion.assertEquals((String)euPoNumList.get(0), euPoNum, "OrderDetails Response: Order EndUserPONumber Validation");
		} 
		}catch (Exception e) {
			String sErrMessage="FAIL: performEUPOValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
}

	private void performLineNotesValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String lineNote=hermesOption.getLineNotes();
			List<? extends Object> lnList;
			boolean lineNoteExists;
			if(!lineNote.equals("")) {
				if(!objTestDataUtil.getCountryGroups().isSAP()) 
				{
					lnList=objJSONUtil.getResponseFromJayway(objJSONUtil, "$..productextendedspecs[?(@.attributename=='lineextracommenttext')].attributevalue");
					lineNoteExists = ((List<String>)lnList).stream().anyMatch(s->s.contains(lineNote.toUpperCase()));
				} else {
					lnList=objJSONUtil.getResponseFromJayway(objJSONUtil, "$..productextendedspecs[?(@.attributename=='commenttext')].attributevalue");
					lineNoteExists = ((List<String>)lnList).stream().anyMatch(s->s.contains(lineNote));
				}
			objAPIAssertion.assertTrue(lineNoteExists, "OrderDetails Response: Order Line Notes Validation");
		}
		}catch (Exception e) {
			String sErrMessage="FAIL: performLineNotesValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	private void performBasketNotesValidation(JsonUtil objJSONUtil) {
		try {
			String cartNote=hermesOption.getBasketNotes();
			if(!cartNote.equals("")) {
				List<? extends Object> bnList=objJSONUtil.getResponseFromJayway(objJSONUtil, "$..extendedspecs[?(@.attributename=='commenttext')].attributevalue");
				boolean cartNoteExists = ((List<String>)bnList).stream().anyMatch(s->s.equals("BN-"+cartNote.toUpperCase()));
				
			//	objAPIAssertion.assertEquals((String)bnList.get(0), "BN-"+cartNote.toUpperCase(), "OrderDetails Response: Order Cart Notes Validation");
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performBasketNotesValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
}

	private void performPaymentTypeConfigValidation(JsonUtil objJSONUtil, String paymentType) throws Exception {
		try {
			List<Object> listPaymentTermCodeData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,"$..extendedspecs[?(@.attributename=='termscode')].attributevalue");
			List<Object> listPaymentTermDescData=(List<Object>)objJSONUtil.getResponseFromJayway(objJSONUtil,"$..extendedspecs[?(@.attributename=='termsdescription')].attributevalue");
			switch(paymentType)
			{
			case "CREDITCARD":
				if(!objTestDataUtil.getCountryGroups().isSAP()) {
				objAPIAssertion.assertTrue(((String)listPaymentTermCodeData.get(0)).equals("998"),"OrderDetails Response: Order TermsCode Validation for Credit Card");
				objAPIAssertion.assertTrue(((String)listPaymentTermDescData.get(0)).equalsIgnoreCase("CREDIT CARD"),"OrderDetails Response: Order Terms Description Validation for Credit Card");
				}
				else {
				objAPIAssertion.assertTrue(((String)listPaymentTermCodeData.get(0)).equals("Z052"),"OrderDetails Response: Order TermsCode Validation for Credit Card");
				objAPIAssertion.assertTrue(((String)listPaymentTermDescData.get(0)).equalsIgnoreCase("PREPAYMENT"),"OrderDetails Response: Order Terms Description Validation for Credit Card");
				}
				break;
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performUnitpriceValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	private void performUnitPriceValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {		
		try {
			String uPrice=String.valueOf(hmpDetailsResData.get(HermesAPIConstants.UNITPRICE));
			double unitPr=Double.parseDouble(uPrice);
			objAPIAssertion.assertEquals(unitPr,Double.parseDouble(oOU.getUnitPrice()), "OrderDetails Response: Item UnitPrice for SKU["+(String)oOU.getSKU()+"]");
		} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performUnitpriceValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void performShiptoAddrValidation(JsonUtil objJSONUtil, String shipToBillToParam) {
		try {
			HashMap<String,String> hmpShipto=(HashMap<String,String>)objJSONUtil.getResponseFromJayway(objJSONUtil, "$..shiptoaddress").get(0);
			HashMap<String, String> hmShipToFromExcel=ShipToAddressData.getShippingAddressForCountry(hmTestData.get("RunConfig"));
			hmShipToFromExcel.remove("COUNTRY");

			switch(shipToBillToParam)
			{
			case "SELECT_DIFFERENT_SHIPTO":
				objAPIAssertion.assertEquals(hmpShipto.get("name"),hmShipToFromExcel.get("addressline1").replaceAll("\\s", ""),"OrderDetails Response: Shipto Addr Line 1 Validation");
				objAPIAssertion.assertEquals(hmpShipto.get("city"),hmShipToFromExcel.get("city").replaceAll("\\s", ""),"OrderDetails Response: Shipto City Validation");
				objAPIAssertion.assertEquals(hmpShipto.get("state"),hmShipToFromExcel.get("state").replaceAll("\\s", ""),"OrderDetails Response: Shipto State Validation");
				objAPIAssertion.assertEquals(hmpShipto.get("postalcode"),hmShipToFromExcel.get("postalcode"),"OrderDetails Response: Shipto PostCode Validation");
				objAPIAssertion.assertEquals(hmpShipto.get("countrycode"),hmShipToFromExcel.get("countrycode"),"OrderDetails Response: Shipto countrycode Validation");
				break;
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: performShiptoAddrValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);  
		}		
	}

	private void performRequestedQtyValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			String quan=String.valueOf(hmpDetailsResData.get("requestedquantity"));
			double qty=Double.parseDouble(quan);
			objAPIAssertion.assertEquals(qty,oOU.getQuantity(), "OrderDetails Response: Item Quantity for SKU["+(String)oOU.getSKU()+"]");
		} catch (NumberFormatException e) {
			String sErrMessage="FAIL: performQuantityValidation() method of OrderCreateAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	
	private void performShipfromwarehouseidAndCarrierValidation(HashMap<String, Object> hmpDetailsResData, OrderUnit oOU) throws Exception {
		try {
			switch(oOU.getProdcode().toString())
			{
			case "DOWNLOADABLE":
				break;
			default:
				JSONArray shipmentInfo = (JSONArray) hmpDetailsResData.get("shipmentdetails");
				HashMap mapIthWarehouse=(HashMap<?, ?>)shipmentInfo.get(0);
				String resWH= (String) mapIthWarehouse.get("shipfromwarehouseid");
				String resCarrierCd= (String) mapIthWarehouse.get(HermesAPIConstants.CARRIERCODE);
				objAPIAssertion.assertEquals(resWH,(String)oOU.getWarehouse(),"OrderDetails Response: Item shipfromwarehouseid for SKU: "+(String)oOU.getSKU());
				objAPIAssertion.assertEquals(resCarrierCd,(String)oOU.getCarriercode(),"OrderDetails Response: Item carriercode for SKU: "+(String)oOU.getSKU());
				break;
			} 
		}catch (Exception e) {
			String sErrMessage="FAIL: performShipfromwarehouseidAndCarrierValidation() method of OrderDetailsAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

}
