package com.im.api.validation;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.business.ProductSummaryRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductSummaryTestDataUtil;

/**
 * @author Vinita Lakhwani
 * ProductSummaryElasticAPIValidation
 * Elastic  Search Product Summary -- Validation Class skeleton
 */
public class ProductSummaryElasticAPIValidation {
	Logger logger = Logger.getLogger("ProductSummaryElasticAPIValidation"); 
	HashMap<String, String> hmTestData = null, hmConfig = null;
	HashMap<String, String> expectedReqSkuData = new HashMap<String, String>();
	String sRequestBody=null, sReqSku =null; 
	FlagOptions_ProductSummary flagType = null;
	APIAssertion objAPIAssertion = null;
	ProductSummaryRequestDataConfig objConfig = null;
	ProductSummaryTestDataUtil objTestDataUtil=null;


	public ProductSummaryElasticAPIValidation(APIAssertion pElem, ProductSummaryRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		hmTestData=objConfig.gethmTestData();
		flagType=objConfig.getSearchConfig();
		objTestDataUtil=objConfig.getProductSummaryTestDataUtil();
	}

	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of ProductSummaryElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			performResponseCodeValidation(objJSONUtil);
			FlagOptions_ProductSummary flagConfig=objConfig.getSearchConfig();
			String instock=null, instockOrOrder=null, sOnOrderQty;
			Object oOnOrderQty=null;

			if(objConfig.isDataCaptured())
			{
				//List<HashMap<String, String>> lstSourceResponseData = objJSONUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
				List<String> matchedQueries = objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathMatchedQueries);

				sReqSku = objConfig.gethmTestData().get(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS);
				expectedReqSkuData = objConfig.getProductSummaryTestDataUtil()
						.getMapContentsOfProductSummaryData().get(sReqSku);

				/*HashMap<String, String> source= new HashMap<>();
				for(HashMap<String, String> mapSource : lstSourceResponseData) {
					if(mapSource.get(ProductSearchElasticAPIConstants.MATERIAL).toString()
							.equals(expectedReqSkuData.get(ProductSearchElasticAPIConstants.MATERIAL).toString()))
						source = mapSource;
				}*/
				String Xpathvalue ="$.._source[?(@.material=='"+expectedReqSkuData.get(ProductSearchElasticAPIConstants.MATERIAL).toString()+"')]";
				List<Object> lstSourceResponseData=objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
				HashMap<String, String> source=(HashMap<String, String>) lstSourceResponseData.get(0);
				
				//HashMap<String, String> source=lstSourceResponseData.get(0);
				validateGenericInformationForSku(source);

				switch(flagConfig)
				{
				case INSTOCK:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_INSTOCK);
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER);				
					break;

				case INSTOCKORORDER:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER);	

					instock = source.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).toString();
					oOnOrderQty= source.get(ProductSearchElasticAPIConstants.ONORDERQTY);
					sOnOrderQty=oOnOrderQty.toString();

					if(!Boolean.parseBoolean(instock)) 				
						objAPIAssertion.assertTrue(Integer.parseInt(sOnOrderQty)>0, "On Order Quantity Validation when product not in stock");
					else
						objAPIAssertion.assertTrue(Integer.parseInt(sOnOrderQty)>=0, "On Order Quantity Validation when product is in stock");

					break;

				case  OUTOFSTOCK:
					instock = source.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).toString();
					objAPIAssertion.assertEquals(instock, "false", "instock Flag Validation for out of stock product");	

					instockOrOrder = (String) source.get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).toString();
					oOnOrderQty=source.get(ProductSearchElasticAPIConstants.ONORDERQTY);
					sOnOrderQty=oOnOrderQty.toString();

					if(!Boolean.parseBoolean(instockOrOrder)) 
						objAPIAssertion.assertTrue(Integer.parseInt(sOnOrderQty)==0, "On Order Quantity Validation when product not in stock");
					else
						objAPIAssertion.assertTrue(Integer.parseInt(sOnOrderQty)>0, "On Order Quantity Validation when product is in stock");
					break;

				case DOWNLOADABLE:
					String enduserrequired=source.get(ProductSearchElasticAPIConstants.FLAG_ENDUSERREQUIRED).toString();
					//not adding this condition as this is not applicable to some SKU's - country AT
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ESD);	
					break;

				case DIRECTSHIP:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP);
					break;

				case SPECIALPRICING:
					objAPIAssertion.assertEquals(source.get("promoenddt").toString(),expectedReqSkuData.get("promoenddt"),"campaign field validation");
					break;

				case QUANTITYBREAK:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK);
					//matchedQueries not retrieving quantitybreak for MX,IT
					//objAPIAssertion.assertTrue(matchedQueries.stream().anyMatch("quantitybreak"::equalsIgnoreCase), "Matched Queries contains Quantity break key");						break;
					break;
				case SPECIALBIDS:
					//checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS);
					objAPIAssertion.assertTrue(matchedQueries.stream().anyMatch("specialBid"::equalsIgnoreCase), "Matched Queries contains specialBid key");
					break;

				case WEBONLYPRICE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE);
					//matchedQueries not retrieving webonlyprice for MX
					//objAPIAssertion.assertTrue(matchedQueries.stream().anyMatch("webonlyprice"::equalsIgnoreCase), "Matched Queries contains webonlyprice key");
					break;

				case REFURBISHED:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_REFURBISHED);
					break;

				case NORETURNS:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_NORETURNS);
					break;

				case AUTHORIZEDTOPURCHASE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE);
					objAPIAssertion.assertTrue(matchedQueries.stream().anyMatch("authorizedToPurchase"::equalsIgnoreCase), "Matched Queries contains authorizedtopurchase key");
					break;

				case NOTORDERABLE:
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).toString(), "N", "backorderswitch should be 'N' for Not_Orderable_Online product");
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).toString(), "false", "Return Limitations should be 'false' for Not_Orderable_Online product");
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).toString(), "true", "directshipflag should be 'true' for Not_Orderable_Online product");
					break;

				case NEW:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_NEW);
					break;

				case FREEITEM:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_FREEITEM);
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.SHIP_ALONG_EXPIRED_DATE).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_EXPIRED_DATE),"shipalongexpiredt field validation");
					if(expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION)!=null)
						objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION).toString().trim(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_PART_DESCRIPTION).trim(),"shipalongpartdes field validation");
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.SHIP_ALONG_MATERIAL).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_MATERIAL),"shipalongmaterial field validation");
					Object shipAlongFreeQty=source.get(ProductSearchElasticAPIConstants.SHIP_ALONG_FREE_QTY);
					Object shipAlongMinQty=source.get(ProductSearchElasticAPIConstants.SHIP_ALONG_MIN_QTY);
					objAPIAssertion.assertEquals(shipAlongFreeQty.toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_FREE_QTY),"shipalongfreeqty field validation");
					objAPIAssertion.assertEquals(shipAlongMinQty.toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.SHIP_ALONG_MIN_QTY),"shipalongminqty field validation");
					objAPIAssertion.assertTrue(matchedQueries.stream().anyMatch("shipalong"::equalsIgnoreCase), "Matched Queries contains shipalong key");
					break;

				case DISCONTINUED:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_DISCONTINUED);
					break;

				case REPLACEMENT:
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE),"subrule field validation");
					if(lstSourceResponseData.size()==1)
						objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL),"submaterial field validation");
					break;

				case LTL:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_LTL);
					break;

				case RETURNLIMITATIONS:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS);
					break;

				case HEAVYWEIGHT:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT);
					break;

				case BUNDLE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_BUNDLE);
					break;

				case AVAILABLE_IN_BUNDLE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_COMPONENT);
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.PARTOFBUNDLE).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.PARTOFBUNDLE),"partofbundle field validation");
					break;

				case ACCESSORIES:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ACCESSORIES);
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_HASCROSSSELL);
					break;

				case SUGGESTED:
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE),"subrule field validation");
					if(lstSourceResponseData.size()==1)
						objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL),"submaterial field validation");
					break;

				case BACKORDER:
					String flag = source.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).toString();
					objAPIAssertion.assertEquals(flag, "Y", ProductSearchElasticAPIConstants.FLAG_BACKORDER +" field Validation");
					break;

				case ENDUSER:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ENDUSERREQUIRED);
					break;

				case WARRANTY:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_WARRANTY);
					break;

				case WARRANTIES:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_WARRANTIES);
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_HASCROSSSELL);
					break;

				case PROMOTION:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_PROMOTION2);
					break;

				case LICENCEPRODUCTS:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS);
					break;

				case BULKFREIGHT:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT);
					break;

				case BLOWOUT:
					break;

				case SHIPFROMPARTNER:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER);
					break;

				case CLEARANCE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_CLEARANCE);
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE_DESC).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE_DESC),"crtvaluedesc field validation");
					objAPIAssertion.assertEquals(source.get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE).toString(),expectedReqSkuData.get(ProductSearchElasticAPIConstants.CLEARANCE_VALUE),"crtvalue field validation");

					break;

				case ACOP_PROMOTION:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION);
					break;

				case ACOP_QUANTITYBREAK:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK);
					break;

				case ACOP_WEBONLYPRICE:
					checkIfFlagIsTrue(source,ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE);
					break;

				default:
					break;

				}
			}
			else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of ProductSummaryElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

	private void checkIfFlagIsTrue(HashMap<String, String> source, String flagOption) {
		String flag = source.get(flagOption).toString();
		objAPIAssertion.assertEquals(flag, "true", flagOption +" Flag Validation. Received :"+flag);

	}

	private void validateGenericInformationForSku(HashMap<String, String> source) throws Exception {
		String imgId=null;
		try {			
			String sSKU = source.get(ProductSearchElasticAPIConstants.MATERIAL).toString();
			objAPIAssertion.assertEquals(sSKU, expectedReqSkuData.get(ProductSearchElasticAPIConstants.MATERIAL), "SKU Number Validation");


			String sVPN = source.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER).toString();
			objAPIAssertion.assertEquals(sVPN, expectedReqSkuData.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER), "VPN Validation");

			if(source.get(ProductSearchElasticAPIConstants.UPCEAN)!=null) {
				String upcean = source.get(ProductSearchElasticAPIConstants.UPCEAN).toString();
				objAPIAssertion.assertEquals(upcean, expectedReqSkuData.get(ProductSearchElasticAPIConstants.UPCEAN), "UPCEAN Validation");	
			}

			if(source.get(ProductSearchElasticAPIConstants.IMAGEMID)!=null) {
				imgId = source.get(ProductSearchElasticAPIConstants.IMAGEMID).toString();
				objAPIAssertion.assertEquals(imgId, expectedReqSkuData.get(ProductSearchElasticAPIConstants.IMAGEMID), "Image link Validation");
			}

			if(source.get(ProductSearchElasticAPIConstants.MEDIUMDESC)!=null)
			{
				String mediumDesc = source.get(ProductSearchElasticAPIConstants.MEDIUMDESC).toString().trim();
				objAPIAssertion.assertEquals(mediumDesc, expectedReqSkuData.get(ProductSearchElasticAPIConstants.MEDIUMDESC).trim(), "Medium Description validation");
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: validateGenericInformationForSku() method of ProductSummaryElasticAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
}
