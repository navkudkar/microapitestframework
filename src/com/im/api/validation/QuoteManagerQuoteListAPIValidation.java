package com.im.api.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SearchType;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SortingOptions;
import com.im.imonline.api.business.QuoteManagerQuoteListRequestDataConfig;
import com.im.imonline.api.constants.QuoteManagerQuoteListAPIConstants;

/**
 * @author
 * InvoiceSearchElasticAPIValidation
 * Invoice Search Automation -- Validation Class skeleton
 */
public class QuoteManagerQuoteListAPIValidation {
	Logger logger = Logger.getLogger("QuoteManagerQuoteListAPIValidation"); 
	//	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	HashMap<String, String> hmKeywordName = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	QuoteManagerQuoteListRequestDataConfig objConfig = null;


	public QuoteManagerQuoteListAPIValidation(APIAssertion pElem, QuoteManagerQuoteListRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		//hmTestData=objConfig.gethmTestData();
	}



	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	public synchronized void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			ArrayList<SearchType> listForValidation=objConfig.getListSetConfig();
			performResponseCodeValidation(objJSONUtil);
			if(objConfig.getListSetConfig() != null) {
				for(SearchType attribute : listForValidation) {
					switch(attribute) {
					case BLANK_RESELLERNUMBER:
						performValidationForBlankResellerNumber(objJSONUtil);
						break;
					case STATUS_ACTIVE:
						performValidationForQuoteStatus(objJSONUtil);
						break;

					case STATUS_CLOSED:
						performValidationForQuoteStatus(objJSONUtil);
						break;
					case STATUS_DRAFT:
						performValidationForQuoteStatus(objJSONUtil);
						break;
					case STATUS_ACTIVE_CLOSED:
						performValidationForMultipleQuoteStatus(objJSONUtil);
						break;
					case STATUS_DRAFT_CLOSED:
						performValidationForMultipleQuoteStatus(objJSONUtil);
						break;
					case STATUS_ACTIVE_DRAFT:
						performValidationForMultipleQuoteStatus(objJSONUtil);
						break;
					case STATUS_ACTIVE_DRAFT_CLOSED:
						performValidationForMultipleQuoteStatus(objJSONUtil);
						break;						
					case SORTBY:
						performSortValidation(objJSONUtil);
						break;	
					case QUOTE_NUMBER:
						performValidationForQuoteNumber(objJSONUtil);
						break;
					case END_USER_NAME:
						performValidationForEndUserName(objJSONUtil);
						break;
					case QUOTES_QUOTESCREATEDLAST30DAYS:
						performValidationForCreatedDateRange(objJSONUtil);
						break;
					case QUOTES_QUOTESCREATEDLAST60DAYS:
						performValidationForCreatedDateRange(objJSONUtil);
						break;						
					default:
						break;

					}//end of switch
				}//end of for
			} 
			objAPIAssertion.setExtentInfo("No Validation is asked to perform the Search Type is empty");

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			System.gc();
		}
	}

	public synchronized void performNegativeValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			ArrayList<SearchType> listForValidation=objConfig.getListSetConfig();
			//performResponseCodeValidation(objJSONUtil);
			if(objConfig.getListSetConfig() != null) {
				for(SearchType attribute : listForValidation) {
					switch(attribute) {
					case BLANK_RESELLERNUMBER:
						performValidationForBlankResellerNumber(objJSONUtil);
						break;			
					case BLANK_COUNTRYCODE:
						performValidationForBlankCountryCode(objJSONUtil);
						break;	
					default:
						break;

					}//end of switch
				}//end of for
			} 
			objAPIAssertion.setExtentInfo("No Validation is asked to perform the Search Type is empty");

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			System.gc();
		}
	}

	private void performValidationForBlankCountryCode(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			Integer iExpectedSatusCode= 400;			
			objAPIAssertion.setExtentInfo("The Expected Status Code for Blank Country Code is "+iExpectedSatusCode);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualQuoteNumber = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteNumber);		
			objAPIAssertion.assertTrue(lstActualQuoteNumber.size()==0, "List is empty");
			int iActualStatusCode= objJSONUtil.getResponse().getStatusCode();

			String sResponseBody =  objJSONUtil.getResponse().getBody().asString();
			String sResponseMessage = sResponseBody.substring(100, 128);
			String sExpectedResponseMessage = "Country Code can't be blank.";

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			objAPIAssertion.assertTrue(iActualStatusCode==iExpectedSatusCode, "Validation of Response Status Code Expected ["+iActualStatusCode+"] Actual ["+iExpectedSatusCode+" ]");
			objAPIAssertion.assertEquals(sResponseMessage, sExpectedResponseMessage, "Validation of Response Message Expected ["+sExpectedResponseMessage+"] Actual ["+sResponseMessage+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForBlankCountryCode() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	  
	}

	private void performValidationForBlankResellerNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			Integer iExpectedSatusCode= 400;			
			objAPIAssertion.setExtentInfo("The Expected Status Code for Blank Reseller Number is "+iExpectedSatusCode);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualQuoteNumber = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteNumber);		
			objAPIAssertion.assertTrue(lstActualQuoteNumber.size()==0, "List is empty");
			int iActualStatusCode= objJSONUtil.getResponse().getStatusCode();

			String sResponseBody =  objJSONUtil.getResponse().getBody().asString();
			String sResponseMessage = sResponseBody.substring(100, 131);
			String sExpectedResponseMessage = "Customer Number can't be blank.";

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			objAPIAssertion.assertTrue(iActualStatusCode==iExpectedSatusCode, "Validation of Response Status Code Expected ["+iActualStatusCode+"] Actual ["+iExpectedSatusCode+" ]");
			objAPIAssertion.assertEquals(sResponseMessage, sExpectedResponseMessage, "Validation of Response Message Expected ["+sExpectedResponseMessage+"] Actual ["+sResponseMessage+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForBlankResellerNumber() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	  
	}

	private void performValidationForCreatedDateRange(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String>hmRequestMap =objConfig.gethmTestData();
			//String sExpectedToDate=hmRequestMap.get(QuoteManagerQuoteListAPIConstants.FROMDATE).substring(13, 23);
			String sExpectedToDate=hmRequestMap.get(QuoteManagerQuoteListAPIConstants.TODATE);
			String sExpectedFromDate=hmRequestMap.get(QuoteManagerQuoteListAPIConstants.FROMDATE);
			objAPIAssertion.setExtentInfo("Expected Quote Created Date range Picked is from: "+sExpectedFromDate+" to: "+sExpectedToDate);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualQuoteCreatedDate = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteCreatedDate);
			objAPIAssertion.assertFalse(lstActualQuoteCreatedDate.size()==0,"List isn't empty");
			// create a new ArrayList 
			// return the array 
			List<String> lstActualQuoteCreatedDateTrimmedValue = new ArrayList<String>();
			for (String s : lstActualQuoteCreatedDate) {
				String newValue = s.substring(0, 10);
				// Add the new element 
				lstActualQuoteCreatedDateTrimmedValue.add(newValue); 	     
			}

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= DateTimeUtil.verifyGivenDateFallsBetween2DatesInMentionedFormat(lstActualQuoteCreatedDateTrimmedValue, sExpectedFromDate, sExpectedToDate,"yyyy-MM-dd");
			objAPIAssertion.assertTrue(bCheck,  "Validation of Quote Created Date Expected ["+sExpectedFromDate +" to "+sExpectedToDate+"] Actual ["+lstActualQuoteCreatedDateTrimmedValue+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForCreatedDateRange() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}


	private void performValidationForEndUserName(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedEndUserName= hmRequestMap.get(QuoteManagerQuoteListAPIConstants.EndUserName).replace("\"", "");
			objAPIAssertion.setExtentInfo("Expected End User Name is: "+sExpectedEndUserName);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualEndUserName = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteEndUserName);		
			objAPIAssertion.assertFalse(lstActualEndUserName.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListValuesWithExpectedValue(lstActualEndUserName, sExpectedEndUserName);
			objAPIAssertion.assertTrue(bCheck,  "Validation of End User Name Expected ["+sExpectedEndUserName +"] Actual ["+lstActualEndUserName+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForEndUserName() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}		
	}



	private void performValidationForQuoteNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedQuoteNumber= hmRequestMap.get(QuoteManagerQuoteListAPIConstants.QuoteNumber).replace("\"", "");
			objAPIAssertion.setExtentInfo("Expected Quote Number is: "+sExpectedQuoteNumber);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualQuoteNumber = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteNumber);		
			objAPIAssertion.assertFalse(lstActualQuoteNumber.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListValuesWithExpectedValue(lstActualQuoteNumber, sExpectedQuoteNumber);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Quote Number Expected ["+sExpectedQuoteNumber +"] Actual ["+lstActualQuoteNumber+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForQuoteNumber() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForQuoteStatus(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedStatus= hmRequestMap.get(QuoteManagerQuoteListAPIConstants.QuoteStatus).replace("\"", "");
			objAPIAssertion.setExtentInfo("Expected Quote Status Code is: "+sExpectedStatus);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualStatus = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteStatus);		
			objAPIAssertion.assertFalse(lstActualStatus.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListValuesWithExpectedValue(lstActualStatus, sExpectedStatus);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Status Expected ["+sExpectedStatus +"] Actual ["+lstActualStatus+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForStatus() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		} 
	}



	private void performValidationForMultipleQuoteStatus(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedStatus= hmRequestMap.get(QuoteManagerQuoteListAPIConstants.QuoteStatus).replace("\"", "").toLowerCase();
			ArrayList<String> sExpectedStatusList = Util.tokenizeString(sExpectedStatus, ",");

			objAPIAssertion.setExtentInfo("Expected Quote Status Code is: "+sExpectedStatus);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualStatus = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteStatus);		
			objAPIAssertion.assertFalse(lstActualStatus.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListOfValuesWithExpectedValueList(lstActualStatus, sExpectedStatusList);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Quote Status Code Expected ["+sExpectedStatus +"] Actual ["+lstActualStatus+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForStatus() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	private synchronized void performSortValidation(JsonUtil objJSONUtil) throws Exception {

		HashMap<String, String>hmTestData =objConfig.gethmTestData();
		String sSortBy = hmTestData.get(QuoteManagerQuoteListAPIConstants.sortingColumnName);//objConfig.getInvoiceSearchTestDataUtil().getSortByObject().sSortBy;
		String sSortDirection = hmTestData.get(QuoteManagerQuoteListAPIConstants.Sorting);//objConfig.getInvoiceSearchTestDataUtil().getSortByObject().sSortDirection;
		try {
			switch (SortingOptions.valueOf(sSortBy)) {
			case createdon:
				List<String> lstActualCreatedDate = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteCreatedDate);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedCreatedDate = Util.getSortedListInRequiredOrder(lstActualCreatedDate, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualCreatedDate.equals(lstExpectedCreatedDate), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualCreatedDate+ "]");
				break;
			case name:
				List<String> lstActualQuoteName = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteName);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> ChangingToUpperCase = lstActualQuoteName.stream().map(String::toUpperCase).collect(Collectors.toList());
				List<String> lstExpectedQuoteName = Util.getSortedListInRequiredOrder(ChangingToUpperCase, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(ChangingToUpperCase.equals(lstExpectedQuoteName), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualQuoteName+ "]");
				break;
			case im360_resendusername:
				List<String> lstActualEndUser = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteEndUserName);
				objAPIAssertion.setExtentInfo("Actual Values Picked");

				lstActualEndUser.removeAll(Arrays.asList("",null));
				lstActualEndUser = lstActualEndUser.stream().map(String::toUpperCase).collect(Collectors.toList());

				List<String> lstExpectedEndUser = Util.getSortedListInRequiredOrder(lstActualEndUser, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualEndUser.equals(lstExpectedEndUser), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualEndUser+ "]");
				break;
				//Iterator<String> e_itr = lstActualEndUser.iterator();

				/*while(e_itr.hasNext())
				{
					if (e_itr.next() == null)
						e_itr.remove();
				}
				 */
			case totalamount:
				List<Object> lstActualTotalAmount = new ArrayList<Object>();
				List<Double> lActual = new ArrayList<Double>();
				List<? extends Double> lstExpectedTotalAmount= new ArrayList<Double>();

				lstActualTotalAmount =objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteTotalAmount);
				for (Object s: lstActualTotalAmount) { 
					if(s != null)
						lActual.add(Double.valueOf(s.toString()));
				}
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				lstExpectedTotalAmount= Util.getSortedDoubleListInRequiredOrder(lActual, sSortDirection);		
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lActual.equals(lstExpectedTotalAmount), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualTotalAmount+ "]");
				break;
			case modifiedon:
				List<String> lstActualLastUpdatedDate = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteUpdated);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedLastUpdatedDate = Util.getSortedListInRequiredOrder(lstActualLastUpdatedDate, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualLastUpdatedDate.equals(lstExpectedLastUpdatedDate), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualLastUpdatedDate+ "]");
				break;
			case effectiveto:
				List<String> lstActualExpiryDate = objJSONUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteExpiryDate);
				objAPIAssertion.setExtentInfo("Actual Values Picked");

				Iterator<String> w_itr = lstActualExpiryDate.iterator();

				while(w_itr.hasNext())
				{
					if (w_itr.next() == null)
						w_itr.remove();
				}

				List<String> lstExpectedExpiryDate= Util.getSortedListInRequiredOrder(lstActualExpiryDate, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualExpiryDate.equals(lstExpectedExpiryDate), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualExpiryDate+ "]");
				break;
			default:
				break;

			}

		}catch(Exception e) {
			String sErrMessage="FAIL: performSortValidation() method of QuoteManagerQuoteListAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			System.gc();
		}

	}



}
