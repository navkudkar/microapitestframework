package com.im.api.validation;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.constants.PNImicroserviceAPIConstants;

public class SpecialPriceMicroserviceAPIValidation {


	APIAssertion objAPIAssertion=null;
	Logger logger = Logger.getLogger("SpecialPriceMicroserviceAPIValidation"); 
	public SpecialPriceMicroserviceAPIValidation(APIAssertion pAPIAssertion) {
		objAPIAssertion = pAPIAssertion;
	}
	
	/** This is the function where we  manage single to multiple set responses
	 * 
	 * @param expectedMultisetDataDrivenMap expected data map from test data sheet
	 * @param lstOfDataToBeVerifiedFromResponse data map from the response
	 * @param sKeysToBeVerify Name of the parameters to verify
	 * @param sKeyForReference Name of column from test data excel against which user wish to see the details into the report 
	 * @throws Exception if data is not configured correctly in test data sheet.
	 * @author Prashant Navkudkar
	 * @Script Updated: Bivas Naik
	 */
	public synchronized  void validateACOPSingleToMultipleSetRequest(HashMap<String,String> expectedMultisetDataDrivenMap, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse,
			String[] sKeysToBeVerify, String sKeyForReference) throws Exception {

		// Validate with actual response
		objAPIAssertion.setExtentInfo("Executing for "+ sKeyForReference +": [" + expectedMultisetDataDrivenMap.get(sKeyForReference) + "]");
		
		List<String> listOfExpectedDataKeys = Arrays.asList(sKeysToBeVerify);

		List<HashMap<String,String>> reqDataForSingleToMultResponse = new ArrayList<HashMap<String, String>>();
		try {
			// Fetch data from Excel and create new data set as per require validation column
			//System.out.println("Size: " + lstOfDataToBeVerifiedFromResponse.size() +"");
			
			if (lstOfDataToBeVerifiedFromResponse.size() > 0) {
				for(int j=0; j<lstOfDataToBeVerifiedFromResponse.size(); j++) {
					HashMap<String,String> mapOfTestData = new HashMap<>();
					for(String sKeydata: listOfExpectedDataKeys) {						
						String sValue[] =StringUtils.split(expectedMultisetDataDrivenMap.get(sKeydata),Constants.DataSeperator);
						mapOfTestData.put(sKeydata, sValue[j]);
					}
					reqDataForSingleToMultResponse.add(mapOfTestData);
				}
				
			
			//} else objAPIAssertion.assertTrue(false, "No Quantity Dicounts fetched...");
			/*// Validate with actual response
			objAPIAssertion.setExtentInfo("=+=+=+=+=+=+=+==+=+=+=+= Executing for "+ sKeyForReference +":"+expectedMultisetDataDrivenMap.get(sKeyForReference)+
					" =+=+=+=+=+=+=+=+=+=+=+=");*/	
			//if (!(reqDataForSingleToMultResponse.size() == 0)) {
				
				
				for(int i=0; i<reqDataForSingleToMultResponse.size(); i++)
				{
					if(reqDataForSingleToMultResponse.get(i)==null)
					{
						String sExpected = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[0])).toUpperCase();
						objAPIAssertion.assertHardEquals(Constants.NULL_VALUE_IN_DATA_SHEET, sExpected, "Status response expected: [No Values],  Actual: [No Values]");
					}
					else {

						for(int j=0; j<sKeysToBeVerify.length; j++) {
							String sActual = String.valueOf(lstOfDataToBeVerifiedFromResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();
							String sExpected = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();		

							if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
							{
								sExpected = "";	
								objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
							}
							else if(sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.ORIGINALPRICE)|| sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.DISCOUNT))
							{
								float fActual= Util.convertStringToFloat(sActual);
								float fExpected= Util.convertStringToFloat(sExpected);
								objAPIAssertion.assertEquals(fActual, fExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
							}
							else
							{
								objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
							} 

						}
					}
				}
			} else objAPIAssertion.assertTrue(false, "Response List size is '0'..[Test Data is Improper]");

		}
		catch(ArrayIndexOutOfBoundsException | NullPointerException ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: Test data maintained in the sheet has been"
					+ " misconfigured. "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
		catch(Exception ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}
	
	/** This is the function where we  manage single to multiple set responses
	 * 
	 * @param expectedMultisetDataDrivenMap expected data map from test data sheet
	 * @param lstOfDataToBeVerifiedFromResponse data map from the response
	 * @param sKeysToBeVerify Name of the parameters to verify
	 * @param sKeyForReference Name of column from test data excel against which user wish to see the details into the report 
	 * @throws Exception if data is not configured correctly in test data sheet.
	 * @author Prashant Navkudkar
	 * @Script Updated: Bivas Naik
	 */
	public synchronized  void validateIndividualACOPdetailsSingleToMultipleSetRequest(HashMap<String,String> expectedMultisetDataDrivenMap, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse,
			String[] sKeysToBeVerify, String sKeyForReference) throws Exception {

		String sValueonIteration = null;
		List<String> listOfExpectedDataKeys = Arrays.asList(sKeysToBeVerify);

		List<HashMap<String,String>> reqDataForSingleToMultResponse = new ArrayList<HashMap<String, String>>();

		try {
			// Fetch data from Excel and create new data set as per require validation column
			for(int j=0; j<lstOfDataToBeVerifiedFromResponse.size(); j++) {
				HashMap<String,String> mapOfTestData = new HashMap<>();
				for(String sKeydata: listOfExpectedDataKeys) {						
					String sValue[] =StringUtils.split(expectedMultisetDataDrivenMap.get(sKeydata),Constants.DataSeperator);
					mapOfTestData.put(sKeydata, sValue[j]);
				}
				reqDataForSingleToMultResponse.add(mapOfTestData);
			}

			for(int i=0; i<reqDataForSingleToMultResponse.size(); i++)
			{	
				objAPIAssertion.setExtentInfo("\nACOP APPLIED ["+ (i+1) +"]");
				if(reqDataForSingleToMultResponse.get(i)==null)
				{
					String sExpected = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[0])).toUpperCase();
					objAPIAssertion.assertHardEquals(Constants.NULL_VALUE_IN_DATA_SHEET, sExpected, "Status response expected: [No Values],  Actual: [No Values]");
				}
				else {

					for(int j=0; j<sKeysToBeVerify.length; j++) {
						
						String sExpected = String.valueOf(reqDataForSingleToMultResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();
						
						//For New key validations for - US
						// Validate with actual response
						if (sKeysToBeVerify[j].contains("_"))
						{
							sValueonIteration = sKeysToBeVerify[j].split("_")[0];
						}else sValueonIteration = sKeysToBeVerify[j];
						
						String sActual = String.valueOf(lstOfDataToBeVerifiedFromResponse.get(i).get(sValueonIteration)).toUpperCase();
								
						
						if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
						{
							sExpected = "";	
						objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						}
						else if(sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.ORIGINALPRICE)|| sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.DISCOUNT))
						{
							float fActual= Util.convertStringToFloat(sActual);
							float fExpected= Util.convertStringToFloat(sExpected);
							objAPIAssertion.assertEquals(fActual, fExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						}
						else
						{
							objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						} 
						
					}
				}
			}	

		}
		catch(ArrayIndexOutOfBoundsException | NullPointerException ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: Test data maintained in the sheet has been"
					+ " misconfigured. "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
		catch(Exception ex) {
			String sErrMessage="FAIL: validateSingleToMultipleSetRequest() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}
	
	/** This is the function where we  manage single to multiple set responses
	 * 
	 * @param expectedMultisetDataDrivenMap expected data map from test data sheet
	 * @param lstOfDataToBeVerifiedFromResponse data map from the response
	 * @param sKeysToBeVerify Name of the parameters to verify
	 * @param sKeyForReference Name of column from test data excel against which user wish to see the details into the report 
	 * @throws Exception if data is not configured correctly in test data sheet.
	 * @Script Updated: Bivas Naik
	 */
	public synchronized void validatePNISPResponse(List<HashMap<String,String>> multisetDataDrivenMap, List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse,
			String[] sKeysToBeVerify) throws Exception {

		try {

			for(int i=0; i<multisetDataDrivenMap.size(); i++)
			{
				if(lstOfDataToBeVerifiedFromResponse.get(i)==null)
				{
					String sExpected = String.valueOf(multisetDataDrivenMap.get(i).get(sKeysToBeVerify[0])).toUpperCase();
					objAPIAssertion.assertHardEquals(Constants.NULL_VALUE_IN_DATA_SHEET, sExpected, "Status response expected: [No Values],  Actual: [No Values]");

				}
				else {
					for(int j=0; j<sKeysToBeVerify.length; j++) {
						String sActual = String.valueOf(lstOfDataToBeVerifiedFromResponse.get(i).get(sKeysToBeVerify[j])).toUpperCase();
						String sExpected = String.valueOf(multisetDataDrivenMap.get(i).get(sKeysToBeVerify[j])).toUpperCase();		
						if(sExpected.equalsIgnoreCase(Constants.EMPTY_VALUE_IN_DATA_SHEET))
						{
							sExpected = "";	
						objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						}
						else if(sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.ORIGINALPRICE)|| sKeysToBeVerify[j].contains(PNImicroserviceAPIConstants.DISCOUNT))
						{

							float fActual= Util.convertStringToFloat(sActual);
							float fExpected= Util.convertStringToFloat(sExpected);
							objAPIAssertion.assertEquals(fActual, fExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						}
						else
						{
							objAPIAssertion.assertEquals(sActual, sExpected, " to verify "+ sKeysToBeVerify[j].toUpperCase());
						}
					}
				}
			}

		}
		catch(Exception ex) {
			String sErrMessage="FAIL: validateMultiSetDetailsForREST() method: "+Util.getExceptionDesc(ex);
			logger.fatal(sErrMessage); 
			objAPIAssertion.assertTrue(false, sErrMessage);
		}
	}


}
