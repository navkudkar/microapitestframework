package com.im.api.validation;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;

public class OrderModifyAPIValidation extends CommonValidation{
	Logger logger = Logger.getLogger("OrderModifyAPIValidation"); 
	HashMap<String, String> hmTestData = null;
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objConfig = null;
	E2EHermesAPITestDataUtil objTestDataUtil=null;

	public OrderModifyAPIValidation(APIAssertion pElem, HermesE2EFlowRequestConfig objConfig2) {
		super(pElem);
		objAPIAssertion=pElem;
		objConfig=objConfig2;
		hmTestData=objConfig.getHmTestData();
	}

	public void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			performResponseCodeAndStatusCodeValidation(objJSONUtil,hmTestData.get(HermesAPIConstants.APITEMPLATE));		
			String expResMsg=hmTestData.get(HermesAPIConstants.ORDERNUMBER)+">Order Deleted Successfully;";
			objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.responsemessage"),expResMsg,"Order Modify - Order Delete Response: responsemessage field");
			objAPIAssertion.assertEquals(objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordermodifyresponse.customerponumber"),hmTestData.get(HermesAPIConstants.CUSTOMERPONUMBER),"Order Modify Response: customerponumber field");
			
		} catch (Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of OrderDeleteAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}

}
