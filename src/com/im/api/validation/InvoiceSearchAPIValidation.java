package com.im.api.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.imonline.api.business.APIEnumerations;
import com.im.imonline.api.business.InvoiceSearchRequestDataConfig;
import com.im.imonline.api.constants.InvoiceSearchAPIConstants;

/**
 * @author usumas00
 * InvoiceSearchElasticAPIValidation
 * Invoice Search Automation -- Validation Class skeleton
 */
public class InvoiceSearchAPIValidation {
	Logger logger = Logger.getLogger("InvoiceSearchAPIValidation"); 
	//	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	HashMap<String, String> hmKeywordName = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	InvoiceSearchRequestDataConfig objConfig = null;


	public InvoiceSearchAPIValidation(APIAssertion pElem, InvoiceSearchRequestDataConfig pobjConfig) {
		objAPIAssertion=pElem;
		objConfig=pobjConfig;
		//hmTestData=objConfig.gethmTestData();
	}



	private void performResponseCodeValidation(JsonUtil objJSONUtil) throws Exception {
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	public synchronized void performValidationforGivenConfig(JsonUtil objJSONUtil) throws Exception {
		try {
			ArrayList<APIEnumerations.InvoiceSearchType> listForValidation=objConfig.getListSetConfig();
			performResponseCodeValidation(objJSONUtil);
			if(objConfig.getListSetConfig() != null) {
				for(APIEnumerations.InvoiceSearchType attribute : listForValidation) {
					switch(attribute) {
					case INVOICE_NUMBER:
						performValidationForInvoiceNumber(objJSONUtil);
						break;
					case PAGESIZE:
						performValidationForPageSize(objJSONUtil);
						break;
					case SERIAL_NUMBER:
						performValidationForSerialNumber(objJSONUtil);
						break;
					case SKU:
						performValidationForIngramPartNumber(objJSONUtil);
						break;
					case VPN:
						performValidationForVendorPartNumber(objJSONUtil);
						break;
					case RESELLER_NUMBER:
						performValidationForResellerNumber(objJSONUtil);
						break;
					case CUSTOMER_ORDER_NUMBER:
						performValidationForCustomerOrderNumber(objJSONUtil);
						break;
					case CPN:
						performValidationForCPN(objJSONUtil);
						break;
					case BID:
						performValidationForBidNumber(objJSONUtil);
						break;
					case RMA_TYPEINVOICE:
					case ORDER_TYPEINVOICE:
					case CRMEMO_TYPEINVOICE:
						performValidationForTypeInvoice(objJSONUtil);
						break;
					case STATUS_OPEN:
					case STATUS_DUE:
					case STATUS_PAID:
						performValidationForStatus(objJSONUtil);
						break;
					case INVOICEDATE_RANDOMDATE:
					case INVOICEDATE_LAST31DAYS:
					case INVOICEDATE_INVOICELAST5DAYS:
					case INVOICEDATE_OF1000DAYSBEFORETODAY_FOR23DAYS:
					case INVOICEDATE_OF760DAYSBEFORETODAY_FOR5DAYS:
					case INVOICEDATE_OF400DAYSBEFORETODAY_FOR10DAYS:
					case INVOICEDATE_OF140DAYSBEFORETODAY_FOR12DAYS:
					case INVOICEDATE_OF50DAYSBEFORETODAY_FOR2DAYS:
						performValidationForInvoiceDate(objJSONUtil);
						break;
					case DUEDATE_LAST31DAYS:
						perfromValidationForDueDaterange(objJSONUtil);
						break;
					case SORTBY:

						performSortValidation(objJSONUtil);

						break;
					default:
						break;

					}//end of switch
				}//end of for
			} else
				objAPIAssertion.setExtentInfo("No Validation is asked to perform the Search Type is empty");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performValidationforGivenConfig() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			System.gc();
		}
	}

	private synchronized void performSortValidation(JsonUtil objJSONUtil) throws Exception {

		HashMap<String, String>hmTestData =objConfig.gethmTestData();
		String sSortBy = hmTestData.get(InvoiceSearchAPIConstants.SORTBY);//objConfig.getInvoiceSearchTestDataUtil().getSortByObject().sSortBy;
		String sSortDirection = hmTestData.get(InvoiceSearchAPIConstants.SORTDIRECTION);//objConfig.getInvoiceSearchTestDataUtil().getSortByObject().sSortDirection;
		try {
			switch (APIEnumerations.InvoiceSortingOptions.valueOf(sSortBy)) {
			case invoicenumber_keyword:
				List<String> lstActualInvoiceNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceNumber);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedInvoiceNumber = Util.getSortedListInRequiredOrder(lstActualInvoiceNumber, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualInvoiceNumber.equals(lstExpectedInvoiceNumber), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualInvoiceNumber+ "]");
				break;
			case customernumber_keyword:
				List<String> lstActualCustomerNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathResellerNumber);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedCustmerNumber = Util.getSortedListInRequiredOrder(lstActualCustomerNumber, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualCustomerNumber.equals(lstExpectedCustmerNumber), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualCustomerNumber+ "]");
				break;
			case customerordernumber_keyword:
				List <String> lstCaseChnagedActualList= new ArrayList<>();
				List<String> lstActualCustomerOrderNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCustomerOrderNumber);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				for(String sCurrentValue: lstActualCustomerOrderNumber) {
					String sChnagedCase=sCurrentValue.toUpperCase();
					lstCaseChnagedActualList.add(sChnagedCase);
				}

				List<String> lstExpectedCustmerOrderNumber = Util.getSortedListInRequiredOrder(lstCaseChnagedActualList, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstCaseChnagedActualList.equals(lstExpectedCustmerOrderNumber), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualCustomerOrderNumber+ "]");
				break;
			case invoicestatuscode_keyword:
				List<String> lstActualStatusCode = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathStatus);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedStatusCode = Util.getSortedListInRequiredOrder(lstActualStatusCode, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualStatusCode.equals(lstExpectedStatusCode), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualStatusCode+ "]");
				break;
			case invoicedate:
				List<String> lstActualInvoiceDate = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceDateRange);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedInvoiceDate = Util.getSortedListInRequiredOrder(lstActualInvoiceDate, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualInvoiceDate.equals(lstExpectedInvoiceDate), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualInvoiceDate+ "]");
				break;
			case duedate:
				List<String> lstActualDUeDate = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathDueDateRange);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedDueDate = Util.getSortedListInRequiredOrder(lstActualDUeDate, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualDUeDate.equals(lstExpectedDueDate), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualDUeDate+ "]");
				break;
			case invoicetype_keyword:
				List<String> lstActualType = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceType);
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				List<String> lstExpectedType = Util.getSortedListInRequiredOrder(lstActualType, sSortDirection);			
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lstActualType.equals(lstExpectedType), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualType+ "]");
				break;
			case totalamount:
				List<Object> lstActualTotalAmount = new ArrayList<Object>();
				List<Double> lActual = new ArrayList<Double>();
				List<? extends Double> lstExpectedTotalAmount= new ArrayList<Double>();

				lstActualTotalAmount =objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathTotalAmount);
				for (Object s: lstActualTotalAmount) { 
					if(s != null)
						lActual.add(Double.valueOf(s.toString()));
				}
				objAPIAssertion.setExtentInfo("Actual Values Picked");
				lstExpectedTotalAmount= Util.getSortedDoubleListInRequiredOrder(lActual, sSortDirection);		
				objAPIAssertion.setExtentInfo("Expected values sorted");
				objAPIAssertion.assertTrue(lActual.equals(lstExpectedTotalAmount), "Validation of Response for Sorting based on "+sSortBy+" in "+sSortDirection+" order : Actual: ["+lstActualTotalAmount+ "]");
				break;
			default:
				break;

			}
		}catch(Exception e) {
			String sErrMessage="FAIL: performSortValidation() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			System.gc();
		}

	}



	private void perfromValidationForDueDaterange(JsonUtil objJSONUtil) throws Exception {
		try {

			HashMap<String, String>hmRequestMap =objConfig.gethmTestData();
			String dExpectedToDate=hmRequestMap.get(InvoiceSearchAPIConstants.DueDateRange).substring(26, 36);
			String dExpectedFromDate=hmRequestMap.get(InvoiceSearchAPIConstants.DueDateRange).substring(8, 18);
			objAPIAssertion.setExtentInfo("Expected Due Dates Picked is from: "+dExpectedFromDate+" to: "+dExpectedToDate);
			//System.out.println(dExpectedFromDate+" to "+dExpectedToDate);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualDate = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathDueDateRange);
			objAPIAssertion.assertFalse(lstActualDate.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= DateTimeUtil.verifyGivenDateFallsBetween2DatesInMentionedFormat(lstActualDate, dExpectedFromDate, dExpectedToDate,"yyyy-MM-dd");
			objAPIAssertion.assertTrue(bCheck,  "Validation of Due Date Expected ["+dExpectedFromDate +" to "+dExpectedToDate+"] Actual ["+lstActualDate+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: perfromValidationForDueDaterange() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForInvoiceDate(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String>hmRequestMap =objConfig.gethmTestData();
			String sExpectedToDate=hmRequestMap.get(InvoiceSearchAPIConstants.InvoiceDateRange).substring(26, 36);
			String sExpectedFromDate=hmRequestMap.get(InvoiceSearchAPIConstants.InvoiceDateRange).substring(8, 18);
			objAPIAssertion.setExtentInfo("Expected Invoice Date range Picked is from: "+sExpectedFromDate+" to: "+sExpectedToDate);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualInvoiceDate = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceDateRange);
			objAPIAssertion.assertFalse(lstActualInvoiceDate.size()==0,"List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= DateTimeUtil.verifyGivenDateFallsBetween2DatesInMentionedFormat(lstActualInvoiceDate, sExpectedFromDate, sExpectedToDate,"yyyy-MM-dd");
			objAPIAssertion.assertTrue(bCheck,  "Validation of Invoice Date Expected ["+sExpectedFromDate +" to "+sExpectedToDate+"] Actual ["+lstActualInvoiceDate+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForInvoiceDate() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForStatus(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedStatus= hmRequestMap.get(InvoiceSearchAPIConstants.STATUS).replace("\"", "");
			objAPIAssertion.setExtentInfo("Expected Invoice Status Code is: "+sExpectedStatus);


			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualStatus = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathStatus);		
			objAPIAssertion.assertFalse(lstActualStatus.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListValuesWithExpectedValue(lstActualStatus, sExpectedStatus);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Status Expected ["+sExpectedStatus +"] Actual ["+lstActualStatus+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForStatus() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForTypeInvoice(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedType= hmRequestMap.get(InvoiceSearchAPIConstants.INVOICETYPE);
			objAPIAssertion.setExtentInfo("Expected Invoice Type is "+sExpectedType);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualTYPE = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceType);
			objAPIAssertion.assertFalse(lstActualTYPE.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListOfValuesWithExpectedValueMoreThanOne(lstActualTYPE, sExpectedType);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Type Expected ["+sExpectedType +"] Actual ["+lstActualTYPE+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForTypeInvoice() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}}



	private void performValidationForBidNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedBid= hmRequestMap.get(InvoiceSearchAPIConstants.Bid);
			objAPIAssertion.setExtentInfo("Expected Bid Number is: "+sExpectedBid);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualBid = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathBid);
			objAPIAssertion.assertFalse(lstActualBid.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListOfValuesWithExpectedValueMoreThanOne(lstActualBid, sExpectedBid);
			objAPIAssertion.assertTrue(bCheck,  "Validation of Bid Expected ["+sExpectedBid +"] Actual ["+lstActualBid+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForBidNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForCPN(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedCPN= hmRequestMap.get(InvoiceSearchAPIConstants.CPN);
			objAPIAssertion.setExtentInfo("Expected CPN is "+sExpectedCPN);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualCPN = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCPN);
			objAPIAssertion.assertFalse(lstActualCPN.size()==0, "List isn't empty");
			//System.out.println("CPN is: "+ lstActualCPN);

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListOfValuesWithExpectedValueMoreThanOne(lstActualCPN, sExpectedCPN);
			objAPIAssertion.assertTrue(bCheck,  "Validation of CPN Expected ["+sExpectedCPN +"] Actual ["+lstActualCPN+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForCPN() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}



	private void performValidationForCustomerOrderNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedCustomerOrderNumber = hmRequestMap.get(InvoiceSearchAPIConstants.Customer_Order_Number);
			objAPIAssertion.setExtentInfo("Expected Reseller PO# is "+sExpectedCustomerOrderNumber);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualCustomnerOrderNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCustomerOrderNumber);
			objAPIAssertion.assertFalse(lstActualCustomnerOrderNumber.size()==0, "List isn't empty");//System.out.println("ResellerPO  was " +lstActualCustomnerOrderNumber);

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualCustomnerOrderNumber, sExpectedCustomerOrderNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Reseller PO Number Expected ["+sExpectedCustomerOrderNumber+"] Actual"+lstActualCustomnerOrderNumber);

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForCustomerOrderNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForResellerNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedReseller = hmRequestMap.get(InvoiceSearchAPIConstants.ResellerNumber);
			objAPIAssertion.setExtentInfo("Expected Reseller Number is "+sExpectedReseller );

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualResellers = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathResellerNumber);
			objAPIAssertion.assertFalse(lstActualResellers.size()==0, "List isn't empty");//System.out.println("Reseller was " +lstActualResellers);

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualResellers, sExpectedReseller);
			objAPIAssertion.assertTrue(bCheck, "Validation of reseller Number Expected ["+sExpectedReseller+"] Actual"+lstActualResellers);

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForResellerNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForVendorPartNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedVPN = hmRequestMap.get(InvoiceSearchAPIConstants.VendorPartNumber);
			objAPIAssertion.setExtentInfo("Expected VPN is "+sExpectedVPN);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualVPN = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathVPN);
			objAPIAssertion.assertFalse(lstActualVPN.size()==0, "List isn't empty");

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualVPN, sExpectedVPN);
			objAPIAssertion.assertTrue(bCheck, "Validation of Vendor Part Number Expected ["+sExpectedVPN+"] Actual"+lstActualVPN+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForVendorPartNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}





	private void performValidationForSerialNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedSerialNumber= hmRequestMap.get(InvoiceSearchAPIConstants.SerialNumber);
			objAPIAssertion.setExtentInfo("Expected Serial Number is "+sExpectedSerialNumber);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualSerialNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathSerialNumber);
			objAPIAssertion.assertFalse(lstActualSerialNumber.size()==0, "List isn't empty");
			//System.out.println("Serial Number is: "+ lstActualSerialNumber);

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck= Util.compareListOfValuesWithExpectedValueMoreThanOne(lstActualSerialNumber, sExpectedSerialNumber);
			objAPIAssertion.assertTrue(bCheck,  "Validation of serial Number Expected ["+sExpectedSerialNumber +"] Actual ["+lstActualSerialNumber+" ]");
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForSerialNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForPageSize(JsonUtil objJSONUtil) throws Exception {
		try {
			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List<String> lstActualInvoices = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceNumber);
			int iCountInvoiceCount=lstActualInvoices.size();
			objAPIAssertion.assertFalse(iCountInvoiceCount==0, "List isn't empty");

			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			int iExpectedPageSize= Integer.parseInt(hmRequestMap.get(InvoiceSearchAPIConstants.PAGESIZE));

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bResult=false;	
			if(iExpectedPageSize==iCountInvoiceCount)
				bResult=true;

			objAPIAssertion.assertTrue(bResult, "Validation of Order Number Expected ["+iExpectedPageSize +"] Actual ["+iCountInvoiceCount+" ]");
		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForOrderNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



	private void performValidationForInvoiceNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedInvoiceNumber = hmRequestMap.get(InvoiceSearchAPIConstants.InvoiceNumber);
			objAPIAssertion.setExtentInfo("Expected Invoice# is "+sExpectedInvoiceNumber);

			objAPIAssertion.setExtentInfo("Picking the values from response");
			List<String> lstActualInvoiceNumber = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceNumber);
			objAPIAssertion.assertFalse(lstActualInvoiceNumber.size()==0, "List isn't empty");//System.out.println("InvoiceNumber was " +lstActualInvoiceNumber);

			objAPIAssertion.setExtentInfo("Comparing the values from Actual to expected");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualInvoiceNumber, sExpectedInvoiceNumber);
			objAPIAssertion.assertTrue(bCheck, "Validation of Invoice Number Expected ["+sExpectedInvoiceNumber+"] Actual"+lstActualInvoiceNumber+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForInvoiceNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}
	private void performValidationForIngramPartNumber(JsonUtil objJSONUtil) throws Exception {
		try {
			HashMap<String, String> hmRequestMap =objConfig.gethmTestData();
			String sExpectedSKU = hmRequestMap.get(InvoiceSearchAPIConstants.IngramPartNumber);
			objAPIAssertion.setExtentInfo("Expected SKU# is "+sExpectedSKU);

			objAPIAssertion.setExtentInfo("Picking the values from the Response to a List in String format in Validation method");
			List <String> lstActualSKU = objJSONUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathSKU);
			objAPIAssertion.assertFalse(lstActualSKU.size()==0, "List isn't empty");//String sActualSKU = lstActualSKU.toString().replace("[", "").replace("]", "").trim();

			objAPIAssertion.setExtentInfo("Comparing the Expected from the actual");
			boolean bCheck = Util.compareListValuesWithExpectedValue(lstActualSKU, sExpectedSKU);
			objAPIAssertion.assertTrue(bCheck, "Validation of Ingram Part Number Expected ["+sExpectedSKU+"] Actual"+lstActualSKU+"");

		}catch (Exception e) {
			String sErrMessage="FAIL: performValidationForIngramPartNumber() method of InvoiceSearchAPIValidation: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

	}



}
