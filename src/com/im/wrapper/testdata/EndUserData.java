package com.im.wrapper.testdata;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;
import com.im.api.core.utility.ReadExcelFile;

public class EndUserData {
	static Logger logger = Logger.getLogger("ShipToAddressData");
	static HashMap<String, HashMap<String, String>> hmEndUserOfCountries = new HashMap<String, HashMap<String, String>>(); 
	private static String psWorkBookName="EnduserInfo";
	private static String TestDataFile="TestConfigDataForHermes.xlsx";
	final static String COUNTRY="COUNTRY";
	public static final String EXCEL_TestDataMisc=Constants.BASEPATH+"\\TestData\\"+TestDataFile;
	//public static final String EXCEL_TestDataMisc=Constants.BASEPATH+RunConfig.getProperty(Constants.EXCELNAME_MISCORDERDATA).trim();
	
	private static final String EXCEL_COUNTRY="COUNTRY", EXCEL_NAME1="name1",
			EXCEL_NAME2="name2", EXCEL_NAME3="name3",EXCEL_PHONENUMBER="phonenumber",
					EXCEL_EMAIL="email",EXCEL_ADDR_LINE_1="addressline1", EXCEL_COUNTRY_CODE="countrycode",
					EXCEL_CITY="city", EXCEL_STATE="state", EXCEL_POSTCODE="postalcode";
	
				


	static {
			ReadExcelFile readExcel = new ReadExcelFile();
			List<HashMap<String, String>> listTestEntry=null;
			try {
				listTestEntry = readExcel.readExcelDataToListofHashMap(EXCEL_TestDataMisc, psWorkBookName);
				
				for(int i=0;i<listTestEntry.size();i++) {
					HashMap<String, String> countryRow=listTestEntry.get(i);
					String sKey = countryRow.get(COUNTRY);
					hmEndUserOfCountries.put(sKey, countryRow);					
				}
				
			} catch (Exception e) {
				logger.error("The excel to read the possible countrywise ship tp address is not available.["+e.getMessage()+"]");
				e.printStackTrace();
			}
	}	

	public static HashMap<String, String> getShippingAddressForCountry(String sCountryCode){
		HashMap<String, String> toReturn=null;
		toReturn=hmEndUserOfCountries.get(sCountryCode);
		return toReturn;
	}
	
	public static void main(String[] args) {
		String st = getShippingAddressForCountry("US").get("attention");
		System.out.println("rwerewrwe["+st+"]");
	}

	public static HashMap<String, String> getEnduserInfoForCountry(String countryCode) {
		HashMap<String, String> toReturn=null;
		toReturn=hmEndUserOfCountries.get(countryCode);
		return toReturn;
	}

}
