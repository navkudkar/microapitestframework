package com.im.wrapper.testdata;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;
import com.im.api.core.utility.ReadExcelFile;

public class CreditCardDetails {
	static Logger logger = Logger.getLogger("CreditCardDetails");
	static HashMap<String, HashMap<String, String>> hmCreditCardDetailsOfCountries = new HashMap<String, HashMap<String, String>>(); 
	private static String psWorkBookName="creditcarddetails";
	private static String TestDataFile="TestConfigDataForHermes.xlsx";
	final static String COUNTRY="COUNTRY";
	public static final String EXCEL_TestDataMisc=Constants.BASEPATH+"\\TestData\\"+TestDataFile;
	//public static final String EXCEL_TestDataMisc=Constants.BASEPATH+RunConfig.getProperty(Constants.EXCELNAME_MISCORDERDATA).trim();
	
	private static final String EXCEL_COUNTRY="COUNTRY",
			EXCEL_CREDITCARDNUMBER="creditcardnumber", EXCEL_PAYMENTCODE="paymentcode", EXCEL_CARDTYPE="cardtype",
				EXCEL_EXPIRATIONDATE="expirationdate", EXCEL_FIRSTNAME="firstname",EXCEL_LASTNAME="lastname",
					EXCEL_ADDRESS="address",EXCEL_CITY="city" ,EXCEL_STATE="state",
						EXCEL_POSTALCODE="postalcode", EXCEL_AUTHORIZATIONAMOUNT="authorizationamount";
			
	static {
			ReadExcelFile readExcel = new ReadExcelFile();
			List<HashMap<String, String>> listTestEntry=null;
			try {
				listTestEntry = readExcel.readExcelDataToListofHashMap(EXCEL_TestDataMisc, psWorkBookName);
				
				for(int i=0;i<listTestEntry.size();i++) {
					HashMap<String, String> countryRow=listTestEntry.get(i);
					String sKey = countryRow.get(COUNTRY);
					hmCreditCardDetailsOfCountries.put(sKey, countryRow);					
				}
				
			} catch (Exception e) {
				logger.error("The excel to read the possible countrywise ship tp address is not available.["+e.getMessage()+"]");
				e.printStackTrace();
			}
	}	

	public static HashMap<String, String> getCreditCardDetailsForCountry(String sCountryCode){
		HashMap<String, String> toReturn=null;
		toReturn=hmCreditCardDetailsOfCountries.get(sCountryCode);
		return toReturn;
	}
	
	public static void main(String[] args) {
		String st = getCreditCardDetailsForCountry("US").get("creditcardnumber");
		System.out.println("rwerewrwe["+st+"]");
	}

}
