package com.im.imonline.api.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.business.APIEnumerations.CarrierOptions;
import com.im.imonline.api.business.APIEnumerations.CheckOutOptions;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.IntermediateResults;
import com.im.imonline.api.business.APIEnumerations.OrderProcessOptionsForHeldOrder;
import com.im.imonline.api.business.APIEnumerations.PaymentTypes;
import com.im.imonline.api.business.APIEnumerations.WarehouseTypes;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;
import com.im.wrapper.testdata.CreditCardDetails;
import com.im.wrapper.testdata.EndUserData;
import com.im.wrapper.testdata.ShipToAddressData;

import net.minidev.json.JSONArray;

public class HermesE2EFlowRequestConfig {
	Logger logger = Logger.getLogger("HermesE2EFlowRequestConfig"); 
	private E2EHermesAPITestDataUtil objTestDataUtil;
	private HermesE2ERegister hermesOption;
	List<List<HashMap<String, String>>> ListMapMultipleSetOfSKUData= new ArrayList<List<HashMap<String,String>>>();
	List<HashMap<String,String>> mapMultipleSetOfSKUData = new ArrayList<HashMap<String,String>>();
	List<HashMap<String,String>> mapMultipleSetOfSKUData1 = new ArrayList<HashMap<String,String>>();
	List<HashMap<String,String>> mapMultipleSetOfSKUData2 = new ArrayList<HashMap<String,String>>();
	private HashMap<String, String> hmTestData=new HashMap<String, String>();
	private String sRequestBody;
	private boolean dataCaptured1=false;
	private boolean  dataCaptured =true;
	private Set<CheckOutOptions> keySet;
	private APIAssertion objAPIAssertion;
	private APIDriver objAPIDriverConf;

	public HermesE2EFlowRequestConfig(E2EHermesAPITestDataUtil objTestData, HermesE2ERegister hOption,APIDriver objAPIDriver) {
		objTestDataUtil=objTestData;
		hermesOption=hOption;
		hmTestData.putAll(objTestDataUtil.getRequestData());
		keySet = hermesOption.getKeySetForConfigurations();
		objAPIDriverConf=objAPIDriver;
		objAPIAssertion = ToolAPI.getAPIAssertion(objAPIDriver);
	}

	
	//sets the request hashmap for P&A request
	public void setPnAReqParameters() {
		try {
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesPnA");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_PNA_QA));
			EnumMap<FlagOptions_Hermes, String> skuTypeMap=objTestDataUtil.getMapSKUDataOfDifferentFlags();

			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				HashMap<String,String> skuMap= new HashMap<>();
				if(skuTypeMap.get(oOU.getProdcode())==null)
				{
					if(hmTestData.get(oOU.getProdcode().toString())!=null && !hmTestData.get(oOU.getProdcode().toString()).equals(Constants.BLANKVALUE))							
						if(oOU.getProdcode().toString().contains(":"))
							oOU.setGlobalskuid((hmTestData.get(oOU.getProdcode().toString()).split(":")[0]));
						else 
							oOU.setGlobalskuid(hmTestData.get(oOU.getProdcode().toString()));

					if(oOU.getGlobalskuid()!=null)
					{
						dataCaptured1=true;
						oOU.setSKU(oOU.getGlobalskuid().split("-")[1]);
						skuMap.put(HermesAPIConstants.GLOBALDKUID,oOU.getGlobalskuid());
					}
					else
						dataCaptured1=false;
					mapMultipleSetOfSKUData.add(skuMap);
				}
				else
				{
					if(null!=skuTypeMap.get(oOU.getProdcode()))
					{
						dataCaptured1=true;
						oOU.setSKU(skuTypeMap.get(oOU.getProdcode()));
						skuMap.put(HermesAPIConstants.GLOBALDKUID,objTestDataUtil.getMapContentsOfProductSummaryData().get(oOU.getSKU()).get(HermesAPIConstants.GLOBALMATERIAL));
						mapMultipleSetOfSKUData.add(skuMap);
					}
					else 
						dataCaptured1=false;
				}				
				dataCaptured=dataCaptured && dataCaptured1;
				}
				ListMapMultipleSetOfSKUData.add(mapMultipleSetOfSKUData);
			} catch (Exception e) {
			String sErrMessage="FAIL: setPnAReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	//sets the request hashmap for OrderSimulate request
	public void setOrderSimulateReqParameters(JsonUtil objJSONUtil) {
		try {
			ListMapMultipleSetOfSKUData.clear();
			mapMultipleSetOfSKUData.clear();
			int lineNo=0;
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderSimulate3");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERQUOTE_QA));
			hmTestData.put(HermesAPIConstants.CUSTOMERPONUMBER,createRandomOrderNo());
			objTestDataUtil.setIntermediateTestData(IntermediateResults.CUSTOMERPONUMBER,hmTestData.get(HermesAPIConstants.CUSTOMERPONUMBER));
			hmTestData.put(HermesAPIConstants.SERVICESPECSSTRUCTURE,"");
			hmTestData.put(HermesAPIConstants.ORDERSIMULATEEXTENDEDSPECS,"");
			addShiptoAndBillToInfoToReqMap(hermesOption);
			if(!objTestDataUtil.getCountryGroups().isSAP())
				hmTestData.put(HermesAPIConstants.ORDERSIMULATEEXTENDEDSPECS,"{\"attributename\": \"regioncode\",\"attributevalue\": \""
						+ hmTestData.get(HermesAPIConstants.REGIONCODE)+ "\"}");
			else
				hmTestData.put(HermesAPIConstants.ORDERSIMULATEEXTENDEDSPECS,"{\"attributename\": \"shippingcondition\",\"attributevalue\": \"40\"},{\"attributename\": \"shipmentcompleteflag\",\"attributevalue\": \"false\"}");
			
			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				lineNo++;
				String sSKU=oOU.getSKU().replaceFirst("^0+(?!$)", "");
				String Xpathvalue ="$..details[?(@.ingrampartnumber=='"+sSKU+"')]";
				List<Object> lstDetailsResponseData=objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
				HashMap<String, Object> details=(HashMap<String, Object>) lstDetailsResponseData.get(0);
				oOU.setLineNumber("00"+String.valueOf(lineNo));
				setOrderUnitParametersForOrderSimulate(details,oOU);

				HashMap<String,String> skuMap= new HashMap<>();
				skuMap.put(HermesAPIConstants.LINENUMBER, oOU.getLineNumber());
				skuMap.put(HermesAPIConstants.UNITPRICE,oOU.getUnitPrice());
				skuMap.put(HermesAPIConstants.WAREHOUSEID,oOU.getWarehouse());
				skuMap.put(HermesAPIConstants.GLOBALDKUID,oOU.getGlobalskuid());
				skuMap.put(HermesAPIConstants.VENDORPARTNUMBER,oOU.getVpn());
				skuMap.put(HermesAPIConstants.QUANTITY,String.valueOf(oOU.getQuantity()));

				switch(oOU.getProdcode())
				{
				case INSTOCK:
				case NEW:
				case REFURBISHED:
				case NORETURNS:
				case FREEITEM:
				case ACOP_PROMOTION:
				case PROMOTION:
				case BUNDLE:
				case HEAVYWEIGHT:
				case DISCONTINUED:
				case JTYPE:
					mapMultipleSetOfSKUData1.add(skuMap);
					break;
				case DIRECTSHIP:
				case DIRECTSHIP_2:
				case DOWNLOADABLE:
				case ENDUSER:
				case WARRANTY:
				case LICENCEPRODUCTS:
					if(!objTestDataUtil.getCountryGroups().isSAP())
						mapMultipleSetOfSKUData2.add(skuMap);
					else
						mapMultipleSetOfSKUData1.add(skuMap);
					break;
				default:
					break;
				}				
			}
			ListMapMultipleSetOfSKUData.add(mapMultipleSetOfSKUData1);
			ListMapMultipleSetOfSKUData.add(mapMultipleSetOfSKUData2);
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderSimulateReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}		
	}


	//sets the Order Unit with different fields from P&A Response 
	//takes details hashmap as parameter
	private void setOrderUnitParametersForOrderSimulate(HashMap<String, Object> details, OrderUnit oOU) throws Exception {
		try {
			int lineNo=Integer.parseInt(oOU.getLineNumber());
			JSONArray warehouseInfo = (JSONArray) details.get(HermesAPIConstants.WAREHOUSEDETAILS);
			HashMap mapIthWarehouse=null;			
			
			oOU.setGlobalLineNumber(oOU.getLineNumber());
			if(details.get(HermesAPIConstants.CUSTOMERPRICE)!=null)
				oOU.setUnitPrice(String.valueOf(details.get(HermesAPIConstants.CUSTOMERPRICE)));
			oOU.setVendorCode((String) details.get(HermesAPIConstants.VENDORNUMBER));
			oOU.setVpn((String)details.get((HermesAPIConstants.MANFACTURERPARTNUMBER)));
			
			if(hermesOption.getWarehouseType().equals(WarehouseTypes.SPLIT.toString()) && (HashMap<?, ?>)warehouseInfo.get(lineNo-1)!=null)
				mapIthWarehouse = (HashMap<?, ?>)warehouseInfo.get(lineNo-1);
			else if (hermesOption.getWarehouseType().equals(WarehouseTypes.SPLIT.toString()) && (HashMap<?, ?>)warehouseInfo.get(lineNo-1)==null)
			{
				dataCaptured=false;
				objAPIAssertion.setExtentSkip("Data not captured for Split Warehouse flow");
			}
			else
				mapIthWarehouse = (HashMap<?, ?>)warehouseInfo.get(0);			
			oOU.setWarehouse((String) mapIthWarehouse.get(HermesAPIConstants.WAREHOUSEID));

			
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderUnitParametersForOrderSimulate() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	//used to fill shipTo and BillTo Information in all the Order Simulate/create/modify requests
	private void addShiptoAndBillToInfoToReqMap(HermesE2ERegister hOption) {
		try {
			switch(hOption.getShippingAddressType())
			{
			case "SELECT_EXISTING_BOTH":
				hmTestData.put(HermesAPIConstants.SHIPTOADDRESS,"\"\"");
				hmTestData.put(HermesAPIConstants.BILLTOADDRESS,"\"\"");
				break;

			case "SELECT_DIFFERENT_SHIPTO":
				hmTestData.put(HermesAPIConstants.SHIPTOSUFFIX,"");
				if(!objTestDataUtil.getCountryGroups().isSAP())
					hmTestData.put(HermesAPIConstants.SHIPTOADDRESS,getShipToAddressStructureForCountry(hmTestData.get("RunConfig")));
				else
					hmTestData.put(HermesAPIConstants.SHIPTOADDRESS,getEnduserStructure(hmTestData.get("RunConfig")));
				hmTestData.put(HermesAPIConstants.BILLTOADDRESS,"\"\"");
				break;

			case "SELECT_DIFFERENT_BILLTO":
				hmTestData.put(HermesAPIConstants.SHIPTOADDRESS,"\"\"");
				hmTestData.put(HermesAPIConstants.BILLTOSUFFIX,"");
				if(!objTestDataUtil.getCountryGroups().isSAP())
					hmTestData.put(HermesAPIConstants.BILLTOADDRESS,getShipToAddressStructureForCountry(hmTestData.get("RunConfig")));
				else
					hmTestData.put(HermesAPIConstants.BILLTOADDRESS,getEnduserStructure(hmTestData.get("RunConfig")));
				
				break;
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: addShiptoAndBillToInfoToReqMap() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}


	private String getShipToAddressStructureForCountry(String countryCode) throws Exception {
		String shipToAddr=new String();
		HashMap<String, String> hmShipTo=ShipToAddressData.getShippingAddressForCountry(countryCode);
		hmShipTo.remove("COUNTRY");
		hmShipTo.values().removeIf(value -> value.contains("<<BLANK>>"));
		shipToAddr=AppUtil.getJSONStringForMap(hmShipTo);
		return shipToAddr;
	}

	private String createRandomOrderNo() throws Exception{
		String sTimeStamp=null,sRandomNo=null;
		sTimeStamp = new SimpleDateFormat("MMddHHmmss").format(Calendar.getInstance().getTime());
		sRandomNo = String.valueOf(Util.getRandomNumberInRange(1, 100));		
		return hmTestData.get(HermesAPIConstants.ISOCOUNTRYCODE)+sTimeStamp+sRandomNo;
	}
	
	private void setServiceSpecData(OrderUnit oOU) {
		try {
			switch(oOU.getProdcode())
			{
			case WARRANTY:
				//this structure needs to be hardcoded based on the product and Vendor
				hmTestData.put(HermesAPIConstants.SERVICESPECSSTRUCTURE, "\"serviceextendedspecs\":[{\"attributename\":\"Date|Serial\",\"attributevalue\":\"Y~"
						+ Util.getTodaysDate()
						+ "|"
						+ hmTestData.get(oOU.getProdcode().toString()).split(":")[1]
						+ "||||\"},"
						+ "{\"attributename\":\"lineverified\",\"attributevalue\":\"Y\"}]}]");
				break;
			default:
				hmTestData.put(HermesAPIConstants.VMF,"");
				break;
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: setVMFData() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}


	public void setOrderCreateReqParameters(JsonUtil objJSONUtil) {
		try {
			ListMapMultipleSetOfSKUData.clear();
			mapMultipleSetOfSKUData.clear();
			mapMultipleSetOfSKUData1.clear();
			mapMultipleSetOfSKUData2.clear();
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderCreate3");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERCREATE_QA));
			hmTestData.put(HermesAPIConstants.VMF,"");
			hmTestData.put(HermesAPIConstants.HOLDORDERATTRIBUTE,"");
			setOrderLevelParametersAsPerGivenConfig();

			for(OrderUnit oOU : hermesOption.getListOfProducts())
			{
				HashMap<String,String> skuMap= new HashMap<>();
				skuMap.put(HermesAPIConstants.ENDUSER,"");
				skuMap.put(HermesAPIConstants.LINENUMBER,oOU.getLineNumber());
				skuMap.put(HermesAPIConstants.GLOBALDKUID,oOU.getGlobalskuid());
				skuMap.put(HermesAPIConstants.QUANTITY,String.valueOf(oOU.getQuantity()));
				skuMap.put(HermesAPIConstants.VENDORPARTNUMBER,oOU.getVpn());
				skuMap.put(HermesAPIConstants.WAREHOUSEID,oOU.getWarehouse());				
				if(!objTestDataUtil.getCountryGroups().isSAP())
					setOrderUnitParametersForOrderCreate(objJSONUtil, oOU);
				skuMap.put(HermesAPIConstants.CARRIERCODE,oOU.getCarriercode());

				switch(oOU.getProdcode())
				{
				case INSTOCK:
				case NEW:
				case REFURBISHED:
				case NORETURNS:
				case FREEITEM:
				case ACOP_PROMOTION:
				case PROMOTION:
				case BUNDLE:
				case HEAVYWEIGHT:
				case DISCONTINUED:
					mapMultipleSetOfSKUData1.add(skuMap);
					break;				
				case DIRECTSHIP_2:					
				case DIRECTSHIP:
				case DOWNLOADABLE:
				case ENDUSER:
				case LICENCEPRODUCTS:					
					setVMFData(oOU);
					if(!objTestDataUtil.getCountryGroups().isSAP())
						mapMultipleSetOfSKUData2.add(skuMap);
					else
					{
						skuMap.put(HermesAPIConstants.ENDUSER,"\"enduser\": "+ getEnduserStructure(hmTestData.get("RunConfig")));
						mapMultipleSetOfSKUData1.add(skuMap);
					}
					break;
				default:
					break;
				}	
				if(!hermesOption.getLineNotes().equals(""))
				{
					if(!objTestDataUtil.getCountryGroups().isSAP())
						skuMap.put(HermesAPIConstants.LINENOTES, "{\"attributename\": \"commenttext\",\"attributevalue\": \""
								+ hermesOption.getLineNotes()
								+ "\"}");
					else
						skuMap.put(HermesAPIConstants.LINENOTES, "{\"attributename\": \"commenttext\",\"attributevalue\": \"Z022-"
								+ hermesOption.getLineNotes()
								+ "\"}");
				}
				else
					skuMap.put(HermesAPIConstants.LINENOTES, "");
			}
			ListMapMultipleSetOfSKUData.add(0, mapMultipleSetOfSKUData1);
			ListMapMultipleSetOfSKUData.add(1, mapMultipleSetOfSKUData2);	
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderCreateReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}


	private String getEnduserStructure(String countryCode) throws Exception {
		String endUserInfo=new String();
		HashMap<String, String> hmShipTo=EndUserData.getEnduserInfoForCountry(countryCode);
		hmShipTo.remove("COUNTRY");
		hmShipTo.values().removeIf(value -> value.contains("<<BLANK>>"));
		endUserInfo=AppUtil.getJSONStringForMap(hmShipTo);
		return endUserInfo;
	}


	private void setOrderUnitParametersForOrderCreate(JsonUtil objJSONUtil, OrderUnit oOU) throws Exception {
		try {
			String Xpathvalue="$..carrierlist";
			JSONArray carrierInfo = (JSONArray) objJSONUtil.getResponseFromJayway(objJSONUtil,Xpathvalue);
			if(!((List<Object>) carrierInfo.get(0)).isEmpty())
			{
				HashMap mapIthCarrier;
				if(((List<Object>) carrierInfo.get(0)).size()>1  && hermesOption.getDeliveryService().equals(CarrierOptions.DIFFCHOICE.toString()))
					mapIthCarrier=(HashMap<?, ?>)(((List) (carrierInfo.get(0))).get(1));
				else 
					mapIthCarrier=(HashMap<?, ?>)(((List) (carrierInfo.get(0))).get(0));
				oOU.setCarriercode((String) mapIthCarrier.get(HermesAPIConstants.CARRIERCODE));
			}
			else if(((List<Object>) carrierInfo.get(0)).isEmpty() && hermesOption.getDeliveryService().equals(CarrierOptions.DIFFCHOICE.toString()))
			{
				dataCaptured=false;
				objAPIAssertion.setExtentSkip("Data not available for non default delivery service");
			}
			else
				oOU.setCarriercode((String)objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderquoteresponse.orderdistribution[0].carriercode"));
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderUnitParametersForOrderCreate() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	private void setOrderLevelParametersAsPerGivenConfig() throws Exception {		
		try {
			//TODO: VL :02/03/21 - create a utility method for populating the below structure.
			if(!objTestDataUtil.getCountryGroups().isSAP())
				{
				hmTestData.put(HermesAPIConstants.ORDERCREATEEXTENDEDSPECS,"{\"attributename\": \"regioncode\",\"attributevalue\": \""
						+ hmTestData.get(HermesAPIConstants.REGIONCODE)
						+ "\"},"
						+ "{\"attributename\": \"continueonerror\",\"attributevalue\": \"N\"},"
						+ "{\"attributename\":\"duplicatecustomerordernumbervalidate\",\"attributevalue\": \"ALLOW\"},"
						+ "{\"attributename\": \"commenttext\",\"attributevalue\":\"RC-FN:;LN:;Email: vinita.lakhwani@ingrammicro.com; PH: 5554167664\"},"
						+ "{\"attributename\": \"resellerctacemail\",\"attributevalue\": \"vinita.lakhwani@ingrammicro.com\"}");
				hmTestData.put(HermesAPIConstants.PRODUCTEXTENDEDSPECS,"{\"attributename\": \"gsaflag\",\"attributevalue\": \"Y\"}");
				}
			else
			{
				String excludeOrDetBoolean;
				if(hmTestData.get("RunConfig").matches("CO|PE|CL"))
					excludeOrDetBoolean="false";
				else
					excludeOrDetBoolean="true";
				
				hmTestData.put(HermesAPIConstants.ORDERCREATEEXTENDEDSPECS,"{\"attributename\":\"excludeorderdetails\",\"attributevalue\":\""
						+ excludeOrDetBoolean+ "\"},"+ "{\"attributename\":\"entrymethod\",\"attributevalue\":\"ZWEB\"},"
						+ "{\"attributename\":\"requesteddeliverydate\",\"attributevalue\":\""
						+ Util.getTodaysDateISOFormat()+ "\"},"
						+ "{\"attributename\":\"shipmentcompleteflag\",\"attributevalue\":\"False\"},"
						+ "{\"attributename\":\"shipnotes\",\"attributevalue\":\"40\"},"
						+ "{\"attributename\":\"requestedshippingcondition\",\"attributevalue\":\"40\"},"
						+ "{\"attributename\":\"commenttext\",\"attributevalue\":\"Z025-IN-Endeavour.Auto@ingrammicro.com\"},"
						+ "{\"attributename\":\"continueonerror\",\"attributevalue\":\"Y\"},"
						+ "{\"attributename\":\"duplicatecustomerordernumbervalidate\",\"attributevalue\":\"ALLOW\"},"
						+ "{\"attributename\":\"resellerctacemail\",\"attributevalue\":\"IN-Endeavour.Auto@ingrammicro.com\"}");
			
				hmTestData.put(HermesAPIConstants.PRODUCTEXTENDEDSPECS,"{\"attributename\": \"promisedate\",\"attributevalue\": \""
						+ Util.getTodaysDateISOFormat()+ "\"}");
			}
			for(CheckOutOptions option:keySet ) {
				switch(option) {
				case BASKET_NOTE: 
					hmTestData.put(HermesAPIConstants.BASKETNOTES,"");
					if(!hermesOption.getBasketNotes().equals(""))
						if(!objTestDataUtil.getCountryGroups().isSAP())
							hmTestData.put(HermesAPIConstants.BASKETNOTES,"{\"attributename\": \"commenttext\",\"attributevalue\": \"////"
								+ hermesOption.getBasketNotes()
								+ "\"}");
						else
							hmTestData.put(HermesAPIConstants.BASKETNOTES,"{\"attributename\": \"commenttext\",\"attributevalue\": \"Z022-"
									+ hermesOption.getBasketNotes()
									+ "\"}");
					break;
				case SHIPTO_BILLTO_ADDRESS: 
					//addShiptoAndBillToInfoToReqMap(hermesOption);
					break;
				case LINE_NOTES: 
					//handled at orderunit level
					break;
				case DELIVERY_SERVICE_TYPE: 
					//handled at orderunit level
					break;
				case ENDUSER_PONUMBER: 
					hmTestData.put(HermesAPIConstants.ENDUSERORDERNUMBER,"");
					hmTestData.put(HermesAPIConstants.ENDUSERPO,"");					
					if(!hermesOption.getEUPONumber().equals(""))
					{
						hmTestData.put(HermesAPIConstants.ENDUSERPO,"{\"attributename\": \"euponumber\",\"attributevalue\": \""
								+ hermesOption.getEUPONumber()
								+ "\"}");
						hmTestData.put(HermesAPIConstants.ENDUSERORDERNUMBER,hermesOption.getEUPONumber());
					}
					break;
				case PAYMENT_TYPE: 
					if(hermesOption.getPaymentType().equals(PaymentTypes.CREDITCARD.toString()))
						hmTestData.put(HermesAPIConstants.CREDITCARDDETAILS,setCreditCardDetails(hmTestData.get("RunConfig")));
					else
						hmTestData.put(HermesAPIConstants.CREDITCARDDETAILS,"\"\"");
					break;
				case WAREHOUSE_TYPE: 
					//handled at orderunit level
					break;
				case HOLD_ORDER_TYPE: 
					if(!hermesOption.getScenarioForHoldOrder().equals(OrderProcessOptionsForHeldOrder.NONE.toString()))
						hmTestData.put(HermesAPIConstants.HOLDORDERATTRIBUTE,"{\"attributename\": \"placeoncustomerhold\",\"attributevalue\": \"true\"}");
					else
						hmTestData.put(HermesAPIConstants.HOLDORDERATTRIBUTE,"");
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderLevelParametersAsPerGivenConfig() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}		
	}

	private String setCreditCardDetails(String countryCode) throws Exception {
		String ccDetails=new String();
		HashMap<String, String> hmCcDetails=CreditCardDetails.getCreditCardDetailsForCountry(countryCode);
		hmCcDetails.remove("COUNTRY");
		ccDetails=AppUtil.getJSONStringForMap(hmCcDetails);
		return ccDetails;
	}

	private void setVMFData(OrderUnit oOU) {
		try {
			switch(oOU.getProdcode())
			{
			case DIRECTSHIP_2:
				//this structure needs to be hardcoded based on the product and Vendor
				hmTestData.put(HermesAPIConstants.VMF, "{\"csnumber\": \"CSN123\",\"vendornumber\": \""
						+ oOU.getVendorCode()
						+ "\","
						+ "\"veud\": {\"conagrflag\": \"A\",\"id\": \"232325444\",\"agreementid\": \"1234570708\",\"contactid\": \"232326441\",\"addressid\": \"91854675\",\"licenseid\": \"\"},"
						+ "\"vmfspecs\": [{\"attributename\": \"commenttext\",\r\n" + 
						"\"attributevalue\": \"HEADER\"},{\"attributename\": \"lineextracommenttext\",\"attributevalue\": \"CS:CSN123\"}]}");
				break;
			default:
				hmTestData.put(HermesAPIConstants.VMF,"");
				break;
			}
		} catch (Exception e) {
			String sErrMessage="FAIL: setVMFData() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	
	//sets request hashmap for OrderDetails Call
	public void setOrderDetailsReqParameters(HashMap<String, Object> lines, JsonUtil objJSONUtil2) throws Exception {
		try {
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderDetails");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERDETAILS_QA));
			if(objTestDataUtil.getCountryGroups().isSAP())
				hmTestData.put(HermesAPIConstants.ORDERNUMBER, objJSONUtil2.getActualValueFromJSONResponseWithoutModify(HermesAPIConstants.XPATHINVOICINGSYSTEMORDERID));
			else
				hmTestData.put(HermesAPIConstants.ORDERNUMBER, (String) lines.get(HermesAPIConstants.ERPORDERNUMBER));
			hmTestData.put(HermesAPIConstants.ORDERBRANCHNUMBER, hmTestData.get(HermesAPIConstants.BRANCH));
			hmTestData.put(HermesAPIConstants.ORDERDATE, objJSONUtil2.getActualValueFromJSONResponseWithoutModify(HermesAPIConstants.XPATHORDERTIMESTAMP).substring(0, 10));
			hmTestData.put(HermesAPIConstants.ORDERTYPE, (String) lines.get(HermesAPIConstants.LINEORDERTYPE));
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderDetailsReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}	

	//sets request hashmap for OrderDelete Call
	public void setOrderDeleteReqParameters(JsonUtil objJSONUtilOrDet) {
		try {
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderDelete");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERDELETE_QA));
			hmTestData.put(HermesAPIConstants.ORDERNUMBERFORDELETE, objJSONUtilOrDet.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.ordernumber"));
			String ordrSuffix=objJSONUtilOrDet.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.lines[0].ordersuffix");
			hmTestData.put(HermesAPIConstants.DISTRIBUTIONNUMBER, ordrSuffix.substring(0,1));
			hmTestData.put(HermesAPIConstants.SHIPMENTNUMBER, ordrSuffix.substring(1,2));
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderDeleteReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}
	
	//sets request hashmap for OrderModify Call
	public void setOrderModifyReqParameters(JsonUtil objJSONUtilOrDet) {
		try {
			//multimap information is used from the create order parameters
			hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderModify");
			hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERMODIFY_QA));
			hmTestData.put(HermesAPIConstants.ORDERNUMBERFORDELETE, objJSONUtilOrDet.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.ordernumber"));
			String ordrSuffix=objJSONUtilOrDet.getActualValueFromJSONResponseWithoutModify("serviceresponse.orderdetailresponse.lines[0].ordersuffix");
			hmTestData.put(HermesAPIConstants.DISTRIBUTIONNUMBER, ordrSuffix.substring(0,1));
			hmTestData.put(HermesAPIConstants.SHIPMENTNUMBER, ordrSuffix.substring(1,2));
			addShiptoAndBillToInfoToReqMap(hermesOption);
			
		} catch (Exception e) {
			String sErrMessage="FAIL: setOrderModifyReqParameters() method of HermesE2EFlowRequestConfig: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
	}

	public void updateHashmapInfoForOrderSimulateRehit(JsonUtil objJSONUtilOrModify) throws Exception {
	hmTestData.put(HermesAPIConstants.APITEMPLATE, "HermesOrderSimulate3");
	hmTestData.put(HermesAPIConstants.ENDPOINTURL_QA, hmTestData.get(HermesAPIConstants.ENDPOINTURL_ORDERQUOTE_QA));
	hmTestData.put("unitprice",hermesOption.getListOfProducts()[0].getUnitPrice());
	hmTestData.put(HermesAPIConstants.ORDERNUMBER,objJSONUtilOrModify.getActualValueFromJSONResponseWithoutModify("serviceresponse.ordermodifyresponse.lines[0].erpordernumber"));
	}
	
	
	public HashMap<String, String> getHmTestData() {
		return hmTestData;
	}

	public List<List<HashMap<String, String>>> getListMapMultipleSetOfSKUData() {
		return ListMapMultipleSetOfSKUData;
	}

	public HermesE2ERegister getHermesRegisterForTestCase() {
		return hermesOption;
	}

	public String getRequestBody() {
		return sRequestBody;
	}

	public E2EHermesAPITestDataUtil getObjTestDataUtil() {
		return objTestDataUtil;
	}

	public boolean isDataCaptured() {
		return dataCaptured;
	}

	public APIDriver getObjAPIDriver() {
		return objAPIDriverConf;
	}
	
}
