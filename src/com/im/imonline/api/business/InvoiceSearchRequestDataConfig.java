package com.im.imonline.api.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.common.Util;
import com.im.imonline.api.business.APIEnumerations.SortDirection;
import com.im.imonline.api.constants.InvoiceSearchAPIConstants;
import com.im.imonline.api.testdata.util.InvoiceSearchTestDataUtil;

public class InvoiceSearchRequestDataConfig {

	Logger logger = Logger.getLogger("InvoiceSearchRequestDataConfig"); 
	private HashMap<String, String> pRequestMap=null;
	private InvoiceSearchTestDataUtil objTestDataUtil = null;
	private HashMap<String, String> pDataMap=new HashMap<String, String>();	
	private ArrayList<APIEnumerations.InvoiceSearchType> attrArray=null;




	public InvoiceSearchRequestDataConfig(InvoiceSearchTestDataUtil objISTD, APIEnumerations.InvoiceSearchType[] reqParameters) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objISTD.getRequestData());
		objTestDataUtil=objISTD;
		attrArray =  Stream.of(reqParameters).collect(Collectors.toCollection(ArrayList::new));
		setTestSpecificBaseConfig();
	}


	public InvoiceSearchTestDataUtil getInvoiceSearchTestDataUtil() {
		return objTestDataUtil;
	}



	private void setTestSpecificBaseConfig() {
		try {

			for(APIEnumerations.InvoiceSearchType attribute : attrArray)
			{			
				switch (attribute) {
				case INVOICE_NUMBER:
					List<String> lstInvoiceNum = objTestDataUtil.getListOfInvoiceNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceNumber, lstInvoiceNum.get(Util.getRandomNumberInRange(0, lstInvoiceNum.size()-1)));
					break;
				case SERIAL_NUMBER:
					List<String> lstSerialNumber= objTestDataUtil.getListOfSerialNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.SerialNumber, lstSerialNumber.get(Util.getRandomNumberInRange(0, lstSerialNumber.size()-1)));
					break;
				case SKU:
					List<String> lstIngramPartNumber = objTestDataUtil.getListOfIngramPartNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.IngramPartNumber, lstIngramPartNumber.get(Util.getRandomNumberInRange(0, lstIngramPartNumber.size()-1)));
					break;
				case VPN:
					List<String> lstVendorPartNumber = objTestDataUtil.getListOfVendorPartNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.VendorPartNumber, lstVendorPartNumber.get(Util.getRandomNumberInRange(0, lstVendorPartNumber.size()-1)));
					break;
				case RESELLER_NUMBER:
					List<String> lstResellerNumber = objTestDataUtil.getListOfResellerNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.ResellerNumber, lstResellerNumber.get(Util.getRandomNumberInRange(0, lstResellerNumber.size()-1)));
					break;
				case CUSTOMER_ORDER_NUMBER:
					List<String> lstCustomerOrderNumber = objTestDataUtil.getListOfCustomerOrderNumber();
					pRequestMap.put(InvoiceSearchAPIConstants.Customer_Order_Number, lstCustomerOrderNumber.get(Util.getRandomNumberInRange(0, lstCustomerOrderNumber.size()-1)));
					break;
				case CPN: 
					List<String> lstCPNNumber = objTestDataUtil.getListOfCPN();
					pRequestMap.put(InvoiceSearchAPIConstants.CPN, lstCPNNumber.get(Util.getRandomNumberInRange(0, lstCPNNumber.size()-1)));
					break;
				case BID:
					List<String> lstBIDNumber = objTestDataUtil.getListOfBID();
					pRequestMap.put(InvoiceSearchAPIConstants.Bid, lstBIDNumber.get(Util.getRandomNumberInRange(0, lstBIDNumber.size()-1)));
					break;
				case RMA_TYPEINVOICE:
					pRequestMap.put(InvoiceSearchAPIConstants.INVOICETYPE, InvoiceSearchAPIConstants.valueRMAType);
					break;
				case ORDER_TYPEINVOICE:
					pRequestMap.put(InvoiceSearchAPIConstants.INVOICETYPE, InvoiceSearchAPIConstants.valueORDERType);
					break;
				case CRMEMO_TYPEINVOICE:
					pRequestMap.put(InvoiceSearchAPIConstants.INVOICETYPE, InvoiceSearchAPIConstants.valueCRMEMOType);
					break;
				case STATUS_OPEN:
					pRequestMap.put(InvoiceSearchAPIConstants.STATUS, InvoiceSearchAPIConstants.valueOpenStatus);
					break;
				case STATUS_PAID:
					pRequestMap.put(InvoiceSearchAPIConstants.STATUS, InvoiceSearchAPIConstants.valuePaidStatus);
					break;
				case STATUS_DUE:
					pRequestMap.put(InvoiceSearchAPIConstants.STATUS, InvoiceSearchAPIConstants.valueDueStatus);
					break;
				case INVOICEDATE_LAST31DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange, InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -31)+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD()+"\"");
					break;
				case DUEDATE_LAST31DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.DueDateRange,  InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -31)+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD()+"\"");
					break;
				case INVOICEDATE_RANDOMDATE:
					List<String> lstRandomDate=objTestDataUtil.getListOfInvoiceFromDate();
					String toDate=lstRandomDate.get(Util.getRandomNumberInRange(0, lstRandomDate.size()-1)).substring(0, 10);
					String fromDate=DateTimeUtil.getDatePlusOrMinusNoOfDaysForStringDate(toDate, InvoiceSearchAPIConstants.elasticDateFormat, -30);
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+fromDate+InvoiceSearchAPIConstants.TODATE+toDate+"\"");
					break;
			
				case INVOICEDATE_OF1000DAYSBEFORETODAY_FOR23DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -1000)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -977)+"\"");
					break;

				case INVOICEDATE_OF760DAYSBEFORETODAY_FOR5DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -760)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -755)+"\"");
					break;
				
				case INVOICEDATE_OF400DAYSBEFORETODAY_FOR10DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -400)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -390)+"\"");
					break;
	
				case INVOICEDATE_OF140DAYSBEFORETODAY_FOR12DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -140)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -128)+"\"");
					break;
				case INVOICEDATE_OF50DAYSBEFORETODAY_FOR2DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -50)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -48)+"\"");
					break;
				case INVOICEDATE_INVOICELAST5DAYS:
					pRequestMap.put(InvoiceSearchAPIConstants.InvoiceDateRange,InvoiceSearchAPIConstants.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, -5)
					+InvoiceSearchAPIConstants.TODATE+DateTimeUtil.getSystemDatePlusNoOfDays(InvoiceSearchAPIConstants.elasticDateFormat, 0)+"\"");
					break;
				default:
					break;

				}
			}			
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig"+e);
		}
	}


	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}

	public ArrayList<APIEnumerations.InvoiceSearchType> getListSetConfig() {
		return attrArray;
	}

	
	public static void main(String[] args) {
		//System.out.println(Util.getTodaysDateInFormat_YYYY_MM_DD());
	}
	/*
	public HashMap<String, String> getDataMap() {
		return pDataMap;
	}


	}	
	 */


	public void setSortConfig(APIEnumerations.InvoiceSortingOptions sortBy, SortDirection sortDirection) {
		try {
		attrArray.add(APIEnumerations.InvoiceSearchType.SORTBY);
		pRequestMap.put(InvoiceSearchAPIConstants.SORTBY,sortBy.toString());
		pRequestMap.put(InvoiceSearchAPIConstants.SORTDIRECTION,sortDirection.toString());		
		pRequestMap.put(InvoiceSearchAPIConstants.SORTCRITERIA, "{\"orderby\":\""+sortBy.toString().replaceAll("_", ".")+"\",\"orderbydirection\":\""+sortDirection.toString()+"\"}");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	//{"orderby":"invoicenumber.keyword","orderbydirection":"asc"}


}
