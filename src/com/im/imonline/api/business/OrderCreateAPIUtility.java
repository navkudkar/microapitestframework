package com.im.imonline.api.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.w3c.dom.NodeList;

import com.im.api.core.business.XMLUtil;

public class OrderCreateAPIUtility {

	public static synchronized List<HashMap<String,String>> getValidDataResponse(NodeList listNodeToBeVerified) {
		
		List<HashMap<String, String>> lstOfTestDataFromResponse = new ArrayList<HashMap<String, String>>();			
		List<HashMap<String, String>> lstOfDataToBeValidated = new ArrayList<HashMap<String, String>>();
		lstOfTestDataFromResponse = XMLUtil.getResponseDataofMultipleSet(listNodeToBeVerified);
		
		for(HashMap<String, String> objHamp: lstOfTestDataFromResponse) {
			if(objHamp.get("lineType").equalsIgnoreCase("P"))
				lstOfDataToBeValidated.add(objHamp);
		}	
		
		return lstOfDataToBeValidated;		
	}
}
