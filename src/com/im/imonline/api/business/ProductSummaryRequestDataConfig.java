package com.im.imonline.api.business;

import java.util.EnumMap;
import java.util.HashMap;
import org.apache.log4j.Logger;

import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductSummaryTestDataUtil;

public class ProductSummaryRequestDataConfig {

	Logger logger = Logger.getLogger("ProductDetailsRequestDataConfig"); 
	private HashMap<String, String> pRequestMap = null;
	private ProductSummaryTestDataUtil objTestDataUtil = null; 
	FlagOptions_ProductSummary flagType=null,flagConfig=null;
	private boolean dataCaptured=false;

	public ProductSummaryRequestDataConfig(ProductSummaryTestDataUtil objCPSTD, FlagOptions_ProductSummary flagOption) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objCPSTD.getRequestData());
		objTestDataUtil = objCPSTD;
		flagType=flagOption;
		resetRequestMap();
		setTestSpecificBaseConfig();

	}

	private void resetRequestMap() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPECPAGESIZE,"<<BLANK>>");
			pRequestMap.put(ProductSearchElasticAPIConstants.PAGESIZE,"16");
			pRequestMap.put(ProductSearchElasticAPIConstants.ID,objTestDataUtil.getRequestData().get(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTSUMMARYID));
		}catch (Exception e) {
			System.out.println("Error in the function resetRequestMap of TypeaheadSearchTestDataUtil "+e);
		}
	}

	private void setTestSpecificBaseConfig() {
		try {
			String sSku=null;
			EnumMap<FlagOptions_ProductSummary, String> skuTypeMap=objTestDataUtil.getMapSKUDataOfDifferentFlags();
			if(skuTypeMap.get(flagType)!=null) {
				switch (flagType) {

				case ACCESSORIES:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ACCESSORIES);
					break;

				case ACOP_PROMOTION:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ACOP_PROMOTION);
					break;

				case ACOP_QUANTITYBREAK:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ACOP_QUANTITYBREAK);
					break;

				case ACOP_SPECIALPRICING:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ACOP_SPECIALPRICING);
					break;
					
				case ACOP_WEBONLYPRICE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ACOP_WEBONLYPRICE);
					break;

				case AUTHORIZEDTOPURCHASE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.AUTHORIZEDTOPURCHASE);
					break;

				case AVAILABLE_IN_BUNDLE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.AVAILABLE_IN_BUNDLE);
					break;

				case BACKORDER:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.BACKORDER);
					break;

				case BIDPRICING:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.BIDPRICING);
					break;

				case BLOWOUT:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.BLOWOUT);
					break;

				case BULKFREIGHT:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.BULKFREIGHT);
					break;

				case BUNDLE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.BUNDLE);
					break;

				case CLEARANCE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.CLEARANCE);
					break;

				case DIRECTSHIP:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.DIRECTSHIP);
					break;


				case DISCONTINUED:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.DISCONTINUED);
					break;

				case DOWNLOADABLE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.DOWNLOADABLE);
					break;

				case ENDUSER:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.ENDUSER);
					break;

				case FREEITEM:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.FREEITEM);
					break;

				case HEAVYWEIGHT:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.HEAVYWEIGHT);
					break;

				case INSTOCK:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.INSTOCK);
					break;

				case INSTOCKORORDER:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.INSTOCKORORDER);
					break;

				case LICENCEPRODUCTS:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.LICENCEPRODUCTS);
					break;

				case LTL:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.LTL);
					break;

				case NEW:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.NEW);
					break;

				case NORETURNS:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.NORETURNS);
					break;

				case NOTAUTHORIZED:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.NOTAUTHORIZED);
					break;

				case NOTORDERABLE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.NOTORDERABLE);
					break;

				case OUTOFSTOCK:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.OUTOFSTOCK);
					break;

				case PROMOTION:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.PROMOTION);
					break;

				case QUANTITYBREAK:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.QUANTITYBREAK);
					break;

				case REFURBISHED:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.REFURBISHED);
					break;

				case REPLACEMENT:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.REPLACEMENT);
					break;

				case RETURNLIMITATIONS:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.RETURNLIMITATIONS);
					break;

				case SHIPFROMPARTNER:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.SHIPFROMPARTNER);
					break;

				case SPECIALBIDS:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.SPECIALBIDS);
					break;

				case SPECIALPRICING:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.SPECIALPRICING);
					break;

				case SUGGESTED:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.SUGGESTED);
					break;

				case WARRANTIES:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.WARRANTIES);
					break;

				case WARRANTY:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.WARRANTY);
					break;

				case WEBONLYPRICE:
					sSku=objTestDataUtil.getMapSKUDataOfDifferentFlags().get(FlagOptions_ProductSummary.WEBONLYPRICE);
					break;

				default:
					break;
				}// end of switch
				if(sSku!=null)
				{
					dataCaptured=true;
					setSearchConfig(flagType);
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS,sSku);
				}
				else
					dataCaptured=false;
			}//end of if
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig of ProductSummaryRequestDataConfig : "+e);
		}
	}

	public boolean isDataCaptured() {
		return dataCaptured;
	}

	private void setSearchConfig(FlagOptions_ProductSummary flagType) {
		flagConfig = flagType;		
	}

	public HashMap<String, String> gethmTestData() {
		return pRequestMap;	
	}

	public FlagOptions_ProductSummary getSearchConfig() {
		return flagConfig;
	}

	public ProductSummaryTestDataUtil getProductSummaryTestDataUtil() {
		return objTestDataUtil;
	}

}
