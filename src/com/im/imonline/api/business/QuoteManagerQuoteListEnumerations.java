package com.im.imonline.api.business;

public class QuoteManagerQuoteListEnumerations {

	public enum SearchType {
	QUOTE_NUMBER, END_USER_NAME, RESELLER_NUMBER, BLANK_RESELLERNUMBER, BLANK_COUNTRYCODE, QUOTES_QUOTESCREATEDLAST30DAYS,QUOTES_QUOTESCREATEDLAST60DAYS,  PAGESIZE,STATUS_OPEN, SORTBY,
	
	STATUS_ACTIVE,STATUS_CLOSED,STATUS_DRAFT,STATUS_ACTIVE_CLOSED,STATUS_ACTIVE_DRAFT,STATUS_DRAFT_CLOSED,STATUS_ACTIVE_DRAFT_CLOSED,createdDate;
	
	}
	
	public enum SortingOptions {
		createdon,name,im360_resendusername,totalamount,modifiedon,effectiveto;
	}
	
	public enum SortDirection{
		asc,desc
	}
}
