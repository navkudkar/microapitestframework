package com.im.imonline.api.business;

import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;

public class OrderUnit {

	private FlagOptions_Hermes prodcode;	private int Quantity=1; 
	private String SKU=null,vpn=null, prodName= null, globalskuid=null, unitPrice="0.0";

	private String warehouse="", carriercode="";
	private String vendorCode;
	private String erpordernumber;
	private String lineNumber;
	private String globalLineNumber;
	private String oUnetAmount;
	private String oUfreightamount;

	public String getGlobalskuid() {
		return globalskuid;
	}

	public void setGlobalskuid(String globalskuid) {
		this.globalskuid = globalskuid;
	}

	public String getVpn() {
		return vpn;
	}

	public void setVpn(String vpn) {
		this.vpn = vpn;
	}



	public String getCarriercode() {
		return carriercode;
	}

	public void setCarriercode(String carriercode) {
		this.carriercode = carriercode;
	}

	public String getSKU() {
		return SKU;
	}

	public void setSKU(String sKU) {
		SKU = sKU;
	}

	public OrderUnit(FlagOptions_Hermes sProdcode, int piProdQuantity) {
		prodcode=sProdcode;
		Quantity=piProdQuantity;
	}


	public FlagOptions_Hermes getProdcode() {
		return prodcode;
	}

	public void setProdcode(FlagOptions_Hermes prodcode) {
		this.prodcode = prodcode;
	}

	public int getQuantity() {
		return Quantity;
	}


	public void setQuantity(int productQuantity) {
		this.Quantity = productQuantity;
	}


	@Override
	public String toString() {
		return "OrderUnit [prodcode=" + prodcode + ", Quantity=" + Quantity + ", SKU=" + SKU + ", prodName=" + prodName
				+ "]";
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode=vendorCode;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setErpOrderNumber(String erpordernumber) {
		this.erpordernumber=erpordernumber;
	}
	
	public String getErpordernumber() {
		return erpordernumber;
	}

	public void setLineNumber(String lineNo) {
		lineNumber=lineNo;
	}
	
	public String getLineNumber() {
		return lineNumber;
	}

	public void setGlobalLineNumber(String glbLineNo) {
		globalLineNumber=glbLineNo;
	}
	
	public String getGlobalLineNumber() {
		return globalLineNumber;
	}

	public void setOrderSubTotalForSKU(String netAmount) {
		oUnetAmount=netAmount;		
	}

	public String getOrderSubTotalForSKU() {
		return oUnetAmount;
	}

	public void setFreightAmountForSKU(String freightamount) {
		oUfreightamount=freightamount;
	}

	public String getFreightAmountForSKU() {
		return oUfreightamount;
	}

	/*public static void main(String[] ar) {
		System.out.println(new OrderUnit(FlagOptions.BUNDLE_INSTOCK,1).getQuantity());


	}*/

}
