package com.im.imonline.api.business;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.common.Util;
import com.im.imonline.api.business.APIEnumerations.OrderSearchType;
import com.im.imonline.api.constants.OrderSearchAPIConstants_New;
import com.im.imonline.api.testdata.util.OrderSearchTestDataUtil;


public class OrderSearchRequestDataConfig {

	Logger logger = Logger.getLogger("InvoiceSearchRequestDataConfig"); 
	private HashMap<String, String> pRequestMap=null;
	private OrderSearchTestDataUtil objTestDataUtil = null;
	private OrderSearchType[] attrArray;



	public OrderSearchRequestDataConfig(OrderSearchTestDataUtil objOSTD, OrderSearchType[] reqParameters) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objOSTD.getRequestData());
		objTestDataUtil=objOSTD;
		attrArray = reqParameters;
		setTestSpecificBaseConfig();
	}


	public OrderSearchTestDataUtil getInvoiceSearchTestDataUtil() {
		return objTestDataUtil;
	}


	private void setTestSpecificBaseConfig() {
		try {

			for(OrderSearchType attribute : attrArray)
			{			
				switch (attribute) {
				case ORDER_NUMBER:
					List<String> lstOrderNum = objTestDataUtil.getListOfOrderNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.OrderNumbers, lstOrderNum.get(Util.getRandomNumberInRange(0, lstOrderNum.size()-1)));
					break;
				case SKU:
					List<String> lstIngramPartNumber = objTestDataUtil.getListOfIngramPartNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.IngramPartNumber, lstIngramPartNumber.get(Util.getRandomNumberInRange(0, lstIngramPartNumber.size()-1)));
					break;
				case VPN:
					List<String> lstVPN = objTestDataUtil.getListOfVendorPartNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.VendorPartNumber, lstVPN.get(Util.getRandomNumberInRange(0, lstVPN.size()-1)));
					break;
				case RESELLER_NUMBER:
					List<String> lstResellerNumber = objTestDataUtil.getListOfResellerNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.ResellerNumber, lstResellerNumber.get(Util.getRandomNumberInRange(0, lstResellerNumber.size()-1)));
					break;
				case CUSTOMER_ORDER_NUMBER:
					List<String> lstCustomerOrderNumber = objTestDataUtil.getListOfCustomerOrderNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.Customer_Order_Number, lstCustomerOrderNumber.get(Util.getRandomNumberInRange(0, lstCustomerOrderNumber.size()-1)));
					break;
				case ENDUSERPONUM:
					List<String> lstEndUserPONumber = objTestDataUtil.getListOfEndUserPONumber();
					pRequestMap.put(OrderSearchAPIConstants_New.EndUserPO, lstEndUserPONumber.get(Util.getRandomNumberInRange(0, lstEndUserPONumber.size()-1)));
					break;
				case VENDOR_NAME:
					List<String> lstVendorName = objTestDataUtil.getListOfVendorName();
					pRequestMap.put(OrderSearchAPIConstants_New.VendorName, (lstVendorName.get(Util.getRandomNumberInRange(0, lstVendorName.size()-1)).replaceAll("\\s+", "")));
					break;
				case SERIAL:
					List<String> lstSerialNumber = objTestDataUtil.getListOfSerialNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.SerialNumber,( lstSerialNumber.get(Util.getRandomNumberInRange(0, lstSerialNumber.size()-1)).replaceAll("\\s+", "")));
					break;
				case UPC:
					List<String> lstUPCNumber = objTestDataUtil.getListOfUPCNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.UPCNumber,( lstUPCNumber.get(Util.getRandomNumberInRange(0, lstUPCNumber.size()-1)).replaceAll("\\s+", "")));
					break;
				case BID:
					List<String> lstBidNumber = objTestDataUtil.getListOfBidNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.Bid,( lstBidNumber.get(Util.getRandomNumberInRange(0, lstBidNumber.size()-1)).replaceAll("\\s+", "")));
					break;
				case CPN:
					List<String> lstCPNNumber = objTestDataUtil.getListOfCPNNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.CPN,( lstCPNNumber.get(Util.getRandomNumberInRange(0, lstCPNNumber.size()-1)).replaceAll("\\s+", "")));
					break;
				case INVOICENUMBER:
					List<String> lstInvoiceNumber = objTestDataUtil.getListOfInvoiceNumber();
					pRequestMap.put(OrderSearchAPIConstants_New.InvoiceNumber, lstInvoiceNumber.get(Util.getRandomNumberInRange(0, lstInvoiceNumber.size()-1)));
					break;
				case ORDER_DATE_RANGE:
					pRequestMap.put(OrderSearchAPIConstants_New.OrderDateRange, OrderSearchAPIConstants_New.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(OrderSearchAPIConstants_New.elasticDateFormat, -31)+OrderSearchAPIConstants_New.TODATE+DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD()+"\"");
					break;
				case SHIP_DATE_RANGE:
					String f= OrderSearchAPIConstants_New.FROMDATE+DateTimeUtil.getSystemDatePlusNoOfDays(OrderSearchAPIConstants_New.elasticDateFormat, -61)+OrderSearchAPIConstants_New.TODATE+DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD()+"\"";
					pRequestMap.put(OrderSearchAPIConstants_New.ShipDateRange,f);
					break;
				case BACKORDER_SAP:
					pRequestMap.put(OrderSearchAPIConstants_New.BackOrderKey,OrderSearchAPIConstants_New.TRUE);
					break;
				case STATUS_INPROGRESS:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.InProgress);
					break;
				case STATUS_PARTIALLYPROCESSED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.PartiallyProcessed);
					break;
				case STATUS_SHIPPED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.Shipped);
					break;
				case STATUS_PARTIALLYINVOICED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.PartiallyInvoiced);
					break;
				case STATUS_INVOICED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.Invoiced);
					break;
				case STATUS_ONHOLD:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.OnHold);
					break;
				case STATUS_CUSTOMERHOLD:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.CustomerHold);
					break;
				case STATUS_VOIDED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.Voided);
					break;
				case STATUS_BACKORDER_IMPLUSE:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.Backorder_IMPluse);
					break;
				case STATUS_ORDERREVIEW:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.OrderReview);
					break;
				case STATUS_CREDITCHECK:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.CreditCheck);
					break;
				case STATUS_PARTIALHOLD:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.PartialHold);
					break;
				case STATUS_PARTIALLYREJECTED:
					pRequestMap.put(OrderSearchAPIConstants_New.STATUS,OrderSearchAPIConstants_New.PartiallyRejected);
					break;
				case SORTBY:
					break;
				default:
					break;
				
				}
			}			
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig"+e);
		}
	}

	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}

	public OrderSearchType[] getListSetConfig() {
		return attrArray;
	}

	public OrderSearchType[] getSearchTypeArray() {
		return attrArray;
	}

	public static void main(String[] args) {
		//System.out.println(Util.getTodaysDateInFormat_YYYY_MM_DD());// TODO Auto-generated method stub
	}

}
