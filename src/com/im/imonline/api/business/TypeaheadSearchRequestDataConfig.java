package com.im.imonline.api.business;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.im.api.core.common.Util;
import com.im.imonline.api.business.APIEnumerations.SearchParam;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.TypeaheadSearchTestDataUtil;

/**
 * @author Vinita Lakhwani
 * TypeaheadSearchRequestDataConfig
 * Elastic Typeahead Search Automation -- Request Hashmap Formation Config Class
 */
public class TypeaheadSearchRequestDataConfig {

	Logger logger = Logger.getLogger("TypeaheadSearchRequestDataConfig"); 
	private HashMap<String, String> pRequestMap=null;
	private TypeaheadSearchTestDataUtil objTestDataUtil = null;
	private SearchParam searchParam=null, searchConfig=null;
	String sCombo=null;
	String[] sSplit=null;
	//private HashMap<String, String> pDataMap=new HashMap<String, String>(2);	


	public TypeaheadSearchRequestDataConfig(TypeaheadSearchTestDataUtil objTestData, SearchParam searchParam) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objTestData.getRequestData());
		objTestDataUtil=objTestData;
		resetRequestMap();		
		this.searchParam=searchParam;
		setTestSpecificBaseConfig();
	}

	private void resetRequestMap() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPECPAGESIZE,"<<BLANK>>");
			pRequestMap.put(ProductSearchElasticAPIConstants.PAGESIZE,"16");
			pRequestMap.put(ProductSearchElasticAPIConstants.ID,objTestDataUtil.getRequestData().get(ProductSearchElasticAPIConstants.EXCELHEADER_TYPEAHEADID));
		}catch (Exception e) {
			System.out.println("Error in the function resetRequestMap of TypeaheadSearchTestDataUtil "+e);
		}
	}

	public void setLoggedoutUserMode() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_RESELLERID, "000001");
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_CUSTOMERKEY, "000001");		
		}catch (Exception e) {
			System.out.println("Error in the function setLoggedoutUserMode of TypeaheadSearchTestDataUtil "+e);
		}
	}

	/**
	 * 
	 * @author Vinita Lakhwani
	 * updates Request Hashmap for Core Prod Search Attributes like Keyword,Category,sub-category,product type,vendor, viewall
	 * 
	 */
	private void setTestSpecificBaseConfig() {
		try {

			switch (searchParam) {

			case CATEGORY:
				if(objTestDataUtil.getLstCategory().size()>0) {
					setSearchConfig(searchParam);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,objTestDataUtil.getLstCategory()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstCategory().size()-1)).substring(0, 3));
				}
				break;

			case SUBCATEGORY:
				if(objTestDataUtil.getLstSubCategory().size()>0) {
					setSearchConfig(searchParam);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,objTestDataUtil.getLstSubCategory()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstSubCategory().size()-1)).substring(0, 3));
				}
				break;

			case PRODUCTTYPE:
				if(objTestDataUtil.getLstProductType().size()>0) {
					setSearchConfig(searchParam);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,objTestDataUtil.getLstProductType()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstProductType().size()-1)).substring(0, 3));
				}
				break;

			case VENDORNAME:
				if(objTestDataUtil.getLstVendorName().size()>0) {
					setSearchConfig(searchParam);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,objTestDataUtil.getLstVendorName()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstVendorName().size()-1)).substring(0, 3));
				}
				break;

			case PRODUCTFAMILY:
				if(objTestDataUtil.getLstPfam().size()>0) {
					setSearchConfig(searchParam);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,objTestDataUtil.getLstPfam()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstPfam().size()-1)).substring(0, 3));
				}
				break;

			case VENDOR_PRODTYPE:
				if(objTestDataUtil.getLstVendorCat3().size()>0) {
					setSearchConfig(searchParam);
					sCombo=objTestDataUtil.getLstVendorCat3()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstVendorCat3().size()-1));
					sCombo=sCombo.substring(0,sCombo.indexOf(" ")+3);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,sCombo);
				}
				break;

			case VENDOR_PRODUCTFAMILY:
				if(objTestDataUtil.getLstVendorPfam().size()>0) {
					setSearchConfig(searchParam);
					sCombo=objTestDataUtil.getLstVendorPfam()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstVendorPfam().size()-1));
					sCombo=sCombo.substring(0,sCombo.indexOf(" ")+3);
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,sCombo);
				}
				break;

			case PRODTYPE_PRODUCTFAMILY:
				if(objTestDataUtil.getLstVendorCat3Pfam().size()>0) {
					setSearchConfig(searchParam);
					sCombo=objTestDataUtil.getLstVendorCat3Pfam()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstVendorCat3Pfam().size()-1));
					sSplit=sCombo.split(" ");
					sCombo=sSplit[2].length()>2?sSplit[1]+" "+sSplit[2].substring(0, 2):sSplit[1]+" "+sSplit[2];
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,sCombo);
				}
				break;

			case VENDOR_PRODTYPE_PRODUCTFAMILY:
				if(objTestDataUtil.getLstVendorCat3Pfam().size()>0) {
					setSearchConfig(searchParam);
					sCombo=objTestDataUtil.getLstVendorCat3Pfam()
							.get(Util.getRandomNumberInRange(0, objTestDataUtil.getLstVendorCat3Pfam().size()-1));
					sSplit=sCombo.split(" ");
					sCombo=sSplit[2].length()>2?sSplit[0]+" "+sSplit[1]+" "+sSplit[2].substring(0, 2):sSplit[0]+" "+sSplit[1]+" "+sSplit[2];
					pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING,sCombo);
				}
				break;


			case INVALID:
				setSearchConfig(searchParam);
				pRequestMap.put(ProductSearchElasticAPIConstants.QUERYSTRING, ProductSearchElasticAPIConstants.INVALIDKEYWORD);
				break;

			}		
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig of TypeaheadSearchRequestDataConfig : "+e);
		}
	}

	void setSearchConfig(SearchParam searchConfig) {
		this.searchConfig = searchConfig;
	}

	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	public SearchParam getSearchConfig() {
		// TODO Auto-generated method stub
		return searchConfig;
	}

}
