package com.im.imonline.api.business;

import java.util.EnumMap;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductDetails;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductDetailsTestDataUtil;

public class ProductDetailsRequestDataConfig {

	Logger logger = Logger.getLogger("ProductDetailsRequestDataConfig"); 
	private HashMap<String, String> pRequestMap = null;
	private ProductDetailsTestDataUtil objTestDataUtil = null;
	private FlagOptions_ProductDetails flagName = null;
	private boolean bIsDataAvailable = false;

	public ProductDetailsRequestDataConfig(ProductDetailsTestDataUtil objCPDTD, FlagOptions_ProductDetails flagName) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objCPDTD.getRequestData());
		objTestDataUtil = objCPDTD;
		resetRequestMap();
		this.flagName = flagName;
		setTestSpecificBaseConfig();
	}

	public ProductDetailsTestDataUtil getProductDetailsTestDataUtil() {
		return objTestDataUtil;
	}

	private void resetRequestMap() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<Blank>>");
			pRequestMap.put(ProductSearchElasticAPIConstants.ID,objTestDataUtil.getRequestData().get(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTDETAILSID));
		} catch(Exception e) {
			System.out.println("Error in the function resetRequestMap of ProductDetailsTestDataUtil "+e);
		}
	}

	public void setLoggedoutUserMode() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_RESELLERID, "000001");
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_CUSTOMERKEY, "000001");		
		}catch (Exception e) {
			System.out.println("Error in the function setLoggedoutUserMode"+e);
		}
	}

	private void setTestSpecificBaseConfig() {
		EnumMap<FlagOptions_ProductDetails, String> mapSKUDataOfDifferentFlags = objTestDataUtil.getSKUDataOfDifferentFlags();
		try {
			if(mapSKUDataOfDifferentFlags.containsKey(flagName)) {
				pRequestMap.put(ProductSearchElasticAPIConstants.MATERIAL, mapSKUDataOfDifferentFlags.get(flagName));
				bIsDataAvailable = true;
			}
		} catch(Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig of ProductDetailsRequestDataConfig : "+e);
		}
	}

	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}
	
	public FlagOptions_ProductDetails getFlagName() {
		return flagName;
	}
	
	public boolean isDataAvailable() {
		return bIsDataAvailable;
	}
}
