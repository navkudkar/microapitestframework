package com.im.imonline.api.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.common.Util;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SearchType;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SortDirection;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SortingOptions;
import com.im.imonline.api.constants.QuoteManagerQuoteListAPIConstants;
import com.im.imonline.api.testdata.util.QuoteManagerQuoteListTestDataUtil;

public class QuoteManagerQuoteListRequestDataConfig {

	Logger logger = Logger.getLogger("QuoteManagerQuoteListRequestDataConfig"); 
	private HashMap<String, String> pRequestMap=null;
	private QuoteManagerQuoteListTestDataUtil objTestDataUtil = null;
	private ArrayList<SearchType> attrArray=null;


	public QuoteManagerQuoteListRequestDataConfig(QuoteManagerQuoteListTestDataUtil objISTD, SearchType[] reqParameters) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objISTD.getRequestData());
		objTestDataUtil=objISTD;
		attrArray =  Stream.of(reqParameters).collect(Collectors.toCollection(ArrayList::new));
		setTestSpecificBaseConfig();
	}


	public QuoteManagerQuoteListTestDataUtil getQuoteManagerQuoteListTestDataUtil() {
		return objTestDataUtil;
	}



	private void setTestSpecificBaseConfig() {
		try {

			for(SearchType attribute : attrArray)
			{			
				switch (attribute) {
				case QUOTE_NUMBER:
					List<String> lstQuoteNum = objTestDataUtil.getListOfQuoteNumber();
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteNumber, lstQuoteNum.get(Util.getRandomNumberInRange(0, lstQuoteNum.size()-1)));
					break;
				case END_USER_NAME:
					List<String> lstEndUserName= objTestDataUtil.getListOfEndUserName();
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.EndUserName, lstEndUserName.get(Util.getRandomNumberInRange(0, lstEndUserName.size()-1)));
					break;
				case RESELLER_NUMBER:
					List<String> lstResellerNumber = objTestDataUtil.getListOfResellerNumber();
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.EXCELHEADER_RESELLERID, lstResellerNumber.get(Util.getRandomNumberInRange(0, lstResellerNumber.size()-1)));
					break;
				case BLANK_RESELLERNUMBER:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.CustomerNumber,"<<BLANK>>");
					break;
				case BLANK_COUNTRYCODE:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.CountryCode,"<<BLANK>>");
					break;
				case STATUS_ACTIVE:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueActiveStatus);
					break;
				case STATUS_CLOSED:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueClosedStatus);
					break;
				case STATUS_DRAFT:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueDraftStatus);
					break;
				case STATUS_ACTIVE_CLOSED:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueActiveClosedStatus);
					break;
				case STATUS_DRAFT_CLOSED:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueDraftClosedStatus);
					break;
				case STATUS_ACTIVE_DRAFT:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueActiveDraftStatus);
					break;
				case STATUS_ACTIVE_DRAFT_CLOSED:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.QuoteStatus, QuoteManagerQuoteListAPIConstants.valueActiveDraftClosedStatus);
					break;
				case QUOTES_QUOTESCREATEDLAST30DAYS:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.FromDate, DateTimeUtil.getSystemDatePlusNoOfDays(QuoteManagerQuoteListAPIConstants.elasticDateFormat, -30));
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.ToDate, DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD());
					break;
				case QUOTES_QUOTESCREATEDLAST60DAYS:
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.FromDate, DateTimeUtil.getSystemDatePlusNoOfDays(QuoteManagerQuoteListAPIConstants.elasticDateFormat, -60));
					pRequestMap.put(QuoteManagerQuoteListAPIConstants.ToDate, DateTimeUtil.getTodaysDateInFormat_YYYY_MM_DD());
					break;	
				default:
					break;

				}
			}			
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig"+e);
		}
	}


	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}

	public ArrayList<SearchType> getListSetConfig() {
		return attrArray;
	}

	
	public static void main(String[] args) {
		//System.out.println(Util.getTodaysDateInFormat_YYYY_MM_DD());
	}
	/*
	public HashMap<String, String> getDataMap() {
		return pDataMap;
	}


	}	
	 */


	public void setSortConfig(SortingOptions sortBy, SortDirection sortDirection) {
		try {
		attrArray.add(SearchType.SORTBY);
		pRequestMap.put(QuoteManagerQuoteListAPIConstants.sortingColumnName,sortBy.toString());
		pRequestMap.put(QuoteManagerQuoteListAPIConstants.Sorting,sortDirection.toString());		
		pRequestMap.put(QuoteManagerQuoteListAPIConstants.SORTCRITERIA, "{\"orderby\":\""+sortBy.toString().replaceAll("_", ".")+"\",\"orderbydirection\":\""+sortDirection.toString()+"\"}");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	//{"orderby":"invoicenumber.keyword","orderbydirection":"asc"}


}
