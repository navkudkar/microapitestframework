package com.im.imonline.api.business;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.imonline.api.business.APIEnumerations.KeywordType;
import com.im.imonline.api.business.APIEnumerations.SearchType;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductSearchTestDataUtil;

/**
 * @author Vinita Lakhwani
 * ProductSearchRequestDataConfig
 * Elastic Product Search Automation -- Request Hashmap Formation Config Class
 */
public class ProductSearchRequestDataConfig {

	Logger logger = Logger.getLogger("ProductSearchRequestDataConfig"); 
	private HashMap<String,HashMap<String,String>> topSKUsDataMap=null;
	private HashMap<String, String> singleSKUSpecificMap=null,multipleSKUSpecificMap=null, pRequestMap=null;
	private ProductSearchTestDataUtil objTestDataUtil = null;
	private int keywordSKURandomNo=0;
	private SearchType[] attrArray=null;
	private Set<String> listSetConfig=new HashSet<String>();
	private String keywordConfig=null;
	private HashMap<String, String> pDataMap=new HashMap<String, String>();	

	public String getKeywordConfig() {
		return keywordConfig;
	}

	public ProductSearchRequestDataConfig(ProductSearchTestDataUtil objCPSTD, SearchType[] reqParameters) {
		pRequestMap= new HashMap<>();
		pRequestMap.putAll(objCPSTD.getRequestData());
		resetRequestMap();
		objTestDataUtil=objCPSTD;
		attrArray=reqParameters;
		setTestSpecificBaseConfig();
	}

	public ProductSearchTestDataUtil getProductSearchTestDataUtil() {
		return objTestDataUtil;
	}

	private void resetRequestMap() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPECPAGESIZE,"<<BLANK>>");
			pRequestMap.put(ProductSearchElasticAPIConstants.PAGESIZE,"16");
		}catch (Exception e) {
			System.out.println("Error in the function resetRequestMap"+e);
		}
	}

	public void setLoggedoutUserMode() {
		try {
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_RESELLERID, "000001");
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_CUSTOMERKEY, "000001");		
		}catch (Exception e) {
			System.out.println("Error in the function setLoggedoutUserMode"+e);
		}
	}

	/**
	 * 
	 * @author Vinita Lakhwani
	 * updates Request Hashmap for Core Prod Search Attributes like Keyword,Category,sub-category,product type,vendor, viewall
	 * 
	 */
	private void setTestSpecificBaseConfig() {
		try {
			topSKUsDataMap=objTestDataUtil.getTopSKUsWithMostOfTheData();
			keywordSKURandomNo=Util.getRandomNumberInRange(0, topSKUsDataMap.size()-1);
			
			updateSKUspecificMapForRequestHashmapProcessing();
			for(SearchType attribute : attrArray)
			{	
					switch (attribute) {

					case KEYWORD:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
						keywordConfig=ProductSearchElasticAPIConstants.LONGDESC1;
						if(topSKUsDataMap.size()>=1)
							pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.LONGDESC1).split(" ")[0]);		//TODO: Update with keywords when implemented
						break;

					case CATEGORY:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_CATEGORY);
						if(topSKUsDataMap.size()>=1 && !(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT1DESC).equals(ProductSearchElasticAPIConstants.NULLCONSTANT)))
							pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_CATEGORY,singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT1DESC));
						break;

					case SUBCATEGORY:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_SUBCATEGORY);
						if(topSKUsDataMap.size()>=1 && !(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT2DESC).equals(ProductSearchElasticAPIConstants.NULLCONSTANT)))
							pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_SUBCATEGORY,singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT2DESC));
						break;

					case PRODTYPE:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_PRODTYPE);
						if(topSKUsDataMap.size()>=1 && !(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT3DESC).equals(ProductSearchElasticAPIConstants.NULLCONSTANT)))
							pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODTYPE,singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.CAT3DESC));
						break;

					case VENDORNAME:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME);
						if(topSKUsDataMap.size()>=1 && !(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME).equals(ProductSearchElasticAPIConstants.NULLCONSTANT)))
							pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME,singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.EXCELHEADER_VENDORNAME));
						break;

					case VIEWALL:
						listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_VIEWALL);
						pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_VIEWALL,"true");
						break;
					}
			}			
		} catch (Exception e) {
			System.out.println("Error in the function setTestSpecificBaseConfig"+e);
		}
	}

	private void updateSKUspecificMapForRequestHashmapProcessing() {
		try {
			Set<String> skuSet=topSKUsDataMap.keySet();
			int noOfSKUs=skuSet.size();
			String[] skus=new String[noOfSKUs];
			skuSet.toArray(skus);
			if(noOfSKUs>0) {
				singleSKUSpecificMap=topSKUsDataMap.get(skus[keywordSKURandomNo]);		
				if(noOfSKUs>1)
					multipleSKUSpecificMap=(keywordSKURandomNo<topSKUsDataMap.size()-1)?topSKUsDataMap.get(skus[keywordSKURandomNo+1]):topSKUsDataMap.get(skus[keywordSKURandomNo-1]);	
			}
		} catch (Exception e) {
			System.out.println("Error in the function updateSKUspecificMapForRequestHashmapProcessing "+e);
		}
	}

	public void setConfigForKeywordTests(KeywordType attribute) {		
		try {
			int upcListRandomNo;
			
			switch (attribute) {
			case KEYWORD_GENERIC:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_GENERIC;
				int keyRandomNo=Util.getRandomNumberInRange(0, objTestDataUtil.getTopMostKeywordList().size()-1);
				if(objTestDataUtil.getTopMostKeywordList().size()>=1)
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, objTestDataUtil.getTopMostKeywordList().get(keyRandomNo));
				break;

			case KEYWORD_SKU:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_SKU;
				if(topSKUsDataMap.size()>=1)
				{
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MATERIAL).replaceFirst("^0+(?!$)", ""));
				}	
				break;

			case KEYWORD_MULTIPLE_SKU:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_SKU;
				if(topSKUsDataMap.size()>1)
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS, singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MATERIAL)+"\",\""+multipleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MATERIAL));
				break;

			case KEYWORD_VPN:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_VPN;
				if(topSKUsDataMap.size()>=1)
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER));
				break;

			case KEYWORD_MULTIPLE_VPN:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_VPN;
				if(topSKUsDataMap.size()>1)
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS, singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER)+"\",\""+multipleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MANUFACTURERPARTNUMBER));
				break;

			case KEYWORD_UPC_EAN:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_UPC_EAN;
				if(objTestDataUtil.getListOfEAN().size()>=1)
				{
					upcListRandomNo=Util.getRandomNumberInRange(0, objTestDataUtil.getListOfEAN().size()-1);
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, objTestDataUtil.getListOfEAN().get(upcListRandomNo));
				}
				break;

			case KEYWORD_MULTIPLE_UPC_EAN:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_MULTIPLE_UPC_EAN;
				if(objTestDataUtil.getListOfEAN().size()>1)
				{
					upcListRandomNo=Util.getRandomNumberInRange(0, objTestDataUtil.getListOfEAN().size()-1);
					String EAN2nd=(upcListRandomNo<objTestDataUtil.getListOfEAN().size()-1)?objTestDataUtil.getListOfEAN().get(upcListRandomNo+1):objTestDataUtil.getListOfEAN().get(upcListRandomNo-1);
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_LISTOFPRODUCTS, objTestDataUtil.getListOfEAN().get(keywordSKURandomNo)+"\",\""+EAN2nd);
				}
				break;

			case KEYWORD_LONGDESC1:
				String longdesc;
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_LONGDESC1;
				if(topSKUsDataMap.size()>=1)
					if(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.LONGDESC1).length()>=3)
					{
						longdesc=singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.LONGDESC1);
						longdesc=longdesc.contains("\"")?longdesc.replaceAll("\"", ""):longdesc;
						pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, longdesc);
					}
					else
					{
						longdesc=multipleSKUSpecificMap.get(ProductSearchElasticAPIConstants.LONGDESC1);
						longdesc=longdesc.contains("\"")?longdesc.replaceAll("\"", ""):longdesc;
						pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, longdesc);
					}
				break;

			case KEYWORD_INVALID:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_INVALID;
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, ProductSearchElasticAPIConstants.INVALIDKEYWORD);
				break;

			case KEYWORD_AUTOCORRECTERRONEOUS:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_AUTOCORRECTERRONEOUS;
				String sSearchString = ProductSearchElasticAPIConstants.VENDORNM_MICROSOFT;
				pDataMap.put(ProductSearchElasticAPIConstants.AUTOCORRECTEDKEYWORD, sSearchString);
				int iLastCharIndex = sSearchString.length()-1;
				if(sSearchString.charAt(iLastCharIndex) != 'e')
					sSearchString = sSearchString.substring(0,iLastCharIndex)+'e';
				else
					sSearchString = sSearchString.substring(0, iLastCharIndex)+'a';
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, sSearchString);
				break;

			case KEYWORD_INCOMPLETE:
				listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD);
				keywordConfig=ProductSearchElasticAPIConstants.KEYWORD_INCOMPLETE;
				if(topSKUsDataMap.size()>=1)
				{
					String sSearchSKU = singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.MATERIAL);
					String strPattern = "^0+(?!$)";
					sSearchSKU = sSearchSKU.replaceAll(strPattern, "");
					sSearchSKU = sSearchSKU.substring(0,5);
					pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, sSearchSKU);
				}
				break;

			default:
				break;
			}
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error in the function setConfigForKeywordTests"+e);
		}
	}

	public void setSortConfig(String sortByKey, String sortOrder) {
		try {
			pDataMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY, Constants.TRUE); //Set sorting config for relevance
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY);
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY,sortByKey);
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION,sortOrder);		
		}catch (Exception e) {
			System.out.println("Error in the function setSortConfig"+e);
		}
	}

	public void setPriceRangeConfig() throws Exception {
		try {
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_PRICERANGESEARCH);
			Float dealerPrice;
			boolean flag=false;
			if(!singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.DEALERPRICE).equals(ProductSearchElasticAPIConstants.NULLCONSTANT)){
				flag=true;
				dealerPrice=Float.parseFloat(singleSKUSpecificMap.get(ProductSearchElasticAPIConstants.DEALERPRICE));
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRICERANGESEARCH,"true");	
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_MAXPRICE,String.valueOf(dealerPrice+20));
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_MINPRICE,String.valueOf(dealerPrice-20));
			}
			pDataMap.put(ProductSearchElasticAPIConstants.PRICERANGESEARCH, ""+flag);
		}catch (Exception e) {
			System.out.println("Error in the function setPriceRangeConfig"+e);
		}
	}

	public void setProductStatusConfig(String flagProdStatus) {
		boolean flag = false;
		String sFlg=null;
		try {
			switch(flagProdStatus) {
			case ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_NEW:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.FLAG_NEW);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.DIRECTSHIP);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_ESD:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.DOWNLOAD);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.HEAVYWEIGHT);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.AUTHORIZEDTOPURCHASE);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_BUNDLE:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.BUNDLEAVAILABLE);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_REFURBISHED:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.REFURBISHED);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			case ProductSearchElasticAPIConstants.FLAG_INSTOCK:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.INSTOCK);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT))
					flag = Integer.parseInt(sFlg)>0;
					break;

			default:
				break;
			}
			pDataMap.put(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG, ""+flag);
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS);
			if(flag) 
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS, flagProdStatus);
		}catch (Exception e) {
			System.out.println("Error in the function setProductStatusConfig"+e);
		}
	}

	public void setProductStatusConfig(String flagProdStatus,int minstockqty) {
		try {
			setProductStatusConfig(flagProdStatus);
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_MINSTOCK);
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_STOCKRANGESEARCH, "true");		
			pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_MINSTOCK, String.valueOf(minstockqty));		
		}catch (Exception e) {
			System.out.println("Error in the function setProductStatusConfig"+e);
		}
	}

	/*public HashMap<String, String> processRequestHashmapForACOPFlagsIncombination(String flagACOP) {
		if(skuSpecificMap.get(flagACOP).equals("true"))
			addACOPFlagToRequestHashMap(flagACOP);
		else
			ExtentTestManager.log(Status.INFO, "The selected SKU does not have "+ flagACOP, "");

		return pRequestMap;
	}*/	

	public void setACOPFlagConfig(String flagACOP) {
		String sFlg;
		boolean flag=false;
		try {
			switch (flagACOP) {
			case ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS:	
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
					listSetConfig.add(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS);
					if(Integer.parseInt(sFlg)>0) {
						flag=true;
						pRequestMap.put(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS, "true");						
					}
				}
				break;
			case ProductSearchElasticAPIConstants.FLAG_QTYBREAK:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.AGGREGATION_QTYBREAK);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
					listSetConfig.add(ProductSearchElasticAPIConstants.FLAG_QTYBREAK);
					if(Integer.parseInt(sFlg)>0) {
						flag=true;				
						pRequestMap.put(ProductSearchElasticAPIConstants.FLAG_QTYBREAK, "true");
					}
				}
				break;
			case ProductSearchElasticAPIConstants.FLAG_PROMOTION:
				sFlg=objTestDataUtil.getAggregationDataOfFilter().get(ProductSearchElasticAPIConstants.AGGREGATION_PROMOTION);
				if(!sFlg.equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {								
					listSetConfig.add(ProductSearchElasticAPIConstants.FLAG_PROMOTION);
					if(Integer.parseInt(sFlg)>0) {
						flag=true;		
						pRequestMap.put(ProductSearchElasticAPIConstants.FLAG_PROMOTION, "true");
					}	
				}
				break;
			}		
			pDataMap.put(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG, ""+flag);
		}catch (Exception e) {
			System.out.println("Error in the function addACOPFlagToRequestHashMap"+e);
		}
	}

	public void setPageSize(String pPageSize, boolean isPageStart) throws Exception {
		try {
			listSetConfig.add(ProductSearchElasticAPIConstants.PAGESIZE);System.out.println(" ");
			pRequestMap.put(ProductSearchElasticAPIConstants.PAGESIZE, pPageSize);

			if(isPageStart) {// will set according last page(Floor)
				int sTotalProd = Integer.valueOf(objTestDataUtil.getTotalProducts());
				int iPageSize = Integer.valueOf(pPageSize);
				int pageStart = (sTotalProd/iPageSize)*iPageSize;
				pageStart = (sTotalProd % Integer.valueOf(pPageSize))==0?pageStart-1:pageStart;

				pRequestMap.put(ProductSearchElasticAPIConstants.PAGESTART, String.valueOf(pageStart));
			}else {
				pRequestMap.put(ProductSearchElasticAPIConstants.PAGESTART, pPageSize);
			}
		}catch (Exception e) {
			System.out.println("Error in the function setPageSize"+e);
		}
	}

	public HashMap<String, String> gethmTestData() {
		return pRequestMap;		
	}

	public Set<String> getListSetConfig() {
		return listSetConfig;
	}

	public SearchType[] getSearchTypeArray() {
		return attrArray;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	public HashMap<String, String> getDataMap() {
		return pDataMap;
	}

	public void setAbsoluteOrNonAbsoluteConfig(String flagACOP) {
		try {
			String randomKeyName = null,sRandomValue = null,sValue;
			HashMap<String, List<String>> objHMAbs = objTestDataUtil.getnonAbsoluteSpecification();
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE);	
			
			
			if(!objHMAbs.isEmpty()) {
				randomKeyName = (String) objHMAbs.keySet().toArray()[new Random().nextInt(objHMAbs.keySet().size())];
				sRandomValue = objHMAbs.get(randomKeyName).get(new Random().nextInt(objHMAbs.get(randomKeyName).size()));
				sValue = "{\"attributename\":\""+randomKeyName+"\",\"attributevalue\":\""+sRandomValue+"\"}";
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE, sValue);
				pDataMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE, Constants.TRUE);
			}

		}catch (Exception e) {
			System.out.println("Error in the function setAbsoluteOrNonAbsoluteConfig "+e);
		}

	}

	public void setInStockStatusPriceRangeConfig() {
		try {
			Set<String> keySet=objTestDataUtil.getInstockData().keySet();
			boolean flag = keySet.size()>0;
			Float dealerPrice;			
			pDataMap.put(ProductSearchElasticAPIConstants.AGGDATAFORPRODSTATUSFLAG, ""+flag);
			pDataMap.put(ProductSearchElasticAPIConstants.PRICERANGESEARCH, ""+flag);
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS);
			listSetConfig.add(ProductSearchElasticAPIConstants.EXCELHEADER_PRICERANGESEARCH);
			if(flag){
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODSTATUS, ProductSearchElasticAPIConstants.FLAG_INSTOCK);
				dealerPrice=Float.parseFloat(objTestDataUtil.getInstockData().get(keySet.toArray()[0]));
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRICERANGESEARCH,"true");	
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_MAXPRICE,String.valueOf(dealerPrice+20));
				pRequestMap.put(ProductSearchElasticAPIConstants.EXCELHEADER_MINPRICE,String.valueOf(dealerPrice-20));
			}
			}catch (Exception e) {
			System.out.println("Error in the function setInStockStatusPriceRangeConfig"+e);
		}
		
	}	
	
	public HashMap<String, String> getSingleSKUSpecificDataMap() {
		return singleSKUSpecificMap;
	}
}
