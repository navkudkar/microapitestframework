package com.im.imonline.api.business;

import java.util.HashMap;
import java.util.Set;

import com.im.imonline.api.business.APIEnumerations.CarrierOptions;
import com.im.imonline.api.business.APIEnumerations.CheckOutOptions;
import com.im.imonline.api.business.APIEnumerations.OrderProcessOptionsForHeldOrder;
import com.im.imonline.api.business.APIEnumerations.PaymentTypes;
import com.im.imonline.api.business.APIEnumerations.ShipToAndBillToAddressOptions;
import com.im.imonline.api.business.APIEnumerations.WarehouseTypes;


public class HermesE2ERegister {
	private HashMap<CheckOutOptions, String> checkOutData= new HashMap<CheckOutOptions, String>();
	//public HashMap<IntermediateResults, String> testIntermediaryResultApartFromOU= new HashMap<IntermediateResults, String>();
	OrderUnit[] listOfProducts=null;


	private HashMap<CheckOutOptions, String> setDefaultValuesForcheckOutData() throws Exception{
		setShipToAndBillToAddressType(ShipToAndBillToAddressOptions.SELECT_EXISTING_BOTH);
		setWarehouseType(WarehouseTypes.DEFAULT);
		setPaymentType(PaymentTypes.TERMS);
		setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.NONE);
		setDeliveryService(CarrierOptions.DEFAULT);
		setLineNotes("");
		setBasketNote("");
		setEndUserPONumber("");
		return checkOutData;
	}


	public HermesE2ERegister(OrderUnit[] productList) throws Exception {
		listOfProducts=productList;
		setDefaultValuesForcheckOutData();
	}


	public void setShipToAndBillToAddressType(ShipToAndBillToAddressOptions ShiptoAddrOption) {
		checkOutData.put(CheckOutOptions.SHIPTO_BILLTO_ADDRESS, ShiptoAddrOption.toString());
	}

	public String getShippingAddressType() {
		return checkOutData.get(CheckOutOptions.SHIPTO_BILLTO_ADDRESS);
	}

	public OrderUnit[] getListOfProducts() {
		return listOfProducts;
	}


	public void setWarehouseType(WarehouseTypes warehouseType) {
		checkOutData.put(CheckOutOptions.WAREHOUSE_TYPE, warehouseType.toString());

	}

	public String getWarehouseType() {
		return checkOutData.get(CheckOutOptions.WAREHOUSE_TYPE);
	}

	public void setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder opt) {
		checkOutData.put(CheckOutOptions.HOLD_ORDER_TYPE, opt.toString());
	}


	public String getScenarioForHoldOrder()
	{
		return checkOutData.get(CheckOutOptions.HOLD_ORDER_TYPE);
	}


	public void setEndUserPONumber(String sEUPONum) {
		checkOutData.put(CheckOutOptions.ENDUSER_PONUMBER, sEUPONum);
	}


	public void setBasketNote(String sBasketNote) {
		checkOutData.put(CheckOutOptions.BASKET_NOTE, sBasketNote);	
	}


	public void setLineNotes(String sLineNote) {
		checkOutData.put(CheckOutOptions.LINE_NOTES, sLineNote);	
	}


	public String getLineNotes() {
		return checkOutData.get(CheckOutOptions.LINE_NOTES);
	}


	public String getEUPONumber() {
		return checkOutData.get(CheckOutOptions.ENDUSER_PONUMBER);
	}


	public String getBasketNotes() {
		return checkOutData.get(CheckOutOptions.BASKET_NOTE);
	}


	public void setPaymentType(PaymentTypes paymentType) {
		checkOutData.put(CheckOutOptions.PAYMENT_TYPE, paymentType.toString());
	}

	public String getPaymentType() {
		return checkOutData.get(CheckOutOptions.PAYMENT_TYPE);
	}


	public void setDeliveryService(CarrierOptions diffchoice) {
		checkOutData.put(CheckOutOptions.DELIVERY_SERVICE_TYPE, diffchoice.toString());
	}
	
	public String getDeliveryService() {
		return checkOutData.get(CheckOutOptions.DELIVERY_SERVICE_TYPE);
	}

	public Set<CheckOutOptions> getKeySetForConfigurations(){
		return checkOutData.keySet();
	}
	
}
