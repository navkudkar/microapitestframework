package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.TypeaheadSearchElasticAPIActionBucket;
import com.im.imonline.api.business.APIEnumerations.SearchParam;
import com.im.imonline.api.business.TypeaheadSearchRequestDataConfig;
import com.im.imonline.api.testdata.util.TypeaheadSearchTestDataUtil;


public class TypeaheadSearchElasticAPITest extends BaseTest{
	static Logger logger = Logger.getLogger("TypeaheadSearchElasticAPITest");
	public final static String ModuleNameInTestApplicability="TypeaheadSearchElasticAPI";
	public final static String TestDataFile="TestDataForProductSearchElasticAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="TypeaheadSearchElasticAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	TypeaheadSearchTestDataUtil objTestData = null;

	public TypeaheadSearchElasticAPITest(HashMap<String, String> pExcelHMap, TypeaheadSearchTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}

	@Test(description="TC01: Logout Mode_Verify Typeahead Search with incomplete category as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedOutModeWithIncompleteCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.CATEGORY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC02: Logout Mode_Verify Typeahead Search with incomplete sub-category as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedOutModeWithIncompleteSubCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.SUBCATEGORY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC03: Logout Mode_Verify Typeahead Search with incomplete ProductType as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedOutModeWithIncompleteProductType() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODUCTTYPE);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC04: Logout Mode_Verify Typeahead Search with incomplete Vendorname as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedOutModeWithIncompleteVendorname() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDORNAME);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC05: Logout Mode_Verify Typeahead Search with incomplete Product Family as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedOutModeWithIncompleteProductFamily() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODUCTFAMILY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC06: Login Mode_Verify Typeahead Search with incomplete category as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedInModeWithIncompleteCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.CATEGORY);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC07: Login Mode_Verify Typeahead Search with incomplete sub-category as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedInModeWithIncompleteSubCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.SUBCATEGORY);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC08: Login Mode_Verify Typeahead Search with incomplete ProductType as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedInModeWithIncompleteProductType() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODUCTTYPE);
				
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC09: Login Mode_Verify Typeahead Search with incomplete Vendorname as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedInModeWithIncompleteVendorname() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDORNAME);
				
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC10: Login Mode_Verify Typeahead Search with incomplete Product Family as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadSuggestionsLoggedInModeWithIncompleteProductFamily() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODUCTFAMILY);
				
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC11: Logout Mode_Verify Typeahead Search with incomplete Vendor and Product type combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogOutModeWithIncompleteVendorAndProdType() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODTYPE);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC12: Logout Mode_Verify Typeahead Search with incomplete Vendor and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogOutModeWithIncompleteVendorAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODUCTFAMILY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC13: Logout Mode_Verify Typeahead Search with incomplete Product Type and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogOutModeWithIncompleteProdTypeAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODTYPE_PRODUCTFAMILY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC14: Logout Mode_Verify Typeahead Search with incomplete Vendor ,Product Type and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogOutModeWithIncompleteVendorProdTypeAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODTYPE_PRODUCTFAMILY);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC15: Login Mode_Verify Typeahead Search with incomplete Vendor and Product type combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogInModeWithIncompleteVendorAndProdType() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODTYPE);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC16: Login Mode_Verify Typeahead Search with incomplete Vendor and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogInModeWithIncompleteVendorAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODUCTFAMILY);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC17: Login Mode_Verify Typeahead Search with incomplete Product Type and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogInModeWithIncompleteProdTypeAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.PRODTYPE_PRODUCTFAMILY);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC18: Login Mode_Verify Typeahead Search with incomplete Vendor ,Product Type and Product Family combination as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogInModeWithIncompleteVendorProdTypeAndPFam() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.VENDOR_PRODTYPE_PRODUCTFAMILY);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC19: Logout Mode_Verify Typeahead Search with invalid string as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogOutModeWithInvalidString() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.INVALID);
		objConfig.setLoggedoutUserMode();
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC20: Login Mode_Verify Typeahead Search with invalid string as the query", groups="API Typeahead Search",enabled=true)
	public void verifyTypeaheadAPILogInModeWithInvalidString() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		TypeaheadSearchRequestDataConfig objConfig=new TypeaheadSearchRequestDataConfig(objTestData,SearchParam.INVALID);
		
		TypeaheadSearchElasticAPIActionBucket action = new TypeaheadSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	/* 
	 **************Factory code starts********** 
	 */
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry,
					TypeaheadSearchTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					TypeaheadSearchTestDataUtil objCPSTD = (TypeaheadSearchTestDataUtil) myData2Dim[i][1];					
					TypeaheadSearchElasticAPITest newInstance=new TypeaheadSearchElasticAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

