package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.QuoteManagerQuoteListAPIActionBucket;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SearchType;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SortDirection;
import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SortingOptions;
import com.im.imonline.api.business.QuoteManagerQuoteListRequestDataConfig;
import com.im.imonline.api.testdata.util.QuoteManagerQuoteListTestDataUtil;


public class QuoteManagerQuoteListAPITest extends BaseTest{
	static Logger logger = Logger.getLogger("QuoteManagerQuoteListAPITest");
	public final static String ModuleNameInTestApplicability="QuoteManagerQuoteListAPI";
	public final static String TestDataFile="TestDataForQuoteManagerQuoteListAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="QuoteManagerQuoteListRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	QuoteManagerQuoteListTestDataUtil objTestData = null;

	public QuoteManagerQuoteListAPITest(HashMap<String, String> pExcelHMap, QuoteManagerQuoteListTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}

	@Test(description="TC01: To Verify the Active Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForActiveQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_ACTIVE};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	


	@Test(description="TC02: To Verify the Closed Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForClosedQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_CLOSED};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	
	
	@Test(description="TC03: To Verify the Draft Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForDraftQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_DRAFT};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	
	
	@Test(description="TC04: To Verify the Active and Closed Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForActiveAndClosedQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_ACTIVE_CLOSED};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC05: To Verify the Active and Draft Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForActiveAndDraftQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_ACTIVE_DRAFT};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC06: To Verify the Draft and Closed Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForDraftandClosedQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_ACTIVE_DRAFT_CLOSED};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
			
	
	@Test(description="TC07: To Verify the Active, Draft and Closed Quote search in Quote List ", groups="API QuoteManager QuoteList",enabled=true)
	public void SearchForActiveDraftandClosedQuotes() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.STATUS_ACTIVE_DRAFT_CLOSED};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC08: Sort the Created Date Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
	public void QuoteListWith_SortBy_CreatedDate_ASC() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(SortingOptions.createdon,SortDirection.asc);		

		
		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC09: Sort the Created Date Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
	public void QuoteListWith_SortBy_CreatedDate_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(SortingOptions.createdon,SortDirection.desc);	

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC10: Sort the Quote Name Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
	public void QuoteListWith_SortBy_QuoteName_ASC() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(SortingOptions.name,SortDirection.asc);		

		
		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC11: Sort the Quote Name Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
	public void QuoteListWith_SortBy_QuoteName_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(SortingOptions.name,SortDirection.desc);	

		QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	
	}	
		
		@Test(description="TC12: Sort the End User Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_EndUser_ASC() throws Exception {
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.im360_resendusername,SortDirection.asc);		

			
			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}
		@Test(description="TC13: Sort the End User Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_EndUser_DESC() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.im360_resendusername,SortDirection.desc);	

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}	

		@Test(description="TC14: Sort the Last Updated Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_LastUpdated_ASC() throws Exception {
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.modifiedon,SortDirection.asc);		

			
			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}
		@Test(description="TC15: Sort the last Updated Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_LastUpdated_DESC() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.modifiedon,SortDirection.desc);	

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}	

		@Test(description="TC16: Sort the Quote Total Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_QuoteTotal_ASC() throws Exception {
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.totalamount,SortDirection.asc);		

			
			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}
		@Test(description="TC17: Sort the Quote Total Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_QuoteTotal_DESC() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.totalamount,SortDirection.desc);	

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}		
		
		@Test(description="TC18: Sort the Expiry Date Column of Quote List Page in Ascending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_ExpiryDate_ASC() throws Exception {
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.effectiveto,SortDirection.asc);		

			
			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}
		@Test(description="TC19: Sort the Expiry Date Column of Quote List Page in Descending Order", groups="API QuoteManager Search",enabled=true)
		public void QuoteListWith_SortBy_ExpiryDate_DESC() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData,attrArray);
			objConfig.setSortConfig(SortingOptions.effectiveto,SortDirection.desc);	

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig); 
			action.performResponseValidation();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();

		}
		
		@Test(description="TC20: Verify the search of Quote Number", groups="API QuoteManager Search",enabled=true)
		public void QuoteNumberSearchInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.QUOTE_NUMBER};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}	
		
		@Test(description="TC21: Verify the search of End User Name", groups="API QuoteManager Search",enabled=true)
		public void EndUserNameSearchInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.END_USER_NAME};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}	
		

		
		@Test(description="TC22: Verify the quotes created in last 60 days", groups="API QuoteManager Search",enabled=true)
		public void QuotesCreatedInLast60DaysInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.QUOTES_QUOTESCREATEDLAST60DAYS};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);
			System.out.println(objConfig);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}	

		@Test(description="TC23: Verify the quotes created in last 30 days", groups="API QuoteManager Search",enabled=true)
		public void QuotesCreatedInLast30DaysInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.QUOTES_QUOTESCREATEDLAST30DAYS};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);
			System.out.println(objConfig);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}		
		
		@Test(description="TC24: Verify the error message is displayed for blank Reseller Number", groups="API QuoteManager Search",enabled=true)
		public void NegativeScenario_BlankCustomerNumberInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.BLANK_RESELLERNUMBER};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);
			System.out.println(objConfig);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseNegativeValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}		
		
		@Test(description="TC25: Verify the error message is displayed for blank Country Code", groups="API QuoteManager Search",enabled=true)
		public void NegativeScenario_BlankCountryCodeInQuoteListPage() throws Exception {

			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

			SearchType[] attrArray= {SearchType.BLANK_COUNTRYCODE};
			QuoteManagerQuoteListRequestDataConfig objConfig = new QuoteManagerQuoteListRequestDataConfig(objTestData, attrArray);
			System.out.println(objConfig);

			QuoteManagerQuoteListAPIActionBucket action = new QuoteManagerQuoteListAPIActionBucket(objAPIDriver, objConfig);
			action.performResponseNegativeValidation(); 

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
		}		

	// **************Factory code starts********** 
	 
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry,
					QuoteManagerQuoteListTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					QuoteManagerQuoteListTestDataUtil objCPSTD = (QuoteManagerQuoteListTestDataUtil) myData2Dim[i][1];		
					QuoteManagerQuoteListAPITest newInstance=new QuoteManagerQuoteListAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}

	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO","Country","TestCaseName", "REQUEST","RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

