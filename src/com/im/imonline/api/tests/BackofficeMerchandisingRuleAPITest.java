package com.im.imonline.api.tests;


import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_EXACT_LOCATION;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_GLOBAL;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_PHRASE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_STATUS;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_TYPE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_SUB_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGIN_AND_LOGOUT_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGIN_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGOUT_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_PRODUCT_TYPE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_SUB_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_VENDOR_NAME;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCT_SELECTION_BY_DYNAMIC;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCT_SELECTION_BY_SPECIFIC;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.BackofficeMerchandisingAPIActionBucket;
import com.im.imonline.api.backoffice.business.BackOfficeMerchandisingEnums;
import com.im.imonline.api.backoffice.business.BackOfficeRuleCreateRegister;
import com.im.imonline.api.testdata.util.BackofficeMerchandisingTestDataUtil;


public class BackofficeMerchandisingRuleAPITest extends BaseTest{
	static Logger logger = Logger.getLogger("BackofficeMerchandisingRuleAPITest");
	public final static String ModuleNameInTestApplicability="BackofficeMerchandisingAPI";
	public final static String ElasticTestDataFile="TestDataForProductSearchElasticAPI.xlsx";
	public final static String BackofficeMerchandisingTestDataFile="TestDataForBackofficeMerchandisingAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="BackofficeMerchandisingAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	BackofficeMerchandisingTestDataUtil objTestData = null;

	public BackofficeMerchandisingRuleAPITest(HashMap<String, String> pExcelHMap, BackofficeMerchandisingTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}

	@Test(description="TC01: HomePageZoneRecommendedRuleInLogInModeSpecificProduct", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void HomePageZoneRecommendedRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC02: HomePageZoneRecommendedRuleInLogInModeDynamicProductByCat", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC03: HomePageZoneRecommendedRuleNegativeExpireRuleDynamicProductByCat", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleNegativeExpireRuleDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC04: HomePageZoneRecommendedRuleInLogOutModeSpecificProduct", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void HomePageZoneRecommendedRuleInLogOutModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC05: HomePageZoneRecommendedRuleInLogOutModeDynamicProductByVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleInLogOutModeDynamicProductByVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC06: HomePageZoneRecommendedRuleNegativeExpireRuleSpecificProduct", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
		
	@Test(description="TC07: HomePageZoneRecommendedRuleInBothModeSpecificProduct", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC08: HomePageZoneRecommendedRuleInBothModeDynamicProductByCatSubCat", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void HomePageZoneRecommendedRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC09: HomePageZoneRecommendedRuleInBothModeDynamicProductByType", groups= {"Legacy","Redesign"},enabled=true)
	public void HomePageZoneRecommendedRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC10: HomePageZoneRecommendedRuleNegativeDeactiveRule", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void HomePageZoneRecommendedRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC11: HomePageZonePromotionRuleInLogInModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZonePromotionRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC12: HomePageZonePromotionRuleInLogInModeDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC13: HomePageZonePromotionRuleNegativeExpireRuleDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleNegativeExpireRuleDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC14: HomePageZonePromotionRuleInLogOutModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZonePromotionRuleInLogOutModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC15: HomePageZonePromotionRuleInLogOutModeDynamicProductByVendor", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleInLogOutModeDynamicProductByVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC16: HomePageZonePromotionRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC17: HomePageZonePromotionRuleInBothModeSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC18: HomePageZonePromotionRuleInBothModeDynamicProductByCatSubCat", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZonePromotionRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC19: HomePageZonePromotionRuleInBothModeDynamicProductByType", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC20: HomePageZonePromotionRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void HomePageZonePromotionRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PROMOTIONS_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC21: HomePageZoneBuyItAgainRuleInLogInModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneBuyItAgainRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC22: HomePageZoneBuyItAgainRuleInLogInModeDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZoneBuyItAgainRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC23: HomePageZoneBuyItAgainRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void HomePageZoneBuyItAgainRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC24: HomePageZoneBuyItAgainRuleInBothModeSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneBuyItAgainRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC25: HomePageZoneBuyItAgainRuleInBothModeDynamicProductByCatSubCat", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneBuyItAgainRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC26: HomePageZoneBuyItAgainRuleInBothModeDynamicProductByType", groups= {"Redesign"},enabled=true)
	public void HomePageZoneBuyItAgainRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC27: HomePageZoneBuyItAgainRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneBuyItAgainRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC28: HomePageZoneYouMayAlsoLikeItRuleInLogInModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC29: HomePageZoneYouMayAlsoLikeItRuleInLogInModeDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC30: HomePageZoneYouMayAlsoLikeItRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC31: HomePageZoneYouMayAlsoLikeItRuleInBothModeSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC32: HomePageZoneYouMayAlsoLikeItRuleInBothModeDynamicProductByCatSubCat", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC33: HomePageZoneYouMayAlsoLikeItRuleInBothModeDynamicProductByType", groups= {"Redesign"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC34: HomePageZoneYouMayAlsoLikeItRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneYouMayAlsoLikeItRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC35: HomePageZoneTrendingProductsRuleInLogInModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneTrendingProductsRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC36: HomePageZoneTrendingProductsRuleInLogInModeDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZoneTrendingProductsRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC37: HomePageZoneTrendingProductsRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void HomePageZoneTrendingProductsRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC38: HomePageZoneTrendingProductsRuleInBothModeSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneTrendingProductsRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC39: HomePageZoneTrendingProductsRuleInBothModeDynamicProductByCatSubCat", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneTrendingProductsRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC40: HomePageZoneTrendingProductsRuleInBothModeDynamicProductByType", groups= {"Redesign"},enabled=true)
	public void HomePageZoneTrendingProductsRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC41: HomePageZoneTrendingProductsRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneTrendingProductsRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TRENDING_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC42: HomePageZonePPRRuleInLogInModeSpecificProduct", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC43: HomePageZonePPRRuleInLogInModeDynamicProductByCat", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC44: HomePageZonePPRRuleNegativeExpireRuleDynamicProductByCat", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleNegativeExpireRuleDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC45: HomePageZonePPRRuleInLogOutModeSpecificProduct", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInLogOutModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC46: HomePageZonePPRRuleInLogOutModeDynamicProductByVendor", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInLogOutModeDynamicProductByVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC47: HomePageZonePPRRuleNegativeExpireRuleSpecificProduct", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
		
	@Test(description="TC48: HomePageZonePPRRuleInBothModeSpecificProduct", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC49: HomePageZonePPRRuleInBothModeDynamicProductByCatSubCat", groups= {"Legacy","P1"},enabled=true)
	public void HomePageZonePPRRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC50: HomePageZonePPRRuleInBothModeDynamicProductByType", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC51: HomePageZonePPRRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void HomePageZonePPRRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC52: HomePageZoneFeaturedCatRuleInLogInModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInLogInModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC53: HomePageZoneFeaturedCatRuleInLogInModeDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInLogInModeDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC54: HomePageZoneFeaturedCatRuleNegativeExpireRuleDynamicProductByCat", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleNegativeExpireRuleDynamicProductByCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC55: HomePageZoneFeaturedCatRuleInLogOutModeSpecificProduct", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInLogOutModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC56: HomePageZoneFeaturedCatRuleInLogOutModeDynamicProductByVendor", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInLogOutModeDynamicProductByVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC57: HomePageZoneFeaturedCatRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
		
	@Test(description="TC58: HomePageZoneFeaturedCatRuleInBothModeSpecificProduct", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInBothModeSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC59: HomePageZoneFeaturedCatRuleInBothModeDynamicProductByCatSubCat", groups= {"Redesign","P1"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInBothModeDynamicProductByCatSubCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC60: HomePageZoneFeaturedCatRuleInBothModeDynamicProductByType", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleInBothModeDynamicProductByType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC61: HomePageZoneFeaturedCatRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void HomePageZoneFeaturedCatRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.FEATURED_CATEGORIES,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC62: HomePageZone223RuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void HomePageZone223RuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC63: HomePageZone223RuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZone223RuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC64: HomePageZone223RuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void HomePageZone223RuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC65: HomePageZone223RuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZone223RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC66: HomePageZone223RuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void HomePageZone223RuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC67: HomePageZone223RuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void HomePageZone223RuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC68: HomePageZoneBackgroundAdvertRuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC69: HomePageZoneBackgroundAdvertRuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC70: HomePageZoneBackgroundAdvertRuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC71: HomePageZoneBackgroundAdvertRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC72: HomePageZoneBackgroundAdvertRuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC73: HomePageZoneBackgroundAdvertRuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneBackgroundAdvertRuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC74: HomePageZoneTextAdsRuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void HomePageZoneTextAdsRuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC75: HomePageZoneTextAdsRuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneTextAdsRuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC76: HomePageZoneTextAdsRuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void HomePageZoneTextAdsRuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC77: HomePageZoneTextAdsRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneTextAdsRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC78: HomePageZoneTextAdsRuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void HomePageZoneTextAdsRuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC79: HomePageZoneTextAdsRuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void HomePageZoneTextAdsRuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.HOME_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	/********************************** Product Landing Zone **********************************************************************/
	
	@Test(description="TC80: ProductLandingZoneTextAdsRuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC81:ProductLandingZoneTextAdsRuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC82: ProductLandingZoneTextAdsRuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC83: ProductLandingZoneTextAdsRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC84: ProductLandingZoneTextAdsRuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC85: ProductLandingZoneTextAdsRuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneTextAdsRuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC86: ProductLandingZone223RuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone223RuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC87:ProductLandingZone223RuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone223RuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC88: ProductLandingZone223RuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone223RuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC89: ProductLandingZone223RuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone223RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC90: ProductLandingZone223RuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void ProductLandingZone223RuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC91: ProductLandingZone223RuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone223RuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC92: ProductLandingZone497RuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone497RuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC93:ProductLandingZone497RuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone497RuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC94: ProductLandingZone497RuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone497RuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC95: ProductLandingZone497RuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone497RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC96: ProductLandingZone497RuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void ProductLandingZone497RuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC97: ProductLandingZone497RuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZone497RuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC98: ProductLandingZoneBackgroundAdvertRuleInLogInMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInLogInMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC99:ProductLandingZoneBackgroundAdvertRuleInLogInModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInLogInModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC100: ProductLandingZoneBackgroundAdvertRuleInLogOutMode", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInLogOutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC101: ProductLandingZoneBackgroundAdvertRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC102: ProductLandingZoneBackgroundAdvertRuleInBothMode", groups= {"Legacy","P1"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInBothMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC103: ProductLandingZoneBackgroundAdvertRuleInBothModeNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductLandingZoneBackgroundAdvertRuleInBothModeNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_LANDING_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC104: SearchPageRecommendedRuleInLogInModeSpecificProductLocGlobal", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC105: SearchPageRecommendedRuleInLogInModeDynamicProductLocCatProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC106: SearchPageRecommendedRuleInLogInModeSpecificProductLocVendorProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC107: SearchPageRecommendedRuleInLogInModeDynamicProductLocKeywordCat", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC108: SearchPageRecommendedRuleInLogInModeSpecificProductLocProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC109: SearchPageRecommendedRuleInLogInModeDynamicProductNegativeExactLoc", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC110: SearchPageRecommendedRuleInLogOutModeSpecificProductLocGlobal", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC111: SearchPageRecommendedRuleInLogOutModeDynamicProductLocKeyword", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC112: SearchPageRecommendedRuleInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC113: SearchPageRecommendedRuleInLogOutModeDynamicProductLocCatAndVendor", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC114: SearchPageRecommendedRuleInLogOutModeDynamicProductLocCatVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC115: SearchPageRecommendedRuleNegativeExpireRuleSpecificProduct", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC116: SearchPageRecommendedRuleInBothModeSpecificProductLocGlobal", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC117: SearchPageRecommendedRuleInBothModeDynamicProductLocKeyword", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC118: SearchPageRecommendedRuleInBothModeSpecificProductLocKeyworAndVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC119: SearchPageRecommendedRuleInBothModeDynamicProductLocCatSubCatVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC120: SearchPageRecommendedRuleInBothModeSpecificProductLocKeywordCat", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC121: SearchPageRecommendedRuleInBothModeSpecificProductLocCat", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC122: SearchPageRecommendedRuleInBothModeDynamicProductLocVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC123: SearchPageRecommendedRuleNegativeDeactiveRule", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPageRecommendedRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC124: SearchPageRecommendedRuleInBothModeDynamicProductNegativeExactLoc", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPageRecommendedRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC125: SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC126: SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC127: SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC128: SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC129: SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC130: SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC131: SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC132: SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC133: SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC134: SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC135: SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC136: SearchPageYouMayAlsoLikeRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC137: SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC138: SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC139: SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC140: SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC141: SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC142: SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC143: SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC144: SearchPageYouMayAlsoLikeRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC145: SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC146: SearchPageTextAdsRuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC147 : SearchPageTextAdsRuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC148: SearchPageTextAdsRuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC149: SearchPageTextAdsRuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC150: SearchPageTextAdsRuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC151: SearchPageTextAdsRuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC152: SearchPageTextAdsRuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC153: SearchPageTextAdsRuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC154: SearchPageTextAdsRuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC155: SearchPageTextAdsRuleInLogOutModeLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC156: SearchPageTextAdsRuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC157: SearchPageTextAdsRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC158: SearchPageTextAdsRuleInBothModeLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC159: SearchPageTextAdsRuleInBothModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC160: SearchPageTextAdsRuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC161: SearchPageTextAdsRuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC162: SearchPageTextAdsRuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC163: SearchPageTextAdsRuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC164: SearchPageTextAdsRuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC165: SearchPageTextAdsRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC166: SearchPageTextAdsRuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPageTextAdsRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}		
	
	@Test(description="TC167: SearchPage223RuleInLogInModeLocGlobal", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC168 : SearchPage223RuleInLogInModeLocCatProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC169: SearchPage223RuleInLogInModeLocVendorProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC170: SearchPage223RuleInLogInModeLocKeywordCat", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPage223RuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC171: SearchPage223RuleInLogInModeLocProdStatus", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC172: SearchPage223RuleInLogInModeNegativeExactLoc", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC173: SearchPage223RuleInLogOutModeLocGlobal", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC174: SearchPage223RuleInLogOutModeLocKeyword", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC175: SearchPage223RuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC176: SearchPage223RuleInLogOutModeLocCatAndVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC177: SearchPage223RuleInLogOutModeLocCatVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC178: SearchPage223RuleInLogOutModeNegativeExpireRule", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC179: SearchPage223RuleInBothModeLocGlobal", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPage223RuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC180: SearchPage223RuleInBothModeLocKeyword", groups= {"Legacy","Redesign","P1"},enabled=true)
	public void SearchPage223RuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC181: SearchPage223RuleInBothModeLocKeyworAndVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC182: SearchPage223RuleInBothModeLocCatSubCatVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC183: SearchPage223RuleInBothModeLocKeywordCat", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC184: SearchPage223RuleInBothModeLocCat", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC185: SearchPage223RuleInBothModeLocVendor", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC186: SearchPage223RuleNegativeDeactiveRule", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC187: SearchPage223RuleInBothModeNegativeExactLoc", groups= {"Legacy","Redesign"},enabled=true)
	public void SearchPage223RuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
		
	@Test(description="TC188: SearchPage497RuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC189 : SearchPage497RuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC190: SearchPage497RuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC191: SearchPage497RuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void SearchPage497RuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC192: SearchPage497RuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC193: SearchPage497RuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC194: SearchPage497RuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC195: SearchPage497RuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC196: SearchPage497RuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC197: SearchPage497RuleInLogOutModeLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void SearchPage497RuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC198: SearchPage497RuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC199: SearchPage497RuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC200: SearchPage497RuleInBothModeLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void SearchPage497RuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC201: SearchPage497RuleInBothModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC202: SearchPage497RuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC203: SearchPage497RuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC204: SearchPage497RuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC205: SearchPage497RuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC206: SearchPage497RuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC207: SearchPage497RuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC208: SearchPage497RuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPage497RuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC209: SearchPageBackgroundAdvertRuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC210 : SearchPageBackgroundAdvertRuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC211: SearchPageBackgroundAdvertRuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC212: SearchPageBackgroundAdvertRuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC213: SearchPageBackgroundAdvertRuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC214: SearchPageBackgroundAdvertRuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC215: SearchPageBackgroundAdvertRuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC216: SearchPageBackgroundAdvertRuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC217: SearchPageBackgroundAdvertRuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC218: SearchPageBackgroundAdvertRuleInLogOutModeLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC219: SearchPageBackgroundAdvertRuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC220: SearchPageBackgroundAdvertRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC221: SearchPageBackgroundAdvertRuleInBothModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC222: SearchPageBackgroundAdvertRuleInBothModeLocKeyword", groups= {"Legacy","P1"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC223: SearchPageBackgroundAdvertRuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC224: SearchPageBackgroundAdvertRuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC225: SearchPageBackgroundAdvertRuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC226: SearchPageBackgroundAdvertRuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC227: SearchPageBackgroundAdvertRuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC228: SearchPageBackgroundAdvertRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC229: SearchPageBackgroundAdvertRuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC230: SearchPageSponsoredProductsRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC231 : SearchPageSponsoredProductsRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC232: SearchPageSponsoredProductsRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC233: SearchPageSponsoredProductsRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC234: SearchPageSponsoredProductsRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC235: SearchPageSponsoredProductsRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC236: SearchPageSponsoredProductsRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC237: SearchPageSponsoredProductsRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC238: SearchPageSponsoredProductsRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC239: SearchPageSponsoredProductsRuleInLogOutModeLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC240: SearchPageSponsoredProductsRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC241: SearchPageSponsoredProductsRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC242: SearchPageSponsoredProductsRuleInBothModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC243: SearchPageSponsoredProductsRuleInBothModeLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC244: SearchPageSponsoredProductsRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC245: SearchPageSponsoredProductsRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC246: SearchPageSponsoredProductsRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC247: SearchPageSponsoredProductsRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC248: SearchPageSponsoredProductsRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC249: SearchPageSponsoredProductsRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC250: SearchPageSponsoredProductsRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC251: SearchPageTopBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC252 : SearchPageTopBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC253: SearchPageTopBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC254: SearchPageTopBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC255: SearchPageTopBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC256: SearchPageTopBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC257: SearchPageTopBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC258: SearchPageTopBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC259: SearchPageTopBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC260: SearchPageTopBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC261: SearchPageTopBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC262: SearchPageTopBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC263: SearchPageTopBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC264: SearchPageTopBannerRuleInBothModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC265: SearchPageTopBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC266: SearchPageTopBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC267: SearchPageTopBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC268: SearchPageTopBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC269: SearchPageTopBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC270: SearchPageTopBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC271: SearchPageTopBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageTopBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC272: SearchPageMiddleBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC273 : SearchPageMiddleBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC274: SearchPageMiddleBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC275: SearchPageMiddleBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC276: SearchPageMiddleBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC277: SearchPageMiddleBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC278: SearchPageMiddleBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC279: SearchPageMiddleBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC280: SearchPageMiddleBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC281: SearchPageMiddleBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC282: SearchPageMiddleBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC283: SearchPageMiddleBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC284: SearchPageMiddleBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC285: SearchPageMiddleBannerRuleInBothModeLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC286: SearchPageMiddleBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC287: SearchPageMiddleBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC288: SearchPageMiddleBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC289: SearchPageMiddleBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC290: SearchPageMiddleBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC291: SearchPageMiddleBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC292: SearchPageMiddleBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC293: SearchPageBottomBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC294 : SearchPageBottomBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC295: SearchPageBottomBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC296: SearchPageBottomBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC297: SearchPageBottomBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC298: SearchPageBottomBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC299: SearchPageBottomBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC300: SearchPageBottomBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC301: SearchPageBottomBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC302: SearchPageBottomBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC303: SearchPageBottomBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC304: SearchPageBottomBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC305: SearchPageBottomBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC306: SearchPageBottomBannerRuleInBothModeLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC307: SearchPageBottomBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC308: SearchPageBottomBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC309: SearchPageBottomBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC310: SearchPageBottomBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC311: SearchPageBottomBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC312: SearchPageBottomBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC313: SearchPageBottomBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageBottomBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC314: SearchPageSkyScraperBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC315 : SearchPageSkyScraperBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC316: SearchPageSkyScraperBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC317: SearchPageSkyScraperBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC318: SearchPageSkyScraperBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC319: SearchPageSkyScraperBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC320: SearchPageSkyScraperBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC321: SearchPageSkyScraperBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC322: SearchPageSkyScraperBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC323: SearchPageSkyScraperBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC324: SearchPageSkyScraperBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC325: SearchPageSkyScraperBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC326: SearchPageSkyScraperBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC327: SearchPageSkyScraperBannerRuleInBothModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC328: SearchPageSkyScraperBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC329: SearchPageSkyScraperBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC330: SearchPageSkyScraperBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC331: SearchPageSkyScraperBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC332: SearchPageSkyScraperBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC333: SearchPageSkyScraperBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC334: SearchPageSkyScraperBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SKYSCRAPPER_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	/*******************************************Product Detail Page ***************************************************/
	
	@Test(description="TC3335: ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC336: ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC337: ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC338: ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC339: ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC340: ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC341: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC342: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC343: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC344: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC345: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC346: ProductDetailPageYouMayAlsoLikeRuleNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC347: ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC348: ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC349: ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC350: ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC351: ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC352: ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC353: ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC354: ProductDetailPageYouMayAlsoLikeRuleNegativeDeactiveRule", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC355: ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC356: ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC357: ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC358: ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC359: ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC360: ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC361: ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC362: ProductDetailPagePeopleAlsoBoughtInLogOutModeSpecificProductLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC363: ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC364: ProductDetailPagePeopleAlsoBoughtInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC365: ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC366: ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC367: ProductDetailPagePeopleAlsoBoughtNegativeExpireRuleSpecificProduct", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC368: ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC369: ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC370: ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC371: ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC372: ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC373: ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC374: ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC375: ProductDetailPagePeopleAlsoBoughtNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC376: ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC377: ProductDetailPagePPRRuleInLogInModeSpecificProductLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC378: ProductDetailPagePPRRuleInLogInModeDynamicProductLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC379: ProductDetailPagePPRRuleInLogInModeSpecificProductLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC380: ProductDetailPagePPRRuleInLogInModeDynamicProductLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC381: ProductDetailPagePPRRuleInLogInModeSpecificProductLocProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC382: ProductDetailPagePPRRuleInLogInModeDynamicProductNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC383: ProductDetailPagePPRRuleInLogOutModeSpecificProductLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC384: ProductDetailPagePPRRuleInLogOutModeDynamicProductLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC385: ProductDetailPagePPRRuleInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC386: ProductDetailPagePPRRuleInLogOutModeDynamicProductLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC387: ProductDetailPagePPRRuleInLogOutModeDynamicProductLocCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC388: ProductDetailPagePPRRuleNegativeExpireRuleSpecificProduct", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC389: ProductDetailPagePPRRuleInBothModeSpecificProductLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC390: ProductDetailPagePPRRuleInBothModeDynamicProductLocKeyword", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC391: ProductDetailPagePPRRuleInBothModeSpecificProductLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC392: ProductDetailPagePPRRuleInBothModeDynamicProductLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC393: ProductDetailPagePPRRuleInBothModeSpecificProductLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC394: ProductDetailPagePPRRuleInBothModeSpecificProductLocCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC395: ProductDetailPagePPRRuleInBothModeDynamicProductLocVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC396: ProductDetailPagePPRRuleNegativeDeactiveRule", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPRRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC397: ProductDetailPagePPRRuleInBothModeDynamicProductNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC398: ProductDetailPagePPCRuleInLogInModeSpecificProductLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC399: ProductDetailPagePPCRuleInLogInModeDynamicProductLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeDynamicProductLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC400: ProductDetailPagePPCRuleInLogInModeSpecificProductLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeSpecificProductLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC401: ProductDetailPagePPCRuleInLogInModeDynamicProductLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeDynamicProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC402: ProductDetailPagePPCRuleInLogInModeSpecificProductLocProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeSpecificProductLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC403: ProductDetailPagePPCRuleInLogInModeDynamicProductNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};	
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC404: ProductDetailPagePPCRuleInLogOutModeSpecificProductLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC405: ProductDetailPagePPCRuleInLogOutModeDynamicProductLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC406: ProductDetailPagePPCRuleInLogOutModeSpecificProductLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeSpecificProductLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC407: ProductDetailPagePPCRuleInLogOutModeDynamicProductLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeDynamicProductLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC408: ProductDetailPagePPCRuleInLogOutModeDynamicProductLocCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeDynamicProductLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		String arrBOProducts[]= {PRODUCTS_BY_PRODUCT_TYPE};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC409: ProductDetailPagePPCRuleNegativeExpireRuleSpecificProduct", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleNegativeExpireRuleSpecificProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC410: ProductDetailPagePPCRuleInBothModeSpecificProductLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeSpecificProductLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC411: ProductDetailPagePPCRuleInBothModeDynamicProductLocKeyword", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeDynamicProductLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC412: ProductDetailPagePPCRuleInBothModeSpecificProductLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeSpecificProductLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC413: ProductDetailPagePPCRuleInBothModeDynamicProductLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeDynamicProductLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_VENDOR_NAME};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC414: ProductDetailPagePPCRuleInBothModeSpecificProductLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeSpecificProductLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC415: ProductDetailPagePPCRuleInBothModeSpecificProductLocCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeSpecificProductLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC416: ProductDetailPagePPCRuleInBothModeDynamicProductLocVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeDynamicProductLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		String arrBOProducts[]= {PRODUCTS_BY_CATEGORY,PRODUCTS_BY_SUB_CATEGORY};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_DYNAMIC);	
		boOptions.setLocationsTypeWithProductsSelectionType(arrBOLocations,arrBOProducts);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC417: ProductDetailPagePPCRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC418: ProductDetailPagePPCRuleInBothModeDynamicProductNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPagePPCRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC419: ProductDetailPageTextAdsRuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC420 : ProductDetailPageTextAdsRuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC421: ProductDetailPageTextAdsRuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC422: ProductDetailPageTextAdsRuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC423: ProductDetailPageTextAdsRuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC424: ProductDetailPageTextAdsRuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC425: ProductDetailPageTextAdsRuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC426: ProductDetailPageTextAdsRuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC427: ProductDetailPageTextAdsRuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC428: ProductDetailPageTextAdsRuleInLogOutModeLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC429: ProductDetailPageTextAdsRuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC430: ProductDetailPageTextAdsRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC431: ProductDetailPageTextAdsRuleInBothModeLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC432: ProductDetailPageTextAdsRuleInBothModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC433: ProductDetailPageTextAdsRuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC434: ProductDetailPageTextAdsRuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC435: ProductDetailPageTextAdsRuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC436: ProductDetailPageTextAdsRuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC437: ProductDetailPageTextAdsRuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC438: ProductDetailPageTextAdsRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC439: ProductDetailPageTextAdsRuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC440: ProductDetailPage223RuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC441 : ProductDetailPage223RuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC442: ProductDetailPage223RuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC443: ProductDetailPage223RuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC444: ProductDetailPage223RuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC445: ProductDetailPage223RuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC446: ProductDetailPage223RuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC447: ProductDetailPage223RuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC448: ProductDetailPage223RuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC449: ProductDetailPage223RuleInLogOutModeLocCatAndVendor", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC450: ProductDetailPage223RuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC451: ProductDetailPage223RuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC452: ProductDetailPage223RuleInBothModeLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC453: ProductDetailPage223RuleInBothModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC454: ProductDetailPage223RuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC455: ProductDetailPage223RuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC456: ProductDetailPage223RuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC457: ProductDetailPage223RuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC458: ProductDetailPage223RuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC459: ProductDetailPage223RuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC460: ProductDetailPage223RuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPage223RuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC461: ProductDetailPageBackgroundAdvertRuleInLogInModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC462 : ProductDetailPageBackgroundAdvertRuleInLogInModeLocCatProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC463: ProductDetailPageBackgroundAdvertRuleInLogInModeLocVendorProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC464: ProductDetailPageBackgroundAdvertRuleInLogInModeLocKeywordCat", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC465: ProductDetailPageBackgroundAdvertRuleInLogInModeLocProdStatus", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC466: ProductDetailPageBackgroundAdvertRuleInLogInModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC467: ProductDetailPageBackgroundAdvertRuleInLogOutModeLocGlobal", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC468: ProductDetailPageBackgroundAdvertRuleInLogOutModeLocKeyword", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC469: ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatSubCatProdType", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC470: ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC471: ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC472: ProductDetailPageBackgroundAdvertRuleInLogOutModeNegativeExpireRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC473: ProductDetailPageBackgroundAdvertRuleInBothModeLocGlobal", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC474: ProductDetailPageBackgroundAdvertRuleInBothModeLocKeyword", groups= {"Legacy","P1"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC475: ProductDetailPageBackgroundAdvertRuleInBothModeLocKeyworAndVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC476: ProductDetailPageBackgroundAdvertRuleInBothModeLocCatSubCatVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC477: ProductDetailPageBackgroundAdvertRuleInBothModeLocKeywordCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC478: ProductDetailPageBackgroundAdvertRuleInBothModeLocCat", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC479: ProductDetailPageBackgroundAdvertRuleInBothModeLocVendor", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC480: ProductDetailPageBackgroundAdvertRuleNegativeDeactiveRule", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC481: ProductDetailPageBackgroundAdvertRuleInBothModeNegativeExactLoc", groups= {"Legacy"},enabled=true)
	public void ProductDetailPageBackgroundAdvertRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC482: ProductDetailPageMiddleBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC483 : ProductDetailPageMiddleBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC484: ProductDetailPageMiddleBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC485: ProductDetailPageMiddleBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC486: ProductDetailPageMiddleBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC487: ProductDetailPageMiddleBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC488: ProductDetailPageMiddleBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC489: ProductDetailPageMiddleBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC490: ProductDetailPageMiddleBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC491: ProductDetailPageMiddleBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC492: ProductDetailPageMiddleBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC493: ProductDetailPageMiddleBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC494: ProductDetailPageMiddleBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC495: ProductDetailPageMiddleBannerRuleInBothModeLocKeyword", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC496: ProductDetailPageMiddleBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC497: ProductDetailPageMiddleBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC498: ProductDetailPageMiddleBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC499: ProductDetailPageMiddleBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC500: ProductDetailPageMiddleBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC501: ProductDetailPageMiddleBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC502: ProductDetailPageMiddleBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	@Test(description="TC503: ProductDetailPageBottomBannerRuleInLogInModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC504 : ProductDetailPageBottomBannerRuleInLogInModeLocCatProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocCatProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);			
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC505: ProductDetailPageBottomBannerRuleInLogInModeLocVendorProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocVendorProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC506: ProductDetailPageBottomBannerRuleInLogInModeLocKeywordCat", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = PRODUCTS_BY_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC507: ProductDetailPageBottomBannerRuleInLogInModeLocProdStatus", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocProdStatus() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_PRODUCT_STATUS;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC508: ProductDetailPageBottomBannerRuleInLogInModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC509: ProductDetailPageBottomBannerRuleInLogOutModeLocGlobal", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC510: ProductDetailPageBottomBannerRuleInLogOutModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC511: ProductDetailPageBottomBannerRuleInLogOutModeLocCatSubCatProdType", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocCatSubCatProdType() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_PRODUCT_TYPE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC512: ProductDetailPageBottomBannerRuleInLogOutModeLocCatAndVendor", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocCatAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC513: ProductDetailPageBottomBannerRuleInLogOutModeLocCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC514: ProductDetailPageBottomBannerRuleInLogOutModeNegativeExpireRule", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeNegativeExpireRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setEndDateExpire(); // End Date expire
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC515: ProductDetailPageBottomBannerRuleInBothModeLocGlobal", groups= {"Redesign","P1"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocGlobal() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC516: ProductDetailPageBottomBannerRuleInBothModeLocKeyword", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocKeyword() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);		
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC517: ProductDetailPageBottomBannerRuleInBothModeLocKeyworAndVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocKeyworAndVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD_SKU+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;
		String sLocation1 = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC518: ProductDetailPageBottomBannerRuleInBothModeLocCatSubCatVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocCatSubCatVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_SUB_CATEGORY+Constants.SEPARATOR_BY_COMMA+LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC519: ProductDetailPageBottomBannerRuleInBothModeLocKeywordCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocKeywordCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_KEYWORD+Constants.SEPARATOR_BY_COMMA+LOC_MATCHMODE_PHRASE;;
		String sLocation1 = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation,sLocation1};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC520: ProductDetailPageBottomBannerRuleInBothModeLocCat", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocCat() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_CATEGORY;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC521: ProductDetailPageBottomBannerRuleInBothModeLocVendor", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeLocVendor() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC522: ProductDetailPageBottomBannerRuleNegativeDeactiveRule", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleNegativeDeactiveRule() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setActive(false); //deactive rule
		boOptions.setLocationsType(arrBOLocations);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC523: ProductDetailPageBottomBannerRuleInBothModeNegativeExactLoc", groups= {"Redesign"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeDynamicProductNegativeExactLoc() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sLocation = LOC_VENDOR_NAME+Constants.SEPARATOR_BY_COMMA+LOC_EXACT_LOCATION;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);		
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setLocationsType(arrBOLocations);
		boOptions.setNegativeExactLocation();
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
	
	/**************Fields Level Test Case ****************************************************************************************/
	
	@Test(description="TC524: SearchPageRecommendedRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void SearchPageRecommendedRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC525: SearchPageRecommendedRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageRecommendedRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC526: SearchPageRecommendedRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageRecommendedRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC527: SearchPageSponsoredProductsRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPageSponsoredProductsRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC528: SearchPageSponsoredProductsRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void SearchPageSponsoredProductsRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC529: SearchPageSponsoredProductsRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageSponsoredProductsRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.SPONSORED_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC530: SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();	
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC531: SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC532: SearchPageYouMayAlsoLikeRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void SearchPageYouMayAlsoLikeRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC533: SearchPageTextAdsRuleInLogInModeLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void SearchPageTextAdsRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC534: SearchPageTextAdsRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageTextAdsRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC535: SearchPageTextAdsRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageTextAdsRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC536: SearchPage223RuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPage223RuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC537: SearchPage223RuleInLogOutModeLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void SearchPage223RuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC538: SearchPage223RuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPage223RuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC539: SearchPage497RuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPage497RuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC540: SearchPage497RuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPage497RuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC541: SearchPage497RuleInBothModeModeLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void SearchPage497RuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_497_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC542: SearchPageBackgroundAdvertRuleInLogInModeLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC543: SearchPageBackgroundAdvertRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageBackgroundAdvertRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC544: SearchPageBackgroundAdvertRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageBackgroundAdvertRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC545: SearchPageTopBannerRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPageTopBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC546: SearchPageTopBannerRuleInLogOutModeLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void SearchPageTopBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC547: SearchPageTopBannerRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageTopBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TOP_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC548: SearchPageMiddleBannerRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPageMiddleBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC549: SearchPageMiddleBannerRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageMiddleBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC550: SearchPageMiddleBannerRuleInBothModeModeLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void SearchPageMiddleBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC551: SearchPageBottomBannerRuleInLogInModeLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void SearchPageBottomBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC552: SearchPageBottomBannerRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void SearchPageBottomBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC553: SearchPageBottomBannerRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageBottomBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC554: SearchPageSkyScraperBannerRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC555: SearchPageSkyScraperBannerRuleInLogOutModeLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void SearchPageSkyScraperBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC556: SearchPageSkyScraperBannerRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void SearchPageSkyScraperBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC557: ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();	
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC558: ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC559: ProductDetailPageYouMayAlsoLikeRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPageYouMayAlsoLikeRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC560: ProductDetailPagePeopleAlsoBoughtRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();	
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC561: ProductDetailPagePeopleAlsoBoughtRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC562: ProductDetailPagePeopleAlsoBoughtRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePeopleAlsoBoughtRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC563: ProductDetailPagePPRRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePPRRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();	
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC564: ProductDetailPagePPRRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePPRRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC565: ProductDetailPagePPRRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void ProductDetailPagePPRRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC566: ProductDetailPagePPCRuleInLogInModeSpecificProductLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void ProductDetailPagePPCRuleInLogInModeSpecificProductLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();	
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC567: ProductDetailPagePPCRuleInLogOutModeSpecificProductLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePPCRuleInLogOutModeSpecificProductLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC568: ProductDetailPagePPCRuleInBothModeModeSpecificProductLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPagePPCRuleInBothModeModeSpecificProductLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_KEYWORD_SKU+","+LOC_KEYWORD_SKU;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,objTestData);
		boOptions.setProductSelection(PRODUCT_SELECTION_BY_SPECIFIC);	
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC569: ProductDetailPageTextAdsRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPageTextAdsRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC570: ProductDetailPageTextAdsRuleInLogOutModeLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void ProductDetailPageTextAdsRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC571: ProductDetailPageTextAdsRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPageTextAdsRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.TEXTADS,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC572: ProductDetailPage223RuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPage223RuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC573: ProductDetailPage223RuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPage223RuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC574: ProductDetailPage223RuleInBothModeModeLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void ProductDetailPage223RuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.AD_223_WIDTH,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC575: ProductDetailPagebackgroundAdvertRuleInLogInModeLocGlobalFieldsCategory", groups={"P1"},enabled=true)
	public void ProductDetailPagebackgroundAdvertRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC576: ProductDetailPagebackgroundAdvertRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPagebackgroundAdvertRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC577: ProductDetailPagebackgroundAdvertRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPagebackgroundAdvertRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BACKGROUND_ADVERT,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC578: ProductDetailPageMiddleBannerRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC579: ProductDetailPageMiddleBannerRuleInLogOutModeLocGlobalFieldsVendor", groups={"P1"},enabled=true)
	public void ProductDetailPageMiddleBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC580: ProductDetailPageMiddleBannerRuleInBothModeModeLocGlobalFieldsSku", groups="API BackOffice",enabled=true)
	public void ProductDetailPageMiddleBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.MIDDLE_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC581: ProductDetailPageBottomBannerRuleInLogInModeLocGlobalFieldsCategory", groups="API BackOffice",enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogInModeLocGlobalFieldsCategory() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};			
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC582: ProductDetailPageBottomBannerRuleInLogOutModeLocGlobalFieldsVendor", groups="API BackOffice",enabled=true)
	public void ProductDetailPageBottomBannerRuleInLogOutModeLocGlobalFieldsVendor() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_VENDOR_NAME+","+LOC_VENDOR_NAME;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};		
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC583: ProductDetailPageBottomBannerRuleInBothModeModeLocGlobalFieldsSku", groups={"P1"},enabled=true)
	public void ProductDetailPageBottomBannerRuleInBothModeModeLocGlobalFieldsSku() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		String sFieldValue = LOC_CATEGORY+","+LOC_CATEGORY;
		String sLocation = LOC_GLOBAL;
		String arrBOLocations[]= {sLocation};
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.PRODUCT_DETAILS_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.BOTTOM_BANNER,objTestData);
		boOptions.setLocationsType(arrBOLocations);	
		boOptions.setTriggerMode(LOGIN_AND_LOGOUT_TRIGGER_MODE);
		boOptions.setFieldsLevelName(sFieldValue);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver, boOptions, hmTestData);
		objAction.createRuleAndPerformResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	/*-----------------------------------------------------------------------------------*/
	
	
	
	
	@Test(description="TC584: DELETE ALL THE RULE", groups="API BackOffice",enabled=true)
	public void DeleteAllRules() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		//---------------Actual TC starts-----
		BackOfficeRuleCreateRegister boOptions = new BackOfficeRuleCreateRegister(BackOfficeMerchandisingEnums.PageZone.SEARCH_PAGE,
				BackOfficeMerchandisingEnums.BackOfficePlacementType.RECOMMENDED_PRODUCTS,objTestData);
		BackofficeMerchandisingAPIActionBucket objAction = new BackofficeMerchandisingAPIActionBucket(objAPIDriver,boOptions, hmTestData);
		objAction.deleteAllRules();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	
	
	/* 
	 **************Factory code starts********** 
	 */
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelElasticFileName=null,sTabName = null,sExcelBaackofficeFileName=null;
		
		String sSheetName=System.getProperty("Environment").trim();	
		sExcelElasticFileName=Constants.BASEPATH+"\\TestData\\"+ElasticTestDataFile;
		sExcelBaackofficeFileName=Constants.BASEPATH+"\\TestData\\"+BackofficeMerchandisingTestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);

		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObjectForBackOffice(sExcelElasticFileName, sTabName, listTCMappingToCountry, BackofficeMerchandisingTestDataUtil.class,sExcelBaackofficeFileName);	
			//read the excel backoffice
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					BackofficeMerchandisingTestDataUtil objCPSTD = (BackofficeMerchandisingTestDataUtil) myData2Dim[i][1];					
					BackofficeMerchandisingRuleAPITest newInstance=new BackofficeMerchandisingRuleAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

