package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.PNImicroServiceActionBucket;
import com.im.imonline.api.constants.PNImicroserviceAPIConstants;

public class PNImicroServiceAPITest extends BaseTest {

	public final static String ModuleNameInTestApplicability="PNImicroService";
	public final static String TestDataFile="TestDataForPNImicroService.xlsx";
	public final static boolean isMultiSet = true;
	public final static String sFileName="PNImicroService";
	public static List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());

	public PNImicroServiceAPITest(HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) throws Exception {
		super(pExcelHMap,pConfigHMap,ModuleNameInTestApplicability);
	}

	@Test(description="TC01: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Only Std Spc Price]", enabled=true)
	public void validateMicroServiceResponse_OnlyStdSpcPrice() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE, PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE, PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify);
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC02: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Only QB]", enabled=true)
	public void validateMicroServiceResponse_OnlyQB() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED, PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC03: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Only ACOP]", enabled=true)
	public void validateMicroServiceResponse_OnlyACOP() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC04: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Only VD]", enabled=true)
	public void validateMicroServiceResponse_OnlyVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC05: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [ACOP and VD]", enabled=true)
	public void validateMicroServiceResponse_ACOPnVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC06: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Std Special Price + ACOP + VD]", enabled=true)
	public void validateMicroServiceResponse_StdSpcPricenACOPnVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC07: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Standard Special Price and VD]", enabled=true)
	public void validateMicroServiceResponse_StdSpcPricenVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC08: [PNI1-95]Validating special pricing flag and Details in PNI-MicroService - On MultiSKUs", enabled=true)
	public void validateMicroServiceResponseOnMultiSKU() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "All MultiSKU for [ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC09: [PNI1-121]Validating on Exclusion of $0 ACOPs in Special Pricing Detail calculations", enabled=true)
	public void validateSpecialPrice_Exclude0DollarACOP() throws Exception {


		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);


		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
			String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
			
			String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
			objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
			
			
			
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC10: [PNI1-25/ECOMM 6282]Validating special pricing flag and Details in PNI-MicroService [Only EOD]", enabled=true)
	public void validateMicroServiceResponse_OnlyEOD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}

	@Test(description="TC11: [PNI1-25]Validating special pricing flag and Details in PNI-MicroService [Only ECOM]", enabled=true)
	public void validateMicroServiceResponse_OnlyECOM() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC12: [PNI1-25/ECOMM 6282]Validating special pricing flag and Details in PNI-MicroService [Only EODVD]", enabled=true)
	public void validateMicroServiceResponse_OnlyEODVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC13: [PNI1-25/ECOMM 6282]Validating special pricing flag and Details in PNI-MicroService [ECOM + EOD]", enabled=true)
	public void validateMicroServiceResponse_OnlyECOMnEOD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC14: [PNI1-25/ECOMM 6282]Validating special pricing flag and Details in PNI-MicroService [Std Special Price + EOD + QB]", enabled=true)
	public void validateMicroServiceResponse_StdSpcPricenEODnQB() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	
	@Test(description="TC15:Special Price microservice validation for US", enabled=true)
	public void validateSP_AllACOPS() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

	
		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@Test(description="TC16: [PNI1-25/ECOMM 6282]Validating special pricing flag and Details in PNI-MicroService [Std Special Price + ECOM]", enabled=true)
	public void validateMicroServiceResponse_StdSpcPricenECOM() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+":  "+hmTestData.get(Constants.SKU_NUMBER)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		PNImicroServiceActionBucket objPNImicroServiceActionBucket = new PNImicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrSpcPriceFlagValuesToVerify = new String[] {PNImicroserviceAPIConstants.HASSPECIALPRICING, PNImicroserviceAPIConstants.ISSTDSPECIALPRICE, PNImicroserviceAPIConstants.HASWEBDISCOUNT, PNImicroserviceAPIConstants.ISACOPAPPLIEDTOPRICE,PNImicroserviceAPIConstants.ISQUANTITYBREAKSAVAILABLE,PNImicroserviceAPIConstants.ISEODACOPAPPLIED,PNImicroserviceAPIConstants.ISACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISEODACOPSPECIALPRICEAPPLIED,PNImicroserviceAPIConstants.ISEODACOPVOLUMEDISCOUNTAPPLIED,PNImicroserviceAPIConstants.ISSTDQUANTITYBREAKAPPLIED};
		String[] arrSpcPriceDiscountDetailsToVerify = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE, PNImicroserviceAPIConstants.DISCOUNT, PNImicroserviceAPIConstants.MINQUANTITY, PNImicroserviceAPIConstants.QUANTITYLIMIT, PNImicroserviceAPIConstants.EXPIRYDATE};
		
		String[] arrSpcPriceDiscountDetailsToVerify_AllACOPs = new String[] {PNImicroserviceAPIConstants.ORIGINALPRICE_EACHACOPAPPLIED, PNImicroserviceAPIConstants.DISCOUNT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.MINQUANTITY_EACHACOPAPPLIED, PNImicroserviceAPIConstants.QUANTITYLIMIT_EACHACOPAPPLIED, PNImicroserviceAPIConstants.EXPIRYDATE_EACHACOPAPPLIED};
		objPNImicroServiceActionBucket.validatePNIMicroServiceResponse_withAllACOPs(arrSpcPriceFlagValuesToVerify, arrSpcPriceDiscountDetailsToVerify, arrSpcPriceDiscountDetailsToVerify_AllACOPs);
		
		
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();      
	}
	
	


	/* 
	 **************Factory code starts********** 
	 */

	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim= 	new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){

					@SuppressWarnings("unchecked")
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					HashMap<String,String> pConfigHMap = new ReadExcelFile().readConfigSheetforRunConfig
							(sExcelFileName,RunConfig.getProperty(Constants.ConfigSheetName),pExcelHMap.get(Constants.ExcelHeaderRunConfig));

					//ChangeClass
					PNImicroServiceAPITest newInstance=new PNImicroServiceAPITest(pExcelHMap,pConfigHMap);

					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}



	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);	
	}

}
