package com.im.imonline.api.tests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.common.Util;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.E2EHermesAPIFlowActionBucket;
import com.im.imonline.api.business.APIEnumerations.CarrierOptions;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.OrderProcessOptionsForHeldOrder;
import com.im.imonline.api.business.APIEnumerations.PaymentTypes;
import com.im.imonline.api.business.APIEnumerations.ShipToAndBillToAddressOptions;
import com.im.imonline.api.business.APIEnumerations.WarehouseTypes;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.business.HermesE2ERegister;
import com.im.imonline.api.business.OrderUnit;
import com.im.imonline.api.testdata.util.E2EHermesAPITestDataUtil;


public class E2EHermesAPIFlowTest extends BaseTest{
	static Logger logger = Logger.getLogger("E2EHermesAPIFlowTest");
	public final static String ModuleNameInTestApplicability="E2EHermesAPIFlow";
	public final static String TestDataFile="TestDataForE2EHermesAPIFlow.xlsx";
	public final static boolean isMultiSet = false; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	E2EHermesAPITestDataUtil objTestData = null;

	public E2EHermesAPIFlowTest(HashMap<String, String> pExcelHMap, E2EHermesAPITestDataUtil objCPSTD) throws Exception {
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objCPSTD;
		objTestData.setCountryGroups(countryGroup);
	}
	
	@Test(description="TC01: orderWithInStockProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = {new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC02: orderWithDirectship2.0Product", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithDirectship2Product() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.DIRECTSHIP_2, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC03: orderWithESDProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithESDProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.DOWNLOADABLE, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC04: orderWithDirectshipProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithDirectshipProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.DIRECTSHIP, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC05: orderWithInstockProductSplitWarehouse", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInstockProductSplitWarehouse() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1),
									new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setWarehouseType(WarehouseTypes.SPLIT);		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC06: orderWithInStockAndESDProducts", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockAndESDProducts() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1),
									new OrderUnit(FlagOptions_Hermes.DOWNLOADABLE, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC07: orderWithInStockAndDirectshipProducts", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockAndDirectshipProducts() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1),
									new OrderUnit(FlagOptions_Hermes.DIRECTSHIP, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}

	@Test(description="TC08: orderWithESDAndDirectshipProducts", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithESDAndDirectshipProducts() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.DOWNLOADABLE, 1),
									new OrderUnit(FlagOptions_Hermes.DIRECTSHIP, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC09: orderWithInStockESDAndDirectshipProducts", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockESDAndDirectshipProducts() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1),
									new OrderUnit(FlagOptions_Hermes.DOWNLOADABLE, 1),
									new OrderUnit(FlagOptions_Hermes.DIRECTSHIP, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC10: cancelNormalHoldOrder", groups="E2E Hermes API Flow",enabled=true)
	public void cancelNormalHoldOrder() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.CANCEL);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.holdOrderAndProcessPerOrderProcessOptionForHeldOrder(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC11: modifyShippingAddressAndReleaseNormalHoldOrder", groups="E2E Hermes API Flow",enabled=true)
	public void modifyShippingAddressAndReleaseNormalHoldOrder() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.MODIFYSHIPPINGADDRESSANDRELEASE);
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.holdOrderAndProcessPerOrderProcessOptionForHeldOrder(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC12: modifyCarrierAndReleaseNormalHoldOrder", groups="E2E Hermes API Flow",enabled=true)
	public void modifyCarrierAndReleaseNormalHoldOrder() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.MODIFYCARRIERANDRELEASE);
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.holdOrderAndProcessPerOrderProcessOptionForHeldOrder(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC13: releaseNormalHoldOrder", groups="E2E Hermes API Flow",enabled=true)
	public void releaseNormalHoldOrder() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.RELEASEWITHOUTMODIFY);
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.holdOrderAndProcessPerOrderProcessOptionForHeldOrder(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC14: orderWithDifferentShippingAddress", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithDifferentShippingAddress() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setShipToAndBillToAddressType(ShipToAndBillToAddressOptions.SELECT_DIFFERENT_SHIPTO);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC15: orderWithDifferentBillingAddress", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithDifferentBillingAddress() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setShipToAndBillToAddressType(ShipToAndBillToAddressOptions.SELECT_DIFFERENT_BILLTO);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC16: orderWithBackorderProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithBackorderProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.BACKORDER, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC17: orderWithNonDefaultDeliveryService", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNonDefaultDeliveryService() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setDeliveryService(CarrierOptions.DIFFCHOICE);
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC18: orderWithCartNoteLineNoteEndCustPO", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithCartNoteLineNoteEndCustPO() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		String sCountryCode=objTestData.getRequestData().get("RunConfig");
		String sTimeStamp = new SimpleDateFormat("MMddHH").format(Calendar.getInstance().getTime());
		String sRandomNo = String.valueOf(Util.getRandomNumberInRange(1, 10));		
		
		hOption.setEndUserPONumber(sCountryCode+sTimeStamp+sRandomNo);
		hOption.setBasketNote(sCountryCode +"_TestCartNote");
		hOption.setLineNotes(sCountryCode +"_TestCartLineNote");
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC19: orderWithNewProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNewProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.NEW, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC20: orderWithRefurbishedProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithRefurbishedProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.REFURBISHED, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC21: orderWithNoReturnProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNoReturnProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.NORETURNS, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC22: orderWithFreeItemProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithFreeItemProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.FREEITEM, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	
	/*@Test(description="TC23: orderWithNormalProductPaymentTypeFlooring", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNormalProductPaymentTypeFlooring() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setPaymentType(PaymentTypes.FLOORING);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}*/
	
	
/*	@Test(description="TC24: orderWithJtypeProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithJtypeProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.JTYPE, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC25: orderWithAppleCareWarrantySKU", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithAppleCareWarrantySKU() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.WARRANTY, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC26: orderWithInStockQuantityDiscounts", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockQuantityDiscounts() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.QUANTITYBREAK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}*/
	
	
	@Test(description="TC27: orderWithInStockPromotionSKUs", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithInStockPromotionSKUs() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		OrderUnit [] productList = { new OrderUnit(FlagOptions_Hermes.PROMOTION, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	
	
	@Test(description="TC28: orderWithBundleProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithBundleProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.BUNDLE, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC29: orderWithWebOnlyPriceProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithWebOnlyPriceProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.ACOP_WEBONLYPRICE, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC30: orderWithHeavyWeightProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithHeavyWeightProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.HEAVYWEIGHT, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC31: orderWithDiscontinuedProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithDiscontinuedProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.DISCONTINUED, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	
	@Test(description="TC32: orderWithNormalProductPaymentTypeTerms", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNormalProductPaymentTypeTerms() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	

	@Test(description="TC33: orderWithEndUserProduct", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithEndUserProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.ENDUSER, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	@Test(description="TC34: orderWithNormalProductPaymentTypeCreditCard", groups="E2E Hermes API Flow",enabled=true)
	public void orderWithNormalProductPaymentTypeCreditCard() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.INSTOCK, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
		hOption.setPaymentType(PaymentTypes.CREDITCARD);
		
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	/*@Test(description="TC35: OrderWithSingleUseEndUser", groups="E2E Hermes API Flow",enabled=true)
	public void OrderWithSingleUseEndUser() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.SINGLEUSEENDUSER, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
	
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	*/
	@Test(description="TC36: OrderWithLicensingSKUForSAP", groups="E2E Hermes API Flow",enabled=true)
	public void OrderWithLicensingSKUForSAP() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		
		OrderUnit []productList = { new OrderUnit(FlagOptions_Hermes.LICENCEPRODUCTS, 1)};
		HermesE2ERegister hOption=new HermesE2ERegister(productList);
	
		HermesE2EFlowRequestConfig objConfig= new HermesE2EFlowRequestConfig(objTestData,hOption,objAPIDriver);
		
		E2EHermesAPIFlowActionBucket action = new E2EHermesAPIFlowActionBucket(objConfig);
		action.submitOrderAndVerifyOrderDetails();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll(); 
	}
	
	/* 
	 **************Factory code starts********** 
	 */
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry, E2EHermesAPITestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					E2EHermesAPITestDataUtil objCPSTD = (E2EHermesAPITestDataUtil) myData2Dim[i][1];					
					E2EHermesAPIFlowTest newInstance=new E2EHermesAPIFlowTest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}