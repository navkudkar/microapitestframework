package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.PNI_QBmicroServiceActionBucket;
import com.im.imonline.api.constants.PNI_QBmicroserviceAPIConstants;

public class PNI_QBmicroServiceAPITest extends BaseTest {

	public final static String ModuleNameInTestApplicability="PNI_QBmicroservice";
	public final static String TestDataFile="TestDataForPNI_QBmicroService.xlsx";
	public final static boolean isMultiSet = true;
	public final static String sFileName="PNI_QBmicroservice";
	public static List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());

	public PNI_QBmicroServiceAPITest(HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) throws Exception {
		super(pExcelHMap,pConfigHMap,ModuleNameInTestApplicability);
	}

	@Test(description="TC01: [PNI1-5]Validating Quantity Break flag and Details in PNI-QBMicroService Only QB", enabled=true)
	public void validate_QBMicroServiceResponse_OnlyQB() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);		

		// Actual code starts here

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		PNI_QBmicroServiceActionBucket objPNI_QBmicroServiceActionBucket = new PNI_QBmicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrQuantityDiscountBreakDetailsToVerify = new String[] {PNI_QBmicroserviceAPIConstants.MINQUANTITY, PNI_QBmicroserviceAPIConstants.MINQUANTITY_END_RANGE, PNI_QBmicroserviceAPIConstants.ORIGINALPRICE, PNI_QBmicroserviceAPIConstants.DISCOUNT, PNI_QBmicroserviceAPIConstants.QTYLIMIT, PNI_QBmicroserviceAPIConstants.EXPIRYDATE, PNI_QBmicroserviceAPIConstants.ISDISCOUNTAVAILABLE_ON_BREAK};
		objPNI_QBmicroServiceActionBucket.validatePNI_QBMicroServiceResponse(arrQuantityDiscountBreakDetailsToVerify);

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC02: [PNI1-5]Validating Quantity Break flag and Details in PNI-QBMicroService Only VD", enabled=true)
	public void validate_QBMicroServiceResponse_OnlyVD() throws Exception {
		System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);		

		// Actual code starts here

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		PNI_QBmicroServiceActionBucket objPNI_QBmicroServiceActionBucket = new PNI_QBmicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrQuantityDiscountBreakDetailsToVerify = new String[] {PNI_QBmicroserviceAPIConstants.MINQUANTITY, PNI_QBmicroserviceAPIConstants.MINQUANTITY_END_RANGE, PNI_QBmicroserviceAPIConstants.ORIGINALPRICE, PNI_QBmicroserviceAPIConstants.DISCOUNT, PNI_QBmicroserviceAPIConstants.QTYLIMIT, PNI_QBmicroserviceAPIConstants.EXPIRYDATE, PNI_QBmicroserviceAPIConstants.ISDISCOUNTAVAILABLE_ON_BREAK};
		objPNI_QBmicroServiceActionBucket.validatePNI_QBMicroServiceResponse(arrQuantityDiscountBreakDetailsToVerify);

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC03: [PNI1-5]Validating Quantity Break flag and Details in PNI-QBMicroService QB And VD", enabled=true)
	public void validate_QBMicroServiceResponse_QBnVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);		

		// Actual code starts here

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		PNI_QBmicroServiceActionBucket objPNI_QBmicroServiceActionBucket = new PNI_QBmicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrQuantityDiscountBreakDetailsToVerify = new String[] {PNI_QBmicroserviceAPIConstants.MINQUANTITY, PNI_QBmicroserviceAPIConstants.MINQUANTITY_END_RANGE, PNI_QBmicroserviceAPIConstants.ORIGINALPRICE, PNI_QBmicroserviceAPIConstants.DISCOUNT, PNI_QBmicroserviceAPIConstants.QTYLIMIT, PNI_QBmicroserviceAPIConstants.EXPIRYDATE, PNI_QBmicroserviceAPIConstants.ISDISCOUNTAVAILABLE_ON_BREAK};
		objPNI_QBmicroServiceActionBucket.validatePNI_QBMicroServiceResponse(arrQuantityDiscountBreakDetailsToVerify);

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC04: [PNI1-5]Validating Quantity Break flag and Details in PNI-QBMicroService ACOP And VD", enabled=true)
	public void validate_QBMicroServiceResponse_ACOPnVD() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;

		ITestContext oIContext = Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);		

		// Actual code starts here

		//Report the - TestCategory-data Serial Number And SKU number and [ACOP Combination]
		ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
		String strCountryNSKUdetails = "[ "+hmTestData.get(Constants.COUNTRY)+",    Branch:"+hmTestData.get(Constants.RESELLER_BRANCH)+", Reseller No:"+hmTestData.get(Constants.RESELLER_NUMBER)+"] -- on combination ["+hmTestData.get(Constants.ACOP_COMBINATION)+"]";
		ExtentTestManager.log(Status.INFO, "Running for TestCategory SR.NO["+hmTestData.get(Constants.SRNO)+"]    ||    " + strCountryNSKUdetails, hmConfig.get(Constants.ExcelHeaderRunConfig));

		PNI_QBmicroServiceActionBucket objPNI_QBmicroServiceActionBucket = new PNI_QBmicroServiceActionBucket(hmTestData, hmConfig, objAPIDriver);

		String[] arrQuantityDiscountBreakDetailsToVerify = new String[] {PNI_QBmicroserviceAPIConstants.MINQUANTITY, PNI_QBmicroserviceAPIConstants.MINQUANTITY_END_RANGE, PNI_QBmicroserviceAPIConstants.ORIGINALPRICE, PNI_QBmicroserviceAPIConstants.DISCOUNT, PNI_QBmicroserviceAPIConstants.QTYLIMIT, PNI_QBmicroserviceAPIConstants.EXPIRYDATE, PNI_QBmicroserviceAPIConstants.ISDISCOUNTAVAILABLE_ON_BREAK};
		objPNI_QBmicroServiceActionBucket.validatePNI_QBMicroServiceResponse(arrQuantityDiscountBreakDetailsToVerify);

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}




	/* 
	 **************Factory code starts********** 
	 */

	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){

					@SuppressWarnings("unchecked")
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					HashMap<String,String> pConfigHMap = new ReadExcelFile().readConfigSheetforRunConfig
							(sExcelFileName,RunConfig.getProperty(Constants.ConfigSheetName),pExcelHMap.get(Constants.ExcelHeaderRunConfig));

					//ChangeClass
					PNI_QBmicroServiceAPITest newInstance=new PNI_QBmicroServiceAPITest(pExcelHMap,pConfigHMap);

					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}



	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);	
	}

}
