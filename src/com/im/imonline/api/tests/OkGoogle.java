package com.im.imonline.api.tests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class OkGoogle {

	public static void main(String[] args) throws Exception {
		
		
		RestAssured.baseURI = "https://imservices-qa1-usch01.corporate.ingrammicro.com:9043/ESOrderSearchUS_vs1";
		RequestSpecification httpRequest=null;
		Response response = null;
			
		 httpRequest = RestAssured.given();
		 httpRequest.header("Content-Type", "application/json");
		 httpRequest.header("Content-Type", "application/json");
		 httpRequest.auth().basic("APPIMONLINE", "APPIMONLINE12345"); 	

		 httpRequest.body("\r\n" + 
		 		"{\"id\":\"IMOrderSearch\",\"params\":{\"countrycode\":\"US\",\"ordersdaterange\":{\"from\":\"2018-12-12\",\"to\":\"2018-12-31\"},\"sortcriteria\":[{\"orderby\":\"customerordernumber.keyword\",\"orderbydirection\":\"desc\"}],\"orderstatuscodes\":[\"1\",\"2\",\"4\",\"5\",\"6\",\"7\",\"9\",\"10\",\"11\",\"3\",\"8\"],\"customernumber\":\"\",\"vendornumbers\":\"\",\"customerordernumber\":\"\",\"pagestart\":0,\"pagesize\":10}}\r\n" + 
		 		"\r\n" + 
		 		"");
		 
		 
		 response=httpRequest.post();
		 
		 
	//	 List<HashMap<String, String>> lstOfDataToBeVerifiedFromResponse =response.jsonPath().get("responses.hits.hits._source");
		/*
		 * Working tested code
		 List<HashMap<String, String>> lstValues=response.jsonPath().getList("responses.hits.hits._source[0]");
		 System.out.println(lstValues.get(0));
		 
		 System.out.println(response.jsonPath().getString("responses.hits.hits[0]._source.ordersubtotalvalue[1]"));
		 
		*/
		 
		 List<String> lstValues=response.jsonPath().getList("responses.hits.hits._source.orderupdatedate[0]");
		 List<String> lstCheck= new ArrayList<String>();
		 
		lstCheck.addAll(lstValues);
		 
		 Collections.sort(lstCheck,Collections.reverseOrder());
		 
		 System.out.println("lstValues:");
		 for(int i=0;i<lstValues.size();i++)
		 System.out.println(lstValues.get(i));
		 //System.out.println(lstValues.get(1));
		 
		 System.out.println("After Sorting lstCheck");
		 for(int i=0;i<lstCheck.size();i++)
			 System.out.println(lstCheck.get(i));
			// System.out.println(lstCheck.get(1));
		 
		 if(lstCheck.equals(lstValues))
		 System.out.println("Equals");
		 else
			 System.out.println("Not Equals");
		 
		 
		/* JSONParser parse = new JSONParser();
		 
		 JSONObject jobj = (JSONObject)parse.parse(response.asString());
		 
		 System.out.println(jobj.keySet());
		 
		 
		 JSONArray jsonarr_1 = (JSONArray) jobj.get("responses"); 
		 
		 
		 JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(0);
		 JSONArray jsonarr_2 = (JSONArray)jsonobj_1.get("hits");
		 
		 JSONObject jsonobj_3 = (JSONObject) jsonarr_2.get(0);
		 
			 
			 System.out.println((JSONArray)jsonobj_3.get("_index"));
		 */
		
		
		 
	}

}
