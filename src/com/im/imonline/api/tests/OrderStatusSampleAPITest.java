
package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.OrderStatusAPIActionBucket;
import com.im.imonline.api.testdata.util.OrderSearchTestDataUtil;


public class OrderStatusSampleAPITest extends BaseTest{
	
	public final static String ModuleNameInTestApplicability="OrderStatusAPI";
	public final static String TestDataFile="TestDataForOrderStatusAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	OrderSearchTestDataUtil objTestData = null;

	public OrderStatusSampleAPITest(HashMap<String, String> pExcelHMap) throws Exception {		
		super(pExcelHMap,ModuleNameInTestApplicability);
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC01: Get Request for OrderHeaderAndLines API", groups="API Order Search",enabled=true)
	public void validateOrderHeaderAndLines() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		OrderStatusAPIActionBucket action = new OrderStatusAPIActionBucket(hmTestData, objAPIDriver);
		String endPointUrlNameKey = "EndPointURL_OrderHeaderAndLines" ;
		action.performResponseValidation(endPointUrlNameKey);
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();	
		
	}	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC01: Get Request for SerialNumbers API", groups="API Order Search",enabled=true)
	public void validateSerialNumbers() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		OrderStatusAPIActionBucket action = new OrderStatusAPIActionBucket(hmTestData, objAPIDriver);
		String endPointUrlNameKey = "EndPointURL_SerialNumbers";
		action.performResponseValidation(endPointUrlNameKey);
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();	
		
	}	
		
	
	
/*	 
	 **************Factory code starts********** 
*/ 
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObjectDefault(sExcelFileName, sTabName);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];					
					OrderStatusSampleAPITest newInstance=new OrderStatusSampleAPITest(pExcelHMap);
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	
}

