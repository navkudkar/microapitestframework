package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.ProductLookUpAPIActionBucket;


public class ProductLookUpAPITest extends BaseTest{

	public final static String ModuleNameInTestApplicability="ProductLookUpAPI";
	public final static String TestDataFile="ProductLookUpAPI.xlsx";
	public final static boolean isMultiSet = false;
	public final static String sFileName="ProductLookUpRequestBody";
	public static  List<Object[]> lstResultSet = new ArrayList<Object[]>();

	public ProductLookUpAPITest(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig) throws Exception {		
		super(phmTestData,phmConfig,ModuleNameInTestApplicability);
	}

	@Test(description="TC1: Status Code Validation", groups="API Product Look Up",enabled=true)
	public void StatusCodeValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		// actual code starts here 
		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performStatusCodeValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC2:InValid Credential Validation", groups="API Product Look Up",enabled=true)
	public void InValidCredentialValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);


		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performCredentialValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC3:Blank ID Validation", groups="API Product Look Up",enabled=true)
	public void BlankIDValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performCredentialValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC4:Blank Credential Validation", groups="API Product Look Up",enabled=true)
	public void BlankCredentailValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();	
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performCredentialValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC5: Company Code Validation", groups="API Product Look Up",enabled=true)
	public void CompanyCodeValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performCompanyCodeValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC6: Customer Number Validation", groups="API Product Look Up",enabled=true)
	public void CustomerNumberValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performCustomerNumberValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC7: Ingram Part Number Validation", groups="API Product Look Up",enabled=true)
	public void IngramPartNumberValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performIngramPartNumberValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC8: Ingram Part Number Validation", groups="API Product Look Up",enabled=true)
	public void ManufacturerPartNumberValidation() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductLookUpAPIActionBucket action = new ProductLookUpAPIActionBucket(hmTestData, hmConfig,objAPIDriver);
		action.performMFGPartNumberValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	/* 
	 **************Factory code starts********** 
	 */

	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);

		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim= 	new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")
					HashMap<String, String> phmTestData = (HashMap<String, String>) myData2Dim[i][0];
					HashMap<String,String> phmConfig = new ReadExcelFile().readConfigSheetforRunConfig
							(sExcelFileName,RunConfig.getProperty(Constants.ConfigSheetName),phmTestData.get(Constants.ExcelHeaderRunConfig));
					ProductLookUpAPITest newInstance=new ProductLookUpAPITest(phmTestData,phmConfig);
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}



	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);

	}

}
