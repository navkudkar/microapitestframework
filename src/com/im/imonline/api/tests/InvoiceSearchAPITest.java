package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.InvoiceSearchAPIActionBucket;
import com.im.imonline.api.business.APIEnumerations;
import com.im.imonline.api.business.APIEnumerations.SortDirection;
import com.im.imonline.api.business.InvoiceSearchRequestDataConfig;
import com.im.imonline.api.testdata.util.InvoiceSearchTestDataUtil;


public class InvoiceSearchAPITest extends BaseTest{
	static Logger logger = Logger.getLogger("InvoiceSearchAPITest");
	public final static String ModuleNameInTestApplicability="InvoiceSearchAPI";
	public final static String TestDataFile="TestDataForInvoiceSearchAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="InvoiceSearchRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	InvoiceSearchTestDataUtil objTestData = null;

	public InvoiceSearchAPITest(HashMap<String, String> pExcelHMap, InvoiceSearchTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}

	@Test(description="TC01: Verify the Page Size Parameter ", groups="API Invoice Search",enabled=true,priority = 40)
	public void searchInvoiceWithPageSizeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.PAGESIZE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData, attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation(); 

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC02: Search invoice details with Invoice Number", groups="API Invoice Search",enabled=true, priority = 1)
	public void searchInvoiceWithInvoiceNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICE_NUMBER};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC03: Search invoice details with Ingram Part Number", groups="API Invoice Search",enabled=true,priority = 18)
	public void searchInvoiceWith_InvoiceDetail_SKU_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.SKU};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC04: Search invoice details with Serial Number", groups="API Invoice Search",enabled=true, priority = 4)
	public void searchInvoiceWith_InvoiceDetail_SerialNumber_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.SERIAL_NUMBER};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC05: Search InvoiceNumber with VPN", groups="API Invoice Search",enabled=true, priority = 19)
	public void searchInvoiceWith_InvoiceDetail_VPN_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.VPN};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC06: Search InvoiceNumber with Customer", groups="API Invoice Search",enabled=true, priority = 2)
	public void searchInvoiceWithResellerAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.RESELLER_NUMBER};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC07: Search InvoiceNumber with ResellerPONumber", groups="API Invoice Search",enabled=true, priority = 3)
	public void searchInvoiceWithCustomerOrderNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.CUSTOMER_ORDER_NUMBER};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC08: Search Invoices with CPN", groups="API Invoice Search",enabled=true, priority = 5)
	public void searchInvoiceWith_InvoiceDetail_CPN_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.CPN};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC09: Search Invoices with Bid Number", groups="API Invoice Search",enabled=true, priority = 6)
	public void searchInvoiceWith_InvoiceDetail_BidNumber_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.BID};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC10: Search Invoices with Invoice Type RMA", groups="API Invoice Search",enabled=true, priority = 20)
	public void searchInvoiceWith_Type_RMA_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.RMA_TYPEINVOICE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC11: Search Invoices with Invoice Type Order", groups="API Invoice Search",enabled=true, priority = 21)
	public void searchInvoiceWith_Type_Order_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.ORDER_TYPEINVOICE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC12: Search Invoices with Invoice status ", groups="API Invoice Search",enabled=true, priority = 7)
	public void searchInvoiceWith_Status_Open_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.STATUS_OPEN};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC13: Search Invoices with Invoice status ", groups="API Invoice Search",enabled=true, priority = 8)
	public void searchInvoiceWith_Status_PAID_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.STATUS_PAID};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC14: Search Invoices with Invoice status ", groups="API Invoice Search",enabled=true,priority = 9)
	public void searchInvoiceWith_Status_DUE_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.STATUS_DUE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC15: Search Invoices with Invoice Date Range  for last 31 Days till current date", groups="API Invoice Search",enabled=true, priority = 10)
	public void searchInvoiceWithInvoiceDate_forLast31DaysAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_LAST31DAYS};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="16: Search Invoices with Due  Date Range for last 31 Days till current date", groups="API Invoice Search",enabled=true, priority = 23)
	public void searchInvoiceWithDUEDate_forLast31DaysAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.DUEDATE_LAST31DAYS};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC17: Sort the Invoice by InvoiceNumber in Ascending Order", groups="API Invoice Search",enabled=true,priority = 24)
	public void searchInvoiceWith_SortBy_InvoiceNumber_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicenumber_keyword,SortDirection.asc);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC18: Search Invoices with Invoice Date Range picked from the API", groups="API Invoice Search",enabled=true, priority = 11)
	public void searchInvoiceWithInvoiceDate_forRandomDate_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_RANDOMDATE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC19: Sort the Invoice by InvoiceNumber and Search by Invoice Date Range in Descending Order", groups="API Invoice Search",enabled=true,priority = 25)
	public void searchInvoiceWith_SortBy_InvoiceNumber_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicenumber_keyword,SortDirection.desc);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC20: Sort the Invoice by Customer in Descending Order", groups="API Invoice Search",enabled=true,priority = 26)
	public void searchInvoiceWith_SortBy_CustomerNumber_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.customernumber_keyword,SortDirection.desc);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC21: Sort the Invoice by CustomerNumber in Ascending Order", groups="API Invoice Search",enabled=true,priority = 27)
	public void searchInvoiceWith_SortBy_CustomerNumber_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.customernumber_keyword,SortDirection.asc);


		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC22: Sort the Invoice by CustomerOrderNumber in Ascending Order", groups="API Invoice Search",enabled=true,priority = 28)
	public void searchInvoiceWith_SortBy_CustomerOrderNumber_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.customerordernumber_keyword,SortDirection.asc);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC23: Sort the Invoice by CustomerOrderNumber in Descending Order", groups="API Invoice Search",enabled=true,priority = 29)
	public void searchInvoiceWith_SortBy_CustomerOrderNumber_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.customerordernumber_keyword,SortDirection.desc);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC24: Sort the Invoice by InvoiceStatus in Ascending Order", groups="API Invoice Search",enabled=true,priority = 34)
	public void searchInvoiceWith_SortBy_InvoiceStatus_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicestatuscode_keyword,SortDirection.asc);		

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC25: Sort the Invoice by Invoice status in Descending Order", groups="API Invoice Search",enabled=true,priority = 35)
	public void searchInvoiceWith_SortBy_InvoiceStatus_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicestatuscode_keyword,SortDirection.desc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC26: Sort the Invoice by InvoiceDate in Ascending Order", groups="API Invoice Search",enabled=true,priority = 30)
	public void searchInvoiceWith_SortBy_InvoiceDate_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicedate,SortDirection.asc);		

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC27: Sort the Invoice  by InvoiceDate in Descending Order", groups="API Invoice Search",enabled=true,priority = 31)
	public void searchInvoiceWith_SortBy_InvoiceDate_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicedate,SortDirection.desc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC28: Sort the Invoice by DUEDate in Ascending Order", groups="API Invoice Search",enabled=true,priority = 32)
	public void searchInvoiceWith_SortBy_DueDate_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.duedate,SortDirection.asc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC29: Sort the Invoice  by InvoiceDate in Descending Order", groups="API Invoice Search",enabled=true,priority = 33)
	public void searchInvoiceWith_SortBy_DueDate_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.duedate,SortDirection.desc);			

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC30: Sort the Invoice by Type in Ascending Order", groups="API Invoice Search",enabled=true,priority = 38)
	public void searchInvoiceWith_SortBy_TYPE_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicetype_keyword,SortDirection.asc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC31: Sort the Invoice  by Type in Descending Order", groups="API Invoice Search",enabled=true,priority = 39)
	public void searchInvoiceWith_SortBy_TYPE_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.invoicetype_keyword,SortDirection.desc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC32: Search Invoices with Invoice Type CRMEMO", groups="API Invoice Search",enabled=true, priority = 28)
	public void searchInvoiceWith_Type_CRMEMO_AndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.CRMEMO_TYPEINVOICE};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC33: Sort the Invoice by Invoice Amount in Ascending Order", groups="API Invoice Search",enabled=true,priority = 36)
	public void searchInvoiceWith_SortBy_TotalAmount_ASC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.totalamount,SortDirection.asc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC34: Sort the Invoice  by Invoice Amount in Descending Order", groups="API Invoice Search",enabled=true,priority = 37)
	public void searchInvoiceWith_SortBy_TotalAmount_DESC() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {};
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(APIEnumerations.InvoiceSortingOptions.totalamount,SortDirection.desc);	

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC35: Search Invoices with Invoice Date in last 5 days", groups="API Invoice Search",enabled=true, priority = 12)
	public void searchInvoiceWith_InvoiceDateForPast5Days() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_INVOICELAST5DAYS};
		//SortBy sort = new SortBy(InvoiceSortingOptions.invoicedate.toString(), SortDirection.desc.toString());
		//objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC36: Search Invoices with Historical Invoice Date in 2017 end", groups="API Invoice Search",enabled=true, priority = 13)
	public void searchInvoiceWith_InvoiceDate_FOR23DAYS_OF1000DAYSBEFORETODAY() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_OF1000DAYSBEFORETODAY_FOR23DAYS};
//		SortBy sort = new SortBy(InvoiceSortingOptions.customernumber_keyword.toString(), SortDirection.asc.toString());
//		objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC37: Search Invoices with Historical Invoice Date in 2018 Mid", groups="API Invoice Search",enabled=true, priority = 14)
	public void searchInvoiceWith_InvoiceDate_FOR5DAYS_OF760DAYSBEFORETODAY() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_OF760DAYSBEFORETODAY_FOR5DAYS};
//		SortBy sort = new SortBy(InvoiceSortingOptions.invoicenumber_keyword.toString(), SortDirection.desc.toString());
//		objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC38: Search Invoices with Historical Invoice Date in 2019 mid", groups="API Invoice Search",enabled=true, priority = 15)
	public void searchInvoiceWith_InvoiceDate_FOR10DAYS_OF400DAYSBEFORETODAY() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_OF400DAYSBEFORETODAY_FOR10DAYS};
//		SortBy sort = new SortBy(InvoiceSortingOptions.invoicenumber_keyword.toString(), SortDirection.desc.toString());
//		objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC39: Search Invoices with Historical Invoice Date", groups="API Invoice Search",enabled=true, priority = 16)
	public void searchInvoiceWith_InvoiceDate_FOR12DAYS_OF140DAYSBEFORETODAY() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_OF140DAYSBEFORETODAY_FOR12DAYS};
//		SortBy sort = new SortBy(InvoiceSortingOptions.invoicedate.toString(), SortDirection.asc.toString());
//		objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC40: Search Invoices with Historical Invoice Date", groups="API Invoice Search",enabled=true, priority = 17)
	public void searchInvoiceWith_InvoiceDate_FOR2DAYS_OF50DAYSBEFORETODAY() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		APIEnumerations.InvoiceSearchType[] attrArray= {APIEnumerations.InvoiceSearchType.INVOICEDATE_OF50DAYSBEFORETODAY_FOR2DAYS};
//		SortBy sort = new SortBy(InvoiceSortingOptions.invoicedate.toString(), SortDirection.asc.toString());
//		objTestData.setSortConfig(sort);
		InvoiceSearchRequestDataConfig objConfig = new InvoiceSearchRequestDataConfig(objTestData,attrArray);

		InvoiceSearchAPIActionBucket action = new InvoiceSearchAPIActionBucket(objAPIDriver, objConfig); 
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	/* 
	 **************Factory code starts********** 
	 */
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry,
					InvoiceSearchTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					InvoiceSearchTestDataUtil objCPSTD = (InvoiceSearchTestDataUtil) myData2Dim[i][1];		
					InvoiceSearchAPITest newInstance=new InvoiceSearchAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}

	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","Country","TEST CASE", "REQUEST","RESPONSE","Invoices IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

