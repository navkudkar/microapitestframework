package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.ProductSearchElasticAPIActionBucket_Old;


public class ProductSearchElasticAPITest_Old extends BaseTest{

	public final static String ModuleNameInTestApplicability="ProductSearchElasticAPI2";
	public final static String TestDataFile="TestDataForProductSearchElasticAPI2.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="ProductSearchElasticAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());	


	public ProductSearchElasticAPITest_Old(HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) throws Exception {		
		super(pExcelHMap,pConfigHMap,ModuleNameInTestApplicability);
	}

	
		@Test(description="TC01: Verify that the product search done with a combination of various filters/attributes returns expected response parameters", groups="API Product Search",enabled=true)
		public void SearchedProductValidation() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC02: Verify that the product searched with keyword returns the same keyword as suggested keyword text", groups="API Product Search",enabled=true)
		public void SearchProductWithKeywordAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC03: Verify that the product searched with Vendor returns the same vendorname in the response", groups="API Product Search",enabled=true)
		public void SearchProductWithVendorAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		

		@Test(description="TC04: Verify that the product searched with Category returns the same category in the response", groups="API Product Search",enabled=true)
		public void SearchProductWithCategoryAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC05: Verify that the product searched with keyword and other filters returns expected response parameters", groups="API Product Search",enabled=true)
		public void SearchProductWithKeywordAndOtherFiltersAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		
		@Test(description="TC06: Verify that the product searched with Category and other filters returns expected response parameters", groups="API Product Search",enabled=true)
		public void SearchProductWithCategoryAndOtherFiltersAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC07: Verify that the product searched with Product status and other filters returns expected response parameters", groups="API Product Search",enabled=true)
		public void SearchProductWithStatusAndOtherFiltersAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC08: Verify that the product searched with Vendor and other filters returns expected response parameters", groups="API Product Search",enabled=true)
		public void SearchProductWithVendorAndOtherFiltersAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForAvailableValues();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC09: Verify that the product searched with view All option retrieves successful response within 6 secs", groups="API Product Search",enabled=true)
		public void SearchProductWithViewAllOptionAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForViewAlloption();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		@Test(description="TC10: Verify that the product searched with sorting option retrieves successful sorted response based on the criteria", groups="API Product Search",enabled=true)
		public void SearchProductWithKeywordAndSortingOptionAndValidate() throws Exception {
	
			if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;	
			ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));
			ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
			APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
			
			ProductSearchElasticAPIActionBucket_Old action = new ProductSearchElasticAPIActionBucket_Old(hmTestData, hmConfig,objAPIDriver);
			action.performValidationForSortingoption();
			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();
	
		}
		
		
	/* 
	 **************Factory code starts********** 
	 */
		@Factory
		public static Object[] invokeObjects() throws Exception {
			Object[][] myData2Dim= null;
			Object[] data1Dim= null;
			String sExcelFileName=null,sTabName = null;;

			String sSheetName=System.getProperty("Environment").trim();	
			sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

			HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
			sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

			try {
				myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);	
				if(null!=myData2Dim) {
					int iTotalCountryGiven = myData2Dim.length;
					data1Dim= new Object[iTotalCountryGiven];
					for(int i=0;i<iTotalCountryGiven;i++){
						@SuppressWarnings("unchecked")
						HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
						HashMap<String,String> pConfigHMap = new ReadExcelFile().readConfigSheetforRunConfig
								(sExcelFileName,RunConfig.getProperty(Constants.ConfigSheetName),pExcelHMap.get(Constants.ExcelHeaderRunConfig));
						ProductSearchElasticAPITest_Old newInstance=new ProductSearchElasticAPITest_Old(pExcelHMap,pConfigHMap);
						newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
						data1Dim[i]=newInstance;
					}
				}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return data1Dim;
		}



		@AfterTest(alwaysRun=true)
		protected synchronized void afterTest() throws Exception {
			String sCallingClassName = this.getClass().getSimpleName();
			String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
			List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
			ReadExcelFile.createWorkbook(sFilePath);
			lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
			ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
			ReadExcelFile.writeResult(sFilePath, lstResultSet);
		}


	}

