package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.ProductSummaryElasticAPIActionBucket;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.business.ProductSummaryRequestDataConfig;
import com.im.imonline.api.testdata.util.ProductSummaryTestDataUtil;

public class ProductSummaryElasticAPITest extends BaseTest{
	
	static Logger logger = Logger.getLogger("ProductSummaryElasticAPITest");
	public final static String ModuleNameInTestApplicability="ProductSummaryElasticAPI";
	public final static String TestDataFile="TestDataForProductSearchElasticAPI.xlsx";
	public final static boolean isMultiSet = false; 
	public final static String sFileName="ProductSummaryElasticAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	ProductSummaryTestDataUtil objTestData = null;

	public ProductSummaryElasticAPITest(HashMap<String, String> pExcelHMap, ProductSummaryTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}
	
	@Test(description="TC01: Login Mode_Verify Instock SKU", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForInstockProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.INSTOCK);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC02: Verify Product Summary Search with Instock or Order Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForInstockOrOrderProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.INSTOCKORORDER);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC03: Login Mode_Verify Product Summary Search with OutofStock Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForOutofstockProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.OUTOFSTOCK);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC04: Login Mode_Verify Product Summary Search with ESD/Downloadable Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForESDProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.DOWNLOADABLE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	
	@Test(description="TC05: Verify Product Summary Search with Directship Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForDirectshipProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.DIRECTSHIP);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC06:Verify Product Summary Search with Special Pricing Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForSpclPriceProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.SPECIALPRICING);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC07: Verify Product Summary Search with Quantity Break Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForQtyBreakProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.QUANTITYBREAK);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	
	//no Flag option available for special bids
	@Test(description="TC08: Verify Product Summary Search with Special Bids Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForSpclBidsProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.SPECIALBIDS);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC09: Verify Product Summary Search with Web Only Price Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForWebOnlyPriceProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.WEBONLYPRICE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC10: Verify Product Summary Search with Refurbished Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForRefurbishedProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.REFURBISHED);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC11:Verify Product Summary Search with No Returns Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForNoReturnsProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.NORETURNS);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	
	//no flagoption defined for ATP
	@Test(description="TC12: Verify Product Summary Search with Authorized To Purchase Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForAuthorizedToPurchaseProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.AUTHORIZEDTOPURCHASE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC13: Verify Product Summary Search with Not Orderable Online Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForNotOrderableOnlineProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.NOTORDERABLE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC14: Verify Product Summary Search with New Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForNewProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.NEW);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC15: Verify Product Summary Search with Free Item Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForFreeProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.FREEITEM);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC16: Verify Product Summary Search with Discontinued Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForDiscontinuedProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.DISCONTINUED);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC17: Verify Product Summary Search with Replacement Products Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForReplacementProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.REPLACEMENT);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC18: Verify Product Summary Search with LTL Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForLTLProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.LTL);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC19: Verify Product Summary Search with Return Limitations Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForReturnLimitationsProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.RETURNLIMITATIONS);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC20: Verify Product Summary Search with Heavy Weight Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForHeavyweightProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.HEAVYWEIGHT);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC21: Verify Product Summary Search with Bundle Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForBundleProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.BUNDLE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC22:Verify Product Summary Search with Available In Bundle Flag..", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForAvailableInBundleProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.AVAILABLE_IN_BUNDLE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC23: Verify Product Summary Search with Accessories Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForAccessoriesProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ACCESSORIES);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC24: Verify Product Summary Search with Suggested Products Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForSuggestedProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.SUGGESTED);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC25: Verify Product Summary Search with Backorder Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForBackorderProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.BACKORDER);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC26: Verify Product Summary Search with End User Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForEndUserProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ENDUSER);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC27: Verify Product Summary Search with Warranty Product Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForWarrantyProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.WARRANTY);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC28: Verify Product Summary Search with Warranties Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForWarrantiesProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.WARRANTIES);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC29: Verify Product Summary Search with Promotion Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForPromotionProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.PROMOTION);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC30: Verify Product Summary Search with License Products Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForLicenseProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.LICENCEPRODUCTS);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC31: Verify Product Summary Search with Bulk Freight Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForBulkFreightProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.BULKFREIGHT);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC32: Verify Product Summary Search with Blowout Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForBlowoutdProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.BLOWOUT);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC33: Verify Product Summary Search with Ship From Partner Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForShipFromPartnerProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.SHIPFROMPARTNER);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC34: Verify Product Summary Search with Clearance Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForClearanceProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.CLEARANCE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC35: Verify Product Summary Search with ACOP Promotion Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForACOPPromotionProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ACOP_PROMOTION);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC36: Verify Product Summary Search with ACOP Quantity Break Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForACOPQuantityBreakProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ACOP_QUANTITYBREAK);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC37: Verify Product Summary Search with ACOP Web Only Price Flag.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForACOPWebOlyPriceProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ACOP_WEBONLYPRICE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC38: Verify Product Summary Search with ACOP Special Pricing.", groups="API Product Search",enabled=true)
	public void verifyProductSummaryResponseForACOPSpecialPricingProduct() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		ProductSummaryRequestDataConfig objConfig= new ProductSummaryRequestDataConfig(objTestData,FlagOptions_ProductSummary.ACOP_WEBONLYPRICE);
		ProductSummaryElasticAPIActionBucket action=new ProductSummaryElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	
	
	/* 
	 **************Factory code starts********** 
	 */
	
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry, ProductSummaryTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					ProductSummaryTestDataUtil objCPSTD = (ProductSummaryTestDataUtil) myData2Dim[i][1];					
					ProductSummaryElasticAPITest newInstance=new ProductSummaryElasticAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}