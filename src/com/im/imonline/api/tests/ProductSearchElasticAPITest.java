package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.ProductSearchElasticAPIActionBucket;
import com.im.imonline.api.business.APIEnumerations.KeywordType;
import com.im.imonline.api.business.APIEnumerations.SearchType;
import com.im.imonline.api.business.ProductSearchRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.ProductSearchTestDataUtil;

public class ProductSearchElasticAPITest extends BaseTest{
	static Logger logger = Logger.getLogger("ProductSearchElasticAPITest");
	public final static String ModuleNameInTestApplicability="ProductSearchElasticAPI";
	public final static String TestDataFile="TestDataForProductSearchElasticAPI.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="ProductSearchElasticAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	ProductSearchTestDataUtil objTestData = null;

	public ProductSearchElasticAPITest(HashMap<String, String> pExcelHMap, ProductSearchTestDataUtil objPSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPSTDU;
	}

	@Test(description="TC01: Login Mode_Verify Search by keyword-Instock-priceRange", groups="API Product Search",enabled=true)
	public void searchProductWithStockInPriceRangeLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.PRODSTATUS};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setInStockStatusPriceRangeConfig();
		
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC02: Logout Mode_Verify viewAll Search Functionality", groups="API Product Search",enabled=true)
	public void searchProductWithViewAllOptionLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.VIEWALL};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}		

	@Test(description="TC03: Logout Mode_Verify Keyword Search Functionality", groups="API Product Search",enabled=true)
	public void searchProductWithKeywordLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_GENERIC);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}			

	@Test(description="TC04: Logout Mode_Verify Search Functionality With single SKU", groups="API Product Search",enabled=true)
	public void searchProductWithSingleSKULoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_SKU);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC05: Logout Mode_Verify Search Functionality With multiple SKU seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleSKULoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_SKU);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC06: Logout Mode_Verify Search Functionality With single VPN", groups="API Product Search",enabled=true)
	public void searchProductWithSingleVPNLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_VPN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC07: Logout Mode_Verify Search Functionality With multiple VPN seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleVPNLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_VPN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC08: Logout Mode_Verify Search Functionality With single EAN/UPC", groups="API Product Search",enabled=true)
	public void searchProductWithSingleUPCorEANLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_UPC_EAN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC09: Logout Mode_Verify Search Functionality With multiple EAN/UPC seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleUPCorEANLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_UPC_EAN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC10: Logout Mode_Verify viewAll Search Functionality", groups="API Product Search",enabled=true)
	public void searchProductWithViewAllOptionLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.VIEWALL};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}		

	@Test(description="TC11: Logout Mode_Verify Keyword Search Functionality", groups="API Product Search",enabled=true)
	public void searchProductWithKeywordLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_GENERIC);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	


	@Test(description="TC12: Logout Mode_Verify Search Functionality With single SKU", groups="API Product Search",enabled=true)
	public void searchProductWithSingleSKULoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_SKU);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC13: Logout Mode_Verify Search Functionality With multiple SKU seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleSKULoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_SKU);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC14: Logout Mode_Verify Search Functionality With single VPN", groups="API Product Search",enabled=true)
	public void searchProductWithSingleVPNLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_VPN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC15: Logout Mode_Verify Search Functionality With multiple VPN seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleVPNLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_VPN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC16: Logout Mode_Verify Search Functionality With single EAN/UPC", groups="API Product Search",enabled=true)
	public void searchProductWithSingleUPCorEANLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_UPC_EAN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC17: Logout Mode_Verify Search Functionality With multiple EAN/UPC seperated by  ' ; '", groups="API Product Search",enabled=true)
	public void searchProductWithMultipleUPCorEANLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_MULTIPLE_UPC_EAN);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC18: Logout Mode_Verify SortBy Functionality - Relevance", groups="API Product Search",enabled=true)
	public void searchProductSortedByRelevanceLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}



	@Test(description="TC19: Logout Mode_Verify SortBy Functionality - Vendor Name", groups="API Product Search",enabled=true)
	public void searchProductSortedByVendornameLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VENDORNAME, ProductSearchElasticAPIConstants.SORTORDERASC); //TODO

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC20: Logout Mode_Verify SortBy Functionality - Vendor Name - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByVendornameDescLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VENDORNAME, ProductSearchElasticAPIConstants.SORTORDERDESC); //TODO

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC21: Logout Mode_Verify SortBy Functionality - VPN", groups="API Product Search",enabled=true)
	public void searchProductSortedByVPNLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VPN, ProductSearchElasticAPIConstants.SORTORDERASC);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC22: Logout Mode_Verify SortBy Functionality - VPN - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByVPNDescLoggedOutModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VPN, ProductSearchElasticAPIConstants.SORTORDERDESC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC23: Login Mode_Verify SortBy Functionality - Relevance", groups="API Product Search",enabled=true)
	public void searchProductSortedByRelevanceLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);

		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}



	@Test(description="TC24: Login Mode_Verify SortBy Functionality - Vendor Name", groups="API Product Search",enabled=true)
	public void searchProductSortedByVendornameLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VENDORNAME, ProductSearchElasticAPIConstants.SORTORDERASC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}



	@Test(description="TC25: Login Mode_Verify SortBy Functionality - Vendor Name - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByVendornameDescLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VENDORNAME, ProductSearchElasticAPIConstants.SORTORDERDESC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC26: Login Mode_Verify SortBy Functionality - VPN", groups="API Product Search",enabled=true)
	public void searchProductSortedByVPNLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VPN, ProductSearchElasticAPIConstants.SORTORDERASC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}		


	@Test(description="TC27: Login Mode_Verify SortBy Functionality - VPN - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByVPNDescLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_VPN, ProductSearchElasticAPIConstants.SORTORDERDESC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC28: Login Mode_Verify SortBy Functionality - Price", groups="API Product Search",enabled=true)
	public void searchProductSortedByDealerPriceLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.DEALERPRICE, ProductSearchElasticAPIConstants.SORTORDERASC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC29: Login Mode_Verify SortBy Functionality - Price - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByDealerPriceDescLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.DEALERPRICE, ProductSearchElasticAPIConstants.SORTORDERDESC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC30: Login Mode_Verify SortBy Functionality - Stock", groups="API Product Search",enabled=true)
	public void searchProductSortedByStockLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_STOCK, ProductSearchElasticAPIConstants.SORTORDERASC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}

	@Test(description="TC31: Login Mode_Verify SortBy Functionality - Stock - Desc", groups="API Product Search",enabled=true)
	public void searchProductSortedByStockDescLoggedInModeAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setSortConfig(ProductSearchElasticAPIConstants.SORTBY_STOCK, ProductSearchElasticAPIConstants.SORTORDERDESC);


		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}


	@Test(description="TC32:Verify Search by price", groups="API Product Search",enabled=true)
	public void verifySearchByPriceLogoutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPriceRangeConfig();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	

	@Test(description="TC33:Verify Search by price", groups="API Product Search",enabled=true)
	public void verifySearchByPriceLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPriceRangeConfig();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	


	@Test(description="TC34:Search with Page size as 16 Loggedout Mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize16InLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPageSize("16", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}



	@Test(description="TC35:Search with Page size as 16 in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize16InLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("16", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC36:Search with Page size as 32 Loggedout Mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize32InLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPageSize("32", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}



	@Test(description="TC37:Search with Page size as 32 in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize32InLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("32", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	


	@Test(description="TC38:Search with Page size as 48 Loggedout Mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize48InLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPageSize("48", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}



	@Test(description="TC39:Search with Page size as 48 in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize48InLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("48", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	


	@Test(description="TC40:Search with Page size as 64 Loggedout Mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize64InLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPageSize("64", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}



	@Test(description="TC41:Search with Page size as 64 in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize64InLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("64", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC42:Search with Page size as 80 Loggedout Mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize80InLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setPageSize("80", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC43:Search with Page size as 80 in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchWithPageSize80InLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("80", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC44:Search by Category in Logged Out mode", groups="API Product Search",enabled=true)
	public void verifySearchByCategoryInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC45:Search by Category in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchByCategoryInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC46:Search by Vendor name in Logged Out mode", groups="API Product Search",enabled=true)
	public void verifySearchByVendorInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.VENDORNAME};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}			

	@Test(description="TC47:Search by Vendor name in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchByVendorInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.VENDORNAME};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC48:Search by Category, sub-Category and ProductType in Logged Out mode", groups="API Product Search",enabled=true)
	public void verifySearchByCatSubCatProdTypeInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY,SearchType.SUBCATEGORY,SearchType.PRODTYPE};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC49:Search by Category, sub-Category and ProductType in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchByCatSubCatProdTypeInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY,SearchType.SUBCATEGORY,SearchType.PRODTYPE};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC50:Search by Category with Vendor in Logged Out mode", groups="API Product Search",enabled=true)
	public void verifySearchByCategoryWithVendorInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY,SearchType.VENDORNAME};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	@Test(description="TC51:Search by Category, sub-Category and ProductType in Logged In mode", groups="API Product Search",enabled=true)
	public void verifySearchByCategoryWithVendorInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.CATEGORY,SearchType.VENDORNAME};

		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC52: Search with Invalid Keyword in Logout mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithInvalidKeywordInLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_INVALID);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC53: Search with Long Description Keyword in Logout mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithLongDescriptionKeywordInLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_LONGDESC1);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC54: Search with Auto-Correct Erroreous Keyword in Logout mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithAutoCorrectErroneousKeywordInLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_AUTOCORRECTERRONEOUS);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries except MI [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC55: Search with Incomplete/Partial Keyword in Logout mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithIncompleteKeywordInLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_INCOMPLETE);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC56: Search with Instock or on Order Status filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithInstockOrOnOrderStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC57: Search with New Product Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithNewProductFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_NEW);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC58: Search with ESD Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithESDFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_ESD);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC59: Search with Invalid Keyword in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithInvalidKeywordInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_INVALID);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC60: Search with Long Description Keyword in Logged In mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithLongDescriptionKeywordInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_LONGDESC1);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC61: Search with Auto-Correct Erroreous Keyword in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithAutoCorrectErroneousKeywordInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_AUTOCORRECTERRONEOUS);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	//not valid for NL
	@Test(description="TC62: Search with Incomplete/Partial Keyword in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithIncompleteKeywordInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.KEYWORD};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setConfigForKeywordTests(KeywordType.KEYWORD_INCOMPLETE);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC 63:Verify page size 16 and start page in loggedIn mode", groups="API Product Search",enabled=true)
	public void verifySearchByPageSize16AndLastPageInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("16", true);
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC 64:Verify page size 16 and start page in logged out mode ", groups="API Product Search",enabled=true)
	public void verifySearchByPageSize16AndLastPageInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("16", true);
		objConfig.setLoggedoutUserMode();
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC 65:Verify page size 80 and start page in LoggedIn mode ", groups="API Product Search",enabled=true)
	public void verifySearchByPageSize80AndLastPageInLoggedInMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("80", true);
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC66: Verify page size 80 and start page in LoggedOut Mode", groups="API Product Search",enabled=true)
	public void verifySearchByPageSize80AndLastPageInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};
		
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("80", true);
		objConfig.setLoggedoutUserMode();
		
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC67: Search with Directship Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithDirectShipFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
		@Test(description="TC68: Logged IN Mode_Verify Search With TechSpec Non Absolute InLoginMode", groups="API Product Search",enabled=true)
	public void verifySearchWithTechSpecNonAbsoluteInLoginMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setAbsoluteOrNonAbsoluteConfig(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE);
		
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC69: Logged Out Mode_Verify Search With TechSpec Non Absolute InLoginMode", groups="API Product Search",enabled=true)
	public void verifySearchWithTechSpecNonAbsoluteInLoggedOutMode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setLoggedoutUserMode();
		objConfig.setAbsoluteOrNonAbsoluteConfig(ProductSearchElasticAPIConstants.EXCELHEADER_TECHSPEC_NON_ABSOLUTE);
		
		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
		@Test(description="TC70: Login Mode_Verify Search by Status - bundle flag", groups="API Product Search",enabled=true)
	public void VerifySearchWithBundleFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.PRODSTATUS};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_BUNDLE);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	@Test(description="TC71: Login Mode_Verify Search by Status - refirbished flag", groups="API Product Search",enabled=true)
	public void VerifySearchWithRefurbishedFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.PRODSTATUS};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_REFURBISHED);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
		
	@Test(description="TC72: Login Mode_Verify Search by Status - instock and minimum", groups="API Product Search",enabled=true)
	public void VerifySearchWithInStockFlagStatusWithMinimumStockFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {SearchType.PRODSTATUS};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_INSTOCK,ProductSearchElasticAPIConstants.MINSTOCKQTY);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC73: Search with Heavy Weight Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithHeavyWeightFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries [AT,BR,CA,CH,DE,ES,FR,HK,HU,IN,IT,MI,MX,PT,SE,UK,US] - Chandra
	@Test(description="TC74: Search with Authorized To Purchase Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithAuthorizedToPurchaseFlagStatusFilterInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setProductStatusConfig(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC75: Login Mode_Verify Search with PageStart as 16",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs16InLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("16", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC76: Login Mode_Verify Search with PageStart as 16",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs16InLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("16", false);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC77: Login Mode_Verify Search with PageStart as 32",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs32InLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("32", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC78: Login Mode_Verify Search with PageStart as 32",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs32InLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("32", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC79: Login Mode_Verify Search with PageStart as 48",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs48InLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("48", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC80: Login Mode_Verify Search with PageStart as 48",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs48InLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("48", false);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	
	//Working for all Impulse countries 
	@Test(description="TC81: Login Mode_Verify Search with PageStart as 64",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs64InLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("64", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	
	//Working for all Impulse countries 
	@Test(description="TC82: Login Mode_Verify Search with PageStart as 64",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs64InLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("64", false);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	
	//Working for all Impulse countries 
	@Test(description="TC83: Login Mode_Verify Search with PageStart as 80",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs80InLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("80", false);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Working for all Impulse countries 
	@Test(description="TC84: Logout Mode_Verify Search with PageStart as 80",groups="API Product Search",enabled=true)
	public void VerifyPageStartAs80InLogoutMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setPageSize("80", false);
		objConfig.setLoggedoutUserMode();

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	//Need to confirm Validation Logic
	@Test(description="TC85: Search with Quantity Break Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithQuantityBreakFlagStatusInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setACOPFlagConfig(ProductSearchElasticAPIConstants.FLAG_QTYBREAK);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	
	//Need to confirm Validation Logic
	@Test(description="TC86: Search with Promotion Flag Status Filter in Login mode", groups="API Product Search",enabled=true)
	public void VerifySearchWithPromotionFlagStatusInLoginMode() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);
		
		SearchType[] attrArray= {};			
		ProductSearchRequestDataConfig objConfig=new ProductSearchRequestDataConfig(objTestData,attrArray);
		objConfig.setACOPFlagConfig(ProductSearchElasticAPIConstants.FLAG_PROMOTION);

		ProductSearchElasticAPIActionBucket action = new ProductSearchElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation(); 
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}
	
	
	/* 
	 **************Factory code starts********** 
	 */
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry, ProductSearchTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					ProductSearchTestDataUtil objCPSTD = (ProductSearchTestDataUtil) myData2Dim[i][1];					
					ProductSearchElasticAPITest newInstance=new ProductSearchElasticAPITest(pExcelHMap, objCPSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

