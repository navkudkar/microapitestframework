package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.ProductDetailsElasticAPIActionBucket;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductDetails;
import com.im.imonline.api.business.ProductDetailsRequestDataConfig;
import com.im.imonline.api.testdata.util.ProductDetailsTestDataUtil;

public class ProductDetailsElasticAPITest extends BaseTest{

	static Logger logger = Logger.getLogger("ProductDetailsElasticAPITest");
	public final static String ModuleNameInTestApplicability="ProductDetailsElasticAPI";
	public final static String TestDataFile="TestDataForProductSearchElasticAPI.xlsx";
	public final static boolean isMultiSet = false; 
	public final static String sFileName="ProductDetailsElasticAPIRequest"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	ProductDetailsTestDataUtil objTestData = null;

	public ProductDetailsElasticAPITest(HashMap<String, String> pExcelHMap, ProductDetailsTestDataUtil objPDTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objPDTDU;
	}

	//Alert Category Flag 
	@Test(description = "TC01: Login Mode_Verify Product Details Elastic Search with Refurbished Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForRefurbishedProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.REFURBISHED);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC02: Login Mode_Verify Product Details Elastic Search with No Returns Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForNoReturnsProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.NO_RETURNS);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC03: Login Mode_Verify Product Details Elastic Search with New Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForNewProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.NEW);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC04: Login Mode_Verify Product Details Elastic Search with Free Item Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForFreeItemProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.FREE_ITEM);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC05: Login Mode_Verify Product Details Elastic Search with Discontinued Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForDiscontinuedProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.DISCONTINUED);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC06: Login Mode_Verify Product Details Elastic Search with Replacement Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForReplacementProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.REPLACEMENT_PRODUCT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC07: Login Mode_Verify Product Details Elastic Search with LTL Products", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForLTLProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.LTL);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Product Association Flag
	@Test(description="TC08: Login Mode_Verify Product Details Elastic Search with Bundle Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForBundleProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.BUNDLE);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Product Association Flag
	@Test(description="TC09: Login Mode_Verify Product Details Elastic Search with Accessories Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForAccessoriesProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.ACCESSORIES);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Product Association Flag
	@Test(description="TC10: Login Mode_Verify Product Details Elastic Search with Available In Bundle Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForAvailableInBundleProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.AVAILABLE_IN_BUNDLE);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Product Association Flag
	@Test(description="TC11: Login Mode_Verify Product Details Elastic Search with Suggested Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForSuggestedProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.SUGGESTED_PRODUCT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Shipment Category Flag
	@Test(description="TC12: Login Mode_Verify Product Details Elastic Search with Return Limitations Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForReturnLimitationsProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.RETURN_LIMITATIONS);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Shipment Category Flag
	@Test(description="TC13: Login Mode_Verify Product Details Elastic Search with Directship Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForDirectShipProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.DIRECTSHIP);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Shipment Category Flag
	@Test(description="TC14: Login Mode_Verify Product Details Elastic Search with ESD/Downloadable Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForESDProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.DOWNLOADABLE);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Shipment Category Flag
	@Test(description="TC15: Login Mode_Verify Product Details Elastic Search with Bulk Freight Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForBulkFreightProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.BULK_FREIGHT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Shipment Category Flag
	@Test(description="TC16: Login Mode_Verify Product Details Elastic Search with Heavy Weight Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForHeavyWeightProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.HEAVY_WEIGHT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC17: Login Mode_Verify Product Details Elastic Search with End User Required Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForEndUserRequiredProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.END_USER);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC18: Login Mode_Verify Product Details Elastic Search with Back Order Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForBackOrderProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.BACK_ORDER);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC19: Login Mode_Verify Product Details Elastic Search with BlowOut Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForBlowOutProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.BLOW_OUT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC20: Login Mode_Verify Product Details Elastic Search with Clearance Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForClearanceProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.CLEARANCE);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC21: Login Mode_Verify Product Details Elastic Search with Licence Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForLicenceProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.LICENCE_PRODUCT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC22: Login Mode_Verify Product Details Elastic Search with Promotion Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForPromotionProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.PROMOTION);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC23: Login Mode_Verify Product Details Elastic Search with Ship From Partner Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForShipFromPartnerProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.SHIP_FROM_PARTNER);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC24: Login Mode_Verify Product Details Elastic Search with Similar Products", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForSimilarProducts() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.SIMILAR_PRODUCTS);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC25: Login Mode_Verify Product Details Elastic Search with Warranties", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForWarranties() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.WARRANTIES);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC26: Login Mode_Verify Product Details Elastic Search with Warranty Product", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForWarrantyProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.WARRANTY_PRODUCT);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC27: Login Mode_Verify Product Details Elastic Search with Instock SKU", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForInstockProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.INSTOCK);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC28: Login Mode_Verify Product Details Elastic Search with Instock or Order SKU", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForInstockOrOrderProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.INSTOCK_OR_ORDER);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Miscellaneous
	@Test(description="TC29: Login Mode_Verify Product Details Elastic Search with Out of Stock SKU", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForOutOfStockProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.OUT_OF_STOCK);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC30: Logout Mode_Verify Product Details Elastic Search with Authorized To Purchase Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForAuthorizedToPurchaseProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.NOT_AUTHORIZED);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	//Alert Category Flag
	@Test(description = "TC31: Login Mode_Verify Product Details Elastic Search with Not Orderable Online Product", groups = "API Product Details", enabled = true)
	public void VerifyProductDetailsResponseForNotOrderableOnlineProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		//Actual Test Case starts here...
		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.NOT_ORDERABLE_ONLINE);

		//Actual Validation Starts here...
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC32: Login Mode_Verify Product Details Elastic Search with Special Pricing Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForSpecialPricingProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig= new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.SPECIAL_PRICING);
		ProductDetailsElasticAPIActionBucket action=new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC33: Login Mode_Verify Product Details Elastic Search with Quantity Break Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForQuantityBreakProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig= new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.QUANTITY_BREAK);
		ProductDetailsElasticAPIActionBucket action=new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC34: Login Mode_Verify Product Details Elastic Search with Special Bids Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForSpecialBidsProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig= new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.SPECIAL_BIDS);
		ProductDetailsElasticAPIActionBucket action=new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC35: Login Mode_Verify Product Details Elastic Search with Web Only Price Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForWebOnlyPriceProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig= new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.WEB_ONLY_PRICE);
		ProductDetailsElasticAPIActionBucket action=new ProductDetailsElasticAPIActionBucket(objAPIDriver,objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC36: Login Mode_Verify Product Details Elastic Search with ACOP Promotion Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForACOPPromotionProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.ACOP_PROMOTION);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC37: Login Mode_Verify Product Details Elastic Search with ACOP Quantity Break Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForACOPQuantityBreakProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.ACOP_QUANTITY_BREAK);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC38: Login Mode_Verify Product Details Elastic Search with ACOP Web Only Price Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForACOPWebOnlyPriceProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.ACOP_WEB_ONLY_PRICE);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	@Test(description="TC39: Login Mode_Verify Product Details Elastic Search with ACOP Special Pricing Flag.", groups="API Product Details",enabled=true)
	public void VerifyProductDetailsResponseForACOPSpecialPricingProduct() throws Exception {
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		ProductDetailsRequestDataConfig objConfig = new ProductDetailsRequestDataConfig(objTestData, FlagOptions_ProductDetails.ACOP_SPECIAL_PRICING);
		ProductDetailsElasticAPIActionBucket action = new ProductDetailsElasticAPIActionBucket(objAPIDriver, objConfig);
		action.performResponseValidation();

		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}

	/* 
	 **************Factory code starts********** 
	 */

	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName, listTCMappingToCountry, ProductDetailsTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					System.out.println(pExcelHMap);
					ProductDetailsTestDataUtil objCPDTD = (ProductDetailsTestDataUtil) myData2Dim[i][1];					
					ProductDetailsElasticAPITest newInstance=new ProductDetailsElasticAPITest(pExcelHMap, objCPDTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}

	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE","PRODUCTS IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}
