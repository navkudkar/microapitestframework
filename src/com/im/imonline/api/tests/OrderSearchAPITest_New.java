
package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.OrderSearchAPIActionBucket_New;
import com.im.imonline.api.business.APIEnumerations.OrderSearchType;
import com.im.imonline.api.business.OrderSearchRequestDataConfig;
import com.im.imonline.api.testdata.util.OrderSearchTestDataUtil;


public class OrderSearchAPITest_New extends BaseTest{
	
	public final static String ModuleNameInTestApplicability="OrderSearchAPI_New";
	public final static String TestDataFile="TestDataForOrderSearchAPI_New.xlsx";
	public final static boolean isMultiSet = false; //method level
	public final static String sFileName="OrderSearchRequest_New"; 
	public static  List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());
	OrderSearchTestDataUtil objTestData = null;

	public OrderSearchAPITest_New(HashMap<String, String> pExcelHMap, OrderSearchTestDataUtil objOSTDU) throws Exception {			
		super(pExcelHMap, ModuleNameInTestApplicability);
		objTestData = objOSTDU;
	}


	@Test(description="TC01: Search Order details with Order Number", groups="API Order Search",enabled=true)
	public void searchOrderWithOrderNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.ORDER_NUMBER};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();
	}	
		
	@Test(description="TC02: Search Order with SKU Number", groups="API Order Search",enabled=true)
	public void searchOrderWithSKUNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.SKU};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	
	@Test(description="TC03: Search Order with VPN Number", groups="API Order Search",enabled=true)
	public void searchOrderWithVPNNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.VPN};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
	@Test(description="TC04: Search Order with Reseller Number", groups="API Order Search",enabled=true)
	public void searchOrderwithResellerNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.RESELLER_NUMBER};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC05: Search Order with Reseller PO", groups="API Order Search",enabled=true)
	public void searchOrderwithCustomer_Order_NumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.CUSTOMER_ORDER_NUMBER};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC06: Search Order with Vendor Name", groups="API Order Search",enabled=true)
	public void searchOrderwithVendorNameandValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.VENDOR_NAME};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC07: Search Order with End user PO#", groups="API Order Search",enabled=true)
	public void searchOrderwithEndUserPONumandValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.ENDUSERPONUM};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC08: Search Order with Serial Number", groups="API Order Search",enabled=true)
	public void searchOrderWithSerialNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.SERIAL};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC09: Search Order with UPC Number", groups="API Order Search",enabled=true)
	public void searchOrderWithUPCNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.UPC};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC10: Search Order with bid Number", groups="API Order Search",enabled=true)
	public void searchOrderWithBidRefenceNumberAndValidate() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.BID};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	
	@Test(description="TC11: Search Order with Customer Part Number", groups="API Order Search",enabled=true)
	public void searchOrderWithCustomerPartNumberAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.CPN};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC12: Search Order with Invoice Number", groups="API Order Search",enabled=true)
	public void searchOrderWithInvoiceNumberAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.INVOICENUMBER};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC13: Search Order with Order Date Range", groups="API Order Search",enabled=true)
	public void searchOrderWithOrderDateAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.ORDER_DATE_RANGE};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC14: Search Order with Ship Date Range", groups="API Order Search",enabled=true)
	public void searchOrderWithShipDateAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.SHIP_DATE_RANGE};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC15: Search Order with back order for SAP", groups="API Order Search",enabled=true)
	public void searchOrderWithBackorderForSAPAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.BACKORDER_SAP};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC16: Search Order with In Progress status", groups="API Order Search",enabled=true)
	public void searchOrderWithInProgress_StatusAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_INPROGRESS};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC17: Search Order with Partially Processed status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_PartiallyProcessedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_PARTIALLYPROCESSED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}@Test(description="TC18: Search Order with Shipped status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_ShippedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_SHIPPED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC19: Search Order with Partially Invoiced status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_PartiallyInvoicedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_PARTIALLYINVOICED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC20: Search Order with Invoiced status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_InvoicedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_INVOICED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC21: Search Order with Onhold status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_OnholdAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_ONHOLD};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC22: Search Order with Customerhold status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_CustomerholdAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_CUSTOMERHOLD};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	@Test(description="TC23: Search Order with Voided status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_VoidedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_VOIDED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC24: Search Order with Backorder_Impluse status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_Backorder_ImpluseAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_BACKORDER_IMPLUSE};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC25: Search Order with Orderreview status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_OrderreviewAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_ORDERREVIEW};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC26: Search Order with Creditcheck status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_CreditcheckAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_CREDITCHECK};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC27: Search Order with Partialhold status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_PartialholdAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_PARTIALHOLD};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}	@Test(description="TC28: Search Order with Partiallyrejected status", groups="API Order Search",enabled=true)
	public void searchOrderWithStatus_PartiallyRejectedAndValidate() throws Exception {
		
		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName()))) return;	
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmConfig);

		OrderSearchType[] attrArray= {OrderSearchType.STATUS_PARTIALLYREJECTED};
		OrderSearchRequestDataConfig objConfig = new OrderSearchRequestDataConfig(objTestData,attrArray);
		
		OrderSearchAPIActionBucket_New action = new OrderSearchAPIActionBucket_New(objAPIDriver, objConfig); 
		action.performResponseValidation();
		
		objAPIDriver.setTestCaseCompleted(true);
		objAPIDriver.getSoftAssert().assertAll();

	}
	
/*	 
	 **************Factory code starts********** 
*/ 
	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);
		
		try {
			myData2Dim = new ReadExcelFile().readExcelDataTo2DimArrayWithTestDataUtilObject(sExcelFileName, sTabName,listTCMappingToCountry, OrderSearchTestDataUtil.class);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){
					@SuppressWarnings("unchecked")						
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					OrderSearchTestDataUtil objOSTD = (OrderSearchTestDataUtil) myData2Dim[i][1];
					OrderSearchAPITest_New newInstance=new OrderSearchAPITest_New(pExcelHMap, objOSTD);								
					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter column of TestData Sheet for given countries");
						
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}
	
	
	
	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","Country","TEST CASE", "REQUEST","RESPONSE","Invoices IN RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);
	}
}

