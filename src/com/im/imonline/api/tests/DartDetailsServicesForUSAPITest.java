package com.im.imonline.api.tests;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.MapTCForNations;
import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.tests.BaseTest;
import com.im.api.core.utility.ReadExcelFile;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIDriver;
import com.im.imonline.api.action.DartDetailsServiceForUSAPIActionBucket;

public class DartDetailsServicesForUSAPITest extends BaseTest {

	public final static String ModuleNameInTestApplicability="USDartDetailsService";
	public final static String TestDataFile="TestDataForUSGetDartDetailsServiceAPI.xlsx";
	public final static boolean isMultiSet = false;
	public final static String sFileName="DartDetailsServicesForUS";
	public static List<Object[]> lstResultSet = Collections.synchronizedList(new ArrayList<Object[]>());

	public DartDetailsServicesForUSAPITest(HashMap<String, String> pExcelHMap, HashMap<String, String> pConfigHMap) throws Exception {
		super(pExcelHMap,pConfigHMap,ModuleNameInTestApplicability);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC01:Verify Dart Details Info with Manditory Fields", enabled=true)
	public void USValidateDartDetailsServicesResponse() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));			
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		DartDetailsServiceForUSAPIActionBucket action = new DartDetailsServiceForUSAPIActionBucket(hmTestData, hmConfig, objAPIDriver);
		action.USValidateDartDetailsServicesResponse();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC02:Verify Dart Details With Invalid Dart Details", enabled=true)
	public void USInvalidDartDetails() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));			
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		DartDetailsServiceForUSAPIActionBucket action = new DartDetailsServiceForUSAPIActionBucket(hmTestData, hmConfig, objAPIDriver);
		action.USInvalidDartDetails();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC03:Verify Dart Details With Blank Country code", enabled=true)
	public void USDartDetailsForBlankCountryCode() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));			
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		DartDetailsServiceForUSAPIActionBucket action = new DartDetailsServiceForUSAPIActionBucket(hmTestData, hmConfig, objAPIDriver);
		action.USDartDetailsForBlankCountryCode();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC04:Verify Dart Details With Different Reseller Account", enabled=true)
	public void USDartDetailsForDifferentResellerAccount() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));			
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		DartDetailsServiceForUSAPIActionBucket action = new DartDetailsServiceForUSAPIActionBucket(hmTestData, hmConfig, objAPIDriver);
		action.USDartDetailsForDifferentResellerAccount();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test(description="TC05:Verify Dart Details With Expired Dart", enabled=true)
	public void USDartDetailsForExpiredDart() throws Exception {

		if(!(isTestMappedToCountry(new Object(){}.getClass().getEnclosingMethod().getName())) || !(isTestCategoryMapped(ModuleNameInTestApplicability)) ) return;
		ExtentTestManager.log(Status.INFO,"Running for Test Data SR.NO["+hmTestData.get(Constants.SRNO)+"]",hmConfig.get(Constants.ExcelHeaderRunConfig));			
		ITestContext oIContext =	Reporter.getCurrentTestResult().getTestContext();
		APIDriver objAPIDriver=AppUtil.preparePrequisites(oIContext,hmTestData);

		// Actual code starts here
		DartDetailsServiceForUSAPIActionBucket action = new DartDetailsServiceForUSAPIActionBucket(hmTestData, hmConfig, objAPIDriver);
		action.USDartDetailsForExpiredDart();

			objAPIDriver.setTestCaseCompleted(true);
			objAPIDriver.getSoftAssert().assertAll();      
	}

	/* 
	 **************Factory code starts********** 
	 */

	@Factory
	public static Object[] invokeObjects() throws Exception {
		Object[][] myData2Dim= null;
		Object[] data1Dim= null;
		String sExcelFileName=null,sTabName = null;;

		String sSheetName=System.getProperty("Environment").trim();	
		sExcelFileName=Constants.BASEPATH+"\\TestData\\"+TestDataFile;

		HashMap<String,MapTCForNations>  listTCMappingToCountry = MapTCForNations.getListOfTestvsCountryMap(ModuleNameInTestApplicability);
		sTabName = (!sSheetName.equalsIgnoreCase(Constants.READ_FROM_PROPERTIES_FILE))?sSheetName:RunConfig.getProperty(Constants.Environment);

		try {
			myData2Dim= 	new ReadExcelFile().readExcelDataTo2DimArrayWithJasonObject(sExcelFileName, sTabName);	
			if(null!=myData2Dim) {
				int iTotalCountryGiven = myData2Dim.length;
				data1Dim= new Object[iTotalCountryGiven];
				for(int i=0;i<iTotalCountryGiven;i++){

					@SuppressWarnings("unchecked")
					HashMap<String, String> pExcelHMap = (HashMap<String, String>) myData2Dim[i][0];
					HashMap<String,String> pConfigHMap = new ReadExcelFile().readConfigSheetforRunConfig
							(sExcelFileName,RunConfig.getProperty(Constants.ConfigSheetName),pExcelHMap.get(Constants.ExcelHeaderRunConfig));

					//ChangeClass
					DartDetailsServicesForUSAPITest newInstance=new DartDetailsServicesForUSAPITest(pExcelHMap,pConfigHMap);

					newInstance.setTheCountriesForTheTC(listTCMappingToCountry);
					data1Dim[i]=newInstance;
				}
			}else System.out.println("Please check the RUNFORCOUNTRIES parameter and IsApplicable column of TestData Sheet for given countries");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data1Dim;
	}


	@AfterTest(alwaysRun=true)
	protected synchronized void afterTest() throws Exception {
		String sCallingClassName = this.getClass().getSimpleName();
		String sFilePath = Constants.getCurrentProjectPath()+"\\ExtentReports\\APIResults\\"+sCallingClassName+".xlsx";      
		List<Object[]> lstResultHeaders = Collections.synchronizedList(new ArrayList<Object[]>());	
		ReadExcelFile.createWorkbook(sFilePath);
		lstResultHeaders.add(new Object[] { "SR.NO TestData","TEST CASE", "REQUEST","RESPONSE" });
		ReadExcelFile.writeResult(sFilePath, lstResultHeaders);
		ReadExcelFile.writeResult(sFilePath, lstResultSet);	
	}

}


	


