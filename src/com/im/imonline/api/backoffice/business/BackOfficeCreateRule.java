package com.im.imonline.api.backoffice.business;

import static io.restassured.path.xml.XmlPath.CompatibilityMode.HTML;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;
import com.im.api.core.common.RunConfig;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.jayway.jsonpath.JsonPath;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class BackOfficeCreateRule {

	Logger logger = Logger.getLogger("BackOfficeCreateRule"); 
	private static final String sBKUsername = "superuser" ;
	private static final String sBKPass = "Ingram@123";		
	private final String sContentTypeMultiPart = "multipart/form-data" ; 

	private Map<String,String> mapCookies ;	
	private String sCountry ,sCreateRequestVerificationToken;
	private String sBackofficeEnv = RunConfig.getProperty(Constants.BACKOFFICE_ENVIRONMENT);
	private Response objResponse = null;
	private RequestSpecification httpRequest = null;
	private boolean flagRuleCreated = false;
	private String sRuleId = null;
	APIAssertion objAPIAssertion = null;
	
	public BackOfficeCreateRule(String sCountry) {
		this.sCountry = sCountry;		
	}	
	
	public BackOfficeCreateRule(String sCountry, String sRequestVerificationToken, Map<String,String> mapCookies) {
		this.sCountry = sCountry;	
		this.mapCookies = mapCookies;
		this.sCreateRequestVerificationToken = sRequestVerificationToken;
		setBackOfcURLEnvt();
	}
	
	
	public BackOfficeCreateRule(String sCountry, String sRequestVerificationToken, Map<String,String> mapCookies,APIAssertion objAPIAssertion ) {
		this.sCountry = sCountry;	
		this.mapCookies = mapCookies;
		this.sCreateRequestVerificationToken = sRequestVerificationToken;
		setBackOfcURLEnvt();
		this.objAPIAssertion = objAPIAssertion;
	}
	
	public void generateRequestVerficationTokenForBackOfficeRuleCreation() throws Exception {		
			String sLoginReqVerificationToken = generateBackofficeLoginRequestVerificationToken();
			postRequestForBackofficeLogin(sLoginReqVerificationToken);
			generateCreateRuleRequestVerificationToken();
	}
	
	public void setBackOfcURLEnvt()
	{
		 sBackofficeEnv=System.getProperty(Constants.BACKOFFICE_ENVIRONMENT).trim();
		 sBackofficeEnv = (!sBackofficeEnv.equalsIgnoreCase("READ_FROM_PROPERTIES_FILE"))?sBackofficeEnv:RunConfig.getProperty(Constants.BACKOFFICE_ENVIRONMENT);
	}
	
	/**
	 * This function hit http login backoffice url get request, retrive response in html format and fetch backoffice login verification token
	 * Also fetch cookies from response which required for next http request
	 * @author Psave
	 */		
	private String generateBackofficeLoginRequestVerificationToken() throws Exception {
		String sLoginRequestVerificationToken=null;		

		RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/";
		objResponse = RestAssured.given()
				.when().get(RestAssured.baseURI)
				.then().contentType(ContentType.HTML).extract()
				.response();		
		
		if(objResponse==null || objResponse.getStatusCode()!=200)
			throw new Exception("Null values in GET Response for LOGIN BACKOFFICE URL in the function generateBackofficeLoginRequestVerificationToken");

		XmlPath htmlPath = new XmlPath(HTML, objResponse.getBody().asString());
		sLoginRequestVerificationToken = htmlPath.getString(BackOfficeMerchandisingAPIConstants.xpathRequestVeificationTokenLogin);	
		
		if(sLoginRequestVerificationToken==null || sLoginRequestVerificationToken.equalsIgnoreCase("") || sLoginRequestVerificationToken.isEmpty())
			throw new Exception("Unable to Fetch BackOffice Login RequestVerificationToken for GET Response LOGIN BACKOFFICE URL in the function generateBackofficeLoginRequestVerificationToken");
		generateMapCookies(objResponse);	
		return sLoginRequestVerificationToken;
	}
	
	/**
	 * This function hit http login backoffice url POST request 
	 * after POST, again hit GET request for 200 status and verify backoffice site login
	 * fetch response cookies which required for next http request
	 * @author Psave
	 */	
	private void postRequestForBackofficeLogin(String sLoginReqVerificationToken) throws Exception {
		RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/site/Login/Login";
		String sReferer = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/site/Login/Login?ReturnUrl=%2fSite%2fhome" ;			
		String sRetuenUrl = "%2fSite%2fhome";
		
		logger.info("Base URL formed after property fetch for Login: "+RestAssured.baseURI);
		httpRequest = RestAssured.given();
			 					  httpRequest.cookies(mapCookies);
			 					  httpRequest.header("Referer", sReferer);
			 					  httpRequest.header("Content-Type", sContentTypeMultiPart);
			 					  httpRequest.queryParam("ReturnUrl", sRetuenUrl );
			 					  httpRequest.multiPart("__RequestVerificationToken", sLoginReqVerificationToken);
			 					  httpRequest.multiPart("UserName", sBKUsername);
			 					  httpRequest.multiPart("Password", sBKPass);
			 					  httpRequest.multiPart("RememberMe", Constants.FALSE);			 					

		objResponse = httpRequest.post(RestAssured.baseURI)
					 			.then().contentType(ContentType.HTML).extract()
					 			.response();
		 if(objResponse==null || objResponse.getStatusCode()!=302)
					throw new Exception("Null values in POST Response for LOGIN BACKOFFICE URL in the function postRequestForBackofficeLogin");			
		 updateMapCookies(objResponse.getHeaders().asList());
			 
		 //HTTP redirect GET request for status 200 
		 RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/home";			
		 objResponse = RestAssured.given()
				 .header("Referer", sReferer)
				 .cookies(mapCookies)
				 .when().get(RestAssured.baseURI)
				 .then().contentType(ContentType.HTML).extract()
				 .response();
		 if(objResponse==null || objResponse.getStatusCode()!=200)
			 throw new Exception("Null values in GEt Redirect Response for LOGIN BACKOFFICE URL in the function postRequestForBackofficeLogin");			
		 XmlPath htmlPathRedirect = new XmlPath(HTML, objResponse.getBody().asString());			 
		 String sLogOut=htmlPathRedirect.getString(BackOfficeMerchandisingAPIConstants.xpathBackOfficeLogOut);			 
		 if(!sLogOut.equalsIgnoreCase(BackOfficeMerchandisingAPIConstants.BACKOFFICE_LOGOUT))
			 throw new Exception("Unable to Fetch BackOffice Login RequestVerificationToken for GET Response LOGIN BACKOFFICE URL in the function postRequestForBackofficeLogin");
		 updateMapCookies(objResponse.getHeaders().asList());					
	}
	
	/**
	 * This function hit http Create rule backoffice url GET request 
	 * fetch create request verification token 
	 * @author Psave
	 */	
	private void generateCreateRuleRequestVerificationToken() throws Exception {
		RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/Create";
		String sReferer = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement" ;			
		objResponse = RestAssured.given()
					 				.header("Referer",sReferer)
					 				.cookies(mapCookies)
					 				.when().get(RestAssured.baseURI)
					 				.then().contentType(ContentType.HTML).extract()
					 				.response();
		if(objResponse==null || objResponse.getStatusCode()!=200)
					throw new Exception("Null values in GET Response for BACKOFFICE CREATE RULE URL in the function generateCreateRuleRequestVerificationToken");			
		XmlPath htmlPath1 = new XmlPath(HTML, objResponse.getBody().asString());
		sCreateRequestVerificationToken=htmlPath1.getString(BackOfficeMerchandisingAPIConstants.xpathRequestVeificationTokenCreateRule);	
		if(sCreateRequestVerificationToken==null || sCreateRequestVerificationToken.equalsIgnoreCase("") || sCreateRequestVerificationToken.isEmpty())
			throw new Exception("Unable to Fetch BackOffice Create Rule RequestVerificationToken for GET Response CREATE RULE BACKOFFICE URL in the function generateCreateRuleRequestVerificationToken");
	}	
	
	private void generateMapCookies(Response res) {
		 mapCookies = new HashMap<String,String>();
		 mapCookies.put("AdobeTargetUserCookie", Constants.TRUE);
		 mapCookies.put("ASP.NET_SessionId", res.getCookie("ASP.NET_SessionId"));
		 mapCookies.put("FeaturesAnnouncements",Constants.TRUE );
		 mapCookies.put("SessionCookie",res.getCookie("ASP.NET_SessionId") );
		 mapCookies.put("ENDEAVOUR_USER_CULTURE_COOKIE",res.getCookie("ENDEAVOUR_USER_CULTURE_COOKIE") );
		 mapCookies.put("__RequestVerificationToken_L1NpdGU1",res.getCookie("__RequestVerificationToken_L1NpdGU1") );
		 mapCookies.put("BO_USER_LOCALE_COOKIE",res.getCookie("ENDEAVOUR_USER_CULTURE_COOKIE") );
	}
	
	private void updateMapCookies(List<Header> lstHeaders) {
		for(Header sHeader:lstHeaders ) {
			if (sHeader.getName().equals("Set-Cookie"))
			{
				mapCookies.put(sHeader.getValue().split("=")[0], sHeader.getValue().split("=")[1].split(";")[0]);
			}
		 }
		
	}	
	public Map<String, String> getCreateRuleRequestCookies() {
		return mapCookies;
	}	
	
	public String getCreateRuleRequestVerificationToken() throws Exception {
		return sCreateRequestVerificationToken;
	}
	
	/**
	 * This function hit http Create rule backoffice url POST request 
	 * after POST, again hit GET request for 200 status and verify backoffice rule creation
	 * @author Psave
	 */	
	public void createBackOfficeRule(Map<String,String> formData) throws Exception {
		int reAttemptCnt = 1;
		int sStatus = 0;
		try {
			RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/Create";
			String sHost = sCountry+"-"+sBackofficeEnv+".ingrammicro.int" ; 
			String sReferer = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/Create" ;
			httpRequest = RestAssured.given().cookies(mapCookies);		
			httpRequest.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig()
					.encodeContentTypeAs("x-www-form-urlencoded",ContentType.URLENC)));		
			httpRequest.header("Referer", sReferer);
			httpRequest.header("Accept-Encoding", "gzip, deflate");
			httpRequest.header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
			httpRequest.contentType(ContentType.URLENC.withCharset("UTF-8"));
			httpRequest.header("Host", sHost);	

			for (Map.Entry<String,String> entry : formData.entrySet())	{
				String key = entry.getKey();
				String value = entry.getValue();
				if(key.equalsIgnoreCase("Location")) {
					ByteBuffer buffer = StandardCharsets.UTF_8.encode(value); 
					String utf8EncodedStringFinal =StandardCharsets.UTF_8.decode(buffer).toString();
					httpRequest.formParam(key, utf8EncodedStringFinal);
				}else {
					httpRequest.formParam(key, value);
				}			
			}		
			while(reAttemptCnt<=10) {
				objResponse = httpRequest.post(RestAssured.baseURI).
						then().contentType(ContentType.HTML).extract()
						.response();
				sStatus =  objResponse.getStatusCode();	
				if(sStatus==302)break;
				reAttemptCnt++;				
			}
			objAPIAssertion.setExtentInfo("POST Request for BACKOFFICE Create URL Expected Status 302 , After Reattempt "+reAttemptCnt+" - Status : "+sStatus );
			if(objResponse==null || (sStatus!=302 && sStatus!=200)) {
				objAPIAssertion.assertHardTrue(objResponse!=null && sStatus==302, "POST Request for BACKOFFICE Create URL Expected Status 302, After Reattempt "+reAttemptCnt+" - Status : "+sStatus+ " RESPONSE : "+objResponse.asString());

				//throw new Exception("Null values in POST Response for BACKOFFICE Create URL in the function createBackOfficeRule, After Reattempt "+reAttemptCnt+" - Status : "+sStatus);	
			}
			//HTTP redirect GET request for status 200 
			reAttemptCnt=1;
			RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement";
			while(reAttemptCnt<=3) {
				objResponse = RestAssured.given()
						.header("Referer", sReferer)
						.cookies(mapCookies)
						.when().get(RestAssured.baseURI)
						.then().contentType(ContentType.HTML).extract()
						.response();
				sStatus =  objResponse.getStatusCode();	
				if(sStatus==200)break;
				reAttemptCnt++;				
			}
			objAPIAssertion.setExtentInfo("GET Redirect Request for BACKOFFICE CREATE RULE URL Expected Status 200 , After Reattempt "+reAttemptCnt+" - Status : "+sStatus );
			if(objResponse==null || objResponse.getStatusCode()!=200) {
				objAPIAssertion.assertHardTrue(objResponse!=null && objResponse.getStatusCode()==200, "GET Redirect Request for BACKOFFICE CREATE RULE URL Expected Status 200, After Reattempt "+reAttemptCnt+" - Status : "+sStatus+ " RESPONSE : "+objResponse.asString());
				//throw new Exception("Null values in GET Redirect Response for BACKOFFICE CREATE RULE URL in the function createBackOfficeRule. Response Status Code : "+objResponse.getStatusCode());	
			}
			
			//Search the Rule in Redirect API response 
			String sResponse = objResponse.getBody().asString();
			String sRuleName = formData.get("Name");
			
			if(sResponse.contains(sRuleName)) {
			
				String sFind1 = sResponse.substring(sResponse.indexOf("data-zone=\""+getPageZoneValue(formData.get("PlacementZone"))+"\""), sResponse.indexOf(formData.get("Name")));
				flagRuleCreated=true;
				sRuleId = sFind1.substring(sFind1.lastIndexOf("id="),sFind1.lastIndexOf("onclick=\"saveState()\"")).replaceAll("\\D+","");
					
			}else {
				//verify the rule name in SEARCH HTTP Response
				searchBackOfficeRule(sRuleName,formData.get("PlacementZone"));
				String sRecordsTotal = objResponse.jsonPath().get("recordsTotal").toString().trim();
				String sNameValue = objResponse.jsonPath().get("data[0].name").toString().trim();
				if(sRecordsTotal.equalsIgnoreCase("1") && sNameValue.equalsIgnoreCase(sRuleName) ) {
					flagRuleCreated=true;
					sRuleId = objResponse.jsonPath().get("data[0].id").toString().trim();							
				}else {		
					objAPIAssertion.assertTrue(flagRuleCreated, "Unable to Search Rule in SEARCH Response, Expected Rule Name = "+sRuleName+" Actual Rule Name = "+sNameValue);
				}				
			}			
		
		}catch(Exception e) {
			String sErrMessage="FAIL: createBackOfficeRule() method of BackOfficeCreateRule: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}	
	
	public boolean deleteBackOfficeRule(Map<String,String> formData) throws Exception{
		boolean flagRuleDeleted = false;
		int sStatus = 0;
		int reAttemptCnt = 1;
		try {
			while(reAttemptCnt<=3) {
				RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/DeleteProductPlacement?id="+sRuleId;
				String sReferer = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement" ;
				httpRequest = RestAssured.given().cookies(mapCookies);
				httpRequest.header("Referer", sReferer);
				httpRequest.header("Accept-Encoding", "GZIP");
				httpRequest.header("Accept", "application/json, text/javascript, */*; q=0.01");		
				httpRequest.header("Content-Type","application/json; charset=utf-8");
				httpRequest.queryParam("id", sRuleId);
				objResponse = httpRequest.post(RestAssured.baseURI);
				sStatus =  objResponse.getStatusCode();
				if(sStatus==200)break;
				reAttemptCnt++;
			}
			if(objResponse==null || sStatus!=200) {
				objAPIAssertion.assertTrue(objResponse!=null && objResponse.getStatusCode()==200, "POST Response for Delete BACKOFFICE RULE URL in the function searchBackOfficeRule for Rule Name "+formData.get("Name")+", After Reattempt "+reAttemptCnt+" - Status : "+objResponse.getStatusCode()+ " RESPONSE : "+objResponse.asString());
				
			}else {
				flagRuleDeleted = true;	
			}
			/*searchBackOfficeRule(formData.get("Name"),formData.get("PlacementZone"));		
			String sRecordsTotal = objResponse.jsonPath().get("recordsTotal").toString().trim();	   	    
			if(sRecordsTotal.equalsIgnoreCase("0")) {
				flagRuleDeleted = true;	   	    
			}	
			*/
		}catch(Exception e) {
			String sErrMessage="FAIL: deleteBackOfficeRule() method of BackOfficeCreateRule: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setSoftAssert_TCFailsButContinuesExecution(sErrMessage);
		}
		return flagRuleDeleted;			
	}
	
	private void searchBackOfficeRule(String sRuleName, String sPageZone) throws Exception {
		int reAttemptCnt = 1;
		try {
		RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/GetProductPlacementsForZone";
		Map<String,String> formDataDelete = new HashMap<String,String>();    	     
		formDataDelete.put("start", "0");
		formDataDelete.put("search[value]", sRuleName);
		formDataDelete.put("search[regex]", "false");
		formDataDelete.put("placementZone", getPageZoneValue(sPageZone));
		formDataDelete.put("order[0][dir]", "asc");
		formDataDelete.put("order[0][column]", "2");
		formDataDelete.put("length", "500");
		formDataDelete.put("draw", "2");  	    

		httpRequest = RestAssured.given().cookies(mapCookies);
		httpRequest.header("Referer", "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement");
		httpRequest.header("Content-Type", sContentTypeMultiPart);
		httpRequest.header("Accept", "application/json, text/javascript, */*; q=0.01");
		httpRequest.header("Accept-Encoding", "GZIP");
		httpRequest.header("Origin", "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int");
		for (Map.Entry<String,String> entry : formDataDelete.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			httpRequest.multiPart(key, value);
		}		
		while(reAttemptCnt<=3) {
			objResponse = httpRequest.post(RestAssured.baseURI);
			if(objResponse.getStatusCode()==200) break;
			reAttemptCnt++;
		}
		objAPIAssertion.setExtentInfo("POST Response for SEARCH BACKOFFICE RULE URL Expected Status 200 for Rule Name "+sRuleName+" , After Reattempt "+reAttemptCnt+" - Status : "+objResponse.getStatusCode() );
		if(objResponse==null || objResponse.getStatusCode()!=200) {
			objAPIAssertion.assertHardTrue(objResponse!=null && objResponse.getStatusCode()==200, "POST Response for SEARCH BACKOFFICE RULE URL in the function searchBackOfficeRule for Rule Name "+sRuleName+", After Reattempt "+reAttemptCnt+" - Status : "+objResponse.getStatusCode()+ " RESPONSE : "+objResponse.asString());
			//throw new Exception("Null values in SEARCH BACKOFFICE RULE URL in the function createBackOfficeRule. Getting Status code : "+objResponse.getStatusCode());	
		}
		}catch(Exception e) {
			String sErrMessage="FAIL: searchBackOfficeRule() method of BackOfficeCreateRule: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}	
	
	
	public void deleteAllTheRules() throws Exception {
		int reAttemptCnt = 1;
		int sStatus = 0;		
		try {			
			for(int i=0;i<=3;i++) {
			searchBackOfficeRule("AUTO_",String.valueOf(i));
			String sRecordsTotal = objResponse.jsonPath().get("recordsTotal").toString().trim();
			System.out.println("Response   : " + objResponse.asString());
			objAPIAssertion.setExtentInfo("Rule Deletion Started For Page Zone : "+getPageZoneValue(String.valueOf(i)));
			objAPIAssertion.setExtentInfo("Total number of Rule need to be delete : "+sRecordsTotal);
			if(!sRecordsTotal.equalsIgnoreCase("0")) {
				String xPathRule ="$..id";					
				String jsonResponse =objResponse.asString();
				List<Integer> sRuleIds = JsonPath.read(jsonResponse, xPathRule);
				for(Integer sId : sRuleIds ) {
					while(reAttemptCnt<=3) {				
						RestAssured.baseURI = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement/DeleteProductPlacement?id="+sId;
						String sReferer = "http://"+sCountry+"-"+sBackofficeEnv+".ingrammicro.int/Site/ProductPlacement" ;
						httpRequest = RestAssured.given().cookies(mapCookies);
						httpRequest.header("Referer", sReferer);
						httpRequest.header("Accept-Encoding", "GZIP");
						httpRequest.header("Accept", "application/json, text/javascript, */*; q=0.01");		
						httpRequest.header("Content-Type","application/json; charset=utf-8");
						httpRequest.queryParam("id", sId);
						objResponse = httpRequest.post(RestAssured.baseURI);
						sStatus =  objResponse.getStatusCode();
						if(sStatus==200)break;
						reAttemptCnt++;
					}				
					objAPIAssertion.assertTrue(objResponse!=null && objResponse.getStatusCode()==200, "POST Response for Delete BACKOFFICE RULE URL. Rule id  "+sId+" is delete successfully, After Reattempt "+reAttemptCnt+" - Status : "+objResponse.getStatusCode());			
				}
			}
			
			}
		}catch(Exception e) {
			String sErrMessage="FAIL: deleteAllTheRules() method of BackOfficeCreateRule: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}	
	
	private String getPageZoneValue(String ePageZone) {
		String sPageZoneValue = null;
		switch (ePageZone) {
		case "3" :			
			sPageZoneValue = BackOfficeMerchandisingAPIConstants.BACKOFFICE_HOME_PAGEZONE;
			break;
		case "1"  :
			sPageZoneValue = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PRODUCTLANDING_PAGEZONE;
			break;	
		case "0"  :		
			sPageZoneValue = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PRODUCTDETAILS_PAGEZONE;
			break;		
		case "2"  :
			sPageZoneValue = BackOfficeMerchandisingAPIConstants.BACKOFFICE_SEARCH_PAGEZONE;
			break;	
	
		default:System.out.println("Placement Zone "+ePageZone.toString()+"is not available!");
		}
		return sPageZoneValue;
	}
	
	public boolean getFlagRuleCreated() {
		return flagRuleCreated;
	}	
	
	public String getRuleID() throws Exception {
		return sRuleId;
	}	
	
	public static void main(String args[]) throws Exception {
	}

}
