package com.im.imonline.api.backoffice.business;


import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_EXACT_LOCATION;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_GLOBAL;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_ALL;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_EXACT;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE_PHRASE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_STATUS;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_PRODUCT_TYPE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_SUB_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_PRODUCT_STATUS;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_PRODUCT_TYPE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_RECORD_FEATURES;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_SUB_CATEGORY;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCTS_BY_VENDOR_NAME;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCT_SELECTION_BY_DYNAMIC;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import com.im.api.core.common.Constants;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.BackofficeMerchandisingTestDataUtil;


public class BackOfficeMerchandisingRuleUnit {
		private String arrLocations[]=null;	
		private String arrProducts[]=null;
		BackofficeMerchandisingTestDataUtil objBkMerchTestDataUtil = null;
		private String arrFields[]=null;
		private String SKU=null;	
		List<String> sFieldsValue=null;
				
		public String getSKU() {
			return SKU.trim();
		}

		public void setSKU(String sKU) {
			SKU = sKU;
		}

		public BackOfficeMerchandisingRuleUnit(String[] arrLocations, BackofficeMerchandisingTestDataUtil objBkMerchTestDataUtil) {
			this.arrLocations=arrLocations;
			this.objBkMerchTestDataUtil=objBkMerchTestDataUtil;
		}
		
		public BackOfficeMerchandisingRuleUnit(String[] arrLocations, String[] arrProducts, BackofficeMerchandisingTestDataUtil objBkMerchTestDataUtil) {
			this.arrLocations=arrLocations;
			this.arrProducts=arrProducts;
			this.objBkMerchTestDataUtil=objBkMerchTestDataUtil;
		}
	
		//category:SOFTWARE,productStatus:instock,RecordFeatures:hasimage
		@SuppressWarnings("null")
		public String getProductsTypesWithData(String sProductSelection) throws Exception {
				String sFinalValue = ProductSearchElasticAPIConstants.NULLCONSTANT;
				if(sProductSelection.equals(PRODUCT_SELECTION_BY_DYNAMIC)) {				
					List<String> listProductTypesWithValue=new ArrayList<String>();
					HashMap<String,HashMap<String,String>> objHsMap = null;
					HashMap<String,String> objSkuSpcData = new HashMap<String,String>();

					if(!getDynamicProductTypeArray(arrProducts).isEmpty()) {
						String[] arrY = getDynamicProductTypeArray(arrProducts).stream().toArray(String[] ::new);
						objHsMap = objBkMerchTestDataUtil.getTopSKUsWithLocationData(arrY);
						if(!objHsMap.isEmpty())	{
							String randomKeyName = (String) objHsMap.keySet().toArray()[new Random().nextInt(objHsMap.keySet().size())];
							objSkuSpcData = objHsMap.get(randomKeyName);
						}
					}	

					for(String sKeyword:arrProducts) {					
						String sValue = fetchDynamicProductTypesValues(objSkuSpcData , sKeyword);	
						String sLocationsiWithValue=sKeyword+Constants.Separator+sValue;				
						listProductTypesWithValue.add(sLocationsiWithValue);	
						sFinalValue = String.join(Constants.SEPARATOR_BY_COMMA, listProductTypesWithValue);
					}
				}
			return sFinalValue;			
		}	
		
		public String fetchDynamicProductTypesValues(HashMap<String,String> objSkuSpcData , String sKeyword) {
				String sValue = null;
				switch (sKeyword) {
				case PRODUCTS_BY_CATEGORY : 
					sValue=objSkuSpcData.isEmpty() ? ProductSearchElasticAPIConstants.NULLCONSTANT 
							: objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT1DESC);
					break;
				case PRODUCTS_BY_SUB_CATEGORY : 
					sValue=objSkuSpcData.isEmpty() ? ProductSearchElasticAPIConstants.NULLCONSTANT 
							: objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT2DESC);
			 		break;				
				case PRODUCTS_BY_PRODUCT_TYPE   : 
					sValue=objSkuSpcData.isEmpty() ? ProductSearchElasticAPIConstants.NULLCONSTANT 
							: objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT3DESC);
			 		break;			
				case PRODUCTS_BY_PRODUCT_STATUS : 
					sValue = ProductSearchElasticAPIConstants.INSTOCK;
					break;
				case PRODUCTS_BY_VENDOR_NAME    : 
					sValue=objSkuSpcData.isEmpty() ? ProductSearchElasticAPIConstants.NULLCONSTANT 
							: objSkuSpcData.get(ProductSearchElasticAPIConstants.VENDORNAME);
			 		break;				
				case PRODUCTS_BY_RECORD_FEATURES :
					sValue = BackOfficeMerchandisingAPIConstants.RECORD_FEATURES_HAS_IMAGE;
					break;	
				default :  System.out.println("Invalid location type!"+sKeyword); 				 
				}
				return sValue;
			}
		@SuppressWarnings("null")
		public String getLocationsWithData() throws Exception {
			String sFinalValue = ProductSearchElasticAPIConstants.NULLCONSTANT;
			ArrayList<String> listLocationsWithValue=new ArrayList<String>();
			HashMap<String,String> objSkuSpcData = null;

				//category:Cables,subCategory:A/V Device Cables,vendorname:V7,exactLocation:true
				for(String sLoc:arrLocations) { 
					String sLocationsiWithValue=null;
					String[] sSplitArr=sLoc.split(Constants.SEPARATOR_BY_COMMA);
					if(!getDynamicLocationsArray(sSplitArr).isEmpty()) {
						String[] arrY = getDynamicLocationsArray(sSplitArr).stream().toArray(String[] ::new);
						HashMap<String,HashMap<String,String>> objHsMap = objBkMerchTestDataUtil.getTopSKUsWithLocationData(arrY);
						if(!objHsMap.isEmpty())	{
							String randomKeyName = (String) objHsMap.keySet().toArray()[new Random().nextInt(objHsMap.keySet().size())];
							objSkuSpcData = objHsMap.get(randomKeyName);
						}else {
							listLocationsWithValue.add(ProductSearchElasticAPIConstants.NULLCONSTANT);
							continue;
						}
					}
					ArrayList<String> strList=getLocationsWithValues(sSplitArr,objSkuSpcData);
					if(strList.size()>1) {
						sLocationsiWithValue=String.join(",",strList);	
					}else if(strList.size()==1) {
						sLocationsiWithValue=strList.get(0);	
					}
					listLocationsWithValue.add(sLocationsiWithValue);


				}
				sFinalValue = String.join(Constants.SEPARATOR_BY_SEMICOLON, listLocationsWithValue);			
			return sFinalValue;		
		}
		
		@SuppressWarnings("null")
		public String getFieldsWithDataAsLoc(String[] sFieldsArr) throws Exception {
			String sFinalValue = ProductSearchElasticAPIConstants.NULLCONSTANT;
			ArrayList<String> strList=getFieldsData(sFieldsArr);				
			sFinalValue = String.join(Constants.SEPARATOR_BY_SEMICOLON, strList);			
			return sFinalValue;		
		}
		
		public String getFieldsWithDataValue() throws Exception {
			return String.join(Constants.SEPARATOR_BY_COMMA, sFieldsValue);		
		}
		
		
		private ArrayList<String> getFieldsData(String[] sSplitArr) {
			ArrayList<String> strList=new ArrayList<String>();
			List<String> lstCategory = objBkMerchTestDataUtil.getCategoryList();
			List<String> lstVendors = objBkMerchTestDataUtil.getVendorsList();
			List<String> lstSku = objBkMerchTestDataUtil.getListOfSKUs();
			String sValue,value;		
			sFieldsValue= new ArrayList<String>();
			Random rand = new Random();
			for(String sKeyword:sSplitArr) {
				switch (sKeyword) {
				case LOC_CATEGORY        :  if(!lstCategory.isEmpty()) {
											value = lstCategory.get(0);
											lstCategory.remove(value);
											sValue=sKeyword+Constants.Separator+value;
				   							strList.add(sValue);
				   							sFieldsValue.add(value);
											}else {
												strList.add(ProductSearchElasticAPIConstants.NULLCONSTANT);	
												sFieldsValue.add(ProductSearchElasticAPIConstants.NULLCONSTANT);
											}
				   							break;				
				case LOC_VENDOR_NAME     : if(!lstVendors.isEmpty()) {
										   value = lstVendors.get(0);
										   lstVendors.remove(value);
										   sValue=sKeyword+Constants.Separator+value;
										   strList.add(sValue);
										   sFieldsValue.add(value);
										   }else {
												strList.add(ProductSearchElasticAPIConstants.NULLCONSTANT);	
												sFieldsValue.add(ProductSearchElasticAPIConstants.NULLCONSTANT);
										   }
										   break;										
				case LOC_KEYWORD_SKU     : if(!lstSku.isEmpty()) {
										   value = lstSku.get(0);
										   lstSku.remove(value);
										   sValue=sKeyword+Constants.Separator+value;
										   strList.add(sValue);
										   sFieldsValue.add(value);
										   }else {
											   strList.add(ProductSearchElasticAPIConstants.NULLCONSTANT);	
											   sFieldsValue.add(ProductSearchElasticAPIConstants.NULLCONSTANT);
										   }
										   break;
				default                  : break; 
				}
			}		
			return strList;			
		}
		
		
		@SuppressWarnings("null")
		private ArrayList<String> getLocationsWithValues(String[] sSplitArr,HashMap<String,String> objSkuSpcData) throws Exception {
			String sValue,value;
			
			ArrayList<String> strList=new ArrayList<String>();
			for(String sKeyword:sSplitArr) {								
				switch (sKeyword) {
				case LOC_GLOBAL          : value=LOC_GLOBAL;
										   strList.add(value);
										   break;	
				case LOC_KEYWORD_SKU     : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.MATERIAL);										   	
										   value=LOC_KEYWORD+Constants.Separator+sValue;
										   strList.add(value);
										   break;											   
				case LOC_KEYWORD         : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.VENDORNAME);										   	
										   value=LOC_KEYWORD+Constants.Separator+sValue;
										   strList.add(value);
										   break;				
				case LOC_CATEGORY        : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT1DESC);										   	
		  								   value=sKeyword+Constants.Separator+sValue;
										   strList.add(value);
										   break;						
				case LOC_MATCHMODE_ALL   : value=LOC_MATCHMODE_ALL;
										   strList.add(value);
										   break;
				case LOC_MATCHMODE_EXACT : value=LOC_MATCHMODE_EXACT;
										   strList.add(value);
										   break;
				case LOC_MATCHMODE_PHRASE: value=LOC_MATCHMODE_PHRASE;
										   strList.add(value);
										   break;
				case LOC_SUB_CATEGORY    : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT2DESC);										   	
										   value=sKeyword+Constants.Separator+sValue;
										   strList.add(value);
										   break;				
				case LOC_PRODUCT_TYPE    : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.CAT3DESC);										   	
		 								   value=sKeyword+Constants.Separator+sValue;
										   strList.add(value);
										   break;										   
				case LOC_VENDOR_NAME     : sValue=objSkuSpcData.get(ProductSearchElasticAPIConstants.VENDORNAME);										   	
										   value=sKeyword+Constants.Separator+sValue;
										   strList.add(value);
										   break;
				case LOC_PRODUCT_STATUS  : sValue="instock"; 
										   value=sKeyword+Constants.Separator+sValue;
										   strList.add(value);
										   break;
				case LOC_EXACT_LOCATION  : value=LOC_EXACT_LOCATION;
										   strList.add(value);
										   break;	
				default                  : System.out.println("Invalid location type!"+sKeyword); 

				}

			}
			System.out.println(strList);	
			return strList;
			
		}		
		
		private ArrayList<String> getDynamicLocationsArray(String[] sSplitArr) {
			ArrayList<String> strList=new ArrayList<String>();
			for(String sKeyword:sSplitArr) {
				switch (sKeyword) {
				case LOC_CATEGORY        : strList.add(ProductSearchElasticAPIConstants.CAT1DESC);
										   break;				
				case LOC_SUB_CATEGORY    : strList.add(ProductSearchElasticAPIConstants.CAT2DESC);
										   break;				
				case LOC_PRODUCT_TYPE    : strList.add(ProductSearchElasticAPIConstants.CAT3DESC);
										   break;									   
				case LOC_VENDOR_NAME     : strList.add(ProductSearchElasticAPIConstants.VENDORNAME);
										   break;
				case LOC_KEYWORD_SKU     : strList.add(ProductSearchElasticAPIConstants.MATERIAL);
										   break;
				case LOC_KEYWORD    	 : strList.add(ProductSearchElasticAPIConstants.VENDORNAME);
				   						   break;			
				default                  : break; 

				}
			}		
			return strList;			
		}
		
		private ArrayList<String> getDynamicProductTypeArray(String[] sSplitArr) {
			ArrayList<String> strList=new ArrayList<String>();
			for(String sKeyword:sSplitArr) {
				switch (sKeyword) {
				case PRODUCTS_BY_CATEGORY     : strList.add(ProductSearchElasticAPIConstants.CAT1DESC);
												break;				
				case PRODUCTS_BY_SUB_CATEGORY : strList.add(ProductSearchElasticAPIConstants.CAT2DESC);
												break;				
				case PRODUCTS_BY_PRODUCT_TYPE : strList.add(ProductSearchElasticAPIConstants.CAT3DESC);
										   		break;									   
				case PRODUCTS_BY_VENDOR_NAME  : strList.add(ProductSearchElasticAPIConstants.VENDORNAME);
												break;
				default                  	  : break; 

				}	
			}
			return strList;			
		}
		
}