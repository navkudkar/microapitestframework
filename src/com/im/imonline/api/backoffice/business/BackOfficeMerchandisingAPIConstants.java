package com.im.imonline.api.backoffice.business;


public class BackOfficeMerchandisingAPIConstants {
	
	//Below are the xpath Constants for Backoffice Rule Creation API
	public static final String xpathRequestVeificationTokenLogin = "html.body.div[0].div.section[1].div.div[1].form.input.@value";
	public static final String xpathRequestVeificationTokenCreateRule = "html.body.div[0].div[0].section[1].form.div[0].div[1].input.@value";
	public static final String xpathBackOfficeLogOut = "html.body.div[0].header.nav.div[1].ul.li.ul.li.div.a";
	
	public static final String BACKOFFICE_LOGOUT = "Log Out";
	
	
	public static final String LOC_CATEGORY ="category";
	public static final String LOC_SUB_CATEGORY ="subCategory";
	public static final String LOC_VENDOR_NAME ="vendorname";
	public static final String LOC_KEYWORD ="keyword";
	public static final String LOC_MATCHMODE="matchMode";
	public static final String LOC_PRODUCT_TYPE="productType";
	public static final String LOC_PRODUCT_STATUS="productStatus";
	public static final String LOC_EXACT_LOCATION="exactLocation:true";
	public static final String LOC_EXACT_LOCATION_FLAG="exactLocation";
	public static final String LOC_GLOBAL ="Global";
	public static final String LOC_KEYWORD_SKU ="keywordsku";
	public static final String LOC_MATCHMODE_PHRASE="matchMode:Match Phrase";
	public static final String LOC_MATCHMODE_EXACT="matchMode:Match Exact";
	public static final String LOC_MATCHMODE_ALL="matchMode:Match All";
	
	public static final String PRODUCT_SELECTION_BY_SPECIFIC ="specific";
	public static final String PRODUCT_SELECTION_BY_DYNAMIC ="dynamic";
	public static final String PRODUCTS_BY_CATEGORY ="category";
	public static final String PRODUCTS_BY_SUB_CATEGORY ="subCategory";
	public static final String PRODUCTS_BY_VENDOR_NAME ="vendorname";
	public static final String PRODUCTS_BY_KEYWORD ="keyword";
	public static final String PRODUCTS_BY_PRODUCT_STATUS="productStatus";
	public static final String PRODUCTS_BY_PRODUCT_TYPE="productType";
	public static final String PRODUCTS_BY_GLOBAL ="Global";
	public static final String PRODUCTS_BY_RECORD_FEATURES="RecordFeatures";
	public static final String RECORD_FEATURES_HAS_IMAGE="hasimage";
	
	public final static String LOGIN_TRIGGER_MODE= "LoggedInOnly"; 
	public final static String LOGOUT_TRIGGER_MODE = "LoggedOutOnly"; 
	public final static String LOGIN_AND_LOGOUT_TRIGGER_MODE = "Both"; 
	public static final String BACKOFFICE_HOME_PAGEZONE = "HomePageZone";
	public static final String BACKOFFICE_SEARCH_PAGEZONE = "SearchPageZone";
	public static final String BACKOFFICE_PRODUCTLANDING_PAGEZONE = "ProductLandingPageZone";
	public static final String BACKOFFICE_PRODUCTDETAILS_PAGEZONE = "ProductDetailsPageZone";
	public static final String BACKOFFICE_PPR_PLACEMENTTYPE = "ProductPlacementRight";
	public static final String BACKOFFICE_PPC_PLACEMENTTYPE = "ProductPlacementCentre";
	public static final String BACKOFFICE_TEXTADS_PLACEMENTTYPE = "TextAds";
	public static final String BACKOFFICE_BGADS_PLACEMENTTYPE = "BackgroundAdvert";
	public static final String BACKOFFICE_AD_497_WIDTH_PLACEMENTTYPE = "FourNineSevenWidthCentreVendorAd2";
	public static final String BACKOFFICE_RECOMMENDED_PRODUCTS_PLACEMENTTYPE = "RecommendedProducts";
	public static final String BACKOFFICE_AD_223_WIDTH_PLACEMENTTYPE = "TwoTwoThreeWidthClickAds";	
	public static final String BACKOFFICE_PROMOTIONS_PRODUCTS_PLACEMENTTYPE = "Promotions";
	public static final String BACKOFFICE_BUY_IT_AGAIN_PLACEMENTTYPE = "BuyItAgain";
	public static final String BACKOFFICE_YOU_MAY_LIKE_PLACEMENTTYPE = "YouMayAlsoLike";
	public static final String BACKOFFICE_TRENDING_PRODUCTS_PLACEMENTTYPE = "TrendingProducts";
	public static final String BACKOFFICE_FEATURED_CATEGORIES_PLACEMENTTYPE = "FeaturedCategories";
	public static final String BACKOFFICE_PEOPLE_ALSO_BAUGHT_PLACEMENTTYPE = "PeopleAlsoBought";
	public static final String BACKOFFICE_FEATURED_CATEGORIES_LOW_PRIORITY = "FeaturedCategories_Low Priority";
	public static final String BACKOFFICE_FEATURED_CATEGORIES_HIGH_PRIORITY = "FeaturedCategories_High Priority";
	public static final String BACKOFFICE_SPONSORED_PLACEMENTTYPE = "SponsoredProducts";
	public static final String BACKOFFICE_TOP_BANNER_PLACEMENTTYPE = "SearchResultSectionTopBanner";
	public static final String BACKOFFICE_BOTTOM_BANNER_PLACEMENTTYPE = "BottomBanner";
	public static final String BACKOFFICE_MIDDLE_BANNER_PLACEMENTTYPE = "MiddleBanner";
	public static final String BACKOFFICE_SKYSCRAPPER_BANNER = "SkyScrapperBanner";
	
	public static final String[] arrKeysToBeCapturedForSKUs = {"cat1desc", "cat2desc", "vendorname", "material"};
	public static final String EXCLUDEBIRULES = "excludebirules";
	public static final boolean FALSE = false;
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String ZONE = "zone";
	public static final String KEYWORDS = "keywords";
	public static final String REFINEMENT_COUNT = "refinementcount";
	public static final String REFINEMENTS = "refinements";
}
