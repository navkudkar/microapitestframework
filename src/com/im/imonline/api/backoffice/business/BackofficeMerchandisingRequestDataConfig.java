package com.im.imonline.api.backoffice.business;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.api.core.common.Constants;

/**
 * @author Paresh Save
 * BackofficeMerchandisingRequestDataConfig
 * Backoffice Merchandising Automation -- Request Hashmap Formation Config Class
 */
public class BackofficeMerchandisingRequestDataConfig {

	Logger logger = Logger.getLogger("BackofficeMerchandisingRequestDataConfig"); 
	private HashMap<String, String> pRequestMap=null;
	private HashMap<String, String> pExcelHMap=null;
	private HashMap<String, String> pFormDataMap=new HashMap<String, String>();	
	private BackOfficeRuleCreateRegister objBkRuleReg = null;	


	public BackofficeMerchandisingRequestDataConfig(BackOfficeRuleCreateRegister objBackofficeReg, HashMap<String, String> pExcelHMap) throws Exception {
		objBkRuleReg = objBackofficeReg;		
		pFormDataMap = (HashMap<String, String>) objBkRuleReg.getFormDataRequest();
		pRequestMap=new HashMap<String,String>();
		this.pExcelHMap = pExcelHMap;		
	}	
	
	public HashMap<String, String> generatRequestMap(String sLocation) {
		try {
			pRequestMap.put(BackOfficeMerchandisingAPIConstants.EXCLUDEBIRULES,"false");	
			pRequestMap.put(BackOfficeMerchandisingAPIConstants.ID,pExcelHMap.get(BackOfficeMerchandisingAPIConstants.ID));
			pRequestMap.put(BackOfficeMerchandisingAPIConstants.TYPE,objBkRuleReg.getPlacementType());
			pRequestMap.put(BackOfficeMerchandisingAPIConstants.ZONE,objBkRuleReg.getPageZone());
			setKeyword(sLocation);		
			setRefinements(sLocation);	
		}catch (Exception e) {
			System.out.println("Error in the function generatRequestMap in BackofficeMerchandisingRequestDataConfig "+e);
		}
		return pRequestMap;
	}	
	
	private void setKeyword(String sLocation) throws Exception {
		try {		
			if(!objBkRuleReg.getFieldsLevelFlag() && sLocation.contains(BackOfficeMerchandisingAPIConstants.LOC_KEYWORD)) {
				int begIndex = sLocation.indexOf(BackOfficeMerchandisingAPIConstants.LOC_KEYWORD+Constants.Separator);
				int endIndex = sLocation.indexOf(Constants.SEPARATOR_BY_COMMA+BackOfficeMerchandisingAPIConstants.LOC_MATCHMODE);
				String sKeywordValue = sLocation.substring(begIndex,endIndex).trim().split(Constants.Separator)[1];
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.KEYWORDS,sKeywordValue);
			}else if(objBkRuleReg.getFieldsLevelFlag() && objBkRuleReg.getFieldsLevelFlagName().contains(BackOfficeMerchandisingAPIConstants.LOC_KEYWORD)) {
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.KEYWORDS,sLocation.split(Constants.Separator)[1]);
			}else if(objBkRuleReg.getExactLocNegative()) {
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.KEYWORDS,"a");
			}else {
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.KEYWORDS,"");
			}
			
		}catch (Exception e) {
			System.out.println("Error in the function setKeyword in BackofficeMerchandisingRequestDataConfig "+e);
		}
	}
	
	private void setRefinementCount() throws Exception {
		try {
			String sLocation =  objBkRuleReg.getLocation();
			if(!(sLocation==null||sLocation.isEmpty()||sLocation.equalsIgnoreCase(BackOfficeMerchandisingAPIConstants.LOC_GLOBAL))) {
				String sFinal = pRequestMap.get(BackOfficeMerchandisingAPIConstants.REFINEMENTS);				
				int size = 0;
				Pattern p = Pattern.compile("\"name\":");
				Matcher m = p.matcher( sFinal );
				while (m.find()) {
					size++;
				}				
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENT_COUNT,Integer.toString(size));
			}else if(objBkRuleReg.getFieldsLevelFlag()) {
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENT_COUNT,Integer.toString(1));
			}
			
		}catch (Exception e) {
			System.out.println("Error in the function setRefinementCount in BackofficeMerchandisingRequestDataConfig "+e);
		}
	}
	
	private void setRefinements(String sLocation) throws Exception {
		try {	
			if(!(sLocation==null||sLocation.isEmpty()||sLocation.equalsIgnoreCase(BackOfficeMerchandisingAPIConstants.LOC_GLOBAL))) {
				
				 List<String> lstListLocation= Arrays.asList(sLocation.split(Constants.SEPARATOR_BY_SEMICOLON)).stream()
						.filter(s->!s.contains(BackOfficeMerchandisingAPIConstants.LOC_KEYWORD)).collect(Collectors.toList());	
				 
				 if(!lstListLocation.isEmpty()) {
					 String sListLocations = lstListLocation.stream().collect(Collectors.toList()).toString();

					 String sFinalLocation = Arrays.asList(sListLocations.split(Constants.SEPARATOR_BY_COMMA)).stream()
							 .filter(s->!s.contains(BackOfficeMerchandisingAPIConstants.LOC_EXACT_LOCATION)).collect(Collectors.toList()).toString();

					 String sfinal = sFinalLocation.replaceAll(":", "\":\"");
					 sfinal = "\""+sfinal.replaceAll(",", "\",\"")+"\"";
					 sfinal = sfinal.replace("[", "");
					 sfinal = sfinal.replace("]", "");
					 List<String> sRefinements = Arrays.asList(sfinal.split(Constants.SEPARATOR_BY_COMMA));

					 String sRefinment = sRefinements.stream().map(s->"{\"name\":"+s.replace(":", ",\"value\":")+"}").collect(Collectors.toList()).toString();
					 sRefinment = sRefinment.replace("[", "");
					 sRefinment = sRefinment.replace("]", "");
					 sRefinment = sRefinment.replace("\" ", "\"");
					 sRefinment = sRefinment.replace(", ", ",");

					 pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENTS,sRefinment);
					 setRefinementCount();
				 }else {
					 pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENT_COUNT,"");
					 pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENTS,"\"\"");
				 }
			}else if(objBkRuleReg.getPageZone().equals(BackOfficeMerchandisingAPIConstants.BACKOFFICE_HOME_PAGEZONE)
					||objBkRuleReg.getPageZone().equals(BackOfficeMerchandisingAPIConstants.BACKOFFICE_PRODUCTLANDING_PAGEZONE)||
					sLocation.equalsIgnoreCase(BackOfficeMerchandisingAPIConstants.LOC_GLOBAL)) {
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENT_COUNT,"");
				pRequestMap.put(BackOfficeMerchandisingAPIConstants.REFINEMENTS,"\"\"");
			}
			
		}catch (Exception e) {
			System.out.println("Error in the function setRefinements in BackofficeMerchandisingRequestDataConfig "+e);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub	
	}
}
