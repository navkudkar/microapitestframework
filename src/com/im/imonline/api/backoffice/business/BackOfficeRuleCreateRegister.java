package com.im.imonline.api.backoffice.business;



import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGIN_AND_LOGOUT_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGIN_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.LOGOUT_TRIGGER_MODE;
import static com.im.imonline.api.backoffice.business.BackOfficeMerchandisingAPIConstants.PRODUCT_SELECTION_BY_SPECIFIC;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.imonline.api.backoffice.business.BackOfficeMerchandisingEnums.BackOfficePlacementType;
import com.im.imonline.api.backoffice.business.BackOfficeMerchandisingEnums.PageZone;
import com.im.imonline.api.testdata.util.BackofficeMerchandisingTestDataUtil;

public class BackOfficeRuleCreateRegister {		
				
		private PageZone ePageZone = null;
		private BackOfficePlacementType eBackOffPlaceType = null;
		private String sPageZone,sPageZoneValue,sPlacementType,sPlacementTypeValue ;
		private BackofficeMerchandisingTestDataUtil objTestData = null;
		private String sRequestVerificationToken = null;
		private Map<String,String> mapCookies = null;
		private String sProductsSelectionValue = null;
		private String sProductsSelectionOrderValue = null;
		private String sLocationValue = "";
		private String sTriggerModeValue = null ,sTriggerMode;
		private String sActiveValue = Constants.TRUE;
		private String sStartDateValue = null;
		private String sEndDateValue ="01/01/2099";
		private final String RULE_NAME_START_INITIAL ="AUTO_";
		private String sRuleNameValue = null;
		private String sTitleValue = null;
		private String sBodyTextValue ="TEXT ADS RULE BODY TEXT FOR RULE :";
		private String sLinkTextValue=null;
		private String sProductSelection=null,sRuleId =null;
		private int cntSpecificProducts;
		private String[] arrBOProducts = null,arrBOLocations = null;
		private boolean flagRuleDisplay = true;
		private boolean flagExcatLocationNegative = false;
		private String sFieldsValueAsLoc = null;
		private String sFieldsNames = null;
		private String sFieldsLevelFlagNames = null;
		private static final String TITLE_URL ="https://www.apple.com/in/";
		private static final String TEXTADS_LINK_URL ="https://www.lenovo.com/in/en/pc/";
		private String sBodyText ="TEXT ADS RULE BODY TEXT FOR RULE :";
		private String sLinkText=null;
		private String[] arrVendorsList = null,arrCategoryList = null;
		BackOfficeMerchandisingRuleUnit objRuleUnit;
		private boolean flagVendorFieldsDisplay = false, flagCategoryFieldsDisplay = false , flagSKUsFieldsDisplay = false ;
		
		private static final String TWOTWOTHREE_IMAGE_URL ="http://at.ingrammicro.com/media/ExternalMediaLibrary/Images/2019/Sidebar/BeQuiet_KW24-27_223x122_DRSlim.jpg";
		private static final String FOURNINESEVEN_IMAGE_URL ="http://es-new.ingrammicro.com/media/ExternalMediaLibrary/LoggedIn_Homepage/zwave25072018vendor.jpg";
		private static final String BACKGROUNDADVERT_IMAGE_URL ="https://at.ingrammicro.com/media/ExternalMediaLibrary/Images/2018/BackgroundAds/BackgroundAd_Fallback.jpg";
		private static final String FEATURED_CATEGORIES_IMAGE_URL = "https://ca.ingrammicro.com/media/ExternalMediaLibrary/Category%20Images/Accessories.jpg";
		private static final String TOP_BANNER_IMAGE_URL = "http://ca.ingrammicro.com/cms/media/ExternalMediaLibrary/NewUI-Images/CloudMarketPlace_1280x160_1.jpg?width=1280&height=160&ext=.jpg";
		private static final String BOTTOM_BANNER_IMAGE_URL = "http://ca.ingrammicro.com/media/ExternalMediaLibrary/223/BannerAd-BenQ-MH760_1.jpg";
		private static final String MIDDLE_BANNER_IMAGE_URL = "https://esecommercestg.ingrammicro.com/cms/media/ExternalMediaLibrary/LoggedIn_Homepage/HomePage_MiddleCentre_A_606x124.jpg";
		private static final String SKYSCRAPPER_IMAGE_URL ="https://ca.ingrammicro.com/media/ExternalMediaLibrary/288/Samsung_2029_BusinessTV_SkyScraper_288x450_EN.jpg";
		
		List<BackOfficePlacementType> lstProductSpecificPlacement = Arrays.asList(BackOfficePlacementType.RECOMMENDED_PRODUCTS,BackOfficePlacementType.PROMOTIONS_PRODUCTS
				,BackOfficePlacementType.FEATURED_CATEGORIES,BackOfficePlacementType.BUY_IT_AGAIN_PRODUCTS,BackOfficePlacementType.YOU_MAY_ALSO_LIKE_PRODUCTS
				,BackOfficePlacementType.TRENDING_PRODUCTS,BackOfficePlacementType.PEOPLE_ALSO_BOUGHT_PRODUCTS,BackOfficePlacementType.PRODUCT_PLACEMENT_CENTRE,
				BackOfficePlacementType.PRODUCT_PLACEMENT_RIGHT, BackOfficePlacementType.SPONSORED_PRODUCTS);
		List<BackOfficePlacementType> lstImageSpecificPlacement = Arrays.asList(BackOfficePlacementType.AD_223_WIDTH,BackOfficePlacementType.AD_497_WIDTH
				,BackOfficePlacementType.FEATURED_CATEGORIES,BackOfficePlacementType.BACKGROUND_ADVERT,BackOfficePlacementType.TEXTADS
				,BackOfficePlacementType.MIDDLE_BANNER,BackOfficePlacementType.BOTTOM_BANNER,BackOfficePlacementType.TOP_BANNER,BackOfficePlacementType.SKYSCRAPPER_BANNER);
		
		public BackOfficeRuleCreateRegister(PageZone ePageZone, BackOfficePlacementType ePlacementType,
				BackofficeMerchandisingTestDataUtil objTestData) throws Exception{
			this.ePageZone = ePageZone;
			this.eBackOffPlaceType = ePlacementType;
			this.objTestData = objTestData;
			setPlacementTypeValue(ePlacementType);
			setPageZoneValue(ePageZone);		
			setDefaultSetting();
		}

		public void setLocationsTypeWithProductsSelectionType(String[] arrBOLocations, String[] arrBOProducts) throws Exception {
			objRuleUnit = new BackOfficeMerchandisingRuleUnit(arrBOLocations,arrBOProducts,objTestData);
			this.arrBOLocations = arrBOLocations;
			this.arrBOProducts = arrBOProducts;
			sProductsSelectionValue = objRuleUnit.getProductsTypesWithData(sProductSelection);
			sLocationValue = objRuleUnit.getLocationsWithData();
		}

		public void setLocationsType(String[] arrBOLocations) throws Exception {
			objRuleUnit = new BackOfficeMerchandisingRuleUnit(arrBOLocations,objTestData);
			this.arrBOLocations = arrBOLocations;
			sLocationValue = objRuleUnit.getLocationsWithData();
		}
		
		public void setProductSelection(String sProductSelection, int cntSpecificProducts) throws Exception {
			this.sProductSelection = sProductSelection;	
			List<String> lstProductSKu = objTestData.getListOfSKUs();
			List<String> lstOrderProductSku = new ArrayList<String>();
			if(sProductSelection.equals(PRODUCT_SELECTION_BY_SPECIFIC)) {
				sProductsSelectionValue = lstProductSKu.stream()
					      		.map(n -> String.valueOf(n)).limit(cntSpecificProducts)
					      		.collect(Collectors.joining(","));
				String[] sArrySkus = sProductsSelectionValue.split(",");
			for(int cnt=0;cnt<sArrySkus.length;cnt++) {
				String sSku = sArrySkus[cnt]+":"+(cnt+1);
				lstOrderProductSku.add(sSku);
					
			}
			sProductsSelectionOrderValue = lstOrderProductSku.stream()
		      		.map(n -> String.valueOf(n))
		      		.collect(Collectors.joining(","));			
			}		
		}
		public void setProductSelection(String sProductSelection) throws Exception {
			int cntSpecificProducts = eBackOffPlaceType==BackOfficePlacementType.SPONSORED_PRODUCTS ? 3 : 15;
			setProductSelection(sProductSelection,cntSpecificProducts) ;		
		}	
		
		public void setTriggerMode(String sTriggerMode) {
			switch (sTriggerMode) {
			case LOGIN_TRIGGER_MODE : 
				sTriggerModeValue="2";
				this.sTriggerMode = sTriggerMode;
				break;
			case LOGOUT_TRIGGER_MODE : 
				sTriggerModeValue="1";
				this.sTriggerMode = sTriggerMode;
		 		break;				
			case LOGIN_AND_LOGOUT_TRIGGER_MODE   : 
				sTriggerModeValue="0";
				this.sTriggerMode = sTriggerMode;
				break;			
			default :  System.out.println("Invalid Trigger type!"+sTriggerMode); 				 
			}				
		}
		
		public void setDefaultSetting() throws Exception {
			setStartDate();
			setRuleNameAndTitle();
			if(eBackOffPlaceType==BackOfficePlacementType.TEXTADS) {
				sBodyTextValue=sBodyTextValue+sRuleNameValue;
				sLinkTextValue=sRuleNameValue;	
			}
		}
		
		public void setActive(boolean bFlag) throws Exception {
		    if(!bFlag) {
		    	sActiveValue=Constants.FALSE;
		    	flagRuleDisplay = false;
		    }
		}
		
		public void setStartDate() throws Exception {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf;
			cal.setTime(new Date());
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-3);// substract 7 days			
			String sDefaultCountry = "CA,US,MI,SE,HU";
			if(sDefaultCountry.contains(objTestData.getCountry())) 
				sdf = new SimpleDateFormat("MM/dd/yyyy");
			else
				sdf = new SimpleDateFormat("dd/MM/YYYY");
			sStartDateValue=sdf.format(cal.getTime());							
		}
		
		public void setEndDateExpire() throws Exception {
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf;
			cal.setTime(new Date());
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);// substract 1 days			
			String sDefaultCountry = "CA,US,MI,SE,HU";
			if(sDefaultCountry.contains(objTestData.getCountry())) 
				sdf = new SimpleDateFormat("MM/dd/yyyy");
			else
				sdf = new SimpleDateFormat("dd/MM/YYYY");
			sEndDateValue=sdf.format(cal.getTime());
			flagRuleDisplay = false;
			
		}
		
		public void setNegativeExactLocation() throws Exception {
			flagRuleDisplay = false;	
			flagExcatLocationNegative = true;
		}
		
		public void setRuleNameAndTitle() throws Exception {
			if(eBackOffPlaceType==BackOfficePlacementType.FEATURED_CATEGORIES) {
				sRuleNameValue=RULE_NAME_START_INITIAL+"CATEGORY"+"_"+Util.getRandomNumberInRange(111,999);
				sTitleValue = sRuleNameValue;
			}else {
				sRuleNameValue=RULE_NAME_START_INITIAL+eBackOffPlaceType.toString()+"_"+Util.getRandomNumber();
				sTitleValue = sRuleNameValue;
			}		    	
		}	
		
		public void setFieldsLevelName(String sFieldsLevelNames) throws Exception {
			String[] arrFieldsList = sFieldsLevelNames.split(Constants.SEPARATOR_BY_COMMA);	
			sFieldsValueAsLoc = objRuleUnit.getFieldsWithDataAsLoc(arrFieldsList);			
			sFieldsNames = objRuleUnit.getFieldsWithDataValue();
			setFlagForFields(arrFieldsList[0]);
			
		}
		
		public void setFlagForFields(String sFieldFlagName) throws Exception {
			switch (sFieldFlagName) {
			case BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME :
				flagVendorFieldsDisplay = true;
				sFieldsLevelFlagNames = BackOfficeMerchandisingAPIConstants.LOC_VENDOR_NAME;
				break;
			case BackOfficeMerchandisingAPIConstants.LOC_CATEGORY  :
				flagCategoryFieldsDisplay = true;
				sFieldsLevelFlagNames = BackOfficeMerchandisingAPIConstants.LOC_CATEGORY;
				break;	
			case BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU  :
				flagSKUsFieldsDisplay = true;
				sFieldsLevelFlagNames = BackOfficeMerchandisingAPIConstants.LOC_KEYWORD_SKU ;
				break;			
			default:System.out.println("Placement Type "+sFieldFlagName.toString()+"is not available!");
			}		
		}		
		
		private void setPlacementTypeValue(BackOfficePlacementType ePlacementType) {
			switch (ePlacementType) {
			case RECOMMENDED_PRODUCTS :
				sPlacementTypeValue = "2";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_RECOMMENDED_PRODUCTS_PLACEMENTTYPE;
				break;
			case PROMOTIONS_PRODUCTS  :
				sPlacementTypeValue = "25";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PROMOTIONS_PRODUCTS_PLACEMENTTYPE;
				break;	
			case YOU_MAY_ALSO_LIKE_PRODUCTS  :
				sPlacementTypeValue = "26";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_YOU_MAY_LIKE_PLACEMENTTYPE;
				break;					
			case BUY_IT_AGAIN_PRODUCTS  :
				sPlacementTypeValue = "21";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_BUY_IT_AGAIN_PLACEMENTTYPE;
				break;	
			case TRENDING_PRODUCTS :
				sPlacementTypeValue = "23";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_TRENDING_PRODUCTS_PLACEMENTTYPE;
				break;
			case FEATURED_CATEGORIES :
				sPlacementTypeValue = "20";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_FEATURED_CATEGORIES_PLACEMENTTYPE;
				break;
			case PEOPLE_ALSO_BOUGHT_PRODUCTS :
				sPlacementTypeValue = "27";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PEOPLE_ALSO_BAUGHT_PLACEMENTTYPE;
				break;
			case AD_223_WIDTH :
				sPlacementTypeValue = "9";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_AD_223_WIDTH_PLACEMENTTYPE;
				break;
			case SPONSORED_PRODUCTS :
				sPlacementTypeValue = "24";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_SPONSORED_PLACEMENTTYPE;
				break;
			case TOP_BANNER :
				sPlacementTypeValue = "43";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_TOP_BANNER_PLACEMENTTYPE;
				break;
			case BOTTOM_BANNER :
				sPlacementTypeValue = "41";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_BOTTOM_BANNER_PLACEMENTTYPE;
				break;
			case MIDDLE_BANNER :
				sPlacementTypeValue = "42";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_MIDDLE_BANNER_PLACEMENTTYPE;
				break;
			case PRODUCT_PLACEMENT_RIGHT : 
				sPlacementTypeValue = "0";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PPR_PLACEMENTTYPE;
				break;
			case PRODUCT_PLACEMENT_CENTRE :
				sPlacementTypeValue = "1";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PPC_PLACEMENTTYPE;
				break;
			case BACKGROUND_ADVERT :
				sPlacementTypeValue = "3";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_BGADS_PLACEMENTTYPE;
				break;
			case AD_497_WIDTH :
				sPlacementTypeValue = "4";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_AD_497_WIDTH_PLACEMENTTYPE;
				break;
			case TEXTADS :
				sPlacementTypeValue = "8";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_TEXTADS_PLACEMENTTYPE;
				break;	
			case SKYSCRAPPER_BANNER : 
				sPlacementTypeValue = "62";
				sPlacementType = BackOfficeMerchandisingAPIConstants.BACKOFFICE_SKYSCRAPPER_BANNER;
				break;
			default:System.out.println("Placement Type "+ePlacementType.toString()+"is not available!");
			}		
			 
		}
		
		private void setPageZoneValue(PageZone ePageZone) {
			switch (ePageZone) {
			case HOME_PAGE :
				sPageZoneValue = "3";
				sPageZone = BackOfficeMerchandisingAPIConstants.BACKOFFICE_HOME_PAGEZONE;
				break;
			case PRODUCT_LANDING_PAGE  :
				sPageZoneValue = "1";
				sPageZone = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PRODUCTLANDING_PAGEZONE;
				break;	
			case PRODUCT_DETAILS_PAGE  :
				sPageZoneValue = "0";
				sPageZone = BackOfficeMerchandisingAPIConstants.BACKOFFICE_PRODUCTDETAILS_PAGEZONE;
				break;		
			case SEARCH_PAGE  :
				sPageZoneValue = "2";
				sPageZone = BackOfficeMerchandisingAPIConstants.BACKOFFICE_SEARCH_PAGEZONE;
				break;	
		
			default:System.out.println("Placement Type "+ePageZone.toString()+"is not available!");
			}				
		}		
		
		public Map<String,String> getFormDataRequest() throws Exception {
			Map<String,String> formData = new HashMap<String,String>();    	     
			formData.put("__RequestVerificationToken", objTestData.getCreateRuleRequestVerificationToken());
			formData.put("Id", "");
			formData.put("CreateType", "0");
			formData.put("Name", sRuleNameValue);
			formData.put("PlacementType",sPlacementTypeValue);
			formData.put("PlacementZone", sPageZoneValue);
			formData.put("Location", sLocationValue);
			formData.put("IsActive", sActiveValue.toLowerCase());
			formData.put("Priority", "");
			formData.put("StartDate", sStartDateValue);
			formData.put("EndDate", sEndDateValue);
			formData.put("Title", sTitleValue);
			//formData1.put("TitleTextColor", "");
			// formData1.put("TitleUrl", "");
			// formData1.put("ImageUrl", "");
			// formData1.put("TargetUrl", "");
			// formData1.put("BodyText", "");
			// formData1.put("LinkText", "");
			// formData1.put("LinkUrl", "");
			// formData1.put("HoverText", "");   	  
			formData.put("TriggerMode", sTriggerModeValue);
			formData.put("NewWindowFlg", "false");
			//formData1.put("ContactGroups", "");
			formData.put("AddSecurityToken", "false");
			// formData1.put("DurationSeconds", "");
			// formData1.put("GlobalSkusForProductIds", "");
						
			// formData1.put("MaxProductRecords", ""); 
			// formData1.put("RotationDuration", ""); 
			formData.put("IsConfigurator", "false");   	    
			formData.put("Random", "false");
			formData.put("ShowAll", "false");   	     
			//  formData1.put("ShowAllText", ""); 
			// formData1.put("TargetMaterial", ""); 
			// formData1.put("Vendor", ""); 
			//  formData1.put("Category1", ""); 
			Map<String,String> formProductSpecificData = generateProductsSpecificMap();    
			Map<String,String> formImageSpecificData = generateImagePlacementSpecificMap(); 
			Map<String,String> formFieldsSpecificData = generateFieldsSpecificMap();
			if(!formProductSpecificData.isEmpty())formData.putAll(formProductSpecificData);
			if(!formImageSpecificData.isEmpty())formData.putAll(formImageSpecificData);
			if(!formFieldsSpecificData.isEmpty())formData.putAll(formFieldsSpecificData);
			return formData;
		}
		
		private Map<String,String> generateProductsSpecificMap() throws Exception {	
			Map<String,String> formData = new HashMap<String,String>();    
			if(lstProductSpecificPlacement.contains(eBackOffPlaceType)) {
				if(sProductSelection.equals(PRODUCT_SELECTION_BY_SPECIFIC)) {
					formData.put("ProductIds", sProductsSelectionValue);
					formData.put("DisplayOrderForProductIds", sProductsSelectionOrderValue);
				}else {
					ByteBuffer buffer = StandardCharsets.UTF_8.encode(sProductsSelectionValue); 
					String utf8EncodedString = StandardCharsets.UTF_8.decode(buffer).toString();
					formData.put("ProductQuery", utf8EncodedString);
				}
			}
			return formData;			
		}	
		
		private Map<String,String> generateFieldsSpecificMap() throws Exception {	
			Map<String,String> formData = new HashMap<String,String>();    
			if(flagVendorFieldsDisplay) {
				formData.put("Vendor",sFieldsNames);
			}else if(flagCategoryFieldsDisplay){
				formData.put("Category1",sFieldsNames);
			}else if(flagSKUsFieldsDisplay) {
				formData.put("Sku",sFieldsNames);
			}
			return formData;			
		}	
		
		private Map<String,String> generateImagePlacementSpecificMap() throws Exception {			
			Map<String,String> formData = new HashMap<String,String>();    
			if(lstImageSpecificPlacement.contains(eBackOffPlaceType)) {
				switch (eBackOffPlaceType) {
				case FEATURED_CATEGORIES :
					formData.put("ImageUrl",FEATURED_CATEGORIES_IMAGE_URL);
					break;
				case AD_223_WIDTH :
					formData.put("ImageUrl",TWOTWOTHREE_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					formData.put("HoverText",sTitleValue) ; 
					break;
				case TOP_BANNER :
					formData.put("ImageUrl",TOP_BANNER_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					formData.put("HoverText",sTitleValue) ; 
					break;					
				case BOTTOM_BANNER :
					formData.put("ImageUrl",BOTTOM_BANNER_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					formData.put("HoverText",sTitleValue) ; 
					break;							
				case MIDDLE_BANNER :
					formData.put("ImageUrl",MIDDLE_BANNER_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					formData.put("HoverText",sTitleValue) ; 
					break;								
				case BACKGROUND_ADVERT :
					formData.put("ImageUrl",BACKGROUNDADVERT_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					break;
				case AD_497_WIDTH :
					formData.put("ImageUrl",FOURNINESEVEN_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					formData.put("HoverText",sTitleValue) ; 
					break;
				case TEXTADS :					
					formData.put("TitleUrl",TITLE_URL);
					formData.put("BodyText",sBodyTextValue);
					formData.put("LinkText",sLinkTextValue);					
					formData.put("LinkUrl",TEXTADS_LINK_URL);
					break;	
				case SKYSCRAPPER_BANNER : 
					formData.put("TitleUrl",TITLE_URL);
					formData.put("ImageUrl",SKYSCRAPPER_IMAGE_URL);
					formData.put("TargetUrl",TITLE_URL);
					//formData.put("BodyText",sBodyTextValue);
					//formData.put("LinkText",sLinkTextValue);					
					//formData.put("LinkUrl",TEXTADS_LINK_URL);
					formData.put("HoverText",sTitleValue) ; 
				default:System.out.println("Placement Type "+eBackOffPlaceType.toString()+"is not available!");
				}		
			}
			return formData;			
		}
		
		
		
		public String getCreateRuleRequestVerificationToken() throws Exception {
			return objTestData.getCreateRuleRequestVerificationToken();	
		}
		
		public Map<String,String> getCreateRuleRequestCookies() throws Exception {
			return objTestData.getCreateRuleRequestCookies();	
		}
		public String getCountry() throws Exception {
			return objTestData.getCountry();	
		}
		public String getPlacementType() throws Exception {
			return sPlacementType;	
		}
		public BackOfficePlacementType getPlacementTypeEnum() throws Exception {
			return eBackOffPlaceType;	
		}
		public String getPageZone() throws Exception {
			return sPageZone;	
		}
		public String getLocation() throws Exception {
			return sLocationValue;	
		}
		public String getProductSelectionValues() throws Exception {
			return sProductsSelectionValue;	
		}
		public String getProductSelection() throws Exception {
			return sProductSelection;	
		}

		public void setRuleID(String sRuleId) {
			this.sRuleId = sRuleId;			
		}	
		
		public String getRuleID() {
			return sRuleId;			
		}
		
		public String getRuleName() {
			return sRuleNameValue;			
		}
		
		public String[] getProductSelectionArr() {
			return arrBOProducts;
		}
		
		public String[] getLocationSelectionArr() {
			return arrBOLocations;
		}
		
		public boolean getRuleDisplayed() {
			return flagRuleDisplay;
		}
		
		public boolean getExactLocNegative() {
			return flagExcatLocationNegative;
		}
		
		public boolean getFieldsLevelFlag() throws Exception {
			boolean fieldsLevelFlag = false;
			if(flagCategoryFieldsDisplay || flagVendorFieldsDisplay || flagSKUsFieldsDisplay) {
				fieldsLevelFlag = true ; 
			}
			return fieldsLevelFlag;
		}
		
		public String getFieldsLevelFlagName() throws Exception {
			return sFieldsLevelFlagNames;
		}
		
		public String getFieldsValueAsLocation() throws Exception {
			return sFieldsValueAsLoc;
		}	
		
		public String getFieldsValue() throws Exception {
			return sFieldsNames;
		}
		
		public boolean getFlagForProductSpecificTypePlacement() {
			boolean flagProductSpecificPlacement = false;
			if(lstProductSpecificPlacement.contains(eBackOffPlaceType))
				flagProductSpecificPlacement = true ; 			
			return flagProductSpecificPlacement;
		}

		

		

		
		
		
	
}
