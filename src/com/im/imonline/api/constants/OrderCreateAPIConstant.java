package com.im.imonline.api.constants;


public class OrderCreateAPIConstant {
	
	
	
	
	
	//Below are the Excel headers of Testdata sheet 
	public static final String PAGESIZE="pagesize";
	
	
	
	//Below are the xpath Constants for Order Search API
	public static final String xpathOrderUpdateDate = "responses.hits.hits._source.orderupdatedate[0]";
	
	
	//Below are the constants specific to response body of Search API
		public static final String Line="ns0:line";
	
	

}
