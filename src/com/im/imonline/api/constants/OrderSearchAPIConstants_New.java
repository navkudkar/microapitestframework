package com.im.imonline.api.constants;

public class OrderSearchAPIConstants_New {
	

	//Below are the Excel headers of Testdata sheet 
	public static final String ID = "id";
	public static final String PAGESIZE="pagesize";
	public static final String PAGESTART="pageStart";
	public static final String EXCELHEADER_RESELLERID="resellersid";
	public static final String EXCELHEADER_CUSTOMERKEY="customerKey";
	public static final String FROMDATE="\"from\":\"";
	public static final String TODATE="\",\"to\":\"";
	public static final String elasticDateFormat="yyyy-MM-dd";
	public static final String TRUE="true";
	public static final String InProgress="\"1\"";
	public static final String PartiallyProcessed="\"2\"";
	public static final String Shipped ="\"3\"";
	public static final String PartiallyInvoiced="\"4\"";
	public static final String Invoiced="\"5\"";
	public static final String OnHold="\"6\"";
	public static final String CustomerHold="\"7\"";
	public static final String Voided="\"8\"";
	public static final String Backorder_IMPluse="\"9\"";
	public static final String OrderReview="\"10\"";
	public static final String CreditCheck="\"11\"";
	public static final String PartialHold="\"12\"";
	public static final String PartiallyRejected="\"13\"";
	//Request body attributes
	public static final String InvoiceNumber = "invoicenumber";
	public static final String ResellerNumber = "customernumber";
	public static final String OrderNumbers = "ordernumbers";
	public static final String SerialNumber = "serialnumber";
	public static final String IngramPartNumber = "ingrampartnumber";
	public static final String VendorPartNumber = "vendorpartnumber";
	public static final String VendorName = "vendorname";
	public static final String Customer_Order_Number="customerordernumber";
	public static final String CPN= "customerpartnumber";
	public static final String Bid= "specialbidnumber";
	public static final String STATUS="orderstatuscodes";
	public static final String OrderDateRange="ordersdaterange";
	public static final String ShipDateRange="shipdaterange";	
	public static final String SORTCRITERIA = "sortcriteria";	
	public static final String EndUserPO = "enduserponumber";
	public static final String UPCNumber = "upcnumber";
	public static final String BackOrderKey="isbackordersearch";
	
	//need to add more data related to validation
		//Below are the xpath Constants for Invoice search Search API
	public static final String INVOICEBEGINING="_index";
	public static final String xpathVendorName = "responses.hits.hits[0]._source.vendors";
	public static final String xpathTotalHits = "responses[0].hits.total";
	public static final String xpathTotalNoRecordsInTheResponse="responses[0].hits.hits";
	public static final String xpathOrderNumber= "responses.hits.hits[0]._source.ordernumber";
	public static final String xpathInvoiceNumber= "responses.hits.hits[0]._source.invoicenumber";
	public static final String xpathSerialNumber = "responses.hits.hits[0]._source.serialnumbers";
	public static final String xpathUPCNumber = "responses.hits.hits[0]._source.upcnumbers";
	public static final String xpathSKU = "responses.hits.hits[0]._source.ingrampartnumbers";
	public static final String xpathVPN = "responses.hits.hits[0]._source.vendorpartnumbers";
	public static final String xpathResellerNumber = "responses.hits.hits[0]._source.customernumber";
	public static final String xpathCustomerOrderNumber = "responses.hits.hits[0]._source.customerordernumber";
	public static final String xpathCPN = "responses.hits.hits[0]._source.customerpartnumbers";
	public static final String xpathBid = "responses.hits.hits[0]._source.specialbidnumbers";
	public static final String xpathStatus= "responses.hits.hits[0]._source.orderstatuscode";
	public static final String xpathOrderCreateDate = "responses.hits.hits[0]._source.ordercreatedate";
	public static final String xpathShipDateRange = "responses.hits.hits[0]._source.shipdates.shipdate";
	public static final String xpathEndUserPO = "responses.hits.hits[0]._source.enduserponumber";	
	public static final String xpathBackOrdered = "responses.hits.hits[0]._source.backorderstatus";
	
}
