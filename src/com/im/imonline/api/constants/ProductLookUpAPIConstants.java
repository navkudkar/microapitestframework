package com.im.imonline.api.constants;


public class ProductLookUpAPIConstants {
	
	
	
	//Below are the Excel headers of Testdata sheet 
	public static final String FAULTSTRING	= "faultstring";
	public static final String FAULTCODE = "faultcode";
	public static final String RETURNMESSAGE = "returnMessage";
	public static final String UPCCODE = "ValUPCCode";
	public static final String MFGPartNum = "ManufacturerPartNumber";
	
	//Below are the tagNames of response body
	public static final String tagFaultString="faultstring";
	public static final String tagFaultCode = "faultcode";
	public static final String tagReturnMessage = "returnMessage";
	public static final String tagUPCCode= "UPCCode";
	
	//Below are the xpath Constants for Order Search API
	public static final String xpathUPCCode = "//PartNumbers/UPCCode";
	public static final String xpathMFGPartNum = "//PartNumbers/ManufacturerPartNumber";
	
	
	

}
