package com.im.imonline.api.constants;

public class PieThon_PSDConstants {

	public static final String PREDICT_TAG = "PSD";
	public static final String ERROR_TAG = "error";
	
	// XPATH of PIE-Thon JSON Response Root
	//public static final String XPATH_PieThone_ResponseROOT = "PSD";
	public static final String XPATH_PieThone_ResponseROOT = ".";
	

}
