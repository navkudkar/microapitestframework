package com.im.imonline.api.constants;

public class GetPartnerAddressServiceAPIConstants 
{


	//Below are the Excel Expected Test Data Results

	public static final String EXPSITEID = "ExpsiteId";
	public static final String EXPSITENAME = "ExpsiteName";
	public static final String EXPSTREETADDRESS1 = "ExpstreetAddress1";
	public static final String EXPCITY = "Expcity";
	public static final String EXPSTATE = "Expstate";
	public static final String EXPCOUNTRY = "Expcountry";
	public static final String EXPZIPCODE = "ExpzipCode";
	public static final String EXPRESPONSESTATUS = "ExpResponsestatus";

	//Below are the xpath Constants for Order Search API

	public static final String xpathsiteId = "dataArea.address.siteId[0]";
	public static final String xpathsiteName = "dataArea.address.siteName[0]";
	public static final String xpathstreetAddress1 = "dataArea.address.streetAddress1[0]";
	public static final String xpathcity = "dataArea.address.city[0]";
	public static final String xpathstate = "dataArea.address.state[0]";
	public static final String xpathcountry = "dataArea.address.country[0]";
	public static final String xpathzipCode = "dataArea.address.zipCode[0]";
	public static final String xpathResponseStatus = "responseStatus";

}


