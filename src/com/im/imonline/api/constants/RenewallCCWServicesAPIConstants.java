package com.im.imonline.api.constants;

public class RenewallCCWServicesAPIConstants 
{

	//Below are the Excel headers of Testdata sheet 
	public static final String Excel_QuoteId= "QuoteId";
	public static final String EXPSTATUS = "ExpStatus";
	public static final String NAME = "Name";
    public static final String  PRICEPROTECTIONENDDATE= "priceProtectionEndDate";
    public static final String  INVOICETOTALAMOUNT= "InvoiceTotoalAmount";
    public static final String  QUOTECREATIONDATE= "quoteCreationDate";
	


	//Below are the Excel Expected Test Data Results
	public static final String EXPINVALIDSTATUS="ExpInvalidStatus";
	public static final String EXPINVALIDRESPONSESTATUS="ExpResponseStatus";
	public static final String EXPINVALIDRESPONSEMESSAGESTATUS="ExpResponseMessageStatus";
	

	
	public static final String EXPQUOTECREATEDNAME="Name";
	public static final String  EXPINVOICETOTALAMOUNT= "InvoiceTotoalAmount";
	public static final String  EXPQUOTECREATIONDATE= "quoteCreationDate";
	public static final String  EXPPRICEPROTECTIONENDDATE= "priceProtectionEndDate";


//        
	//Below are the xpath Constants for  RenewallCCWServicesAPIConstants API
	//TODO:
	public static final String xpathInvaldidQuoteIdStatus ="serviceresponse.responsepreamble.statuscode";
	public static final String xpathInvaldidResponseStatus ="serviceresponse.responsepreamble.responsestatus";
	public static final String xpathInvaldidResponseMessageStatus ="serviceresponse.responsepreamble.responsemessage";
	

	//Below are the xpath Constants for  RenewallCCWServicesAPIConstants API
//	
    public static final String xpathQuotecreated = "QuoteDetails.name";
	public static final String xpathquoteId = "QuoteDetails.quoteId";
	public static final String xpathQuoteiDstatus = "QuoteDetails.status";
	public static final String xpathpriceProtectionEndDate = "QuoteDetails.priceProtectionEndDate";
	public static final String xpathInvoiceTotoalAmount = "QuoteDetails.invoiceTotalAmount";
	public static final String xpathquoteCreationDate = "QuoteDetails.quoteCreationDate";
	


	
	

}

