package com.im.imonline.api.constants;

public class GetQuoteListServicesAPIConstants 
{
	
	
	//Below are the Excel Expected Test Data Results
	
		public static final String EXPSTATUSCODE = "ExpStatuscode";
		public static final String EXPRESPONSESTATUS = "ExpResponsestatus";
		public static final String EXPRESPONSEMESSAGE = "ExpResponsmessage";
		public static final String EXPQUOTENUMBER = "ExpQuoteNumber";
		public static final String EXPACCOUNT = "ExpAccount";
		public static final String EXPQUOTENAME = "ExpQuoteName";
		public static final String EXPSTATUS = "ExpStatus";
		public static final String EXPSTATUSREASON = "ExpStatusReason";
		public static final String EXPDARTNUMBER = "ExpDartNumber";
		public static final String EXPESTIMATEID = "ExpEstimateId";
		public static final String EXPDEALID = "ExpDealId";
		
		//Below are the xpath Constants for Order Search API
		
	    public static final String xpathStatusCode = "responsepreamble.statuscode";
	    public static final String xpathResponseStatus = "responsepreamble.responsestatus";
	    public static final String xpathResponsemessage = "responsepreamble.responsemessage";
	    public static final String xpathQuoteNumber = "quotelist.quoteNumber[0]";
	    public static final String xpathAccount = "quotelist.account[0]";
	    public static final String XpathQuoteName = "quotelist.quoteName[0]";
	    public static final String xpathStatus = "quotelist.status[0]";
	    public static final String xpathStatusReason = "quotelist.statusReason[0]";
	    public static final String xpathDartNumbar = "quotelist.dartNumber[0]";
	    public static final String xpathEstimateId = "quotelist.estimateId[0]";
	    public static final String xpathDealId = "quotelist.dealId[0]";
	    
	    
	//  invalid quote deatis information
	   
	  
}


