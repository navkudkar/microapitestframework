package com.im.imonline.api.constants;


public class SearchAPIConstants {
	
	
	
	
	
	//Below are the Excel headers 
	public static final String DEALERPRICE="DEALER_PRICE";
	public static final String TATIER1="TaTier1"; 
	
	
	//Below are the xpath Constants for Search API
	public static final String xpathSource = "hits.hits._source";
	public static final String xpathInnerHits = "hits.hits";
	public static final String xpathOuterHits="hits";
	public static final String xpathShards="_shards";
	
	public static final String xpathDealerPrice = "hits.hits._source.dealerprice";
	public static final String xpathTatier1="hits.hits._source.tatier1";
	
	
	//Below are the constants specific to response body of Search API
		//public static final String DealerPrice="dealerprice";
	
	

}
