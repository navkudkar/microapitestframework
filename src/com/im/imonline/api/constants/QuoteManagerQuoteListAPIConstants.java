package com.im.imonline.api.constants;

import com.im.imonline.api.business.QuoteManagerQuoteListEnumerations.SearchType;

public class QuoteManagerQuoteListAPIConstants {
	public static final String FROMDATE="fromDate";
	public static final String TODATE="toDate";
	
	public static final String elasticDateFormat="yyyy-MM-dd";
	
	public static final String valueClosedStatus="3";
	public static final String valueActiveStatus="1";
	public static final String valueDraftStatus="0";
	public static final String valueActiveClosedStatus="1,3";
	public static final String valueActiveDraftStatus="0,1";
	public static final String valueDraftClosedStatus="0,3";
	public static final String valueActiveDraftClosedStatus="0,1,3";
	
	
	//Below are the Excel headers of Testdata sheet 
	public static final String ID = "id";
//	public static final String RECORDPERPAGE="recordPerPage";
/*	public static final String PAGESTART="pageStart";*/
	public static final String EXCELHEADER_RESELLERID="resellersid";

	
	//Request body attributes
	public static final String CountryCode="RunConfig";
	public static final String CustomerNumber="customernumber";
	public static final String CustomerContact="customercontact";
	public static final String QuoteNumber="quoteNumber";
	public static final String Name="name";
	public static final String EndUserName="endUserName";
	public static final String FromDate="fromDate";
	public static final String ToDate="toDate";
	public static final String Source="source";
	public static final String QuoteStatus="quoteStatus";
	public static final String Validatecontact="validatecontact";
	public static final String Sorting="sorting";
	public static final String sortingColumnName="sortingColumnName";
	public static final String PageIndex="pageIndex";
	public static final String RecordPerPage="recordPerPage";
	public static final String RevisionNumber="revisionNumber";
	public static final String IsRenewvueEnabled="isRenewvueEnabled";
	public static final String SearchRenewalQuotes="searchRenewalQuotes";
	public static final String IsQuickQuotesEnabled="isQuickQuotesEnabled";
	public static final String CustomDateColumn="customDateColumn";
	public static final String SORTCRITERIA = "sortcriteria";	
	public static final String SortCriteria="sortcriteria";
	public static final String QuoteDateRange="quoteDateRange";
	
	public static final String SORTBY="sortBy";
	public static final String SORTDIRECTION="sortDirection";
	
	
	//need to add more data related to validation
	
	public static final String xpathQuoteNumber = "quotelist.quoteNumber";	
	public static final String xpathQuoteName = "quotelist.quoteName";	
	public static final String xpathQuoteCreatedDate = "quotelist.created";
	public static final String xpathQuoteExpiryDate = "quotelist.effectiveTo";	
	public static final String xpathQuoteStatus = "quotelist.statusCode";	
	public static final String xpathQuoteSource = "quotelist.source";	
	public static final String xpathQuoteStatusReason = "quotelist.statusReason";	
	public static final String xpathQuoteCreatedByName = "quotelist.createdBy.name";
	public static final String xpathQuoteUpdated = "quotelist.modified";
	public static final String xpathQuoteTotalAmount = "quotelist.totalAmount";
	public static final String xpathQuoteEndUserName = "quotelist.endUserName";
	
	// Values to be stored in TestDataUtil
	
	public static final String[] arrKeysToBeCapturedForSKUs = {"cat1desc", "vendorname", "longdesc1", "cat2desc", "material", "manufacturerpartnumber", "dealerprice","cat3desc","upcean"}; 
	
	public static final String[] arrKeysToBeForAggregations = {"newproductflag", "download", "directship", "heavyweight", "authorizedtopurchase", "bundleavailable", "refurbished",  "quantitybreak", "promotion", "instockororder","instock"};
	
	//Keyword Constants for Keyword Specific validation
	public static final String KEYWORD_GENERIC="KEYWORD_GENERIC";	
	public static final String KEYWORD_SKU="KEYWORD_SKU";
	public static final String KEYWORD_MULTIPLE_SKU="KEYWORD_MULTIPLE_SKU";
	public static final String KEYWORD_VPN="KEYWORD_VPN";
	public static final String KEYWORD_MULTIPLE_VPN="KEYWORD_MULTIPLE_VPN";
	public static final String KEYWORD_UPC_EAN="KEYWORD_UPC_EAN";
	public static final String KEYWORD_MULTIPLE_UPC_EAN="KEYWORD_MULTIPLE_UPC_EAN";
	public static final String KEYWORD_LONGDESC1="KEYWORD_LONGDESC1";
//	public static final String KEYWORD_INVALID="KEYWORD_INVALID";
//	public static final String KEYWORD_AUTOCORRECTERRONEOUS="KEYWORD_AUTOCORRECTERRONEOUS";
//	public static final String KEYWORD_INCOMPLETE="KEYWORD_INCOMPLETE";
	
	public static final String INVALIDKEYWORD = "MOPD2354252523424";
//	public static final String AUTOCORRECTEDKEYWORD = "AUTOCORRECTEDKEYWORD";
	public static final String AGGDATAFORPRODSTATUSFLAG = "AGGDATAFORPRODSTATUSFLAG";
	
	
	
}
