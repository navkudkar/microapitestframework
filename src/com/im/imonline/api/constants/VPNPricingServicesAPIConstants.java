package com.im.imonline.api.constants;

public class VPNPricingServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String COMPANYCD= "companyCd";
	public static final String BUSICUSTNUMBER= "BusiCustNumber";
	public static final String VENDPARTANDQUANTITY= "VendPartAndQuantity";
	public static final String LISTPRICE= "ListPrice";
	public static final String DISTIDISCOUNT= "DistiDiscount";
	public static final String VPNCATEGORY= "VpnCategory";
	
	
	//Below are the Excel Expected Test Data Results
	public static final String EXPSTATUSFORVALID = "SUCCESS";
	public static final String EXPSTATUSFORERROR = "ERROR";
	
	
	public static final String EXPESTIMATECOUNT = "ExpEstimateCount";
	public static final String EXPERROR1 = "Error 1";
	public static final String EXPERROR2 = "Error 2";
	public static final String EXPERROR3 = "Error 3";
	
	//Below are the xpath Constants for Order Search API 
	public static final String xpathStatus = "serviceresponse.status";
	public static final String xpathStatusCode = "serviceresponse.statuscode";
	
	public static final String xpathVPNCatagory = "serviceresponse.message.results.vpnCategory";
	public static final String xpathQuantities = "serviceresponse.message.results.quantity";
	public static final String xpathVendorPartNumber = "serviceresponse.message.results.vendorPartNumber";
	public static final String xpathresellerDiscountPercent = "serviceresponse.message.results.resellerDiscountPercent";
	public static final String xpathresellerPrice = "serviceresponse.message.results.resellerPrice";
	public static final String xpathlistPrice = "serviceresponse.message.results.listPrice";
	public static final String xpathextendedResellerPrice = "serviceresponse.message.results.extendedResellerPrice";
	public static final String xpathRecordCount = "serviceresponse.message.recordCount";
	public static final String xpathError = "serviceresponse.message.errors";
	
	//Below are the Constant used for Error Check 
	
	
	
}

