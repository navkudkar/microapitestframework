package com.im.imonline.api.constants;

public class DartListServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String NUMBEROFRESULTSPERPAGE= "NumberOfResultsPerPage";
	public static final String ORDERBY = "OrderBy";
	public static final String ORDERBYDIRECTION = "OrderByDirection";
	public static final String PAGENUMBER = "PageNumber";
	public static final String DARTID= "DartID";
	public static final String DEALID = "DealID";
	public static final String ENDUSERNAME = "EndUserName";
	public static final String CUSTOMERNUMBER = "CustomerNumber";
	public static final String APPLICATION = "Application";
	public static final String COUNTRYCODE = "CountryCode";
	public static final String VENDORCODE = "VendorCode";
	//Below are the Excel Expected Test Data Results
	
	public static final String EXPDARTLISTCOUNT = "ExpDartListCount";
	public static final String EXPNUMBEROFRESULTS = "ExpNumberOfResults";
	public static final String EXPSTATUS = "ExpStatus";
	public static final String EXPENDUSERNAME = "ExpEndUserName";
	public static final String EXPDARTS  = "ExpDarts";
	public static final String EXPSTATUSREASON = "ExpStatusReason";
	public static final String EXPDEALID= "ExpDealId";

	//Below are the xpath Constants for Order Search API
	
	public static final String xpathDartListCount = "Darts";
	public static final String xpathDartExpiration = "Darts.DartExpirationDate";
	public static final String xpathNumberOfResult = "NumberOfResults";
	public static final String xpathStatus = "ResponsePreamble.Status";
	public static final String xpathEndUserName = "Darts.EndUserName";
	public static final String xpathDarts = "Darts";
	public static final String xpathStatusReason = "ResponsePreamble.StatusReason";
	public static final String xpathDealId = "Darts.DealID";
	
	

	
	
}

