package com.im.imonline.api.constants;

public class InvoiceSearchAPIConstants {
		public static final String FROMDATE="\"from\":\"";
		public static final String TODATE="\",\"to\":\"";
	public static final String valueRMAType="RMA";
	public static final String valueORDERType="ORDER";
	public static final String valueCRMEMOType="CRMEMO";
	public static final String valuePaidStatus="\"2\"";
	public static final String valueOpenStatus="\"1\"";
	public static final String valueDueStatus= "\"3\"";
	public static final String elasticDateFormat="yyyy-MM-dd";
	//public static final String valueInvoiceDateRange="\"from\":\"\",\"to\":\"\"";

	//Below are the Excel headers of Testdata sheet 
	public static final String ID = "id";
	public static final String PAGESIZE="pagesize";
	public static final String PAGESTART="pageStart";
	public static final String EXCELHEADER_RESELLERID="resellersid";
	public static final String EXCELHEADER_CUSTOMERKEY="customerKey";
	
	//Request body attributess
	public static final String InvoiceNumber = "invoicenumber";
	public static final String ResellerNumber = "customernumber";
	public static final String OrderNumber = "ordernumber";
	public static final String SerialNumber = "serialnumber";
	public static final String IngramPartNumber = "ingrampartnumber";
	public static final String VendorPartNumber = "vendorpartnumber";
	public static final String Customer_Order_Number="customerordernumber";
	public static final String CPN= "customerpartnumber";
	public static final String Bid= "specialbidnumber";
	public static final String INVOICETYPE="invoicetype";
	public static final String STATUS="invoicestatuscode";
	public static final String InvoiceDateRange="invoicesdaterange";
	public static final String DueDateRange="duedaterange";	
	public static final String SORTCRITERIA = "sortcriteria";	
	public static final String SortCriteria="sortcriteria";
	public static final String RunConfig="RunConfig";
	
	public static final String SORTBY="sortBy";
	public static final String SORTDIRECTION="sortDirection";
	
	//need to add more data related to validation
		//Below are the xpath Constants for Invoice search Search API
	public static final String INVOICEBEGINING="_index";
//	public static final String xpathKeywordText = "responses.suggest.didyoumean[0].text";
//	public static final String xpathSubCatDesc = "responses.hits.hits[0]._source.cat2desc";
//	public static final String xpathCatDesc = "responses.hits.hits[0]._source.cat1desc";
	public static final String xpathVendorName = "responses.hits.hits[0]._source.vendors";
//	public static final String xpathInStockStatus = "responses.hits.hits[0]._source.instock";
//	public static final String xpathIsEsdProductStatus = "responses.hits.hits[0]._source.isesdproduct";
//	public static final String xpathprodTypeCatDesc = "responses.hits.hits[0]._source.cat3desc";
//	public static final String xpathLongDesc = "responses.hits.hits[0]._source.longdesc1";
	public static final String xpathTotalHits = "responses[0].hits.total";
//	public static final String xpathDealerPrice="responses.hits.hits[0]._source.dealerprice";
	public static final String xpathTotalNoRecordsInTheResponse="responses[0].hits.hits";
//	public static final String xpathMaterial="responses.hits.hits[0]._source.material";
//	public static final String xpathVendorPartNumber = "responses.hits.hits[0]._source.manufacturerpartnumber";
	public static final String xpathInvoiceNumber= "responses.hits.hits[0]._source.invoicenumber";
//	public static final String xpathOrderNumber= "responses.hits.hits[0]._source.ordernumbers";
	public static final String xpathSerialNumber = "responses.hits.hits[0]._source.serialnumbers";
	public static final String xpathSKU = "responses.hits.hits[0]._source.ingrampartnumbers";
	public static final String xpathVPN = "responses.hits.hits[0]._source.vendorpartnumbers";
	public static final String xpathResellerNumber = "responses.hits.hits[0]._source.customernumber";
	public static final String xpathCustomerOrderNumber = "responses.hits.hits[0]._source.customerordernumber";
	public static final String xpathCPN = "responses.hits.hits[0]._source.customerpartnumbers";
	public static final String xpathBid = "responses.hits.hits[0]._source.specialbidnumbers";
	public static final String xpathInvoiceType= "responses.hits.hits[0]._source.invoicetype";
	public static final String xpathStatus= "responses.hits.hits[0]._source.invoicestatuscode";
	public static final String xpathInvoiceDateRange = "responses.hits.hits[0]._source.invoicedate";
	public static final String xpathDueDateRange = "responses.hits.hits[0]._source.duedate";
	public static final String xpathTotalAmount = "responses.hits.hits[0]._source.totalamount";
//	public static final String xpathAggAvailableQty="responses.hits.hits[0]._source.aggavailableqty";
//	public static final String xpathAutoCorrectKeywordText = "responses[0].suggest.didyoumean[0].options[0].text";
//	public static final String xpathNewProductFlag = "responses[0].hits.hits._source.newproductflag";
//	public static final String xpathInStockOrOrderFlag = "responses[0].hits.hits._source.instockororder";
//	public static final String xpathESDFlag = "responses[0].hits.hits._source.isesdproduct";	
//	public static final String xpathDirectshipFlag = "responses.hits.hits[0]._source.directshipflag";	
//	public static final String xpathBomFlag = "responses.hits.hits[0]._source.bomflag";	
//	public static final String xpathRefurbishedFlag="responses.hits.hits[0]._source.refurbishedflag";	
//	public static final String xpathHeavyWeightFlag = "responses.hits.hits[0]._source.heavyweightflag";
//	public static final String xpathPurchaseToAllFlag = "responses.hits.hits[0]._source.purchasetoall";
//	public static final String xpathMatchedQueries = "responses.hits.hits[0].matched_queries";
//	public static final String xpathEndUserRequiredFlag = "responses.hits.hits[0]._source.enduserrequired";
//	public static final String xpathPromotionflag = "responses.hits.hits[0]._source.promotionflag";
//	public static final String xpathQuantityBreakFlag = "responses.hits.hits[0]._source.quantitybreakflag";
	// Values to be stored in TestDataUtil
	
	public static final String[] arrKeysToBeCapturedForSKUs = {"cat1desc", "vendorname", "longdesc1", "cat2desc", "material", "manufacturerpartnumber", "dealerprice","cat3desc","upcean"}; 
	
	public static final String[] arrKeysToBeForAggregations = {"newproductflag", "download", "directship", "heavyweight", "authorizedtopurchase", "bundleavailable", "refurbished",  "quantitybreak", "promotion", "instockororder","instock"};
	
	//Keyword Constants for Keyword Specific validation
	public static final String KEYWORD_GENERIC="KEYWORD_GENERIC";	
	public static final String KEYWORD_SKU="KEYWORD_SKU";
	public static final String KEYWORD_MULTIPLE_SKU="KEYWORD_MULTIPLE_SKU";
	public static final String KEYWORD_VPN="KEYWORD_VPN";
	public static final String KEYWORD_MULTIPLE_VPN="KEYWORD_MULTIPLE_VPN";
	public static final String KEYWORD_UPC_EAN="KEYWORD_UPC_EAN";
	public static final String KEYWORD_MULTIPLE_UPC_EAN="KEYWORD_MULTIPLE_UPC_EAN";
	public static final String KEYWORD_LONGDESC1="KEYWORD_LONGDESC1";
//	public static final String KEYWORD_INVALID="KEYWORD_INVALID";
//	public static final String KEYWORD_AUTOCORRECTERRONEOUS="KEYWORD_AUTOCORRECTERRONEOUS";
//	public static final String KEYWORD_INCOMPLETE="KEYWORD_INCOMPLETE";
	
	public static final String INVALIDKEYWORD = "MOPD2354252523424";
//	public static final String AUTOCORRECTEDKEYWORD = "AUTOCORRECTEDKEYWORD";
	public static final String AGGDATAFORPRODSTATUSFLAG = "AGGDATAFORPRODSTATUSFLAG";
	
	
	
}
