package com.im.imonline.api.constants;

public class RenewalCCWDownloadInvoiceServiceAPIConstants 
{

	//Below are the Excel Expected Test Data Results

	public static final String EXPQUOTECREATEDNAME="Name";
	public static final String  EXPINVOICETOTALAMOUNT= "listPrice";
	public static final String  EXPQUOTECREATIONDATE= "CreationDate";
	public static final String  EXPSTATUS= "ExpStatus";
	public static final String  EXPTOTALLINES= "ExpTotalLines";

	public static final String EXPINVALIDSTATUS="ExpInvalidStatus";
	public static final String EXPINVALIDRESPONSESTATUS="ExpResponseStatus";
	public static final String EXPINVALIDRESPONSEMESSAGESTATUS="ExpResponseMessageStatus";
	
	//Below are the xpath Constants for  RenewallCCWServicesAPIConstants API
	//TODO:
	public static final String xpathInvaldidQuoteIdStatus ="serviceresponse.responsepreamble.statuscode";
	public static final String xpathInvaldidResponseStatus ="serviceresponse.responsepreamble.responsestatus";
	public static final String xpathInvaldidResponseMessageStatus ="serviceresponse.responsepreamble.responsemessage";


	//Below are the xpath Constants for  RenewallCCWServicesAPIConstants API
	//	
	public static final String xpathQuotecreated = "QuoteHeader.quote.header.name[0]";
	public static final String xpathstatus = "QuoteHeader.quote.status[0]";
	public static final String xpathInvoiceTotoalAmount = "QuoteHeader.quote.header.listPrice[0]";
	public static final String xpathquoteCreationDate = "QuoteHeader.quote.header.creationDate[0]";
	public static final String xpathTotalLines = "totalLines";






}

