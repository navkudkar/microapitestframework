package com.im.imonline.api.constants;


public class HermesAPIConstants {

	public static final String INGRAMPARTNUMBER = "ingrampartnumber";
	public static final String ISOCOUNTRYCODE = "isocountrycode";
	public static final String APITEMPLATE = "apiTemplate";
	public static final String ENDPOINTURL_QA = "EndPointURL_QA";
	public static final String ENDPOINTURL_PNA_QA = "EndPointURL_PnA_QA";
	public static final String ENDPOINTURL_ORDERQUOTE_QA = "EndPointURL_OrderSimulate_QA";
	public static final String LINENUMBER = "linenumber";
	public static final String CUSTOMERPRICE = "customerprice";
	public static final String UNITPRICE = "unitprice";
	public static final String WAREHOUSEDETAILS = "warehousedetails";
	public static final String WAREHOUSEID = "warehouseid";
	public static final String GLOBALDKUID = "globalskuid";
	public static final String MANUFACTURERPARTNUMBER = "manufacturerpartnumber";														 
	public static final String VENDORPARTNUMBER = "vendorpartnumber";
	public static final String QUANTITY = "quantity";
	public static final String CUSTOMERPONUMBER = "customerponumber";
	public static final String SHIPTOSUFFIX = "shiptosuffix";
	public static final String BILLTOSUFFIX = "billtosuffix";
	public static final String ENDPOINTURL_ORDERCREATE_QA = "EndPointURL_OrderCreate_QA";
	public static final String CARRIERCODE = "carriercode";
	public static final String GLOBALLINENUMBER = "globallinenumber";
	public static final String ENDPOINTURL_ORDERDETAILS_QA = "EndPointURL_OrderDetails_QA";
	public static final String ORDERNUMBER = "ordernumber";
	public static final String ORDERBRANCHNUMBER = "orderbranchnumber";
	public static final String ORDERTYPE = "ordertype";
	public static final String ORDERDATE = "orderdate";
	public static final String XPATHORDERTIMESTAMP = "serviceresponse.ordercreateresponse.ordertimestamp";
	public static final String BRANCH = "branch";
	public static final String LINEORDERTYPE = "lineordertype";
	public static final String ERPORDERNUMBER = "erpordernumber";
	public static final String VMF = "vmf";
	public static final String SERVICESPECSSTRUCTURE = "serviceextendedspecsstruct";
	public static final String VENDORNUMBER = "vendornumber";
	public static final String HOLDORDERATTRIBUTE = "holdorderAttrValues";
	public static final String SHIPTOADDRESS = "shiptoaddress";
	public static final String BILLTOADDRESS = "billtoaddress";
	public static final String BASKETNOTES = "cartNotes";
	public static final String ENDUSERPO = "endUserPONo";
	public static final String LINENOTES = "lineNote";
	public static final String ENDUSERORDERNUMBER = "eurdernumber";
	public static final String ENDPOINTURL_PRODSEARCH_QA = "EndPointURL_ProdSearch_QA";
	public static final String CREDITCARDDETAILS = "creditcarddetails";
	public static final String CCCUSTOMERNUMBER = "cc_customernumber";
	public static final String CUSTOMERNUMBER = "customernumber";
	public static final String ENDPOINTURL_ORDERDELETE_QA = "EndPointURL_OrderCancel_QA";
	public static final String DISTRIBUTIONNUMBER = "distributionnumber";
	public static final String SHIPMENTNUMBER = "shipmentnumber";
	public static final String ORDERNUMBERFORDELETE = "ordernumberfordelete";
	public static final String ENDPOINTURL_ORDERMODIFY_QA = "EndPointURL_OrderModify_QA";
	public static final String SUBVENDORCODE = "subvendorcode";
	public static final String GLOBALMATERIAL = "globalmaterial";
	public static final String XPATHSTATUSCODE = "serviceresponse.responsepreamble.statuscode";
	public static final String ITEMSTATUSCODE = "itemstatuscode";
	public static final String MANFACTURERPARTNUMBER = "manfacturerpartnumber";
	public static final String NETAMOUNT = "netamount";
	public static final String FREIGHTAMOUNT = "freightamount";
	public static final String REQUESTEDUNITPRICE = "requestedunitprice";
	public static final String UNITPRODUCTPRICE = "unitproductprice";
	public static final String REQUESTEDQUANTITY = "requestedquantity";
	public static final String ITEMSTATUS = "itemstatus";
	public static final String XPATHINVOICINGSYSTEMORDERID = "serviceresponse.ordercreateresponse.invoicingsystemorderid";
	public static final String ENDUSER = "eUserStructure";
	public static final String ORDERCREATEEXTENDEDSPECS = "orderCreateExtendedSpecs";
	public static final String REGIONCODE = "regioncode";
	public static final String ORDERSIMULATEEXTENDEDSPECS = "ordersimulateextendedspecs";
	public static final String PRODUCTEXTENDEDSPECS = "productextendedspecs";
	

}
