package com.im.imonline.api.constants;

public class ListEstimateServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String LOGICALIDVALUE= "LogicalIDValue";
	public static final String COMPONENTIDVALUE = "ComponentIDValue";
	public static final String REFERENCEIDVALUE = "ReferenceIDValue";
	public static final String CREATIONDATETIME = "CreationDateTime";
	public static final String FROMDATETEXT= "FromDateText";
	public static final String DATEFROMEXPRSSIONVALUES = "DateFromExprssionValues";
	public static final String TODATETEXT = "ToDateText";
	public static final String TODATEEXPRESSIONVALUES = "ToDateExpressionValues";
	public static final String NAMEDARTESTIMATE = "NameDartEstimate";
	public static final String NAMEDARTESTIMATEVALUES = "NameDartEstimateValues";
	public static final String SORTBYTEXT = "SortByText";
	public static final String LASTMODIFICATIONDATETIMEVALUES = "LastModificationDateTimeValues";
	public static final String SORTORDERTEXT = "SortOrderText";
	public static final String SORTEDORDERVALUES = "SortedOrderValues";
	public static final String INDEXTEXT = "IndexText";
	public static final String INDEXVALUES = "IndexValues";
	public static final String PAGECOUNTTEXT = "PageCountText";
	public static final String PAGECOUNTVALUES = "PageCountValues";
	
	//Below are the Excel Expected Test Data Results
	
	
	public static final String EXPESTIMATECOUNT = "ExpEstimateCount";
	public static final String EXPSTATUSOFESTIMATE = "ExpStatusOfEstimate";
	public static final String EXPOWNERNAME = "ExpOwnerName";
	public static final String EXPESTIMATENAME = "ExpEstimateName";
	public static final String EXPESTIMATEID = "ExpEstimateID";
	public static final String EXPCOUNTRYCODE = "ExpCountryCode";
	//Below are the xpath Constants for Order Search API
	
	
	public static final String xpathStatusCode = "ShowQuote.DataArea.Show.ResponseCriteria.ChangeStatus.Reason.value";
	public static final String xpathEstimateCount = "ShowQuote.DataArea.Quote.QuoteHeader";
	public static final String xpathStatusOfEstimate = "ShowQuote.DataArea.Quote.QuoteHeader.Status.Reason.value";
	public static final String xpathExpOwnerName = "ShowQuote.DataArea.Quote.QuoteHeader.Party.Contact.ID.value";
	public static final String xpathEstimateName = "ShowQuote.DataArea.Quote.QuoteHeader.Extension.ValueText.value";
	public static final String xpathEstimateID = "ShowQuote.DataArea.Quote.QuoteHeader.ID.value";
	public static final String xpathCountryCode = "ShowQuote.DataArea.Quote.QuoteHeader.Extension.ID.value";

	
	
}

