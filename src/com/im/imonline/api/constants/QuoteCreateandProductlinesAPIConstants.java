package com.im.imonline.api.constants;

public class QuoteCreateandProductlinesAPIConstants 
{
	

	//Below are the Excel Expected Test Data Results
	
	public static final String EXPSTATUSCODE = "ExpStatus";
	public static final String EXPRESPONSESTATUS = "ExpResponsestatus";
	public static final String EXPRESPONSEMESSAGE = "ExpResponsmessage";
	public static final String EXPSTATUSRESON = "Expstatusreason";
	public static final String EXPQUOTENUMBER = "ExpQuotenumber";
	
	//Below are the xpath Constants for Order Search API
	
    public static final String xpathStatusCode = "serviceresponses.responsepreamble.statuscode";
    public static final String xpathResponseStatus = "serviceresponses.responsepreamble.responsestatus";
    public static final String xpathResponsemessage = "serviceresponses.responsepreamble.responsemessage";
    public static final String xpathSatatusResason = "serviceresponses.createquoteresponse.statusreason";
   
	
}

