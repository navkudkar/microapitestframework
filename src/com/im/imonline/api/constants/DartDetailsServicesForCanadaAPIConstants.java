package com.im.imonline.api.constants;

public class DartDetailsServicesForCanadaAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String DEALID	= "DealID";
	public static final String AUTHORIZATIONNUMBER = "AuthorizationNumber";
	public static final String CUSTOMERNUMBER = "CustomerNumber";
	public static final String COUNTRYCODE = "CountryCode";
	
	//Below are the Excel Expected Test Data Results
	
	public static final String EXPSTATUS = "ExpStatus";
	public static final String EXPDEALID = "ExpdealId";
	public static final String EXPCUSTOMERNAME  = "ExpCustomerName";
	public static final String EXPBRANCHCUSTNUMBER = "ExpBranchCustNumber";
	
	//Manufacturer Test Result Info
	
	public static final String EXPMANUFACTUREPARTNUMBER= "ExpmanufacturePartNumber";
	public static final String EXPSKU = "Expsku";
	public static final String EXPAPPROVEDQUANTITY = "ExpapprovedQuantity";
	public static final String EXPREMAININGQUANTITY = "ExpremainingQuantity";
	public static final String EXPLISTPRICE = "ExplistPrice";
	public static final String EXPDISTIDISCOUNTPERCENTAGE= "ExpdistiDiscountPercentage";
	public static final String EXPEXTENDEDLISTPRICE = "ExpextendedListPrice";
	public static final String EXPACOPNUMBER = "ExpacopNumber";
	public static final String EXPACOPEUID = "Expacopeuid";
	public static final String EXPZEROCLAIM = "ExpzeroClaim";
	
	//InvalidDart Info
	public static final String EXPINVALIDDARTSTATUS = "ExpInvalidDartStatus";
	public static final String EXPSTATUSREASON  = "ExpstatusReason";
	public static final String EXPSTATUSCODE  = "ExpstatusCode";
	

	//Below are the xpath Constants for Order Search API
	
	public static final String xpathStatus = "requestStatus.status";
	public static final String xpathDealId = "dealInformationPackage.dealInformation.header.dealId";
	public static final String xpathAuthorizationNumber = "dealInformationPackage.dealInformation.header.authorizationNumber";
	public static final String xpathCustomerName  = "dealInformationPackage.dealInformation.customer.customerName";
	public static final String xpathBranchCustNumber  = "dealInformationPackage.dealInformation.customer.branchCustomerNumber";
	

	
	//Below are the Xpath constant for Product Information.
	public static final String xpathManufacturePartNumber = "dealInformationPackage.dealInformation.productInformation.manufacturePartNumber";
	public static final String xpathsku = "dealInformationPackage.dealInformation.productInformation.sku";
	public static final String xpathApprovedQuantity = "dealInformationPackage.dealInformation.productInformation.approvedQuantity";
	public static final String xpathRemainingQuantity =  "dealInformationPackage.dealInformation.productInformation.remainingQuantity";
	public static final String xpathListPrice =  "dealInformationPackage.dealInformation.productInformation.listPrice";
	public static final String xpathDistiDiscountPercentage =  "dealInformationPackage.dealInformation.productInformation.distiDiscountPercentage";
	public static final String xpathExtendedListPrice =  "dealInformationPackage.dealInformation.productInformation.extendedListPrice";
	public static final String xpathAcopNumber =  "dealInformationPackage.dealInformation.productInformation.acopNumber";
	public static final String xpathAcopeuid =  "dealInformationPackage.dealInformation.productInformation.acopeuid";
	public static final String xpathZeroClaim =  "dealInformationPackage.dealInformation.productInformation.zeroClaim";
	
	
	//Below are the Xpath for invalidDart Info
	
	public static final String xpathInvalidDartStatus = "requestStatus.status";
	public static final String xpathstatusReason = "requestStatus.statusReason";
	public static final String xpathstatusCode = "requestStatus.statusCode";
}

