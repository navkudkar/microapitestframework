package com.im.imonline.api.constants;


public class ProductSearchElasticAPIConstants {

	//Below are the Excel headers of Testdata sheet 
	public static final String ID = "id";
	public static final String PAGESIZE="pageSize";
	public static final String PAGESTART="pageStart";
	public static final String EXCELHEADER_RESELLERID="resellersid";
	public static final String EXCELHEADER_CUSTOMERKEY="customerKey";
	public static final String EXCELHEADER_KEYWORD="keywords";
	public static final String EXCELHEADER_KEYWORD2="keyword";
	public static final String EXCELHEADER_PRODUCTSTATUS="productstatus";
	public static final String EXCELHEADER_VENDORNAME="vendorname";
	public static final String EXCELHEADER_CATEGORY="category";
	public static final String EXCELHEADER_SUBCATEGORY="subCategory";
	public static final String EXCELHEADER_BRANCH="branch";
	public static final String EXCELHEADER_SORTBY="sortBy";
	public static final String EXCELHEADER_SORTDIRECTION="sortDirection";
	public static final String EXCELHEADER_PRODTYPE="productType";
	public static final String EXCELHEADER_PRICERANGESEARCH="priceRangeSearch";
	public static final String EXCELHEADER_MAXPRICE="maxPrice";
	public static final String EXCELHEADER_MINPRICE="minPrice";
	public static final String EXCELHEADER_PRODSTATUS="productstatus";
	public static final String EXCELHEADER_VIEWALL="viewAll";
	public static final String EXCELHEADER_MINSTOCK = "minStock";
	public static final String EXCELHEADER_STOCKRANGESEARCH = "stockRangeSearch";
	public static final String EXCELHEADER_LISTOFPRODUCTS="listOfProducts";
	public static final String EXCELHEADER_TECHSPECPAGESIZE = "techspecpagesize";
	public static final String EXCELHEADER_TECHSPEC_ABSOLUTE = "absoluteAttributes";
	public static final String EXCELHEADER_TECHSPEC_NON_ABSOLUTE = "nonAbsoluteAttributes";	
	public static final String EXCELHEADER_TYPEAHEADID = "typeaheadid";
	public static final String EXCELHEADER_PRODUCTSUMMARYID = "productsummaryid";    
	public static final String QUERYFIELD = "query_field";  
	public static final String QUERYSTRING = "query_string";
	public static final String EXCELHEADER_PRODUCTDETAILSID="productdetailsid";
	
	//need to add more data related to validation
	public static final String EXCELHEADER_RES_KEYWORD_TEXT="VAL_suggest_didyoumean_text";
	public static final String EXCELHEADER_RES_CAT1DESC="VAL_cat1desc";	
	public static final String EXCELHEADER_RES_CAT2DESC="VAL_cat2desc";
	public static final String EXCELHEADER_RES_VENDORNAME="VAL_vendorName";
	public static final String EXCELHEADER_RES_INSTOCK="VAL_instock";
	public static final String EXCELHEADER_RES_ISESDPRODUCT="VAL_isesdproduct";
	public static final String EXCELHEADER_RES_CAT3DESC="VAL_cat3desc";
	public static final String EXCELHEADER_RES_LONGDESC_CONTENT="VAL_longdesc1_content";
	


	public static final String PRODUCTBEGINING="_index";
	public static final String LOGGEDIN_MODE="Loggedin";
	public static final String LOGGEDOUT_MODE="Loggedout";
	public static final String CAT1DESC="cat1desc";
	public static final String CAT2DESC="cat2desc";
	public static final String CAT3DESC ="cat3desc";
	public static final String LONGDESC1 ="longdesc1";
	public static final String DEALERPRICE ="dealerprice";
	public static final String SORTBY_STOCK="aggavailableqty";
	public static final String SORTORDERASC="asc";
	public static final String SORTORDERDESC="desc";
	public static final String MATERIAL="material";
	public static final String TRIMMED_MATERIAL = "trimmedmaterial";
	public static final String MANUFACTURERPARTNUMBER = "manufacturerpartnumber";
	public static final String UPCEAN = "upcean";
	public static final String SORTBY_VPN = "manufacturerpartnumber.keyword";
	public static final String SORTBY_VENDORNAME = "vendorname.keyword";
	public static final String VENDORNAME = "vendorname";
	public static final int MINSTOCKQTY = 100;
	public static final String HEIGHT="height";
	public static final String WIDTH="width";
	public static final String LENGTH="length";
	public static final String PALLETQTY="palletquantity";
	public static final String SHIPPINGWEIGHT="netweight";
	public static final String MEDIUMDESC="mediumdesc";
	public static final String IMAGELOW="gimagelow";
	public static final String IMAGEMID="gimagemid";
	public static final String IMAGEHIGH="gimagehigh";
	public static final String ONORDERQTY="onorderqty";
	public static final String SHIP_ALONG_EXPIRED_DATE = "shipalongexpiredt";
	public static final String SHIP_ALONG_PART_DESCRIPTION = "shipalongpartdes";
	public static final String SHIP_ALONG_MATERIAL = "shipalongmaterial";
	public static final String SHIP_ALONG_FREE_QTY = "shipalongfreeqty";
	public static final String SHIP_ALONG_MIN_QTY = "shipalongminqty";
	public static final String PARTOFBUNDLE="partofbundle";
	public static final String CLEARANCE_VALUE_DESC = "crtvaluedesc";
	public static final String CLEARANCE_VALUE = "crtvalue";
	public static final String BROCHURE_URL = "brochureurl";
	public static final String POWER_POINT_URL = "powerpointurl";
	public static final String VIDEO_URL = "videourl";
	public static final String WHITE_PAPER_URL = "whitepaperurl";
	public static final String PROD_SPECS = "prod_spec";
	public static final String ProdSpecs_HeaderName = "headername";
	public static final String ProdSpecs_AttributeName = "attributename";
	public static final String ProdSpecs_AttributeValue = "attributevalue";
	
	//Response parameters
	public static final String DIRECTSHIP="directship";
	public static final String DOWNLOAD="download";
	public static final String HEAVYWEIGHT="heavyweight";
	public static final String BUNDLEAVAILABLE = "bundleavailable";
	public static final String INSTOCK = "instock";
	public static final String REFURBISHED = "refurbished";
	public static final String AUTHORIZEDTOPURCHASE = "authorizedtopurchase";
	public static final String MATCHEDQUERIES = "matched_queries";
	public static final String COMPONENTPRODUCTS = "componentproducts";
	public static final String DISCONTINUED = "discontinued";
	public static final String WARRANTYPRODUCTS = "warrantyproducts";
	public static final String WEBONLYPRICE = "webonlyprice";
	public static final String SHIPALONG = "shipalong";
	
	
	//Product Flags to be passed in Product statuses
	public static final String FLAG_INSTOCK="instock";
	public static final String FLAG_INSTOCKORORDER="instockororder";
	public static final String FLAG_NEW="newproductflag";
	public static final String FLAG_ESD="isesdproduct";
	public static final String FLAG_DIRECTSHIP="directshipflag";
	public static final String FLAG_HEAVYWEIGHT="heavyweightflag";
	public static final String FLAG_AUTHORIZEDTOPURCHASE="purchasetoall";
	public static final String FLAG_BUNDLE="bomflag";
	public static final String FLAG_REFURBISHED="refurbishedflag";
	public static final String FLAG_ENDUSER="enduserrequired";
	public static final String FLAG_WARRANTY="warrantyproduct";
	public static final String FLAG_WARRANTIES="haswarranty";
	public static final String FLAG_COMPONENT="componentflag";
	public static final String FLAG_ACCESSORIES="hasaccessory";
	public static final String FLAG_BACKORDER="backorderswitch";
	public static final String FLAG_FREEITEM="shipalongflagdim";
	public static final String FLAG_DISCONTINUED="discontinuedflag";
	public static final String FLAG_QUANTITYBREAK="quantitybreakflag";
	public static final String FLAG_PROMOTION2="promotionflag";
	public static final String FLAG_NORETURNS="noreturnsflag";
	public static final String FLAG_RETURNLIMITATIONS="returnlimitations";
	public static final String FLAG_WEBONLYPRICE="webdiscountflag";
	public static final String FLAG_BLOWOUT="blowoutflag";
	public static final String FLAG_LICENCEPRODUCTS="licenseproductsflag";
	public static final String FLAG_LTL="ltlflag";
	public static final String FLAG_SHIPFROMPARTNER="endlessaisleflag";
	public static final String FLAG_BULKFREIGHT="oversizeflag";
	public static final String FLAG_CLEARANCE="clearanceflag";
	public static final String FLAG_NOTAUTHORIZED="purchasetoall";
	public static final String FLAG_ACOPPROMOTION="acoppromotionflag";
	public static final String FLAG_ACOPQUANTITYBREAK="acopquantitybreakflag";
	public static final String FLAG_ACOPWEBONLYPRICE="acopwebonlypriceflag";
	public static final String FLAG_ACOPSPECIALPRICING = "acopwebdiscountflag";  
	public static final String FLAG_ENDUSERREQUIRED = "enduserrequired";
	public static final String FLAG_SUBMATERIAL="submaterial";
	public static final String FLAG_SUBRULE="subrule";
	public static final String FLAG_SIMILARPRODUCTS="similarsku";
	public static final String FLAG_SPECIALPRICING="promoenddt";
	public static final String FLAG_HASCROSSSELL="hascrosssell";
	
	//Product Flags to be passed as separate parameters other than Prod status
	public static final String FLAG_VENDORSPCLBIDS="vendorspecialbids";
	public static final String FLAG_QTYBREAK="QuantityBreak";
	public static final String AGGREGATION_QTYBREAK="quantitybreak";
	public static final String FLAG_PROMOTION="Promotion";
	public static final String AGGREGATION_PROMOTION="promotion";
	
	//Below are the xpath Constants for Product search Search API
	public static final String xpathKeywordText = "responses.suggest.didyoumean[0].text";
	public static final String xpathSubCatDesc = "responses.hits.hits[0]._source.cat2desc";
	public static final String xpathCatDesc = "responses.hits.hits[0]._source.cat1desc";
	public static final String xpathVendorName = "responses.hits.hits[0]._source.vendorname";
	public static final String xpathInStockStatus = "responses.hits.hits[0]._source.instock";
	public static final String xpathIsEsdProductStatus = "responses.hits.hits[0]._source.isesdproduct";
	public static final String xpathprodTypeCatDesc = "responses.hits.hits[0]._source.cat3desc";
	public static final String xpathLongDesc = "responses.hits.hits[0]._source.longdesc1";
	public static final String xpathTotalHits = "responses[0].hits.total.value";
	public static final String xpathDealerPrice="responses.hits.hits[0]._source.dealerprice";
	public static final String xpathTotalNoRecordsInTheResponse="responses[0].hits.hits";
	public static final String xpathMaterial="responses.hits.hits[0]._source.material";
	public static final String xpathVendorPartNumber = "responses.hits.hits[0]._source.manufacturerpartnumber";
	public static final String xpathUpcEan= "responses.hits.hits[0]._source.upcean";
	public static final String xpathAggAvailableQty="responses.hits.hits[0]._source.aggavailableqty";
	public static final String xpathAutoCorrectKeywordText = "responses[0].suggest.didyoumean[0].options[0].text";
	public static final String xpathNewProductFlag = "responses[0].hits.hits._source.newproductflag";
	public static final String xpathInStockOrOrderFlag = "responses[0].hits.hits._source.instockororder";
	public static final String xpathESDFlag = "responses[0].hits.hits._source.isesdproduct";	
	public static final String xpathDirectshipFlag = "responses.hits.hits[0]._source.directshipflag";	
	public static final String xpathBomFlag = "responses.hits.hits[0]._source.bomflag";	
	public static final String xpathRefurbishedFlag="responses.hits.hits[0]._source.refurbishedflag";	
	public static final String xpathHeavyWeightFlag = "responses.hits.hits[0]._source.heavyweightflag";
	public static final String xpathPurchaseToAllFlag = "responses.hits.hits[0]._source.purchasetoall";
	public static final String xpathMatchedQueries = "responses[0].hits.hits[0].matched_queries";
	public static final String xpathEndUserRequiredFlag = "responses.hits.hits[0]._source.enduserrequired";
	public static final String xpathPromotionflag = "responses.hits.hits[0]._source.promotionflag";
	public static final String xpathQuantityBreakFlag = "responses.hits.hits[0]._source.quantitybreakflag";
	public static final String xpathAcopPromotionflag = "responses.hits.hits[0]._source.acoppromotionflag";
	public static final String xpathAcopQuantityBreakFlag = "responses.hits.hits[0]._source.acopquantitybreakflag";
	public static final String xpathIndexScore = "responses.hits.hits[0]._score";
	public static final String xpathErrorPath = "responses.error.reason";
	public static final String xpathTypeaheadResKeys="responses.aggregations.data.buckets.key";
	public static final String xpathAllSources="responses[0].hits.hits._source";
	public static final String xpathAllAggregations="responses[0].aggregations.all_aggregations";
	public static final String xpathProdSpecs = "responses[0].hits.hits._source.prod_spec";
	
	// Values to be stored in TestDataUtil
	
	public static final String[] arrKeysToBeCapturedForSKUs = {"cat1desc", "vendorname", "longdesc1", "cat2desc", "material", "manufacturerpartnumber", "dealerprice","cat3desc"};
		
	public static final String[] arrKeysToBeForAggregations = {"newproductflag", "download", "directship", "heavyweight","bundleavailable", "refurbished",  "quantitybreak", "promotion", "instockororder","instock","authorizedtopurchase","vendorspecialbids"};
	
	//single values to be stored for typeahead API
	public static final String[] attributeArrForTypeahead= {"cat1desc","cat2desc","cat3desc","vendorname","productfamily"};
	
	//values to be stored for Product Details Elastic Search API
	public static final String[] attributeArrForProductDetails = {"cat1desc","cat2desc","cat3desc","vendorname","longdesc1","manufacturerpartnumber","upcean","mediumdesc","gimagehigh",
																	"gimagemid","gimagelow","trimmedmaterial","height","width","length","palletquantity","netweight","shipalongexpiredt",
																	"shipalongpartdes","shipalongmaterial","shipalongfreeqty","shipalongminqty","shipalong","promoenddt","partofbundle",
																	"subrule","submaterial","crtvaluedesc","crtvalue","similarsku","brochureurl","powerpointurl","videourl"};
	public static final String[] attributeArrForFreeItem = {"shipalongexpiredt","shipalongpartdes","shipalongmaterial","shipalongfreeqty","shipalongminqty","shipalong"};
	public static final String[] attributeArrForClearanceFlag = {"crtvaluedesc","crtvalue"};
	public static final String[] attributeArrForProductSummary = {"material","manufacturerpartnumber","upcean","gimagemid","mediumdesc","aggavailableqty","dealerprice","shipalongexpiredt",
																	"shipalongpartdes","shipalongmaterial","shipalongfreeqty","shipalongminqty","shipalong","subrule","promoenddt",
																		"submaterial","partofbundle","subrule","submaterial","crtvaluedesc","crtvalue","subvendorcode","globalmaterial"};
																		
	//Below are the xpath Constants for Product Details Elastic Search API
	
	//Keyword Constants for Keyword Specific validation
	public static final String KEYWORD_GENERIC="KEYWORD_GENERIC";	
	public static final String KEYWORD_SKU="KEYWORD_SKU";
	public static final String KEYWORD_MULTIPLE_SKU="KEYWORD_MULTIPLE_SKU";
	public static final String KEYWORD_VPN="KEYWORD_VPN";
	public static final String KEYWORD_MULTIPLE_VPN="KEYWORD_MULTIPLE_VPN";
	public static final String KEYWORD_UPC_EAN="KEYWORD_UPC_EAN";
	public static final String KEYWORD_MULTIPLE_UPC_EAN="KEYWORD_MULTIPLE_UPC_EAN";
	public static final String KEYWORD_LONGDESC1="KEYWORD_LONGDESC1";
	public static final String KEYWORD_INVALID="KEYWORD_INVALID";
	public static final String KEYWORD_AUTOCORRECTERRONEOUS="KEYWORD_AUTOCORRECTERRONEOUS";
	public static final String KEYWORD_INCOMPLETE="KEYWORD_INCOMPLETE";
	
	public static final String INVALIDKEYWORD = "MOPD2354252523424";
	public static final String AUTOCORRECTEDKEYWORD = "AUTOCORRECTEDKEYWORD";
	public static final String AGGDATAFORPRODSTATUSFLAG = "AGGDATAFORPRODSTATUSFLAG";
	public static final String PRICERANGESEARCH = "PRICERANGESEARCH";
	public static final String NULLCONSTANT = "null";	

    public static final String VENDORNM_MICROSOFT = "Microsoft";
	public static final String WARRANTYSWITCH = "iniswitch";
	public static final String CLASSCD = "classcd";
	public static final String PURCHASEABLEFLAG = "purchasableflag";
	  

}
