package com.im.imonline.api.constants;

public class MultiSkuPriceandStockAPIConstants 
{

	//Below are the Excel Expected Test Data Results
	
	public static final String EXPRESPONSESTATUS="ExpResponseStatus";
	public static final String EXPSTATUSCODE="ExpStatusCode";
	public static final String EXPRESPONSEMESSAGESTATUS="ExpResponseMessage";	
	public static final String EXPITEAMSTATUS="ExpItemStatus"; 
	public static final String EXPITEAMSTATUSCODE="ExpItemStatusCode"; 
	public static final String EXPSTATUSMESSAGE="ExpStatusMessage";
	public static final String EXPINGRAMPARTNUMBER="ExpIngramPartNumber";
	public static final String EXPMPN="ExpManfacturerPartNumber";
	public static final String EXPUPC="ExpUpc";
	public static final String EXPQUANTITY="ExpQuantity";
	public static final String EXPRETAILPRICE="ExpRetailPrice";
	public static final String EXPWAREHOUSEID="ExpWarehouseid";

	//Below are the xpath Constants for  RenewallCCWServicesAPIConstants API
	//TODO:
	public static final String xpathResponseStatus ="serviceresponse.responsepreamble.responsestatus";
	public static final String xpathStatusCode ="serviceresponse.responsepreamble.statuscode";
	public static final String xpathResponseMessage ="serviceresponse.responsepreamble.responsemessage";
	public static final String xpathItemStatus ="serviceresponse.priceandstockresponse.details.itemstatus[0]";
	public static final String xpathItemStatusCode ="serviceresponse.priceandstockresponse.details.itemstatuscode[0]";
	public static final String xpathStatusMessage ="serviceresponse.priceandstockresponse.details.statusmessage[0]";
	public static final String xpathIngramPartNumber ="serviceresponse.priceandstockresponse.details.ingrampartnumber[0]";
	public static final String xpathmanfacturerpartnumber ="serviceresponse.priceandstockresponse.details.manfacturerpartnumber[0]";
	public static final String xpathupc ="serviceresponse.priceandstockresponse.details.upc[0]";
	public static final String xpathquantity ="serviceresponse.priceandstockresponse.details.quantity[0]";
	public static final String xpatretailprice ="serviceresponse.priceandstockresponse.details.retailprice[0]";
	public static final String xpatWarehouseid ="serviceresponse.priceandstockresponse.details.warehousedetails.warehouseid[0]";


	


	
	

}

