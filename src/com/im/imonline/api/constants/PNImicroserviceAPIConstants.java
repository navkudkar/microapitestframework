package com.im.imonline.api.constants;

public class PNImicroserviceAPIConstants {

	//Tage name constants
	public static final String ITEMSTATUSMESSAGE = "itemStatusMessage";
	public static final String HASSPECIALPRICING = "hasSpecialPricing";
	public static final String ISSTDSPECIALPRICE = "isStdSpecialPrice";
	public static final String HASWEBDISCOUNT = "hasWebDiscount";
	public static final String ISACOPAPPLIEDTOPRICE = "isACOPAppliedToPrice";
	
	//New Flag Constant added
	public static final String ISQUANTITYBREAKSAVAILABLE = "isQuantityBreaksAvailable";
	public static final String ISEODACOPAPPLIED = "isEODAcopApplied";
	public static final String ISACOPSPECIALPRICEAPPLIED = "isACOPSpecialPriceApplied";
	public static final String ISACOPVOLUMEDISCOUNTAPPLIED = "isACOPVolumeDiscountApplied";
	public static final String ISEODACOPSPECIALPRICEAPPLIED= "isEODACOPSpecialPriceApplied";
	public static final String ISEODACOPVOLUMEDISCOUNTAPPLIED = "isEODACOPVolumeDiscountApplied";
	public static final String ISSTDQUANTITYBREAKAPPLIED = "isStdQuantityBreakApplied";
	
	
	public static final String SPECIALPRICESUMMERY = "specialPriceSummary";

	public static final String ORIGINALPRICE = "originalPrice";
	public static final String DISCOUNT = "discount";
	public static final String MINQUANTITY = "minQty";
	public static final String QUANTITYLIMIT = "qtyLimit";
	public static final String EXPIRYDATE = "expiryDate";
	public static final String INGRAMPART_NUMBER = "ingrampartnumber";
	
	//Added for extra fields - US 
	public static final String ORIGINALPRICE_EACHACOPAPPLIED = "originalPrice_eachacopapplied";
	public static final String DISCOUNT_EACHACOPAPPLIED = "discount_eachacopapplied";
	public static final String MINQUANTITY_EACHACOPAPPLIED = "minQty_eachacopapplied";
	public static final String QUANTITYLIMIT_EACHACOPAPPLIED = "qtyLimit_eachacopapplied";
	public static final String EXPIRYDATE_EACHACOPAPPLIED = "expiryDate_eachacopapplied";

	// XPATH of Response[for Error Track]
	public static final String XPATH_MICROSERVICE_STATUS = "status";
	public static final String XPATH_MICROSERVICE_STATUS_Code = "statusCode";
	public static final String XPATH_MICROSERVICE_ErrorMSG = "errorMessage";
	
	// XPATH of Response[Special Pricing Flag and Discount Details]
	public static final String XPATH_SPCPRICE_FLAG_DETAILS = "specialPrcingSummaryList";
	public static final String XPATH_SPCPRICE_MESSAGE_DETAILS = "specialPrcingSummaryList.specialPriceSummary";	//Doubt - Array involved in case of Multi-set

}
