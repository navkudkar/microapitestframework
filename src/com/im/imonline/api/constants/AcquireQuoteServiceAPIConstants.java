package com.im.imonline.api.constants;

public class AcquireQuoteServiceAPIConstants 
{
	
	
	//Below are the Excel Expected Test Data Results
	
		public static final String EXPREASON = "ExpReason";
		public static final String EXPMAGICKEY = "ExpMagicKey";
		public static final String EXPNETPRICEPROTECTION = "ExpNetPriceProtectionDate";
		public static final String EXPDESCRIPTION = "ExpDescription";
		public static final String EXPID = "ExpID";

		//Below are the xpath Constants for Order Search API
		
	    public static final String xpathReason = "ShowQuote.DataArea.Show.ResponseCriteria.ChangeStatus.Reason";
	    public static final String xpathMagicKey = "ShowQuote.DataArea.Quote.QuoteLine.UserArea.CiscoExtensions.CiscoLine.MagicKey[0]";
	    public static final String xpathNetPriceProtectionDate = "ShowQuote.DataArea.Quote.QuoteLine.UserArea.CiscoExtensions.CiscoLine.NetPriceProtectionDate[0]";
	    public static final String xpathDescription = "ShowQuote.DataArea.Quote.QuoteHeader.UserArea.CiscoExtensions.CiscoHeader.ConfigurationMessages.Description";
	    public static final String xpathID = "ShowQuote.DataArea.Quote.QuoteHeader.UserArea.CiscoExtensions.CiscoHeader.ConfigurationMessages.ID";

	  
}


