package com.im.imonline.api.constants;


public class PNAHermisAPIConstant {	
	
	
	//Tag name constants
	public static final String CUSTOMER_PRICE = "customerprice";
	public static final String DEALER_PRICE = "dealerprice";
	
	
	// XPATH of details array
	
	public static final String XPATH_DETAILS = "serviceresponse.priceandstockresponse.details";
	
	
	

}
