package com.im.imonline.api.constants;

public class ConfigQuoteValidateServicesAPIConstants 
{


	//Below are the Excel Expected Test Data Results

	public static final String EXPSTATUS = "ExpStatus";
	public static final String EXPSTATUSCODE = "ExpStatuscode";
	public static final String EXPITEMNAME = "ExpItemName";

	//Without ItemName  Excel Expected Test Data Results
	public static final String EXPMESSAGECODE = "ExpMessageCode";
	public static final String EXPDESCRIPTION = "ExpDescription";

	//Below are the xpath Constants for Order Search API	
	public static final String xpathStatus = "serviceresponse.status";
	public static final String xpathStatusCode = "serviceresponse.code";
	public static final String xpathItemName = "serviceresponse.message.AcknowledgeConfiguration.ConfigurationLine.ItemName[0]";	

	//Without ItemName  Excel Expected Test Data Results
	public static final String xpathMessageCode = "serviceresponse.message.AcknowledgeConfiguration.ConfigurationHeader.Message.MessageCode[0]";
	public static final String xpathDescription = "serviceresponse.message.AcknowledgeConfiguration.ConfigurationHeader.Message.Description[0]";






}


