package com.im.imonline.api.constants;

public class AcquireEstimateServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String ESTIMATENAME= "EstimateName";
	public static final String ESTIMATEID= "EstimateID";
	
	//Expected constant values common across all the nations
	public static final String EXP_Error= "Error";
	public static final String Exp_ErrorDescription = "Estimate ID or Estimate Name is Mandatory for Acquire Estimate Request. Please submit request with valid Estimate ID or Estimate Name";
	public static final String Expected_Valid_Status= "[VALID]";
	
	//Below are the Excel Expected Test Data Results
	
	public static final String EXPPRICELISTCOUNTRYCODE = "ExpPriceListCountryCode";
	public static final String EXPPRICELISTDESCRIPTION = "ExpPriceListDescription";
	public static final String EXPNAMEESTIMATE = "ExpNameEstimate";
	public static final String EXPIDESTIMATE= "ExpIDEstimate";
	public static final String EXPSTATUSESTIMATE= "ExpStatusEstimate";
	public static final String EXPENDUSERNAME  = "ExpEndUserName";
	public static final String EXPOWNERESTIMATE  = "ExpOwnerEstimate";
	public static final String EXPCCWLINENUMBER  = "ExpCCWLineNumber";
	public static final String EXPLINEITEM  = "ExpLineItem";
	public static final String EXPLINEITEMDESCRIPTION  = "ExpLineItemDescription";
	public static final String EXPLINEITEMQUANTITY  = "ExpLineItemQuantity";
	public static final String EXPLISTPRICE  = "ExpListPrice";
	public static final String EXPCURRENCYCODE  = "ExpCurrencyCode";
	public static final String EXPEXTENDEDLISTPRICE  = "ExpExtendedListPrice";
	public static final String EXPTOTALAMOUNT  = "ExpTotalAmount";
	public static final String EXPDISCOUNT  = "ExpDiscount";
	public static final String EXPTYPEOFSERVICE  = "ExpTypeOfService";
	public static final String EXPSERVICETERM  = "ExpServiceTerm";
	public static final String EXPRESPSTATUS  = "ExpRespStatus";
	public static final String EXPDESCRIPTION  = "ExpDescription";
	//Below are the xpath Constants for Order Search API
	
	public static final String xpathPriceListCountryCode = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Extension.ID.value";
	public static final String xpathPriceListDescription = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Extension.ID.value";	
	public static final String xpathNameEstimate = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Extension.ValueText.value";
	public static final String xpathIDEstimate = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.ID.value";
	public static final String xpathStatusEstimate = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Status.Reason.value";
	public static final String xpathEndUserName = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Party.Name.value";
	public static final String xpathOwnerEstimate = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Party.Contact.ID.value";
	public static final String xpathCCWLineNumber = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.Specification.Property.NameValue.value";
	public static final String xpathLineItem = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.ID.value";
	public static final String xpathLineItemDescription = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.Description.value";
	public static final String xpathLineItemQuantity = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.Extension.Quantity.value";
	public static final String xpathListPrice = "AcknowledgeQuote.DataArea.Quote.QuoteLine.UnitPrice.Extension.Amount.value";
	public static final String xpathCurrencyCode = "AcknowledgeQuote.DataArea.Quote.QuoteLine.UnitPrice.Extension.Amount.currencyCode";
	public static final String xpathExtendedListPrice = "AcknowledgeQuote.DataArea.Quote.QuoteLine.ExtendedAmount.value";
	public static final String xpathTotalAmount = "AcknowledgeQuote.DataArea.Quote.QuoteLine.TotalAmount.value";
	public static final String xpathDiscount = "AcknowledgeQuote.DataArea.Quote.QuoteLine.AmountDiscount.Percentage.value";
	public static final String xpathTypeOfService = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.Specification.Property.Effectivity.typeCode";
	public static final String xpathServiceTerm = "AcknowledgeQuote.DataArea.Quote.QuoteLine.Item.Specification.Property.Effectivity.EffectiveTimePeriod.Duration";
	public static final String xpathRespStatus = "AcknowledgeQuote.DataArea.Acknowledge.ResponseCriteria.ChangeStatus.Reason.value";
	public static final String xpathDescription = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Message.Description.value";
}

