package com.im.imonline.api.constants;


public class OrderSearchAPIConstants {





	//Below are the Excel headers of Testdata sheet 
	public static final String PAGESIZE="pagesize";
	public static final String EXCELHEADER_CUSTOMERORDERNUMBER="customerordernumber";
	public static final String EXCELHEADER_CUSTOMERNUMBER="customernumber";
	public static final String EXCELHEADER_ENDUSERPONUMBER="enduserponumber";
	public static final String EXCELHEADER_ORDERNUMBER="ordernumber";
	public static final String EXCELHEADER_INGRAMPARTNUMBER="ingrampartnumber";
	public static final String EXCELHEADER_VENDORPARTNUMBER="vendorpartnumber";
	public static final String EXCELHEADER_SERIALNUMBER="serialnumber";
	public static final String EXCELHEADER_UPCNUMBER="upcnumber";
	public static final String EXCELHEADER_INVOICENUMBER="invoicenumber";
	public static final String EXCELHEADER_ORDERSTATUSNUMBER="orderstatuscode";
	public static final String EXCELHEADER_ORDERCREATEFROMDATE="ordercreatefromdate";
	public static final String EXCELHEADER_ORDERCREATETODATE="ordercreatetodate";
	public static final String EXCELHEADER_SHIPDATEFROMDATE="shipdatefromdate";
	public static final String EXCELHEADER_SHIPDATETODATE="shipdatetodate";
	public static final String EXCELHEADER_CUSTOMERPARTNUMBER="customerpartnumber";
	public static final String EXCELHEADER_VENDORNAME = "vendorname";
	public static final String EXCELHEADER_SPECIALBIDNUMBER="specialbidnumber";

	//Below are the xpath Constants for Order Search API
	public static final String xpathOrderUpdateDate = "responses.hits.hits._source.orderupdatedate[0]";
	public static final String ENDUSERPONUM = "responses.hits.hits._source.enduserponumber[0]";
	public static final String IMORDERNUM= "responses.hits.hits._source.ordernumber[0]";
	public static final String CUSTOMERORDERNUMBER="responses.hits.hits._source.customerordernumber[0]";
	public static final String CUSTOMERNUMBER="responses.hits.hits._source.customernumber[0]";
	public static final String SKU = "responses.hits.hits._source.ingrampartnumbers[0]";
	public static final String VPN = "responses.hits.hits._source.vendorpartnumbers[0]";
	public static final String CPN = "responses.hits.hits._source.customerpartnumbers[0]";
	public static final String SERIALNUMBER = "responses.hits.hits._source.serialnumbers[0]";
	public static final String UPCNUMBER = "responses.hits.hits._source.upcnumbers[0]";
	public static final String INVOICENUMBER = "responses.hits.hits._source.invoicenumber[0]";
	public static final String ORDERSTATUSNUMBER = "responses.hits.hits._source.orderstatuscode[0]";
	public static final String ORDERBEGINING="_index";
	public static final String ORDERCREATEDATE= "responses.hits.hits._source.ordercreatedate[0]";
	public static final String SHIPDATE= "responses.hits.hits._source.shipdates.shipdate[0]";
	public static final String VENDORNAME = "responses.hits.hits._source.vendors[0]";
	public static final String SPECIALBIDNUMBER="responses.hits.hits._source.specialbidnumbers[0]";

			//Below are the constants specific to response body of Search API
			//public static final String DealerPrice="dealerprice";



}
