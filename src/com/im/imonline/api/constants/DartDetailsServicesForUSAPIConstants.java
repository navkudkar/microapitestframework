package com.im.imonline.api.constants;

public class DartDetailsServicesForUSAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String DEALID	= "DealID";
	public static final String AUTHORIZATIONNUMBER = "AuthorizationNumber";
	public static final String CUSTOMERNUMBER = "CustomerNumber";
	public static final String COUNTRYCODE = "CountryCode";
	
	//Below are the Excel Expected Test Data Results
	
	public static final String EXPSTATUS = "ExpStatus";
	public static final String EXPDEALID = "ExpdealId";
	public static final String EXPCUSTOMERNAME  = "ExpCustomerName";
	public static final String EXPBRANCHCUSTNUMBER = "ExpBranchCustNumber";
	
	//Manufacturer Test Result Info
	
	public static final String EXPMANUFACTUREPARTNUMBER= "ExpmanufacturePartNumber";
	public static final String EXPSKU = "Expsku";
	public static final String EXPAPPROVEDQUANTITY = "ExpapprovedQuantity";
	public static final String EXPREMAININGQUANTITY = "ExpremainingQuantity";
	public static final String EXPLISTPRICE = "ExplistPrice";
	public static final String EXPDISTIDISCOUNTPERCENTAGE= "ExpdistiDiscountPercentage";
	public static final String EXPEXTENDEDLISTPRICE = "ExpextendedListPrice";
	public static final String EXPACOPNUMBER = "ExpacopNumber";
	public static final String EXPACOPEUID = "Expacopeuid";
	public static final String EXPZEROCLAIM = "ExpzeroClaim";
	
	//InvalidDart Info
	public static final String EXPINVALIDDARTSTATUS = "ExpInvalidDartStatus";
	public static final String EXPSTATUSREASON  = "ExpstatusReason";
	public static final String EXPSTATUSCODE  = "ExpstatusCode";
	

	//Below are the xpath Constants for Order Search API
	
	public static final String xpathStatus = "RequestStatus.Status";
	public static final String xpathDealId = "DealInformationPackage.DealInformation.Header.DealId";
	public static final String xpathAuthorizationNumber = "DealInformationPackage.DealInformation.Header.AuthorizationNumber";
	public static final String xpathCustomerName  = "DealInformationPackage.DealInformation.Customer.CustomerName";
	public static final String xpathBranchCustNumber  = "DealInformationPackage.DealInformation.Customer.BranchCustomerNumber";
	

	
	//Below are the Xpath constant for Product Information.
	public static final String xpathManufacturePartNumber = "DealInformationPackage.DealInformation.ProductInformation.ManufacturePartNumber";
	public static final String xpathsku = "DealInformationPackage.DealInformation.ProductInformation.SKU";
	public static final String xpathApprovedQuantity = "DealInformationPackage.DealInformation.ProductInformation.ApprovedQuantity";
	public static final String xpathRemainingQuantity =  "DealInformationPackage.DealInformation.ProductInformation.RemainingQuantity";
	public static final String xpathListPrice =  "DealInformationPackage.DealInformation.ProductInformation.ListPrice";
	public static final String xpathDistiDiscountPercentage =  "DealInformationPackage.DealInformation.ProductInformation.DistiDiscountPercentage";
	public static final String xpathExtendedListPrice =  "DealInformationPackage.DealInformation.ProductInformation.ExtendedListPrice";
	public static final String xpathAcopNumber =  "DealInformationPackage.DealInformation.ProductInformation.ACOPNumber";
	public static final String xpathAcopeuid =  "DealInformationPackage.DealInformation.ProductInformation.ACOPEUID";
	public static final String xpathZeroClaim =  "DealInformationPackage.DealInformation.ProductInformation.ZeroClaim";
	
	
	//Below are the Xpath for invalidDart Info
	
	public static final String xpathInvalidDartStatus = "RequestStatus.Status";
	public static final String xpathstatusReason = "RequestStatus.StatusReason";
	public static final String xpathstatusCode = "RequestStatus.StatusCode";
}

