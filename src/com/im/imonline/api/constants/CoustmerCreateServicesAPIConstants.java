package com.im.imonline.api.constants;

public class CoustmerCreateServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String PARTYNAME= "PartyName";
	public static final String ADDRESSLINE1 = "AddressLine1";
	public static final String CITY = "City";
	public static final String STATE = "State";
	public static final String POSTALCODE= "PostalCode";
	public static final String COUNTRY= "Country";


	//Create Estimate Xpath
	public static final String xpathStatus= "status";
	public static final String xpathCountryCode = "party.countryCode";
	public static final String xpathPartyName = "party.locale_en_us.partyName";
	public static final String xpathAddressLine1 = "party.locale_en_us.addressLine1";
	public static final String xpathCity = "party.locale_en_us.city";
	public static final String xpathState = "party.locale_en_us.state";
	public static final String xpathCounty = "party.locale_en_us.county";
	public static final String xpathPostalCode = "party.locale_en_us.postalCode";
	public static final String xpathMessage= "message";
	public static final String xpathPostalCodeMessage= "party.locale_en_us.cleansedAttr.compFaultDesc";
}