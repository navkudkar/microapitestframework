package com.im.imonline.api.constants;

public class PNI_QBmicroserviceAPIConstants {

	//Tage name constants
	public static final String ITEMSTATUS = "itemStatus";
	public static final String ITEMSTATUSCODE = "itemStatusCode";
	public static final String ITEMSTATUSMessage = "itemStatusMessage";
	public static final String SKU_INRESPONSE = "sku";
	public static final String HASQBAvailable_Flag = "hasQuantityBreaksAvailable";	
	
	public static final String MINQUANTITY = "minQuantity";
	public static final String MINQUANTITY_END_RANGE = "minQuantityEndRange";
	public static final String ORIGINALPRICE = "originalPrice";
	public static final String DISCOUNT = "discount";
	public static final String QTYLIMIT = "qtyLimit";
	public static final String EXPIRYDATE = "expiryDate";
	public static final String ISDISCOUNTAVAILABLE_ON_BREAK = "isDiscountAvailable";
	public static final String INGRAMPART_NUMBER = "ingrampartnumber";
	

	// XPATH of Response[for Error Track]
	public static final String XPATH_QBMICROSERVICE_STATUS = "status";
	public static final String XPATH_QBMICROSERVICE_STATUS_Code = "statusCode";
	public static final String XPATH_QBMICROSERVICE_ErrorMSG = "errorMessage";
	
	// XPATH of Response[Special Pricing Flag and Discount Details]
	public static final String XPATH_QB_FLAG_VALIDATION = "quantityBreaksSummaryList";

}
