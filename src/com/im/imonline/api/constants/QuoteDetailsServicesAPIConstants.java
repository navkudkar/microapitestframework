package com.im.imonline.api.constants;

public class QuoteDetailsServicesAPIConstants 
{
	
	
	//Below are the Excel Expected Test Data Results
	
		public static final String EXPSTATUSCODE = "ExpStatus";
		public static final String EXPRESPONSESTATUS = "ExpResponsestatus";
		public static final String EXPRESPONSEMESSAGE = "ExpResponsmessage";
		public static final String EXPQUOTENUMBER = "ExpQuoteNumber";
		public static final String EXPACCOUNT = "ExpAccount";
		public static final String EXPQUOTEPRICE = "ExpQuotePrice";
		
		
		
		//Below are the xpath Constants for Order Search API
		
	    public static final String xpathStatusCode = "responsepreamble.statuscode";
	    public static final String xpathResponseStatus = "responsepreamble.responsestatus";
	    public static final String xpathResponsemessage = "responsepreamble.responsemessage";
	    public static final String xpathQuoteNumber = "retrievequoteresponse.quoteNumber";
	    public static final String xpathAccount = "retrievequoteresponse.account";
	    public static final String xpathQuotePrice = "quoteproductlist.quotePrice[0]";
	    
	
	   
	  
}


