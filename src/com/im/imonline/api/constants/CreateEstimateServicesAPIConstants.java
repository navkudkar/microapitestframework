package com.im.imonline.api.constants;

public class CreateEstimateServicesAPIConstants 
{
	
	//Below are the Excel headers of Testdata sheet 
	public static final String NAMEDARTESTIMATEVALUES= "NameDartEstimateValues";
	public static final String PARTNUMBER = "PartNumber";
	public static final String PARTDESCRIPTION = "PartDescription";
	public static final String COUNTRYCODE = "CountryCode";
	public static final String COUNTRYCURRENCYCODE= "CountryCurrencyCode";
	public static final String VPNCATEGORY= "VpnCategory";
	public static final String LINENUMBERID= "LineNumberId";

	//Create Estimate Xpath
	public static final String xpathStatusCode = "AcknowledgeQuote.DataArea.Acknowledge.ResponseCriteria.ChangeStatus.Reason.value";
	public static final String xpathEstimateNameTypeCode = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Extension.ValueText.typeCode";
	public static final String xpathEstimateNameTypeCodeValue = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Extension.ValueText.value[0]";
	public static final String xpathEstimateIDTypeCode = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.ID.typeCode";
	public static final String xpathEstimateIDTypeCodeValue = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.ID.value";
	public static final String xpathEstimateValid = "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Status.Reason.value";
	public static final String xpathProductLineMissMessage= "AcknowledgeQuote.DataArea.Quote.QuoteHeader.Message.Description.value";
	
	
	//List Estimate Xpath
	public static final String xpathEstimateIDOfListEstimate = "ShowQuote.DataArea.Quote.QuoteHeader.ID.value";
	public static final String xpathEstimateNameInListEstimate = "ShowQuote.DataArea.Quote.QuoteHeader.Extension.ValueText.value";
	public static final String xpathListEstimateStatus = "ShowQuote.DataArea.Show.ResponseCriteria.ChangeStatus.Reason.value";
	public static final String xpathListEstimateValid = "ShowQuote.DataArea.Quote.QuoteHeader.Status.Reason.value";

}

