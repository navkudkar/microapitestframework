package com.im.imonline.api.testdata.util;

/**
 * This class responsible for generating test data for Typeahead search 
 * @author Vinita Lakhwani
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.TypeaheadSearchElasticAPITest;

public class TypeaheadSearchTestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("TypeaheadSearchTestDataUtil"); 
	private JsonUtil objJsonUtil=null;
	int iTotalProducts;

	private Set<String> stCategory = null,stSubCategory = null,stProductType = null,
			stVendorName = null,stPfam = null, stVendorCat3, stVendorPfam = null,stVendorCat3Pfam = null;

	List<String> lstCategory = null,lstSubCategory = null,lstProductType = null
			,lstVendorName = null,lstPfam = null, lstVendorCat3 = null,lstVendorPfam = null,
			lstVendorCat3Pfam = null;
	
	List<HashMap<String, String>> srcList;
	HashMap<String, String> srcMap;


	public TypeaheadSearchTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);		
		//making the parent call without keywords filter so as to get all required data -- pfam/cat3
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<BLANK>>");
		processForCountrySpecificData();
	}

	private void captureTotalProductsCount() throws Exception {
		try {
			iTotalProducts = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathTotalNoRecordsInTheResponse).size();
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}

	}

	/**
	 * This function helps to run the utilities to capture the data
	 */
	private void processForCountrySpecificData(){
		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(TypeaheadSearchElasticAPITest.TestDataFile, TypeaheadSearchElasticAPITest.isMultiSet, TypeaheadSearchElasticAPITest.sFileName);	

			if(objJsonUtil!=null && objJsonUtil.getStatusCode()==200)
			{
				captureTotalProductsCount();
				
				//getting the list of Source Objects from the response
				srcList=objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
				srcMap=new HashMap<String, String>();

				// capture most popular category,sub-category, Product Type,Vendor Name,ProductFamily
				fetchListOfRequiredAttributes();

				// capture List of Vendor-ProductType, Vendor-PFam, Vendor-ProductType-PFam 
				fetchVendorPfamCat3CombinationData();			
			}

			System.gc();
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}



	private void fetchVendorPfamCat3CombinationData() throws Exception {
		stVendorCat3 = new HashSet<String>(2);
		stVendorPfam = new HashSet<String>(2);
		stVendorCat3Pfam = new HashSet<String>(2);

		try {
			for(int i=0; i<iTotalProducts; i++) {
				boolean allListsFullFlag= (stVendorCat3Pfam.size()<2 || stVendorCat3.size()<2 || stVendorPfam.size()<2);
				if(!allListsFullFlag)
					break;
				srcMap=srcList.get(i);
				String vendorName = srcMap.get("vendorname");
				if(vendorName!=null)
					vendorName=getSpclCharFreeValue(vendorName);
								
				if(vendorName!=null)
				{
					String cat3=srcMap.get("cat3desc");
					if(cat3!=null)
						cat3=getSpclCharFreeValue(cat3);
					
					String pFam=srcMap.get("productfamily");
					if(pFam!=null)
						pFam=getSpclCharFreeValue(pFam);

					if(cat3!=null && pFam!=null && pFam.length()>2 && stVendorCat3Pfam.size()<2)
						stVendorCat3Pfam.add(vendorName+" "+cat3+" "+pFam);

					if(cat3!=null && cat3.length()>2 && stVendorCat3.size()<2)
						stVendorCat3.add(vendorName+" "+cat3);

					if(pFam!=null && pFam.length()>2 && stVendorPfam.size()<2)
						stVendorPfam.add(vendorName+" "+pFam);
				}

			}
			lstVendorCat3 = new ArrayList<String>(stVendorCat3);
			lstVendorPfam = new ArrayList<String>(stVendorPfam);
			lstVendorCat3Pfam = new ArrayList<String>(stVendorCat3Pfam);

			logger.info("List of Vendor-ProductType : "+lstVendorCat3);
			logger.info("List of Vendor-PFam : "+lstVendorPfam);
			logger.info("List of Vendor-ProductType-PFam : "+lstVendorCat3Pfam);
		} catch (Exception e) {
			logger.error("Error in the function fetchVendorPfamCat3CombinationData", e);
			e.printStackTrace();
		}

	}

	private void fetchListOfRequiredAttributes() throws Exception {
		String sAttrValue;
		stCategory = new HashSet<String>(2);
		stSubCategory = new HashSet<String>(2);
		stProductType = new HashSet<String>(2);
		stVendorName = new HashSet<String>(2);
		stPfam = new HashSet<String>(2);
		try {
			List<String> lstAttribute=new ArrayList<String>(5);
			for(int i=0;i<5;i++)
				lstAttribute.add(ProductSearchElasticAPIConstants.attributeArrForTypeahead[i]);

			for(int i=0; i<iTotalProducts; i++)
			{				
				if(stCategory.size()==2)
					lstAttribute.remove("cat1desc");
				if(stSubCategory.size()==2)
					lstAttribute.remove("cat2desc");
				if(stProductType.size()==2)
					lstAttribute.remove("cat3desc");
				if(stVendorName.size()==2)
					lstAttribute.remove("vendorname");
				if(stPfam.size()==2)
					lstAttribute.remove("productfamily");
				
				for(String attribute :lstAttribute)
					switch(attribute)
					{
					case "cat1desc":			
						
						srcMap=srcList.get(i);
						sAttrValue = srcMap.get("cat1desc");
						if(sAttrValue!=null && sAttrValue.length()>3)
						{
							sAttrValue = getSpclCharFreeValue(sAttrValue);												
							stCategory.add(sAttrValue);	
						}
						break;
					case "cat2desc":
						srcMap=srcList.get(i);
						sAttrValue = srcMap.get("cat2desc");
						if(sAttrValue!=null && sAttrValue.length()>3)
						{
							sAttrValue = getSpclCharFreeValue(sAttrValue);
							stSubCategory.add(sAttrValue);
						}
						break;
					case "cat3desc":
						srcMap=srcList.get(i);
						sAttrValue = srcMap.get("cat3desc");
						if(sAttrValue!=null && sAttrValue.length()>3) 
						{
							sAttrValue = getSpclCharFreeValue(sAttrValue);						
							stProductType.add(sAttrValue);
						}
						break;
					case "vendorname":
						srcMap=srcList.get(i);
						sAttrValue = srcMap.get("vendorname");
						if(sAttrValue!=null && sAttrValue.length()>3) 
						{
							sAttrValue = getSpclCharFreeValue(sAttrValue);
							stVendorName.add(sAttrValue);
						}
						break;
					case "productfamily":
						srcMap=srcList.get(i);
						sAttrValue = srcMap.get("productfamily");
						if(sAttrValue!=null && sAttrValue.length()>3)
						{
							sAttrValue = getSpclCharFreeValue(sAttrValue);
							stPfam.add(sAttrValue);
						}
						break;
					}
			}
			lstCategory = new ArrayList<String>(stCategory);
			lstSubCategory = new ArrayList<String>(stSubCategory);
			lstProductType = new ArrayList<String>(stProductType);
			lstVendorName = new ArrayList<String>(stVendorName);
			lstPfam = new ArrayList<String>(stPfam);

			logger.info("List of categories : "+lstCategory);
			logger.info("List of sub-categories : "+lstSubCategory);
			logger.info("List of product types : "+lstProductType);
			logger.info("List of vendors : "+lstVendorName);
			logger.info("List of Product Families : "+lstPfam);
		} catch (Exception e) {
			logger.error("Error in the function fetchListOfRequiredAttributes", e);
			e.printStackTrace();
		}

	}


	private String getSpclCharFreeValue(String sAttrValue) {
		sAttrValue=sAttrValue.trim();
		try {
			Pattern special = Pattern.compile ("[()./-]");
			Matcher hasSpecial = special.matcher(sAttrValue);
			boolean spclCharFlag=false;		
			spclCharFlag=hasSpecial.find();
			
			if (spclCharFlag) {
				sAttrValue=sAttrValue.replaceAll("[()./-]", " ");
				sAttrValue=sAttrValue.trim().replaceAll("  +", " ");
			}
		} catch (Exception e) {
			logger.error("Error in the function getSpclCharFreeValue", e);
			e.printStackTrace();
		}
		
		return sAttrValue;
	}

	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil
	public List<String> getLstCategory() {
		return lstCategory;
	}

	public List<String> getLstSubCategory() {
		return lstSubCategory;
	}

	public List<String> getLstProductType() {
		return lstProductType;
	}

	public List<String> getLstVendorName() {
		return lstVendorName;
	}

	public List<String> getLstPfam() {
		return lstPfam;
	}
	
	public List<String> getLstVendorCat3() {
		return lstVendorCat3;
	}

	public List<String> getLstVendorPfam() {
		return lstVendorPfam;
	}

	public List<String> getLstVendorCat3Pfam() {
		return lstVendorCat3Pfam;
	}
	
	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	public String getTotalProducts() throws Exception {
		return objJsonUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits);
	}

	public static void main(String args[]) throws Exception {
	}

}
