package com.im.imonline.api.testdata.util;

/**
 * This class responsible for generating test data for Elastic search 
 * @author Prashant Navkudkar
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;

public class ProductSearchTestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("ProductSearchTestDataUtil"); 
	private JsonUtil objJsonUtil=null;
	
	private List<String> lstSuitableCategory, lstEAN, lstCategoryFromResponse;
	private HashMap<String, HashMap<String, String>> mapOfAllSKUandData;
	private HashMap<String, String> mapOfAggregationData;
	private HashMap<String, List<String>> mapOfDataOfAbsolute, mapDataOfNonAbsolute;
	private HashMap<String, String> mapInstockData;
	List<HashMap<String, String>> lstAllSourceDataFromResponse;
	int iTotalProducts;

	public ProductSearchTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		//making the parent call without keywords filter so as to get all required data 
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<BLANK>>");
		processForCountrySpecificData();
	}

	/**
	 * This function helps to run the utilities to capture the data
	 * @author prashant navkudkar
	 */
	private void processForCountrySpecificData(){

		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(ProductSearchElasticAPITest.TestDataFile, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.sFileName);	
			
			if(objJsonUtil==null || objJsonUtil.getStatusCode()!=200)
				throw new Exception("Null values in Json Response in the function findOutMostSuitableSKUcategory");
			
			// Capture total product
			captureTotalProductsCount();
			
			// capture most popular category
			findOutMostSuitableSKUcategory();

			// Store SKU specific data
			setSKUSpecificData(ProductSearchElasticAPIConstants.arrKeysToBeCapturedForSKUs);
			
			// capture flags and different types of product data
			captureAggregations(ProductSearchElasticAPIConstants.arrKeysToBeForAggregations);
			
			// capture TechSpecification
			captureTechSpecificationData();
			
			// Capture EAN data
			captureEANData();
			
			//Capture In stock SKU Data
			captureInstocSKUData();
			
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}	
	
	private void captureTotalProductsCount() throws Exception {
		try {
			lstAllSourceDataFromResponse = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			lstCategoryFromResponse = objJsonUtil.getListofStringsFromJSONResponse("responses[0].hits.hits._source.cat1desc");
			iTotalProducts = lstAllSourceDataFromResponse.size();
			
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}

	}

	/** This method capture Tech Spec data for absolute and non absolute
	 * @author prashant navkudkar
	 */
	@SuppressWarnings({"unused", "unchecked"})
	private void captureTechSpecificationData() {		
		int absolute = 0, nonabsolute=0;				
		try {
			
//			mapOfDataOfAbsolute = new HashMap<>();
			mapDataOfNonAbsolute = new HashMap<>();
			String sXpathOfAggregation = "responses[0].aggregations.all_aggregations.attributes.techSpecAggregation";
			HashMap<Object, Object> mapTechSpecData= objJsonUtil.getMapFromJSONResponse(sXpathOfAggregation);			
			ArrayList<HashMap<Object,  Object>> lstParentBucketData =  (ArrayList<HashMap<Object, Object>>) mapTechSpecData.get("buckets");

			for(HashMap<Object,  Object> objPArentBucketCurrentData : lstParentBucketData) {

				HashMap<String, Integer> mapOfAbsoluteData = (HashMap<String, Integer>) objPArentBucketCurrentData.get("isabsolute");
				String sKey = (String) objPArentBucketCurrentData.get("key");

				if(mapOfAbsoluteData.get("doc_count")==0 && nonabsolute<3) {						
					mapDataOfNonAbsolute.put(sKey, getListOfKeys(objPArentBucketCurrentData));
					nonabsolute++;

				} /*** Commented by Prashant Navkudkar on 26th Aug 2020: As we are not running testcases for absolute **/ 
/*				else if(mapOfAbsoluteData.get("doc_count")>0 && absolute<3){

					mapOfDataOfAbsolute.put(sKey, getListOfKeys(objPArentBucketCurrentData));
					absolute++;
				}
				if(absolute>=3 && nonabsolute>=3) break;*/
				
				if(nonabsolute>=3) break;
               }

			System.out.println("Non-absolute: "+mapDataOfNonAbsolute);

		} catch (Exception e) {
			logger.error("Error in the function captureTechSpecificationData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}
	
	@SuppressWarnings({"unchecked"})
	private List<String> getListOfKeys(HashMap<Object,  Object> objPArentBucketCurrentData){
		List<String> sSpecValues = new ArrayList<>();
		HashMap<Object, Object> objAttributeValue = (HashMap<Object, Object>) objPArentBucketCurrentData.get("attributevalue");					
		ArrayList<HashMap<Object,  Object>> lstTechSpecBucketData = (ArrayList<HashMap<Object, Object>>) objAttributeValue.get("buckets");

		for(int x=0; x<3 && lstTechSpecBucketData.size()>x ; x++) {
			String sValue = (String) lstTechSpecBucketData.get(x).get("key");
			sSpecValues.add(sValue);
		}
		return sSpecValues;

	}
	
	private void captureEANData() {
		
		try {
			lstEAN = new ArrayList<>();
			List<String> lstOFEAN = objJsonUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathUpcEan);			
			for(String sCurrentValue: lstOFEAN) {				
					if(!lstEAN.contains(sCurrentValue) && sCurrentValue!=null)
						lstEAN.add(sCurrentValue);
					
					if(lstEAN.size()==5) break;
			}
			
			System.out.println("EAN data captured: "+lstEAN);
						
		} catch (Exception e) {
			logger.error("Error in the function captureEANData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}
	
	/**
	 * This function helps to capture data of different kinf of SKU like total downlodable procuts etc
	 * @param pArrKeysToCaptureForAggregation Keys to be captured for the products
	 * @author prashant navkudkar
	 */
	@SuppressWarnings({"unused", "rawtypes"})
	private void captureAggregations(String[] pArrKeysToCaptureForAggregation) {
		String sXpathOfAggregations = null;
		
		try {
			
			mapOfAggregationData = new HashMap<>();
			
			for(String sAggKey :pArrKeysToCaptureForAggregation) {
				sXpathOfAggregations = "responses[0].aggregations.all_aggregations."+sAggKey;
				HashMap<Object, Object> lstMapSourceData = objJsonUtil.getMapFromJSONResponse(sXpathOfAggregations);
				
				if(lstMapSourceData!=null)
				for( Map.Entry mapElement : lstMapSourceData.entrySet()) {
					if(mapElement.getKey().equals("doc_count")) {
						mapOfAggregationData.put(sAggKey, mapElement.getValue()+"");
						break;
					}
				}
				else
					mapOfAggregationData.put(sAggKey, ProductSearchElasticAPIConstants.NULLCONSTANT);
			}
			
			System.out.println("Aggregation data captured: "+mapOfAggregationData);
			

		} catch (Exception e) {
			logger.error("Error in the function captuteAggregations", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}

	/**
	 * This function helps to find out most popular SKUs with large no of data
	 * @author prashant navkudkar
	 */
	@SuppressWarnings({"unused", "rawtypes"})
	private void findOutMostSuitableSKUcategory() {
		
		try {
			
			lstSuitableCategory = new ArrayList<String>();
			lstSuitableCategory.addAll(lstCategoryFromResponse);
			lstSuitableCategory.removeAll(Collections.singletonList(null)); // removing null values

			// Count the total no of Categories present in the given list
			Map<String, Long> counted = lstSuitableCategory.stream()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

			// Sorting map in descending order
			Map<String, Long> mapSortedCategory = new LinkedHashMap<>();  

			counted.entrySet().stream()
			.sorted(Map.Entry.<String, Long>comparingByValue()
					.reversed()).forEachOrdered(e -> mapSortedCategory.put(e.getKey(), e.getValue()));

			lstSuitableCategory.clear();
			
			lstSuitableCategory.addAll(mapSortedCategory.entrySet().stream()
					  .map(Map.Entry::getKey)
					  .limit(5)
					  .collect(Collectors.toList()));

			System.out.println("Products with most suitable data categoey "+lstSuitableCategory);

		} catch (Exception e) {
			logger.error("Error in the function findOutMostSuitableSKUcategory", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}

	
	/**
	 * This function helps to capture SKU specific data
	 * @param pArrKeysToBeCapturedForSKUs: Keys to be captures for the SKU's
	 * @author prashant navkudkar
	 */
	@SuppressWarnings({"unused"})
	private void setSKUSpecificData(String[] pArrKeysToBeCapturedForSKUs) throws Exception {
		mapOfAllSKUandData = new HashMap<String, HashMap<String, String>>();
		try {
			if(lstSuitableCategory.size()>1)
			{
				for(String sCategory: lstSuitableCategory) {				
					for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
						String sCurrentCategory = currentMap.get("cat1desc");
						if(sCategory.trim().equals(sCurrentCategory)) {
							String sSKU = (String) currentMap.get("material");						
							mapOfAllSKUandData.put(sSKU, getRequireDataOfSKU(currentMap, pArrKeysToBeCapturedForSKUs));	
							break;
						}								
					}
				}
				System.out.println("Data Captured for SKU: "+mapOfAllSKUandData); 
			}
			else
			{
				int i=0;
				for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
					if(i<5) {
						String sSKU = (String) currentMap.get("material");						
						mapOfAllSKUandData.put(sSKU, getRequireDataOfSKU(currentMap, pArrKeysToBeCapturedForSKUs));
						i++;
					}
				}
				System.out.println("Data Captured for SKU: "+mapOfAllSKUandData); 
			}
		}
		catch(Exception e) {
			logger.error("Error in the function setSKUSpecificData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}


	private HashMap<String, String> getRequireDataOfSKU(HashMap<String, String> pLstMapFromResponse, String []pArrKeysToBeCapturedForSKUs) {

		HashMap<String, String> mapOfDataRelatedSKU = new HashMap<String, String>();

		try {
			// Storing data against SKU		
			for(String sKey : pArrKeysToBeCapturedForSKUs) {
				if(pLstMapFromResponse.containsKey(sKey))
				{
					Object oValue = pLstMapFromResponse.get(sKey);
					String sValue=(oValue!=null)?oValue.toString():ProductSearchElasticAPIConstants.NULLCONSTANT;
					mapOfDataRelatedSKU.put(sKey, sValue);
				}
				else
					mapOfDataRelatedSKU.put(sKey, ProductSearchElasticAPIConstants.NULLCONSTANT);
			}

		} catch (Exception e) {
			logger.error("Error in the function getRequireDataOfSKU", e);
			e.printStackTrace();
		}

		return mapOfDataRelatedSKU;
	}
	
	
	private void captureInstocSKUData() {
		
		try {		
			mapInstockData = new HashMap<>();
			
			for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
				if(currentMap.get("instock")!=null && currentMap.get("instock").equals("true")) {
					String sSKU = (String) currentMap.get("material");
					String sPrice = String.valueOf(currentMap.get("dealerprice"));
					mapInstockData.put(sSKU, sPrice);
					break;
				}	
			}
			System.out.println("Data Captured for Instock SKU: "+mapInstockData); 


		} catch (Exception e) {
			logger.error("Error in the function captureInstocSKUData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}

	}



	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil

	public List<String> getTopMostCategoryList() {
		return lstSuitableCategory;
	}
	
	public List<String> getTopMostKeywordList() {
		return lstSuitableCategory;
	}
	
	public HashMap<String, HashMap<String, String>> getTopSKUsWithMostOfTheData() {
		return mapOfAllSKUandData;
	}
	
	public HashMap<String, String> getAggregationDataOfFilter() {
		return mapOfAggregationData;
	}
	
	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}
	
	// Commneted as not capturing data for this category
/*	public HashMap<String, List<String>> getAbsoluteSpecification() {
		return mapOfDataOfAbsolute;
	}*/
	
	public HashMap<String, List<String>> getnonAbsoluteSpecification() {
		return mapDataOfNonAbsolute;
	}
	
	public List<String> getListOfEAN() {
		return lstEAN;
	}
	
	public String getTotalProducts() throws Exception {
		return String.valueOf(iTotalProducts);
	}
	
	public HashMap<String, String> getInstockData() {
		return mapInstockData;
	}
	
	public static void main(String args[]) throws Exception {
	}

}
