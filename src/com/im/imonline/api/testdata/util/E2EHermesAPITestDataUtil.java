package com.im.imonline.api.testdata.util;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.control.CountryGroups;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_Hermes;
import com.im.imonline.api.business.APIEnumerations.IntermediateResults;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;

public class E2EHermesAPITestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("E2EHermesAPITestDataUtil");
	private JsonUtil objJsonUtil = null;
	private EnumMap<FlagOptions_Hermes, String> mapSKUDataOfDifferentFlags;
	HashMap<String, HashMap<String, String>> mapContentsOfProductSummaryData;
	private List<HashMap<String, String>> lstSourceResponseData;
	int iTotalProducts;
	private HashMap<String, String> testIntermediaryResultApartFromOU = new HashMap<String, String>();
	private CountryGroups countryGroupTD = null;

	public E2EHermesAPITestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<BLANK>>");
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.PAGESIZE, "10");
		// if(countryGroupTD.isSAP())
		if (!"NZ~BE~CL~SG~NL~AU~MY~ID~PE~CO".contains(mapReqData.get(Constants.ExcelHeaderRunConfig)))
			mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTSTATUS, "purchasetoall");
		processForCountrySpecificData();
	}

	private void captureTotalProductsCount() throws Exception {
		try {
			lstSourceResponseData = objJsonUtil
					.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			iTotalProducts = objJsonUtil
					.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathTotalNoRecordsInTheResponse)
					.size();
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}
	}

	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	/**
	 * This function helps to run the utilities to capture the data
	 */
	private void processForCountrySpecificData() {
		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(
					ProductSearchElasticAPITest.TestDataFile, ProductSearchElasticAPITest.isMultiSet,
					ProductSearchElasticAPITest.sFileName);
			if (null != objJsonUtil || objJsonUtil.getStatusCode() != 200) {
				captureTotalProductsCount();
				captureSKUDataOfDifferentFlags();
			} else
				throw new Exception("Null values in Json Response in the function findOutMostSuitableSKUcategory");
			System.gc();
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}

	private HashMap<String, String> captureDataForFlagsIfNotRetrievedInFirstCallNew(FlagOptions_Hermes flagOpt,
			String productStatus) {
		JsonUtil objJsonUtilFlag = null;
		HashMap<String, String> sKUSource = null;
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY, "<<BLANK>>");
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION, "<<BLANK>>");

		try {
			if (!productStatus.equals(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK)
					&& !productStatus.equals(ProductSearchElasticAPIConstants.FLAG_INSTOCK))
				mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTSTATUS, productStatus);
			else if (productStatus.equals(ProductSearchElasticAPIConstants.FLAG_INSTOCK))
				mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.FLAG_INSTOCK, "true");
			else
				mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.FLAG_QTYBREAK, "true");
			mapReqDataForTemplate.put(HermesAPIConstants.ENDPOINTURL_QA,
					mapReqDataForTemplate.get(HermesAPIConstants.ENDPOINTURL_PRODSEARCH_QA));

			HashMap<String, String> hmForSpecificFlag = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(
					ProductSearchElasticAPITest.sFileName, mapReqDataForTemplate,
					ProductSearchElasticAPITest.isMultiSet);
			hmForSpecificFlag.put(ProductSearchElasticAPIConstants.PAGESIZE, "5");
			String sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSearchElasticAPITest.sFileName,
					hmForSpecificFlag, ProductSearchElasticAPITest.isMultiSet,
					ProductSearchElasticAPITest.TestDataFile);
			objJsonUtilFlag = new JsonUtil("\r\n" + sRequestBody + "\r\n" + "", mapReqDataForTemplate);
			objJsonUtilFlag.postRequest();
			List<HashMap<String, String>> lstSourcesFlag = objJsonUtilFlag
					.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);

			for (HashMap<String, String> response : lstSourcesFlag) {

				if (absenceOfPnAForSKU(response.get(ProductSearchElasticAPIConstants.MATERIAL),
						response.get(HermesAPIConstants.GLOBALMATERIAL)))
					continue;
				else {
					if (FlagOptions_Hermes.values().length != mapSKUDataOfDifferentFlags.size()) {
						// if(countryGroupTD.isSAP())
						if ("NZ~BE~CL~SG~NL~AU~MY~ID~PE~CO"
								.contains(mapReqDataForTemplate.get(Constants.ExcelHeaderRunConfig))) {
							switch (flagOpt) {
							case ACOP_PROMOTION:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION)
								.equals("true")) {
									return response;
								}
								break;
							case ACOP_QUANTITYBREAK:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK)
								.equals("true")) {
									return response;
								}
								break;
							case ACOP_WEBONLYPRICE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE)
								.equals("true")) {
									return response;
								}
								break;
							case BACKORDER:
								// here also pnaa
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("Y")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
								.equals("false")) {
									return response;
								}
								break;
							case BUNDLE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).equals("true")) {
									return response;
								}
								break;
							case DIRECTSHIP:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals("true")
								&& (null == response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
								|| !(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
										.equals("W")))
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("false")) {
									return response;
								}
								break;
							case DISCONTINUED:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) && response
								.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED).equals("true")) {
									return response;
								}
								break;
							case DOWNLOADABLE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("true")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")
								&& (null == response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
								|| !(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
										.equals("W")))) {
									return response;
								}
								break;
							case ENDUSER:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")) {
									return response;
								}
								break;
							case FREEITEM:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM) && response
								.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).equals("true")) {
									return response;
								}
								break;
							case HEAVYWEIGHT:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) && response
								.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT).equals("true")) {
									return response;
								}
								break;
							case INSTOCK:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("true")) {
									return response;
								}
								break;
							case LICENCEPRODUCTS:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS)
								.equals("true")) {
									return response;
								}
								break;
							case NEW:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_NEW)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_NEW).equals("true")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								.equals("false")) {
									return response;
								}
								break;
							case NORETURNS:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS) && response
								.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).equals("true")) {
									return response;
								}
								break;
							case PROMOTION:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) && response
								.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).equals("true")) {
									return response;
								}
								break;
							case QUANTITYBREAK:
								/*
								 * if(null != response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK)
								 * && response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).equals(
								 * "true") && null !=
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE).
								 * equals("true")) { return response; }
								 */break;
							case REFURBISHED:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) && response
								.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED).equals("true")) {
									return response;
								}
								break;
							case WARRANTY:
								/// need to check backoffice also
								/*
								 * if(null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).equals("true")
								 * && null !=
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE).
								 * equals("true")) { return response; }
								 */
								break;
							case WEBONLYPRICE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) && response
								.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE).equals("true")) {
									return response;
								}
								break;
							default:
								break;
							}
						} else
							switch (flagOpt) {
							case ACOP_PROMOTION:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case ACOP_QUANTITYBREAK:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case ACOP_WEBONLYPRICE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case BACKORDER:
								// here also pnaa
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("Y")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("false")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case BUNDLE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case DIRECTSHIP:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals("true")
								&& (null == response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
								|| !(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
										.equals("W")))
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("false")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case DISCONTINUED:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case DOWNLOADABLE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("true")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")
								&& (null == response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
								|| !(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH)
										.equals("W")))
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case ENDUSER:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true"))
									return response;
								break;
							case FREEITEM:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case HEAVYWEIGHT:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case INSTOCK:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case LICENCEPRODUCTS:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case NEW:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_NEW)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_NEW).equals("true")
								&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("false")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case NORETURNS:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case PROMOTION:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case QUANTITYBREAK:
								/*
								 * if(null != response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK)
								 * && response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).equals(
								 * "true") && null !=
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE).
								 * equals("true")) { return response; }
								 */break;
							case REFURBISHED:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							case WARRANTY:
								/// need to check backoffice also
								/*
								 * if(null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).equals("true")
								 * && null !=
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) &&
								 * response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE).
								 * equals("true")) { return response; }
								 */
								break;
							case WEBONLYPRICE:
								if (null != response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE)
								.equals("true")
								&& null != response
								.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								&& response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE)
								.equals("true")) {
									return response;
								}
								break;
							default:
								break;

							}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in the function captureDataForFlagsIfNotRetrievedInFirstCallNew", e);
			e.printStackTrace();
		}
		return sKUSource;
	}

	@SuppressWarnings("rawtypes")
	private void captureSKUDataOfDifferentFlags() {
		try {
			mapSKUDataOfDifferentFlags = new EnumMap<>(FlagOptions_Hermes.class);
			mapContentsOfProductSummaryData = new HashMap<String, HashMap<String, String>>();
			HashMap<Object, Object> hmAggregationsInfo = objJsonUtil
					.getMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllAggregations);
			boolean bStatus;
			for (HashMap response : lstSourceResponseData.subList(1, lstSourceResponseData.size())) {
				// for(HashMap response : lstSourceResponseData) {
				String sSKU = (String) response.get(ProductSearchElasticAPIConstants.MATERIAL);
				String sGlobalSKUID = (String) response.get(HermesAPIConstants.GLOBALMATERIAL);

				if (absenceOfPnAForSKU(sSKU, sGlobalSKUID))
					continue;

				for (FlagOptions_Hermes flag : FlagOptions_Hermes.values()) {
					bStatus = false;
					if (FlagOptions_Hermes.values().length == mapSKUDataOfDifferentFlags.size())
						break;

					if (!mapSKUDataOfDifferentFlags.containsKey(flag)) {
						switch (flag) {

						case ACOP_PROMOTION: 
							if( null !=response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.ACOP_PROMOTION, sSKU); }
							break; 

						case ACOP_QUANTITYBREAK: 
							if( null !=response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.ACOP_QUANTITYBREAK, sSKU);} 
							break; 

						case ACOP_WEBONLYPRICE: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.ACOP_WEBONLYPRICE, sSKU); }
							break; 

						case BACKORDER: 
							//here also pnaa 
							if( null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("Y") &&
							null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("false") ){ 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.BACKORDER, sSKU); } 
							break;

						case BUNDLE: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).equals("true")) {
								bStatus = true; 
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.BUNDLE,sSKU); } 
							break;

						case DIRECTSHIP: 
							if(null !=response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals("true")
							&& (null==response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH) ||
							!(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH).equals("W")))
							&& null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("false")) {
								bStatus = true; 
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.DIRECTSHIP,sSKU); } 
							break; 

						case DISCONTINUED: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.DISCONTINUED, sSKU); }
							break; 

						case DOWNLOADABLE: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("true") &&
							null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true") &&
							(null==response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH) ||
							!(response.get(ProductSearchElasticAPIConstants.WARRANTYSWITCH).equals("W")))) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.DOWNLOADABLE, sSKU); }
							break;

						case ENDUSER: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.DOWNLOADABLE, sSKU); }
							break; 

						case FREEITEM: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).equals("true")){ 
								bStatus = true; 
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.FREEITEM,sSKU); } 
							break; 

						case HEAVYWEIGHT: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.HEAVYWEIGHT, sSKU); }
							break;

						case INSTOCK:
							if (!qtyAvailableForSKU(sSKU, sGlobalSKUID, flag))
								continue;
							/*if (null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK)
										&& response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("true")) {*/
							bStatus = true;
							mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.INSTOCK, sSKU);
							//}
							break;

						case LICENCEPRODUCTS: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.LICENCEPRODUCTS, sSKU); }
							break; 

						case NEW: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_NEW) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_NEW).equals("true") &&
							null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("false")){ 
								bStatus = true; 
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.NEW,sSKU); } 
							break; 

						case NORETURNS: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).equals("true")){ 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.NORETURNS, sSKU); } 
							break;

						case PROMOTION: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.PROMOTION, sSKU); } 
							break;

						case QUANTITYBREAK: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.QUANTITYBREAK, sSKU); }
							break; 

						case REFURBISHED: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.REFURBISHED, sSKU); }
							break; 

						case WARRANTY: ///need to check backoffice also 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).equals("true")){ 
								bStatus = true; 
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.WARRANTY,sSKU); } 
							break; 

						case WEBONLYPRICE: 
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) &&
							response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE).equals("true")) { 
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_Hermes.WEBONLYPRICE, sSKU); }
							break;

						default:
							break;
						} // end of switch-case.
					} // end of if loop

					if (bStatus)
						mapContentsAgainstSku(response, sSKU);
				} // end of inner-for loop
			} // end of outer-for loop

			for (FlagOptions_Hermes flag : FlagOptions_Hermes.values()) {
				// if flagoption is not a part of flags hashmap , rehit for specific flag
				// getDataForNonCapturedFlagsIfExpected(flag);
				if (mapSKUDataOfDifferentFlags.get(flag) == null) {
					switch (flag) {
					case BACKORDER:
						break;
					case BUNDLE:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.BUNDLEAVAILABLE))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.BUNDLE,
									ProductSearchElasticAPIConstants.FLAG_BUNDLE);
						break;
					case DIRECTSHIP:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.DIRECTSHIP))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.DIRECTSHIP,
									ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP);
						break;
					case DISCONTINUED:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.DISCONTINUED))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.DISCONTINUED,
									ProductSearchElasticAPIConstants.FLAG_DISCONTINUED);
						break;
					case DOWNLOADABLE:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.DOWNLOAD))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.DOWNLOADABLE,
									ProductSearchElasticAPIConstants.FLAG_ESD);
						break;
					case ENDUSER:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.FLAG_ENDUSERREQUIRED))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.ENDUSER,
									ProductSearchElasticAPIConstants.FLAG_ENDUSERREQUIRED);
					case FREEITEM:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.SHIPALONG))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.FREEITEM,
									ProductSearchElasticAPIConstants.FLAG_FREEITEM);
						break;
					case HEAVYWEIGHT:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.HEAVYWEIGHT))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.HEAVYWEIGHT,
									ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT);
						break;
					case INSTOCK:
						// if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER))
						fillProdSummaryMapForFlag(FlagOptions_Hermes.INSTOCK, ProductSearchElasticAPIConstants.INSTOCK);
						break;
					case LICENCEPRODUCTS:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.LICENCEPRODUCTS,
									ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS);
						break;
					case NEW:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.FLAG_NEW))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.NEW,
									ProductSearchElasticAPIConstants.FLAG_NEW);
						break;
					case NORETURNS:
						break;
					case PROMOTION:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.AGGREGATION_PROMOTION))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.PROMOTION,
									ProductSearchElasticAPIConstants.FLAG_PROMOTION2);
						break;
					case QUANTITYBREAK:
						/*
						 * if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
						 * ProductSearchElasticAPIConstants.AGGREGATION_QTYBREAK))
						 * fillProdSummaryMapForFlag(FlagOptions_Hermes.QUANTITYBREAK,
						 * ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK);
						 */break;
					case REFURBISHED:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.REFURBISHED))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.REFURBISHED,
									ProductSearchElasticAPIConstants.FLAG_REFURBISHED);
						break;
					case WARRANTY:
						/*
						 * if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
						 * ProductSearchElasticAPIConstants.WARRANTYPRODUCTS))
						 * fillProdSummaryMapForFlag(FlagOptions_Hermes.WARRANTY,
						 * ProductSearchElasticAPIConstants.FLAG_WARRANTY);
						 */break;
					case WEBONLYPRICE:
						if (checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,
								ProductSearchElasticAPIConstants.WEBONLYPRICE))
							fillProdSummaryMapForFlag(FlagOptions_Hermes.WEBONLYPRICE,
									ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE);
						break;
					default:
						break;
					}
				}
			}
			System.out.println(mapSKUDataOfDifferentFlags);
			System.out.println(mapContentsOfProductSummaryData);
			System.out.println("Successfully captured data for " + mapSKUDataOfDifferentFlags.size() + " products");

		} catch (Exception e) {
			logger.error("Error in the function captureSKUDataWithDifferentFlags", e);
			e.printStackTrace();
		}
	}

	private boolean qtyAvailableForSKU(String sSKU, String sGlobalSKUID, FlagOptions_Hermes flag) throws Exception {
		mapReqDataForTemplate.put(HermesAPIConstants.APITEMPLATE, "HermesPnA");
		mapReqDataForTemplate.put(HermesAPIConstants.ENDPOINTURL_QA,
				mapReqDataForTemplate.get(HermesAPIConstants.ENDPOINTURL_PNA_QA));

		HashMap<String, String> skuMap = new HashMap<>();
		skuMap.put(HermesAPIConstants.INGRAMPARTNUMBER, sSKU);
		skuMap.put(HermesAPIConstants.GLOBALDKUID, sGlobalSKUID);
		List<HashMap<String, String>> mapMultipleSetOfSKUData = new ArrayList<HashMap<String, String>>();
		mapMultipleSetOfSKUData.add(skuMap);
		List<List<HashMap<String, String>>> ListMapMultipleSetOfSKUData = new ArrayList<List<HashMap<String, String>>>();
		ListMapMultipleSetOfSKUData.add(mapMultipleSetOfSKUData);

		String sRequestBody = AppUtil.getRequestBodyForMultiSetRestRequestWithTestDataUtil(mapReqDataForTemplate.get(HermesAPIConstants.APITEMPLATE), mapReqDataForTemplate,ListMapMultipleSetOfSKUData);
		logger.info("PnA Req: \n" + sRequestBody);
		JsonUtil objJSONUtil = new JsonUtil("\r\n" + sRequestBody + "\r\n" + "", mapReqDataForTemplate);
		objJSONUtil.postRequest();
		String jsonRes = objJSONUtil.getResponse().asString();

		boolean absFlag = true;
		boolean qtyFlag = false;
		absFlag = !(objJsonUtil.getStatusCode() == 200 && objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode").equals("200")
				&& objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.priceandstockresponse.details[0].itemstatus").equalsIgnoreCase("SUCCESS"));

		if (absFlag) 
		{
			logger.info(sSKU + " is not present in P&A");
		}
		else 
		{
			List<? extends Object> qtyValue;
			qtyValue = objJSONUtil.getResponseFromJayway(objJSONUtil,"$..availablequantity");

			String stockableFlag = objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.priceandstockresponse.details[0].isstocakable");

			if(stockableFlag.equals("true"))
				qtyFlag = ((List<Integer>)qtyValue).stream().anyMatch(s->s!=0);

			if (qtyFlag)
				logger.info(sSKU + " has quantity available in stock");
			else
				logger.info(sSKU + " has 0 quantity available in stock");
		}
		return qtyFlag;
	}

	private boolean absenceOfPnAForSKU(String sSKU, String sGlobalSKUID) throws Exception {
		// mapReqDataForTemplate.put(HermesAPIConstants.ISOCOUNTRYCODE,mapReqDataForTemplate.get("RunConfig"));
		mapReqDataForTemplate.put(HermesAPIConstants.APITEMPLATE, "HermesPnA");
		mapReqDataForTemplate.put(HermesAPIConstants.ENDPOINTURL_QA,
				mapReqDataForTemplate.get(HermesAPIConstants.ENDPOINTURL_PNA_QA));

		HashMap<String, String> skuMap = new HashMap<>();
		skuMap.put(HermesAPIConstants.INGRAMPARTNUMBER, sSKU);
		skuMap.put(HermesAPIConstants.GLOBALDKUID, sGlobalSKUID);
		List<HashMap<String, String>> mapMultipleSetOfSKUData = new ArrayList<HashMap<String, String>>();
		mapMultipleSetOfSKUData.add(skuMap);
		List<List<HashMap<String, String>>> ListMapMultipleSetOfSKUData = new ArrayList<List<HashMap<String, String>>>();
		ListMapMultipleSetOfSKUData.add(mapMultipleSetOfSKUData);

		String sRequestBody = AppUtil.getRequestBodyForMultiSetRestRequestWithTestDataUtil(
				mapReqDataForTemplate.get(HermesAPIConstants.APITEMPLATE), mapReqDataForTemplate,
				ListMapMultipleSetOfSKUData);
		logger.info("PnA Req: \n" + sRequestBody);
		JsonUtil objJSONUtil = new JsonUtil("\r\n" + sRequestBody + "\r\n" + "", mapReqDataForTemplate);
		objJSONUtil.postRequest();
		String jsonRes = objJSONUtil.getResponse().asString();

		boolean absFlag = true;
		absFlag = !(objJsonUtil.getStatusCode() == 200 && objJSONUtil
				.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode")
				.equals("200")
				// &&
				// objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.priceandstockresponse.details[0].itemstatuscode").equals("S"));
				&& objJSONUtil
				.getActualValueFromJSONResponseWithoutModify(
						"serviceresponse.priceandstockresponse.details[0].itemstatus")
				.equalsIgnoreCase("SUCCESS"));

		if (absFlag)
			logger.info(sSKU + " is not present in P&A");
		else
			logger.info(sSKU + " is present in P&A");

		return absFlag;
	}

	private boolean checkIfDoccountGreaterThanZeroForFlag(HashMap<Object, Object> hmAggregationsInfo,
			String fieldAggregation) {
		int doccount = 0;
		String doccounttext = null;
		try {
			doccounttext = hmAggregationsInfo.get(fieldAggregation) != null
					? hmAggregationsInfo.get(fieldAggregation).toString()
							: "0";
					if (!doccounttext.equals("0"))
						doccounttext = doccounttext.indexOf(",") == -1
						? doccounttext.substring(doccounttext.indexOf("=") + 1, doccounttext.indexOf("}"))
								: doccounttext.substring(doccounttext.indexOf("=") + 1, doccounttext.indexOf(","));
						doccount = Integer.parseInt(doccounttext);
		} catch (Exception e) {
			logger.error("Error in the function checkIfDoccountGreaterThanZeroForFlag", e);
			e.printStackTrace();
		}
		return doccount > 0;
	}

	private void fillProdSummaryMapForFlag(FlagOptions_Hermes flagOpt, String flag) {
		String sSku2 = null;
		try {
			if (mapSKUDataOfDifferentFlags.get(flagOpt) == null) {
				HashMap<String, String> sKUSource = captureDataForFlagsIfNotRetrievedInFirstCallNew(flagOpt, flag);
				if (sKUSource != null) {
					sSku2 = sKUSource.get(ProductSearchElasticAPIConstants.MATERIAL).toString();
					mapSKUDataOfDifferentFlags.put(flagOpt, sSku2);
					mapContentsAgainstSku(sKUSource, sSku2);
				}
			}
		} catch (Exception e) {
			logger.error("Error in the function fillProdSummaryMapForFlag", e);
			e.printStackTrace();
		}
	}

	/*
	 * To capture
	 * "material","manufacturerpartnumber","upcean","gimagemid","mediumdesc",
	 * "aggavailableqty","dealerprice","shipalongexpiredt","shipalongpartdes",
	 * "shipalongmaterial",
	 * "shipalongfreeqty","shipalongminqty","shipalong","subrule","submaterial",
	 * "partofbundle","subrule", "submaterial","crtvaluedesc","crtvalue","campaign",
	 * "promoenddt" with SKU Number as Key.
	 */
	private void mapContentsAgainstSku(HashMap response, String sSKU) {
		if (!mapContentsOfProductSummaryData.containsKey(sSKU)) {
			String mapValue = null;
			HashMap<String, String> mapContentsOfProductSummary = new HashMap<String, String>();
			for (String sBasicData : ProductSearchElasticAPIConstants.attributeArrForProductSummary) {
				if (response.containsKey(sBasicData) && null != response.get(sBasicData)) {
					mapValue = (String) response.get(sBasicData).toString().trim();
					mapContentsOfProductSummary.put(sBasicData, mapValue);
				}
			}
			mapContentsOfProductSummaryData.put(sSKU, mapContentsOfProductSummary);
		}
	}

	// Getter Section: Here will have all public methods to fetch the data from
	// TestDataUtil

	public EnumMap<FlagOptions_Hermes, String> getMapSKUDataOfDifferentFlags() {
		return mapSKUDataOfDifferentFlags;
	}

	public HashMap<String, HashMap<String, String>> getMapContentsOfProductSummaryData() {
		return mapContentsOfProductSummaryData;
	}

	public void setIntermediateTestData(IntermediateResults enumVar, String sValue) {
		testIntermediaryResultApartFromOU.put(enumVar.toString() + Thread.currentThread().getId(), sValue);
	}

	public String getIntermediateTestData(String enumVar) {
		return testIntermediaryResultApartFromOU.get(enumVar.toString() + Thread.currentThread().getId());
	}

	public void setCountryGroups(CountryGroups countryGroup) {
		countryGroupTD = countryGroup;
	}

	public CountryGroups getCountryGroups() {
		return countryGroupTD;
	}

}
