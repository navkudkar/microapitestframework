package com.im.imonline.api.testdata.util;

/**
 * This class is responsible for generating test data for Quote Manager Quote List at initial hit
 * @author Sumeet Umalkar
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.constants.QuoteManagerQuoteListAPIConstants;
import com.im.imonline.api.tests.QuoteManagerQuoteListAPITest;


public class QuoteManagerQuoteListTestDataUtil extends BaseTestDataUtil {

	transient Logger logger = Logger.getLogger("QuoteManagerQuoteListTestDataUtil"); 
	transient private JsonUtil objJsonUtil=null;
	HashMap<String, String> hmTestData = null;


	private List<String> lstQuoteNumber = null;
	private List<String> lstQuoteName = null;
	private List<String> lstQuoteCreatedDate = null;
	private List<String> lstQuoteExpiryDate = null;
	private List<String> lstQuoteSource = null;
	private List<Integer> lstQuoteStatus = null;	
	private List<String> lstQuoteStatusReason  = null;
	private List<String> lstQuoteCreatedByName = null;
	private List<String> lstQuoteUpdated  = null;
	private List<String> lstQuoteTotalAmount = null;
	private List<String> lstEndUserName = null;
	private List<String> lstResellerNumber = null;



	public QuoteManagerQuoteListTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		processForCountrySpecificData();
	}


	private void processForCountrySpecificData(){

		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(QuoteManagerQuoteListAPITest.TestDataFile, QuoteManagerQuoteListAPITest.isMultiSet, QuoteManagerQuoteListAPITest.sFileName);	
			
			captureDataForQuoteNumber();	
			captureDataForQuoteName();			
			captureDataForQuoteCreatedDate();
			captureDataForQuoteExpiryDate();
			captureDataForQuoteCreatedByName();
			captureDataForQuoteStatus();
			captureDataForQuoteStatusReason();
			captureDataForQuoteSource();
			captureDataForQuoteUpdated();
			captureDataForQuoteTotalAmount();
			captureDataForEndUserName();

		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
		finally {
			System.gc();
		}
	}



	private void captureDataForQuoteNumber(){
		try {

			List<String> lstOFQuoteNumber = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteNumber);
			lstQuoteNumber= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteNumber) {				
				if(!lstQuoteNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteNumber.add(sCurrentValue);

				if(lstQuoteNumber.size()==5) break;
			}

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForQuoteName(){
		try {


			List<String> lstOFQuoteName = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteName);
			lstQuoteName= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteName) {				
				if(!lstQuoteName.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteName.add(sCurrentValue);

				if(lstQuoteName.size()==5) break;
			}


		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteName", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}

	}
	private void captureDataForQuoteCreatedDate(){
		try {

			System.out.println(objJsonUtil.getResponse().asString());


			List<String> lstOFQuoteCreatedDate = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteCreatedDate);
			lstQuoteCreatedDate= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteCreatedDate) {				
				if(!lstQuoteCreatedDate.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteCreatedDate.add(sCurrentValue);

				if(lstQuoteCreatedDate.size()==5) break;
			}

			System.out.println("QuoteCreatedDate data captured: "+lstQuoteCreatedDate);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteCreatedDate", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}

	}
	private void captureDataForQuoteExpiryDate(){
		try {


			List<String> lstOFQuoteExpiryDate = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteExpiryDate);
			lstQuoteExpiryDate= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteExpiryDate) {				
				if(!lstQuoteExpiryDate.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteExpiryDate.add(sCurrentValue);

				if(lstQuoteExpiryDate.size()==5) break;
			}

			System.out.println("QuoteExpiryDate data captured: "+lstQuoteExpiryDate);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteExpiryDate", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForQuoteCreatedByName(){
		try {


			List<String> lstOFQuoteCreatedByName = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteCreatedByName);
			lstQuoteCreatedByName= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteCreatedByName) {				
				if(!lstQuoteCreatedByName.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteCreatedByName.add(sCurrentValue);

				if(lstQuoteCreatedByName.size()==5) break;
			}

			System.out.println("QuoteCreatedByName data captured: "+lstQuoteCreatedByName);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteCreatedByName", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}

	}

	private void captureDataForQuoteStatus(){
		try {

			List<Integer> lstOFQuoteStatus = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteStatus);
			lstQuoteStatus= new ArrayList<Integer>();
			for(Integer iCurrentValue: lstOFQuoteStatus) {				
				if(!lstQuoteStatus.contains(iCurrentValue) && iCurrentValue!=null)
					lstQuoteStatus.add(iCurrentValue);

				if(lstQuoteStatus.size()==5) break;
			}

			System.out.println("QuoteStatus data captured: "+lstQuoteStatus);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteStatus", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForQuoteStatusReason(){
		try {


			List<String> lstOFQuoteStatusReason = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteStatusReason);
			lstQuoteStatusReason= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteStatusReason) {				
				if(!lstQuoteStatusReason.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteStatusReason.add(sCurrentValue);

				if(lstQuoteStatusReason.size()==5) break;
			}

			System.out.println("QuoteStatusReason data captured: "+lstQuoteStatusReason);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteStatusReason", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForQuoteSource(){
		try {


			List<String> lstOFQuoteSource = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteSource);
			lstQuoteSource= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteSource) {				
				if(!lstQuoteSource.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteSource.add(sCurrentValue);

				if(lstQuoteSource.size()==5) break;
			}

			System.out.println("QuoteSource data captured: "+lstQuoteSource);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteSource", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForQuoteUpdated(){
		try {


			List<String> lstOFQuoteUpdated = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteUpdated);
			lstQuoteUpdated= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteUpdated) {				
				if(!lstQuoteUpdated.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteUpdated.add(sCurrentValue);

				if(lstQuoteUpdated.size()==5) break;
			}

			System.out.println("QuoteUpdated data captured: "+lstQuoteUpdated);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteUpdated", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForQuoteTotalAmount(){
		try {


			List<String> lstOFQuoteTotalAmount = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteTotalAmount);
			lstQuoteTotalAmount= new ArrayList<String>();
			for(String sCurrentValue: lstOFQuoteTotalAmount) {				
				if(!lstQuoteTotalAmount.contains(sCurrentValue) && sCurrentValue!=null)
					lstQuoteTotalAmount.add(sCurrentValue);

				if(lstQuoteTotalAmount.size()==5) break;
			}

			System.out.println("QuoteTotalAmount data captured: "+lstQuoteTotalAmount);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForQuoteTotalAmount", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}


	private void captureDataForEndUserName(){
		try {


			List<String> lstOFEndUserName = objJsonUtil.getListofStringsFromJSONResponse(QuoteManagerQuoteListAPIConstants.xpathQuoteEndUserName);
			lstEndUserName= new ArrayList<String>();
			for(String sCurrentValue: lstOFEndUserName) {				
				if(!lstEndUserName.contains(sCurrentValue) && sCurrentValue!=null)
					lstEndUserName.add(sCurrentValue);

				if(lstEndUserName.size()==5) break;
			}

			System.out.println("EndUserNames data captured: "+lstEndUserName);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForEndUserName", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}



	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil


	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	public List<String> getListOfQuoteNumber() {
		return lstQuoteNumber;
	}

	public List<String> getListOfEndUserName() {
		return lstEndUserName;
	}
	public List<String> getListOfQuoteName() {
		return lstQuoteName;
	}
	public List<String> getListOfQuoteCreatedDate() {
		return lstQuoteCreatedDate;
	}

	public List<String> getListOfQuoteExpiryDate() {
		return lstQuoteExpiryDate;
	}
	public List<String> getListOfQuoteSource(){
		return lstQuoteSource;
	}
	public List<Integer> getListOfQuoteStatus(){
		return lstQuoteStatus;
	}
	public List<String> getListOfQuoteStatusReason() { 
		return lstQuoteStatusReason;
	}
	public List<String> getListOfQuoteCreatedByName () { 
		return lstQuoteCreatedByName;
	}

	public List<String> getListOfQuoteUpdated(){
		return lstQuoteUpdated;
	}

	public List<String> getListOfQuoteTotalAmount(){
		return lstQuoteTotalAmount;
	}

	public List<String> getListOfResellerNumber(){
		return lstResellerNumber;
	}

	
}
