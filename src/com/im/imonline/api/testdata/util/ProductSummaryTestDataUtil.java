package com.im.imonline.api.testdata.util;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductSummary;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;

public class ProductSummaryTestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("ProductSummaryTestDataUtil"); 
	private JsonUtil objJsonUtil = null;
	private EnumMap<FlagOptions_ProductSummary, String> mapSKUDataOfDifferentFlags;
	HashMap<String, HashMap<String, String>> mapContentsOfProductSummaryData;
	private List<HashMap<String, String>> lstSourceResponseData;
	int iTotalProducts;

	public ProductSummaryTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<BLANK>>");
		processForCountrySpecificData();
	}

	private void captureTotalProductsCount() throws Exception {
		try {
			lstSourceResponseData = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			iTotalProducts = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathTotalNoRecordsInTheResponse).size();
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}
	}

	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	/**
	 * This function helps to run the utilities to capture the data
	 */
	private void processForCountrySpecificData(){
		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(ProductSearchElasticAPITest.TestDataFile, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.sFileName);	
			if(null != objJsonUtil || objJsonUtil.getStatusCode() != 200) {
				captureTotalProductsCount();
				captureSKUDataOfDifferentFlags();
			}
			else 
				throw new Exception("Null values in Json Response in the function findOutMostSuitableSKUcategory");
			System.gc();	
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}

	private HashMap<String, String> captureDataForFlagsIfNotRetrievedInFirstCall(String productStatus){
		JsonUtil objJsonUtilFlag=null;
		HashMap<String, String> sKUSource=null;
		try {
			HashMap<String, String> hmForSpecificFlag = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(ProductSearchElasticAPITest.sFileName,mapReqDataForTemplate,ProductSearchElasticAPITest.isMultiSet);
			hmForSpecificFlag.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTSTATUS, productStatus);
			String sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSearchElasticAPITest.sFileName, hmForSpecificFlag, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.TestDataFile);
			objJsonUtilFlag= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
			objJsonUtilFlag.postRequest();
			List<HashMap<String, String>> lstSourcesFlag = objJsonUtilFlag.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			if(lstSourcesFlag.size()>0)
				if (lstSourcesFlag.get(0).get(productStatus).equals("true") || lstSourcesFlag.get(0).get(productStatus).equals("Y"))
					sKUSource=lstSourcesFlag.get(0);
					//sSku= lstSourcesFlag.get(0).get(ProductSearchElasticAPIConstants.MATERIAL).toString():"0";
		}
		catch (Exception e) {
			logger.error("Error in the function captureDataForFlagsIfNotRetrievedInFirstCall", e);
			e.printStackTrace();
		}
		return sKUSource;		
	}
	
	@SuppressWarnings("rawtypes")
	private void captureSKUDataOfDifferentFlags() {
		try {
			mapSKUDataOfDifferentFlags = new EnumMap<>(FlagOptions_ProductSummary.class);
			mapContentsOfProductSummaryData = new HashMap<String, HashMap<String, String>>();
			HashMap<Object, Object> hmAggregationsInfo=objJsonUtil.getMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllAggregations);
			boolean bStatus;
			
			for(HashMap response : lstSourceResponseData.subList(1, lstSourceResponseData.size())) {
				String sSKU = (String)response.get(ProductSearchElasticAPIConstants.MATERIAL);

				for(FlagOptions_ProductSummary flag : FlagOptions_ProductSummary.values()) {
					bStatus = false;
					if(FlagOptions_ProductSummary.values().length == mapSKUDataOfDifferentFlags.size())
						break;

					if(!mapSKUDataOfDifferentFlags.containsKey(flag)) {
						switch(flag) {
						case ACCESSORIES:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES) && response.get(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ACCESSORIES, sSKU);
							}
							break;	
						case ACOP_PROMOTION:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ACOP_PROMOTION, sSKU);
							}
							break;
						case ACOP_QUANTITYBREAK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ACOP_QUANTITYBREAK, sSKU);
							}
							break;
						case ACOP_SPECIALPRICING:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ACOP_SPECIALPRICING, sSKU);
							}
							break;
							
						case ACOP_WEBONLYPRICE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ACOP_WEBONLYPRICE, sSKU);
							}
							break;
						case AUTHORIZEDTOPURCHASE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE) && response.get(ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.AUTHORIZEDTOPURCHASE, sSKU);
							}
							break;							
						case AVAILABLE_IN_BUNDLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_COMPONENT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_COMPONENT) && response.get(ProductSearchElasticAPIConstants.FLAG_COMPONENT).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.AVAILABLE_IN_BUNDLE, sSKU);
							}
							break;
						case BACKORDER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("Y")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.BACKORDER, sSKU);
							}
							break;
						case BLOWOUT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BLOWOUT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BLOWOUT) && response.get(ProductSearchElasticAPIConstants.FLAG_BLOWOUT).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.BLOWOUT, sSKU);
							}
							break;
						case BULKFREIGHT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT) && response.get(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.BULKFREIGHT, sSKU);
							}
							break;
						case BUNDLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BUNDLE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE) && response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.BUNDLE, sSKU);
							}
							break;
						case CLEARANCE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_CLEARANCE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_CLEARANCE) && response.get(ProductSearchElasticAPIConstants.FLAG_CLEARANCE).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.CLEARANCE, sSKU);
							}
							break;
						case DIRECTSHIP:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && null != response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.DIRECTSHIP, sSKU);
							}
							break;
						case DISCONTINUED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) && null != response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) && response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.DISCONTINUED, sSKU);
							}
							break;
						case DOWNLOADABLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ESD) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD) && response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals("true")
									&& response.containsKey(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.DOWNLOADABLE, sSKU);
							}
							break;
						case ENDUSER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.ENDUSER, sSKU);
							}
							break;
						case FREEITEM:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_FREEITEM) && null != response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM) && response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.FREEITEM, sSKU);
							}
							break;
						case HEAVYWEIGHT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) && response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.HEAVYWEIGHT, sSKU);
							}
							break;
						case INSTOCK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.INSTOCK, sSKU);
							}
							break;
						case INSTOCKORORDER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.INSTOCKORORDER, sSKU);
							}
							break;
						case LICENCEPRODUCTS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS) && response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.LICENCEPRODUCTS, sSKU);
							}
							break;
						case LTL:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_LTL) && null != response.get(ProductSearchElasticAPIConstants.FLAG_LTL) && response.get(ProductSearchElasticAPIConstants.FLAG_LTL).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.LTL, sSKU);
							}
							break;
						case NEW:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_NEW) && null != response.get(ProductSearchElasticAPIConstants.FLAG_NEW) && response.get(ProductSearchElasticAPIConstants.FLAG_NEW).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.NEW, sSKU);
							}
							break;
						case NORETURNS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_NORETURNS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS) && response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.NORETURNS, sSKU);
							}
							break;
						case NOTORDERABLE:
							if(null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("N") 
								&& response.containsKey(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).equals("false") 
								&& response.containsKey(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals("true")) {
	                            bStatus = true;
	                            mapSKUDataOfDifferentFlags.put(flag, sSKU);
	                        }
	                        break;
						case OUTOFSTOCK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals("false")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.OUTOFSTOCK, sSKU);
							}
							break;
						case PROMOTION:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) && null != response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) && response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.PROMOTION, sSKU);
							}
							break;
						case QUANTITYBREAK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK) && response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.QUANTITYBREAK, sSKU);
							}
							break;
						case REFURBISHED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) && null != response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) && response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.REFURBISHED, sSKU);
							}
							break;
						case REPLACEMENT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).equals("1")) {
                                bStatus = true;
                                mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.REPLACEMENT, sSKU);
                            }
                            break;
						case RETURNLIMITATIONS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.RETURNLIMITATIONS, sSKU);
							}
							break;
						case SHIPFROMPARTNER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER) && response.get(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.SHIPFROMPARTNER, sSKU);
							}
							break;
						case SIMILARPRODUCTS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.SIMILARPRODUCTS, sSKU);
							}
							break;
						case SPECIALBIDS:
							//not getting Data as of now for this SKU
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS) && response.get(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.BIDPRICING, sSKU);
							}
							break;
						case SPECIALPRICING:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.SPECIALPRICING, sSKU);
							}
							break;
						case SUGGESTED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).equals("2")) {
                                bStatus = true;
                                mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.SUGGESTED, sSKU);
                            }
                            break;
						case WARRANTIES:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WARRANTIES) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTIES) && response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTIES).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.WARRANTIES, sSKU);
							}
							break;
						case WARRANTY:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WARRANTY) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY) && response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.WARRANTY, sSKU);
							}
							break;
						case WEBONLYPRICE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) && response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE).equals("true")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(FlagOptions_ProductSummary.WEBONLYPRICE, sSKU);
							}
							break;						
						default:
							break;
						} //end of switch-case.
					} //end of if loop

					if(bStatus) 
						mapContentsAgainstSku(response,sSKU);
				} //end of inner-for loop
			} //end of outer-for loop

			for(FlagOptions_ProductSummary flag : FlagOptions_ProductSummary.values())
			{
				//if flagoption is not a part of flags hashmap , rehit for specific flag
				//getDataForNonCapturedFlagsIfExpected(flag);
				if(mapSKUDataOfDifferentFlags.get(flag)==null)
				{
					switch(flag)
					{
					case ACCESSORIES:
						break;
					case AVAILABLE_IN_BUNDLE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.COMPONENTPRODUCTS))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.AVAILABLE_IN_BUNDLE,ProductSearchElasticAPIConstants.FLAG_COMPONENT);
						break;
					case AUTHORIZEDTOPURCHASE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.AUTHORIZEDTOPURCHASE))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.AUTHORIZEDTOPURCHASE,ProductSearchElasticAPIConstants.FLAG_AUTHORIZEDTOPURCHASE);
						break;
					case BACKORDER:
						break;
					case BLOWOUT:
						break;
					case BULKFREIGHT:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.BULKFREIGHT,ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT);
						break;
					case BUNDLE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.BUNDLEAVAILABLE))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.BUNDLE,ProductSearchElasticAPIConstants.FLAG_BUNDLE);
						break;
					case CLEARANCE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_CLEARANCE))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.CLEARANCE,ProductSearchElasticAPIConstants.FLAG_CLEARANCE);
						break;
					case DIRECTSHIP:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.DIRECTSHIP))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.DIRECTSHIP,ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP);
						break;
					case DISCONTINUED:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.DISCONTINUED))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.DISCONTINUED,ProductSearchElasticAPIConstants.FLAG_DISCONTINUED);
						break;
					case DOWNLOADABLE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.DOWNLOAD))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.DOWNLOADABLE,ProductSearchElasticAPIConstants.FLAG_ESD);
						break;
					case ENDUSER:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_ENDUSER))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.ENDUSER,ProductSearchElasticAPIConstants.FLAG_ENDUSER);
						break;
					case FREEITEM:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.SHIPALONG))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.FREEITEM,ProductSearchElasticAPIConstants.FLAG_FREEITEM);
						break;
					case HEAVYWEIGHT:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.HEAVYWEIGHT))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.HEAVYWEIGHT,ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT);
						break;
					case INSTOCK:
						break;
					case INSTOCKORORDER:
						break;
					case LICENCEPRODUCTS:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.LICENCEPRODUCTS,ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS);
						break;
					case LTL:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_LTL))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.LTL,ProductSearchElasticAPIConstants.FLAG_LTL);					
						break;	
					case NEW:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_NEW))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.NEW,ProductSearchElasticAPIConstants.FLAG_NEW);					
						break;
					case NORETURNS:
						break;
					case NOTORDERABLE:
						break;
					case OUTOFSTOCK:
						break;						
					case PROMOTION:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.AGGREGATION_PROMOTION))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.PROMOTION,ProductSearchElasticAPIConstants.FLAG_PROMOTION2);					
						break;
					case QUANTITYBREAK:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.AGGREGATION_QTYBREAK))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.QUANTITYBREAK,ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK);					
						break;
					case REFURBISHED:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.REFURBISHED))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.REFURBISHED,ProductSearchElasticAPIConstants.FLAG_REFURBISHED);					
						break;
					case REPLACEMENT:
						break;
					case RETURNLIMITATIONS:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.RETURNLIMITATIONS,ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS);					
						break;
					case SHIPFROMPARTNER:
						//no aggregation info pertaining to SHIPFROMPARTNER, hitting the call anyway
						fillProdSummaryMapForFlag(FlagOptions_ProductSummary.SHIPFROMPARTNER,ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER);
						break;
					case SIMILARPRODUCTS:
						break;
					case SPECIALBIDS:
						break;
					case SPECIALPRICING:
						break;
					case SUGGESTED:
						break;
					case WARRANTIES:
						break;
					case WARRANTY:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.WARRANTYPRODUCTS))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.WARRANTY,ProductSearchElasticAPIConstants.FLAG_WARRANTY);					
						break;
					case WEBONLYPRICE:
						if(checkIfDoccountGreaterThanZeroForFlag(hmAggregationsInfo,ProductSearchElasticAPIConstants.WEBONLYPRICE))
							fillProdSummaryMapForFlag(FlagOptions_ProductSummary.WEBONLYPRICE,ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE);
						break;
					default:
						break;
					}
				}
			}

			System.out.println(mapSKUDataOfDifferentFlags);
			System.out.println(mapContentsOfProductSummaryData);
			System.out.println("Successfully captured data for "+mapSKUDataOfDifferentFlags.size()+" products");

		} catch (Exception e) {
			logger.error("Error in the function captureSKUDataWithDifferentFlags", e);
			e.printStackTrace();
		} 
	}

	private boolean checkIfDoccountGreaterThanZeroForFlag(HashMap<Object, Object> hmAggregationsInfo, String fieldAggregation) {
		int doccount=0;
		String doccounttext=null;
		try {
			doccounttext=hmAggregationsInfo.get(fieldAggregation)!=null?hmAggregationsInfo.get(fieldAggregation).toString():"0";
			if(!doccounttext.equals("0"))
				doccounttext=doccounttext.indexOf(",")==-1?doccounttext.substring(doccounttext.indexOf("=")+1,doccounttext.indexOf("}")):doccounttext.substring(doccounttext.indexOf("=")+1,doccounttext.indexOf(","));
			doccount=Integer.parseInt(doccounttext);
		} catch (Exception e) {
			logger.error("Error in the function checkIfDoccountGreaterThanZeroForFlag", e);
			e.printStackTrace();
		} 
		return doccount>0;
	}

	private void fillProdSummaryMapForFlag(FlagOptions_ProductSummary flagOpt, String flag) {
		String sSku2=null;
		try {
			if(mapSKUDataOfDifferentFlags.get(flagOpt)==null)
			{
				HashMap<String, String> sKUSource=captureDataForFlagsIfNotRetrievedInFirstCall(flag);
				if(sKUSource!=null)
				{
					sSku2=sKUSource.get(ProductSearchElasticAPIConstants.MATERIAL).toString();
					mapSKUDataOfDifferentFlags.put(flagOpt,sSku2);
					mapContentsAgainstSku(sKUSource,sSku2);
				}
			}} catch (Exception e) {
				logger.error("Error in the function fillProdSummaryMapForFlag", e);
				e.printStackTrace();
			} 
	}

	/*	To capture "material","manufacturerpartnumber","upcean","gimagemid","mediumdesc",
	"aggavailableqty","dealerprice","shipalongexpiredt","shipalongpartdes","shipalongmaterial",
	"shipalongfreeqty","shipalongminqty","shipalong","subrule","submaterial","partofbundle","subrule",
	"submaterial","crtvaluedesc","crtvalue","campaign", "promoenddt" with SKU Number as Key.*/
	private void mapContentsAgainstSku(HashMap response, String sSKU) {
		if(!mapContentsOfProductSummaryData.containsKey(sSKU)) {
			String mapValue = null;
			HashMap<String, String> mapContentsOfProductSummary = new HashMap<String, String>();
			for(String sBasicData : ProductSearchElasticAPIConstants.attributeArrForProductSummary) {
				if(response.containsKey(sBasicData) && null != response.get(sBasicData)) {
					mapValue = (String) response.get(sBasicData).toString().trim();
					mapContentsOfProductSummary.put(sBasicData, mapValue);
				}
			}
			mapContentsOfProductSummaryData.put(sSKU, mapContentsOfProductSummary);
		}
	}
	
	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil
	
	public EnumMap<FlagOptions_ProductSummary, String> getMapSKUDataOfDifferentFlags() {
		return mapSKUDataOfDifferentFlags;
	}

	public HashMap<String, HashMap<String, String>> getMapContentsOfProductSummaryData() {
		return mapContentsOfProductSummaryData;
	}

}
