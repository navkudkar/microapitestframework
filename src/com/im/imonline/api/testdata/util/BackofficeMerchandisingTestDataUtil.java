package com.im.imonline.api.testdata.util;

/**
 * This class responsible for generating test data for Merchadising Backoffice rule 
 * @author Paresh Save
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.backoffice.business.BackOfficeCreateRule;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;

public class BackofficeMerchandisingTestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("MerchandisingBackofficeTestDataUtil"); 
	private JsonUtil objJsonUtil=null;	
	private List<String> lstSuitableCategory, lstCategoryFromResponse;
	private List<String> lstVendorsFromResponse;
	private HashMap<String, HashMap<String, String>> mapOfAllSKUandData,mapOfAllSKUandDataForDynamicContent;
	private List<String> listOfSKUs;
	List<HashMap<String, String>> lstAllSourceDataFromResponse;
	List<HashMap<String, String>> lstMapBackOfficeRuleTestCase;
	private Map<String,String> mapCookies ;	
	int iTotalProducts;
	String  sCountry;
	private String sCreateRequestVerificationToken;
	
	
	public BackofficeMerchandisingTestDataUtil(HashMap<String, String> mapElasticReqData) throws Exception {
		super(mapElasticReqData);
		processForBackofficeRuleSpecificData();
		this.sCountry = mapElasticReqData.get(Constants.ExcelHeaderRunConfig);
		generateRequestVerficationTokenForBackOfficeRuleCreation();
	}
	
	private void processForBackofficeRuleSpecificData(){
		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(ProductSearchElasticAPITest.TestDataFile, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.sFileName);	
			
			if(objJsonUtil==null || objJsonUtil.getStatusCode()!=200)
				throw new Exception("Null values in Json Response in the function findOutMostSuitableSKUcategory");
			
			// Capture total product
			captureTotalProductsCount();
			captureVendorsNamesList();
			setListOfSKU();	

			// Store SKU specific data
			//setSKUSpecificData(BackOfficeMerchandisingAPIConstants.arrKeysToBeCapturedForSKUs);			
			
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}	
	
	private void captureTotalProductsCount() throws Exception {
		try {
			lstAllSourceDataFromResponse = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			lstCategoryFromResponse = objJsonUtil.getListofStringsFromJSONResponse("responses[0].hits.hits._source.cat1desc");
			iTotalProducts = lstAllSourceDataFromResponse.size();
			
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}

	}
	
	private void captureVendorsNamesList() throws Exception {
		try {
			lstAllSourceDataFromResponse = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			lstVendorsFromResponse = objJsonUtil.getListofStringsFromJSONResponse("responses[0].hits.hits._source.vendorname");	
			
		} catch (Exception e) {
			logger.error("Error in the function captureVendorsNamesList", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}

	}
	
	@SuppressWarnings({"unused", "rawtypes"})
	private void findOutMostSuitableSKUcategory() {
		
		try {
			
			lstSuitableCategory = new ArrayList<String>();
			lstSuitableCategory.addAll(lstCategoryFromResponse);
			lstSuitableCategory.removeAll(Collections.singletonList(null)); // removing null values

			// Count the total no of Categories present in the given list
			Map<String, Long> counted = lstSuitableCategory.stream()
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

			// Sorting map in descending order
			Map<String, Long> mapSortedCategory = new LinkedHashMap<>();  

			counted.entrySet().stream()
			.sorted(Map.Entry.<String, Long>comparingByValue()
					.reversed()).forEachOrdered(e -> mapSortedCategory.put(e.getKey(), e.getValue()));

			lstSuitableCategory.clear();
			
			lstSuitableCategory.addAll(mapSortedCategory.entrySet().stream()
					  .map(Map.Entry::getKey)
					  .limit(5)
					  .collect(Collectors.toList()));

			System.out.println("Products with most suitable data categoey "+lstSuitableCategory);

		} catch (Exception e) {
			logger.error("Error in the function findOutMostSuitableSKUcategory", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}

	@SuppressWarnings({"unused"})
	private void setSKUSpecificData(String[] pArrKeysToBeCapturedForSKUs) throws Exception {
		mapOfAllSKUandData = new HashMap<String, HashMap<String, String>>();
		try {
			
			for(String sCategory: lstSuitableCategory) {				
				for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
					String sCurrentCategory = currentMap.get("cat1desc");
					if(sCategory.trim().equals(sCurrentCategory)) {
						String sSKU = (String) currentMap.get("material");						
						mapOfAllSKUandData.put(sSKU, getRequireDataOfSKU(currentMap, pArrKeysToBeCapturedForSKUs));	
						break;
					}								
				}
			}
			System.out.println("Total List of SKUs Size: "+listOfSKUs.size()); 
		}
		catch(Exception e) {
			logger.error("Error in the function setSKUSpecificData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}
	
	@SuppressWarnings({"unused"})
	private void setListOfSKU() throws Exception {
		listOfSKUs = new ArrayList<String>();
		try {						
				for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
					String sSKU = (String) currentMap.get("material");
					//for HU instock is getting null for all sku currentMap.get("instock")!=null&&currentMap.get("instock").equalsIgnoreCase("true")&&
					if(!sSKU.isEmpty()) {
						listOfSKUs.add(sSKU);						
					}
				}
			System.out.println("Data Captured for SKU: "+listOfSKUs); 
		}
		catch(Exception e) {
			logger.error("Error in the function setSKUSpecificData", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}

	private HashMap<String, String> getRequireDataOfSKU(HashMap<String, String> pLstMapFromResponse, String []pArrKeysToBeCapturedForSKUs) {

		HashMap<String, String> mapOfDataRelatedSKU = new HashMap<String, String>();

		try {
			// Storing data against SKU		
			for(String sKey : pArrKeysToBeCapturedForSKUs) {
				if(pLstMapFromResponse.containsKey(sKey))
				{
				Object oValue = pLstMapFromResponse.get(sKey);
				String sValue=(oValue!=null)?oValue.toString():ProductSearchElasticAPIConstants.NULLCONSTANT;
				mapOfDataRelatedSKU.put(sKey, sValue);
				}
				else
				mapOfDataRelatedSKU.put(sKey, ProductSearchElasticAPIConstants.NULLCONSTANT);
			}

		} catch (Exception e) {
			logger.error("Error in the function getRequireDataOfSKU", e);
			e.printStackTrace();
		}

		return mapOfDataRelatedSKU;
	}
	
	@SuppressWarnings({"unused"})
	private void setSKUSpecificDataForDynamicContent(String[] pArrKeysToBeCapturedForSKUs) throws Exception {
		mapOfAllSKUandDataForDynamicContent = new HashMap<String, HashMap<String, String>>();	
		try {
			for(HashMap<String, String> currentMap:lstAllSourceDataFromResponse) {
						String sSKU = (String) currentMap.get("material");	
						Map<String, String> mapString = getRequireDataOfSKU(currentMap, pArrKeysToBeCapturedForSKUs);
						boolean flag = mapString.entrySet().stream().anyMatch(s -> s.getValue().equalsIgnoreCase(ProductSearchElasticAPIConstants.NULLCONSTANT));
						if(!flag)					
							mapOfAllSKUandDataForDynamicContent.put(sSKU, getRequireDataOfSKU(currentMap, pArrKeysToBeCapturedForSKUs));													
			}			
		}
		catch(Exception e) {
			logger.error("Error in the function setSKUSpecificDataForDynamicContent", e);
			e.printStackTrace();
		}finally{
			System.gc();
		}
	}
	
	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil

	public List<String> getTopMostCategoryList() {
		return lstSuitableCategory;
	}
	
	public List<String> getVendorsList() {
		lstVendorsFromResponse.removeAll(Collections.singletonList(null));
		return lstVendorsFromResponse.stream().distinct().limit(5).collect(Collectors.toList());
	}
	
	public List<String> getCategoryList() {
		lstCategoryFromResponse.removeAll(Collections.singletonList(null));
		return lstCategoryFromResponse.stream().distinct().limit(5).collect(Collectors.toList());
	}
	
	public List<String> getTopMostKeywordList() {
		return lstSuitableCategory;
	}
	
	public List<String> getListOfSKUs() {
		return listOfSKUs;
	}	
	
	public HashMap<String, HashMap<String, String>> getTopSKUsWithLocationData(String[] arrLocations) throws Exception {
		setSKUSpecificDataForDynamicContent(arrLocations);
		return mapOfAllSKUandDataForDynamicContent;
	}
	
	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}	
	
	public String getTotalProducts() throws Exception {
		return String.valueOf(iTotalProducts);
	}
	
	public void generateRequestVerficationTokenForBackOfficeRuleCreation() throws Exception {
		try {
			BackOfficeCreateRule objBkCreateRule = new BackOfficeCreateRule(sCountry);
			objBkCreateRule.generateRequestVerficationTokenForBackOfficeRuleCreation();
			this.mapCookies = objBkCreateRule.getCreateRuleRequestCookies();
			this.sCreateRequestVerificationToken = objBkCreateRule.getCreateRuleRequestVerificationToken();		
			System.out.println("Request Verification Token Create BO Rule for ["+sCountry+"] = "+sCreateRequestVerificationToken);		
		}
		catch(Exception e) {
			logger.error("Error in the function generateRequestVerficationTokenForBackOfficeRuleCreation", e);
			e.printStackTrace();
		}
	}	
	
	public Map<String, String> getCreateRuleRequestCookies() {
		return mapCookies;
	}	
	
	public String getCreateRuleRequestVerificationToken() throws Exception {
		return sCreateRequestVerificationToken;
	}	
	
	public String getCountry() throws Exception {
		return sCountry;
	}
	
	public static void main(String args[]) throws Exception {
	}

}
