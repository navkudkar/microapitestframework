package com.im.imonline.api.testdata.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.constants.OrderSearchAPIConstants_New;
import com.im.imonline.api.tests.OrderSearchAPITest_New;

public class OrderSearchTestDataUtil extends BaseTestDataUtil {

	transient Logger logger = Logger.getLogger("OrderSearchTestDataUtil");
	//private JsonUtil objJsonUtil=null;
	HashMap<String, String> hmTestData = null;

	private List<String> lstOrderNumber = null;
	private List<String> lstResellerNumber = null;
	private List<String> lstCustomerOrderNumber = null;
	private Set<String> setIngramPartNumber = null;
	private Set<String> setVendorPartNumber = null;
	private Set<String> setSerialNumber = null;
	private Set<String> setVendorName = null;
	private Set<String> setEndUserPONumber = null;
	private Set<String> setUPCNumber= null;
	private Set<String> setBidNumber= null;
	private Set<String> setCPNumber= null;
	private List<String> lstInvoiceNumber = null;
	public OrderSearchTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		processForCountrySpecificData();
	}


	private void processForCountrySpecificData(){

		try {
			JsonUtil objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(OrderSearchAPITest_New.TestDataFile, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.sFileName);	

			captureDataForOrderNumber(objJsonUtil);
			//captureDataForInvoiceNumber(objJsonUtil);
			captureDataForSKU(objJsonUtil);
			captureDataForVPN(objJsonUtil);
			captureDataForVendorName(objJsonUtil);
			captureDataForResellerNumber(objJsonUtil);
			captureDataForCustomerOrderNumber(objJsonUtil);
			captureDataForEndUserPONumber(objJsonUtil);
			captureDataForSerialNumber(objJsonUtil);
			captureDataForUPCNumber(objJsonUtil);
			captureDataForBidNumber(objJsonUtil);
			captureDataForCPNNumber(objJsonUtil);
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
		finally {
			System.gc();
		}
	}

	private void captureDataForInvoiceNumber(JsonUtil objJsonUtil) {
		try {
			JsonUtil objJsonUtilInvoice=null;
			lstInvoiceNumber = new ArrayList<String>();

			List<String> lstOfInvoiceNumber = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathOrderNumber);
			if(lstOfInvoiceNumber.isEmpty() || null==lstOfInvoiceNumber) {
				HashMap<String, String> hmCPNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmCPNumber.put(OrderSearchAPIConstants_New.InvoiceNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmCPNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilInvoice = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilInvoice.postRequest();

				lstInvoiceNumber = objJsonUtilInvoice.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCPN);
				lstInvoiceNumber.removeAll(Collections.singletonList(null));
				lstInvoiceNumber.removeIf(p -> p.isEmpty());
			}
			for(String sCurrentValue: lstOfInvoiceNumber) {				
				if(!lstInvoiceNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstInvoiceNumber.add(sCurrentValue);

				if(lstInvoiceNumber.size()==5) break;
			}

			//System.out.println(lstOfOrderNumber);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForInvoice", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}


	private void captureDataForCPNNumber(JsonUtil objJsonUtil) {
		try {
			JsonUtil objJsonUtilCPN=null;
			List<ArrayList<String>> lstCPN = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCPN);

			lstCPN.removeAll(Collections.singletonList(null));
			lstCPN.removeIf(p -> p.isEmpty());

			if(lstCPN.isEmpty() || null==lstCPN) {
				HashMap<String, String> hmCPNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmCPNumber.put(OrderSearchAPIConstants_New.CPN, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmCPNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilCPN = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilCPN.postRequest();

				lstCPN = objJsonUtilCPN.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCPN);
				lstCPN.removeAll(Collections.singletonList(null));
				lstCPN.removeIf(p -> p.isEmpty());
			}
			setCPNumber= new HashSet<>();
			for(ArrayList<String> arrlstCPN : lstCPN) {

				if(arrlstCPN!=null)
					for(String sCPN : arrlstCPN) {
						setCPNumber.add(sCPN.trim());
					}
				if(setCPNumber.size()>=5) break;
			}

			//	System.out.println("BidRef captured: "+setCPNNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForCPNNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForBidNumber(JsonUtil objJsonUtil) {
		try {
			JsonUtil objJsonUtilBid=null;
			List<ArrayList<String>> lstBidRef = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathBid);

			lstBidRef.removeAll(Collections.singletonList(null));
			lstBidRef.removeIf(p -> p.isEmpty());

			if(lstBidRef.isEmpty() || null==lstBidRef) {
				HashMap<String, String> hmBidRef = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmBidRef.put(OrderSearchAPIConstants_New.Bid, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmBidRef, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilBid = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilBid.postRequest();

				lstBidRef = objJsonUtilBid.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathBid);
				lstBidRef.removeAll(Collections.singletonList(null));
				lstBidRef.removeIf(p -> p.isEmpty());
			}
			setBidNumber= new HashSet<>();
			for(ArrayList<String> arrlstBid : lstBidRef) {

				if(arrlstBid!=null)
					for(String sBid : arrlstBid) {
						setBidNumber.add(sBid.trim());
					}
				if(setBidNumber.size()>=5) break;
			}

			//	System.out.println("BidRef captured: "+setBidNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForBid", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForSerialNumber(JsonUtil objJsonUtil) {
		try {
			JsonUtil objJsonUtilSerial=null;
			List<ArrayList<String>> lstSerial = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSerialNumber);

			lstSerial.removeAll(Collections.singletonList(null));
			lstSerial.removeIf(p -> p.isEmpty());

			if(lstSerial.isEmpty() || null==lstSerial) {
				HashMap<String, String> hmSerialNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmSerialNumber.put(OrderSearchAPIConstants_New.SerialNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmSerialNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilSerial = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilSerial.postRequest();

				lstSerial = objJsonUtilSerial.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSerialNumber);
				lstSerial.removeAll(Collections.singletonList(null));
				lstSerial.removeIf(p -> p.isEmpty());
			}
			setSerialNumber= new HashSet<>();
			for(ArrayList<String> arrlstSerial : lstSerial) {

				if(arrlstSerial!=null)
					for(String sSKU : arrlstSerial) {
						setSerialNumber.add(sSKU.trim());
					}
				if(setSerialNumber.size()>=5) break;
			}

			//	System.out.println("Serial captured: "+setSerialNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForSerial", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForUPCNumber(JsonUtil objJsonUtil) {
		try {
			//	int CountOfSKU=objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU).size();
			JsonUtil objJsonUtilUPC=null;
			List<ArrayList<String>> lstUPC = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathUPCNumber);

			lstUPC.removeAll(Collections.singletonList(null));
			lstUPC.removeIf(p -> p.isEmpty());

			if(lstUPC.isEmpty() || null==lstUPC) {
				HashMap<String, String> hmUPC = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmUPC.put(OrderSearchAPIConstants_New.UPCNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmUPC, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilUPC = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilUPC.postRequest();

				lstUPC = objJsonUtilUPC.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathUPCNumber);
				lstUPC.removeAll(Collections.singletonList(null));
				lstUPC.removeIf(p -> p.isEmpty());
			}
			setUPCNumber= new HashSet<>();
			for(ArrayList<String> arrlstUPC : lstUPC) {

				if(arrlstUPC!=null)
					for(String sUPC : arrlstUPC) {
						setUPCNumber.add(sUPC.trim());
					}
				if(setUPCNumber.size()>=5) break;
			}

			//System.out.println("UPC captured: "+setUPCNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForUPC", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForEndUserPONumber(JsonUtil objJsonUtil) {
		try {
			//	int CountOfSKU=objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU).size();
			JsonUtil objJsonUtilEndUserPO=null;
			List<ArrayList<String>> lstEndUserPO = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathEndUserPO);

			lstEndUserPO.removeAll(Collections.singletonList(null));
			lstEndUserPO.removeIf(p -> p.isEmpty());
			HashMap<String, String> hmEndUserPONumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
			if(lstEndUserPO.isEmpty() || null==lstEndUserPO) {

				hmEndUserPONumber.put(OrderSearchAPIConstants_New.EndUserPO, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmEndUserPONumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilEndUserPO = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilEndUserPO.postRequest();

				lstEndUserPO = objJsonUtilEndUserPO.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathEndUserPO);
				lstEndUserPO.removeAll(Collections.singletonList(null));
				lstEndUserPO.removeIf(p -> p.isEmpty());
			}
			setEndUserPONumber= new HashSet<>();
			for(ArrayList<String> arrlstSku : lstEndUserPO) {

				if(arrlstSku!=null) {
					//					String idValue=hmEndUserPONumber.get(OrderSearchAPIConstants_New.ID);
					//					if(idValue.equalsIgnoreCase("IMOrderSearch_CRM")){
					for(String sSKU : arrlstSku) {
						String sEndUserPO=sSKU.replaceAll("[^a-zA-Z0-9]", "").trim();
						setEndUserPONumber.add(sEndUserPO);

					}
					//					}else {
					//					for(String sSKU : arrlstSku) {
					//						//String sEndUserPO=sSKU.replaceAll("[^a-zA-Z0-9]", "").trim();
					//						setEndUserPONumber.add(sSKU);
					//					
					//					}}
					if(setEndUserPONumber.size()>=5) break;
				}

				//System.out.println("End User PO captured: "+setEndUserPONumber);
			}}catch(Exception e) {
				logger.error("Error in the function captureDataForEndUserPONumber", e);
				e.printStackTrace();
			}finally {
				System.gc();
			}
	}
	private void captureDataForCustomerOrderNumber(JsonUtil objJsonUtil) {
		try {
			lstCustomerOrderNumber = new ArrayList<String>();

			List<String> lstOfCustomerOrderNumber = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathCustomerOrderNumber);
			lstOfCustomerOrderNumber.removeAll(Collections.singletonList(null));
			for(String sCurrentValue: lstOfCustomerOrderNumber) {		
				String sCurrentValueWithoutSPplChar=sCurrentValue.replaceAll("[^a-zA-Z0-9]", "").trim();
				if(!lstCustomerOrderNumber.contains(sCurrentValueWithoutSPplChar) && sCurrentValueWithoutSPplChar!=null)
					lstCustomerOrderNumber.add(sCurrentValueWithoutSPplChar);

				if(lstCustomerOrderNumber.size()==5) break;
			}

			//System.out.println(lstOfCustomerOrderNumber);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForCustomerOrderNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForResellerNumber(JsonUtil objJsonUtil) {
		try {
			lstResellerNumber = new ArrayList<String>();

			List<String> lstOfResllerNumber = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathResellerNumber);
			lstOfResllerNumber.removeAll(Collections.singletonList(null));
			for(String sCurrentValue: lstOfResllerNumber) {				
				if(!lstResellerNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstResellerNumber.add(sCurrentValue);

				if(lstResellerNumber.size()==5) break;
			}

			//System.out.println(lstOfResllerNumber);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForResellerNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	private void captureDataForOrderNumber(JsonUtil objJsonUtil) {
		try {
			lstOrderNumber = new ArrayList<String>();

			List<String> lstOfOrderNumber = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathOrderNumber);

			for(String sCurrentValue: lstOfOrderNumber) {				
				if(!lstOrderNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstOrderNumber.add(sCurrentValue);

				if(lstOrderNumber.size()==5) break;
			}

			//System.out.println(lstOfOrderNumber);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForInvoice", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}


	private void captureDataForSKU(JsonUtil objJsonUtil) throws Exception {
		try {
			//	int CountOfSKU=objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU).size();
			JsonUtil objJsonUtilSKU=null;
			List<ArrayList<String>> lstSKU = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU);

			lstSKU.removeAll(Collections.singletonList(null));
			lstSKU.removeIf(p -> p.isEmpty());

			if(lstSKU.isEmpty() || null==lstSKU) {
				HashMap<String, String> hmSKUNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmSKUNumber.put(OrderSearchAPIConstants_New.IngramPartNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmSKUNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilSKU = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilSKU.postRequest();

				lstSKU = objJsonUtilSKU.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU);
				lstSKU.removeAll(Collections.singletonList(null));
				lstSKU.removeIf(p -> p.isEmpty());
			}
			setIngramPartNumber= new HashSet<>();
			for(ArrayList<String> arrlstSku : lstSKU) {

				if(arrlstSku!=null)
					for(String sSKU : arrlstSku) {
						setIngramPartNumber.add(sSKU.trim());
					}
				if(setIngramPartNumber.size()>=5) break;
			}

			//	System.out.println("SKU captured: "+setIngramPartNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForSKU", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForVendorName(JsonUtil objJsonUtil) {
		try {
			//	int CountOfSKU=objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU).size();
			JsonUtil objJsonUtilVendorName=null;
			List<ArrayList<String>> lstVendorName = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVendorName);
			lstVendorName.removeAll(Collections.singletonList(null));
			lstVendorName.removeIf(p -> p.isEmpty());

			if(lstVendorName.isEmpty() || null==lstVendorName) {
				HashMap<String, String> hmSKUNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmSKUNumber.put(OrderSearchAPIConstants_New.VendorName, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmSKUNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilVendorName = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilVendorName.postRequest();

				lstVendorName = objJsonUtilVendorName.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVendorName);
				lstVendorName.removeAll(Collections.singletonList(null));
				lstVendorName.removeIf(p -> p.isEmpty());
			}
			setVendorName= new HashSet<>();
			for(ArrayList<String> arrlstVendorName : lstVendorName) {

				if(arrlstVendorName!=null)
					for(String sVN : arrlstVendorName) {
						String[] sFirstWordFromVendorName=sVN.split(" ");
						setVendorName.add(sFirstWordFromVendorName[0]);
					}
				if(setVendorName.size()>=5) break;
			}

			//System.out.println("Vendor captured: "+setVendorName);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForVendorName", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForVPN(JsonUtil objJsonUtil) throws Exception {
		try {
			//	int CountOfSKU=objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathSKU).size();
			JsonUtil objJsonUtilVPN=null;
			List<ArrayList<String>> lstVPN = objJsonUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVPN);
			lstVPN.removeAll(Collections.singletonList(null));
			lstVPN.removeIf(p -> p.isEmpty());

			if(lstVPN.isEmpty() || null==lstVPN) {
				HashMap<String, String> hmSKUNumber = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(OrderSearchAPITest_New.sFileName,mapReqDataForTemplate,OrderSearchAPITest_New.isMultiSet);
				hmSKUNumber.put(OrderSearchAPIConstants_New.VendorPartNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName, hmSKUNumber, OrderSearchAPITest_New.isMultiSet, OrderSearchAPITest_New.TestDataFile);
				objJsonUtilVPN = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilVPN.postRequest();

				lstVPN = objJsonUtilVPN.getListofStringsFromJSONResponse(OrderSearchAPIConstants_New.xpathVPN);
				lstVPN.removeAll(Collections.singletonList(null));
				lstVPN.removeIf(p -> p.isEmpty());
			}
			setVendorPartNumber= new HashSet<>();
			for(ArrayList<String> arrlstSku : lstVPN) {

				if(arrlstSku!=null)
					for(String sSKU : arrlstSku) {
						setVendorPartNumber.add(sSKU.trim());
					}
				if(setVendorPartNumber.size()>=5) break;
			}

			//	System.out.println("VPN captured: "+setVendorPartNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForVPN", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}

	public static void main(String args[]) throws Exception {
	}


	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}


	public List<String> getListOfOrderNumber() {
		return lstOrderNumber;
	}
	public List<String> getListOfInvoiceNumber() {
		return lstInvoiceNumber;
	}
	public List<String> getListOfIngramPartNumber() { 
		return new ArrayList<>(setIngramPartNumber);
	}
	public List<String> getListOfVendorName() {
		return new ArrayList<>(setVendorName);
	}

	public List<String> getListOfVendorPartNumber() {
		return new ArrayList<>(setVendorPartNumber);
	}

	public List<String> getListOfResellerNumber() {
		return lstResellerNumber;
	}

	public List<String> getListOfCustomerOrderNumber() {
		return lstCustomerOrderNumber;
	}


	public List<String> getListOfEndUserPONumber() {
		return new ArrayList<>( setEndUserPONumber);
	}
	public List<String> getListOfSerialNumber() {
		return new ArrayList<>(setSerialNumber);
	}
	public List<String> getListOfUPCNumber() {
		return new ArrayList<>(setUPCNumber);
	}
	public List<String> getListOfBidNumber() {
		return new ArrayList<>(setBidNumber);
	}
	public List<String> getListOfCPNNumber() {
		return new ArrayList<>(setCPNumber);
	}

}
