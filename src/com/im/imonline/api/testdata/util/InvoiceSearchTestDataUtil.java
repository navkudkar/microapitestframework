package com.im.imonline.api.testdata.util;

/**
 * This class is responsible for generating test data for Invoice search at initial hit
 * @author Sumeet Umalkar
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.constants.InvoiceSearchAPIConstants;
import com.im.imonline.api.tests.InvoiceSearchAPITest;

public class InvoiceSearchTestDataUtil extends BaseTestDataUtil {

	transient Logger logger = Logger.getLogger("InvoiceSearchTestDataUtil"); 
	transient private JsonUtil objJsonUtil=null;
	HashMap<String, String> hmTestData = null;

	private List<String> lstInvoiceNumber = null;
	private Set<String> setSerialNumber = null;
	private Set<String> setIngramPartNumber=null;
	private Set<String> setVendorPartNumber=null;
	private List<String> lstResellerNumber = null;
	private List<String> lstCustomerOrderNumber = null;
	private Set<String> setBidNumber = null;
	private List<String> lstInvoiceDate = null;
	private Set<String> setCPN = null;

	public InvoiceSearchTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		processForCountrySpecificData();
	}


	private void processForCountrySpecificData(){

		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(InvoiceSearchAPITest.TestDataFile, InvoiceSearchAPITest.isMultiSet, InvoiceSearchAPITest.sFileName);	

			captureDataForInvoiceNumber();			
			captureDataForSerialNumber();
			captureDataForSKU();
			captureDataForVPN();
			captureDataForResellerNumber();
			captureDataForCPN();
			captureDataForCustomerOrderNumber();
			captureDataForBidNumber();
			captureDataForInvoiceDate();
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
		finally {
			System.gc();
		}
	}



	private void captureDataForResellerNumber(){
		try {
			List<String> lstOFResellerNumber = objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathResellerNumber);
			lstResellerNumber= new ArrayList<String>();
			for(String sCurrentValue: lstOFResellerNumber) {				
				if(!lstResellerNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstResellerNumber.add(sCurrentValue);

				if(lstResellerNumber.size()==5) break;
			}

			//System.out.println("ResellerNumber data captured: "+lstResellerNumber);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForResellerNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}

	}

	private void captureDataForCustomerOrderNumber(){
		try {
			List<String> lstOFCustomerOrderNumber = objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCustomerOrderNumber);
			lstCustomerOrderNumber= new ArrayList<String>();
			for(String sCurrentValue: lstOFCustomerOrderNumber) {	
				String sNoWhiteSpaceCurrentValue=sCurrentValue.trim().replaceAll("\\s","");

				if(!(lstCustomerOrderNumber.contains(sNoWhiteSpaceCurrentValue)) && sNoWhiteSpaceCurrentValue!=null && (!(sNoWhiteSpaceCurrentValue.contains("\\s+"))))
					lstCustomerOrderNumber.add(sNoWhiteSpaceCurrentValue);
				if(lstCustomerOrderNumber.size()==5) break;
			}

			//System.out.println("Customer Order Number data captured: "+lstCustomerOrderNumber);

		} catch (Exception e) {
			logger.error("Error in the function captureDataForCustomerOrderNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}
	private void captureDataForCPN(){

		JsonUtil objJsonUtilCPN=null;
		setCPN=new HashSet<>();
		try {
			List<ArrayList<String>> lstVerifyCPN=objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCPN);
			lstVerifyCPN.removeAll(Collections.singletonList(null));
			lstVerifyCPN.removeIf(p->p.isEmpty());

			if(lstVerifyCPN.contains(null)||lstVerifyCPN.isEmpty()) {
				HashMap<String, String> hmForCPN = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(InvoiceSearchAPITest.sFileName,mapReqDataForTemplate,InvoiceSearchAPITest.isMultiSet);
				hmForCPN.put(InvoiceSearchAPIConstants.CPN, "*");
				String sRequestBody = AppUtil.getRequestBodyForRestRequest(InvoiceSearchAPITest.sFileName, hmForCPN, InvoiceSearchAPITest.isMultiSet, InvoiceSearchAPITest.TestDataFile);
				objJsonUtilCPN= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilCPN.postRequest();

				lstVerifyCPN=objJsonUtilCPN.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathCPN);
				lstVerifyCPN.removeAll(Collections.singletonList(null));
				lstVerifyCPN.removeIf(p->p.isEmpty());

			}

			for(ArrayList<String> arrlstCPN : lstVerifyCPN) {

				if(arrlstCPN!=null)
					for(String sSKU : arrlstCPN) {
						setCPN.add(sSKU.trim().replaceAll("[^a-zA-Z0-9]", ""));
						if(setCPN.size()>=5) break;
					}
				if(setCPN.size()>=5) break;
			}

			//System.out.println("CPN captured: "+setCPN);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForCPN", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}}
	private void captureDataForBidNumber() {

		JsonUtil objJsonUtilBid=null;
		setBidNumber= new HashSet<>();
		try {
			List<ArrayList<String>> lstVerifyBid=objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathBid);
			lstVerifyBid.removeAll(Collections.singletonList(null));
			lstVerifyBid.removeIf(p -> p.isEmpty());

			if(lstVerifyBid.contains(null)||lstVerifyBid.isEmpty()) {
				HashMap<String, String> hmForBid = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(InvoiceSearchAPITest.sFileName,mapReqDataForTemplate,InvoiceSearchAPITest.isMultiSet);
				hmForBid.put(InvoiceSearchAPIConstants.Bid, "*");
				String sRequestBody = AppUtil.getRequestBodyForRestRequest(InvoiceSearchAPITest.sFileName, hmForBid, InvoiceSearchAPITest.isMultiSet, InvoiceSearchAPITest.TestDataFile);
				objJsonUtilBid= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilBid.postRequest();
				lstVerifyBid=objJsonUtilBid.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathBid);
				lstVerifyBid.removeAll(Collections.singletonList(null));
				lstVerifyBid.removeIf(p -> p.isEmpty());
			}
			for(ArrayList<String> arrlstBid : lstVerifyBid) {

				if(arrlstBid!=null)
					for(String sBID : arrlstBid) {
						setBidNumber.add(sBID.trim());
						if(setBidNumber.size()>=5) break;
					}
				if(setBidNumber.size()>=5) break;

			} //System.out.println("Bid captured: "+setBidNumber);
		}catch (Exception e) {
			logger.error("Error in the function captureDataForBidNumber", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}

	}

	private void captureDataForVPN() throws Exception {
		try {
			setVendorPartNumber= new HashSet<>();
			List<ArrayList<String>> lstVendorPartNumber= objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathVPN);
			lstVendorPartNumber.removeAll(Collections.singletonList(null));
			lstVendorPartNumber.removeIf(p -> p.isEmpty());

			for(ArrayList<String> arrlstVPN : lstVendorPartNumber) {

				if(arrlstVPN!=null)
					for(String sVPN : arrlstVPN) {
						setVendorPartNumber.add(sVPN.trim());
						if(setVendorPartNumber.size()>=5) break;
					}
				if(setVendorPartNumber.size()>=5) break;
			}

			//System.out.println("VPN captured: "+setVendorPartNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForVPN", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}}

	private void captureDataForSKU() throws Exception {
		try {
			List<ArrayList<String>> lstIngramPartNumber=objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathSKU);
			lstIngramPartNumber.removeAll(Collections.singletonList(null));
			lstIngramPartNumber.removeIf(p -> p.isEmpty());
			setIngramPartNumber= new HashSet<>();
			for(ArrayList<String> arrlstSku : lstIngramPartNumber) {
				
				if(arrlstSku!=null)
					for(String sSKU : arrlstSku) {
						setIngramPartNumber.add(sSKU.trim());
						if(setIngramPartNumber.size()>=5) break;
					}
				if(setIngramPartNumber.size()>=5) break;
			}
			//System.out.println("SKU captured: "+setIngramPartNumber);
		}catch(Exception e) {
			logger.error("Error in the function captureDataForSKU", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}}

	private void captureDataForSerialNumber() {

		JsonUtil objJsonUtilSerial=null;
		setSerialNumber = new HashSet<>();
		try {
			List<ArrayList<String>>	lstSerialArray=objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathSerialNumber);
			lstSerialArray.removeAll(Collections.singletonList(null));
			lstSerialArray.removeIf(p->p.isEmpty());

			if(lstSerialArray.contains(null)||lstSerialArray.isEmpty()) {
				HashMap<String, String> hmForSerial=AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(InvoiceSearchAPITest.sFileName,mapReqDataForTemplate,InvoiceSearchAPITest.isMultiSet);
				hmForSerial.put(InvoiceSearchAPIConstants.SerialNumber, "*");
				String sRequestBody=AppUtil.getRequestBodyForRestRequest(InvoiceSearchAPITest.sFileName, hmForSerial, InvoiceSearchAPITest.isMultiSet, InvoiceSearchAPITest.TestDataFile);
				objJsonUtilSerial= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
				objJsonUtilSerial.postRequest();

				lstSerialArray = objJsonUtilSerial.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathSerialNumber);
				lstSerialArray.removeAll(Collections.singletonList(null));
				lstSerialArray.removeIf(p->p.isEmpty());
			}

			for(ArrayList<String> arrlstSerial : lstSerialArray) {
				if(arrlstSerial!=null)
					for(String sSerial : arrlstSerial) {
						setSerialNumber.add(sSerial.trim().replaceAll("[^a-zA-Z0-9]", ""));
						if(setSerialNumber.size()>=5) break;
					}
				if(setSerialNumber.size()>=5) break;
			}
			//System.out.println("Serial Number data captured: "+setSerialNumber);
		}catch (Exception e) {
			logger.error("Error in the function captureDataForSerialNumber", e);
			e.printStackTrace();
		}
		finally {
			System.gc();
		}}


	private void captureDataForInvoiceNumber(){

		try {
			lstInvoiceNumber = new ArrayList<String>();
			List<String> lstOFInvoiceNumber = objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceNumber);

			for(String sCurrentValue: lstOFInvoiceNumber) {				
				if(!lstInvoiceNumber.contains(sCurrentValue) && sCurrentValue!=null)
					lstInvoiceNumber.add(sCurrentValue);

				if(lstInvoiceNumber.size()==5) break;
			}

		} catch (Exception e) {
			logger.error("Error in the function captureDataForInvoice", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}}

	private void captureDataForInvoiceDate(){

		try {
			lstInvoiceDate = new ArrayList<String>();
			List<String> lstOFInvoiceDate = objJsonUtil.getListofStringsFromJSONResponse(InvoiceSearchAPIConstants.xpathInvoiceDateRange);

			for(String sCurrentValue: lstOFInvoiceDate) {				
				if(!lstInvoiceDate.contains(sCurrentValue) && sCurrentValue!=null)
					lstInvoiceDate.add(sCurrentValue);

				if(lstInvoiceDate.size()==5) break;
			}

		} catch (Exception e) {
			logger.error("Error in the function captureDataForInvoiceDate", e);
			e.printStackTrace();
		}finally {
			System.gc();
		}
	}



	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil


	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	public List<String> getListOfInvoiceNumber() {
		return lstInvoiceNumber;
	}
	public List<String> getListOfResellerNumber() {
		return lstResellerNumber;
	}
	public List<String> getListOfCPN() {
		return  new ArrayList<>(setCPN);
	}
	public List<String> getListOfCustomerOrderNumber() {
		return lstCustomerOrderNumber;
	}
	public List<String> getListOfSerialNumber(){
		return new ArrayList<>(setSerialNumber);
	}
	public List<String> getListOfBID(){
		return new ArrayList<>(setBidNumber);
	}
	public List<String> getListOfIngramPartNumber() { 
		return new ArrayList<>(setIngramPartNumber);
	}
	public List<String> getListOfVendorPartNumber(){
		return new ArrayList<>(setVendorPartNumber);
	}
	public List<String> getListOfInvoiceFromDate(){
		return lstInvoiceDate;
	}
	public String getTotalProducts() throws Exception {
		return objJsonUtil.getActualValueFromJSONResponseWithoutModify(InvoiceSearchAPIConstants.xpathTotalHits);
	}


	public static void main(String args[]) throws Exception {
	}

}
