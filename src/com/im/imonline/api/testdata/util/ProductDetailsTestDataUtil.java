package com.im.imonline.api.testdata.util;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.common.collect.Multimap;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.ExecuteSQLscript;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.testdata.util.BaseTestDataUtil;
import com.im.imonline.api.business.APIEnumerations.FlagOptions_ProductDetails;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;
import com.jayway.jsonpath.JsonPath;

public class ProductDetailsTestDataUtil extends BaseTestDataUtil {

	Logger logger = Logger.getLogger("ProductDetailsTestDataUtil"); 
	private JsonUtil objJsonUtil = null;
	private EnumMap<FlagOptions_ProductDetails, String> mapSKUDataOfDifferentFlags;
	private Map<String, HashMap<String, String>> mapContentsOfProductDetailsData, mapClearanceFlagData, mapFreeItemData;
	private List<HashMap<String, String>> lstSourceResponseData;
	private String sJsonResponse = null;
	private Map<String, List<String>> mapComponentData = null;
	private static final String lstOfCountriesAvailableInPIM = "US|AT|FR|DE";
	private Map<String, String> mapSubMaterialData = null, mapSimilarProductsData = null, mapPromotionDataForSpecialPricing = null, mapPartOfBundleData = null;
	
	int iTotalProducts;
	ExecuteSQLscript objES;
	String sCountry;
	
	public ProductDetailsTestDataUtil(HashMap<String, String> mapReqData) {
		super(mapReqData);
		//mapReqDataForTemplate.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<BLANK>>");
		processForCountrySpecificData();
	}

	private void captureTotalProductsCount() throws Exception {
		try {
			lstSourceResponseData = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			iTotalProducts = objJsonUtil.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathTotalNoRecordsInTheResponse).size();
			sJsonResponse = objJsonUtil.getResponse().asString();
			sCountry = mapReqDataForTemplate.get(Constants.ExcelHeaderRunConfig);
		} catch (Exception e) {
			logger.error("Error in the function captureTotalProductsCount", e);
			e.printStackTrace();
		}
	}

	public HashMap<String, String> getRequestData() {
		return mapReqDataForTemplate;
	}

	/**
	 * This function helps to run the utilities to capture the data
	 */
	private void processForCountrySpecificData(){
		try {
			objJsonUtil = requestDataAsperCountryAndReplaceValueAsBlankForNotMentionedData(ProductSearchElasticAPITest.TestDataFile, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.sFileName);	
			if(null != objJsonUtil || objJsonUtil.getStatusCode() != 200) {
				captureTotalProductsCount();
				captureDataOfDifferentFlags();
			}
			else 
				throw new Exception("Null values in Json Response in the function processForCountrySpecificData");
			System.gc();	
		} catch (Exception e) {
			logger.error("Error in the function processForCountrySpecificData", e);
			e.printStackTrace();
		}
	}

	private void captureDataOfDifferentFlags() {
		boolean bStatus;
		String sSKUNumber = null, sTrue = "true", sFalse = "false";
		mapSKUDataOfDifferentFlags = new EnumMap<FlagOptions_ProductDetails, String>(FlagOptions_ProductDetails.class);
		mapContentsOfProductDetailsData = new HashMap<String, HashMap<String, String>>();
		try {			
			for(HashMap<String, String> response : lstSourceResponseData) {
				bStatus = false;
				sSKUNumber = response.get(ProductSearchElasticAPIConstants.MATERIAL);
				for(FlagOptions_ProductDetails flag : FlagOptions_ProductDetails.values()) {
					if(FlagOptions_ProductDetails.values().length == mapSKUDataOfDifferentFlags.size())
						break;
					if(!mapSKUDataOfDifferentFlags.containsKey(flag)) {
						switch(flag) {
						//TODO modify if condition with Jayway Json format...
						case ACCESSORIES:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES) && response.get(ProductSearchElasticAPIConstants.FLAG_ACCESSORIES).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case ACOP_PROMOTION:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPPROMOTION).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case ACOP_QUANTITY_BREAK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPQUANTITYBREAK).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case ACOP_WEB_ONLY_PRICE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPWEBONLYPRICE).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case ACOP_SPECIAL_PRICING:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING) && response.get(ProductSearchElasticAPIConstants.FLAG_ACOPSPECIALPRICING).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case AVAILABLE_IN_BUNDLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_COMPONENT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_COMPONENT) && response.get(ProductSearchElasticAPIConstants.FLAG_COMPONENT).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapPartOfBundleData = new HashMap<String, String>();
									mapPartOfBundleData.put(sSKUNumber, response.get(ProductSearchElasticAPIConstants.PARTOFBUNDLE));
								}
							}
							break;
						case BACK_ORDER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("Y")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case BLOW_OUT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BLOWOUT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BLOWOUT) && response.get(ProductSearchElasticAPIConstants.FLAG_BLOWOUT).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case BULK_FREIGHT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT) && response.get(ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case BUNDLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BUNDLE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE) && response.get(ProductSearchElasticAPIConstants.FLAG_BUNDLE).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry))
									getComponentDataForBundleSKU(sSKUNumber);
							}
							break;
						case CLEARANCE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_CLEARANCE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_CLEARANCE) && response.get(ProductSearchElasticAPIConstants.FLAG_CLEARANCE).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapClearanceFlagData = new HashMap<String, HashMap<String, String>>();
									HashMap<String, String> mapObj = new HashMap<String, String>();
									for(String item : ProductSearchElasticAPIConstants.attributeArrForClearanceFlag)
										mapObj.put(item, response.get(item));
									mapClearanceFlagData.put(sSKUNumber, mapObj);
								}
							}
							break;
						case DIRECTSHIP:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && null != response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case DISCONTINUED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) && null != response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED) && response.get(ProductSearchElasticAPIConstants.FLAG_DISCONTINUED).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case DOWNLOADABLE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ESD) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ESD) && response.get(ProductSearchElasticAPIConstants.FLAG_ESD).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case END_USER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER) && response.get(ProductSearchElasticAPIConstants.FLAG_ENDUSER).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case FREE_ITEM:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_FREEITEM) && null != response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM) && response.get(ProductSearchElasticAPIConstants.FLAG_FREEITEM).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapFreeItemData = new HashMap<String, HashMap<String, String>>();
									HashMap<String, String> mapObj = new HashMap<String, String>();
									for(String freeItem : ProductSearchElasticAPIConstants.attributeArrForFreeItem)
										mapObj.put(freeItem, response.get(freeItem));
									mapFreeItemData.put(sSKUNumber, mapObj);
								}
							}
							break;
						case HEAVY_WEIGHT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) && null != response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT) && response.get(ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case INSTOCK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case INSTOCK_OR_ORDER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case LICENCE_PRODUCT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS) && response.get(ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case LTL:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_LTL) && null != response.get(ProductSearchElasticAPIConstants.FLAG_LTL) && response.get(ProductSearchElasticAPIConstants.FLAG_LTL).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case NEW:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_NEW) && null != response.get(ProductSearchElasticAPIConstants.FLAG_NEW) && response.get(ProductSearchElasticAPIConstants.FLAG_NEW).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case NO_RETURNS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_NORETURNS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS) && response.get(ProductSearchElasticAPIConstants.FLAG_NORETURNS).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case NOT_AUTHORIZED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_NOTAUTHORIZED) && null != response.get(ProductSearchElasticAPIConstants.FLAG_NOTAUTHORIZED) && response.get(ProductSearchElasticAPIConstants.FLAG_NOTAUTHORIZED).equals(sFalse)) {	
								bStatus = true;						
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case NOT_ORDERABLE_ONLINE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER) && response.get(ProductSearchElasticAPIConstants.FLAG_BACKORDER).equals("N") && response.containsKey(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).equals(sFalse) && response.containsKey(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP) && response.get(ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case OUT_OF_STOCK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK) && response.get(ProductSearchElasticAPIConstants.FLAG_INSTOCK).equals(sFalse)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case PROMOTION:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) && null != response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2) && response.get(ProductSearchElasticAPIConstants.FLAG_PROMOTION2).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case QUANTITY_BREAK:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK) && null != response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK) && response.get(ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case REFURBISHED:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) && null != response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED) && response.get(ProductSearchElasticAPIConstants.FLAG_REFURBISHED).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case REPLACEMENT_PRODUCT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).equals("1")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapSubMaterialData = new HashMap<String, String>();
									mapSubMaterialData.put(sSKUNumber, response.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL));
								}
							}
							break;
						case RETURN_LIMITATIONS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS) && response.get(ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case SHIP_FROM_PARTNER:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER) && response.get(ProductSearchElasticAPIConstants.FLAG_SHIPFROMPARTNER).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case SIMILAR_PRODUCTS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapSimilarProductsData = new HashMap<String, String>();
									mapSimilarProductsData.put(sSKUNumber, response.get(ProductSearchElasticAPIConstants.FLAG_SIMILARPRODUCTS));
								}
							}
							break;
						case SPECIAL_BIDS:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS) && null != response.get(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS) && response.get(ProductSearchElasticAPIConstants.FLAG_VENDORSPCLBIDS).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case SPECIAL_PRICING:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapPromotionDataForSpecialPricing = new HashMap<String, String>();
									mapPromotionDataForSpecialPricing.put(sSKUNumber, response.get(ProductSearchElasticAPIConstants.FLAG_SPECIALPRICING));
								}
							}
							break;
						case SUGGESTED_PRODUCT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE) && response.get(ProductSearchElasticAPIConstants.FLAG_SUBRULE).equals("2")) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber); 
								if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
									mapSubMaterialData = new HashMap<String, String>();
									mapSubMaterialData.put(sSKUNumber, response.get(ProductSearchElasticAPIConstants.FLAG_SUBMATERIAL));
								}
							}
							break;
						case WARRANTIES:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WARRANTIES) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTIES) && response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTIES).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case WARRANTY_PRODUCT:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WARRANTY) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY) && response.get(ProductSearchElasticAPIConstants.FLAG_WARRANTY).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						case WEB_ONLY_PRICE:
							if(response.containsKey(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) && null != response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE) && response.get(ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE).equals(sTrue)) {
								bStatus = true;
								mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
							}
							break;
						default:
							break;
						} //end of switch.	
					}
				} //end of inner for loop.
				if(bStatus && !lstOfCountriesAvailableInPIM.contains(sCountry))
					captureBasicContentsOfProductDetails(sSKUNumber, response);
			} //end of outer for loop.

			retryToCaptureDataIfNotRetrivedInFirstCall();

			System.out.println(mapSKUDataOfDifferentFlags);
			//System.out.println(mapContentsOfProductDetailsData);
			System.out.println("Successfully captured data for "+mapSKUDataOfDifferentFlags.size()+" products");

		} catch (Exception e) {
			logger.error("Error in the function captureSKUDataWithDifferentFlags()", e);
			e.printStackTrace();
		} 
	}

	private void retryToCaptureDataIfNotRetrivedInFirstCall() {
		String sFlagKeyInResponse = null, sSKUNumber = null;
		HashMap<String, String> mapResponseSourceData = null;
		try {
			HashMap<Object, Object> hmAggregationsInfo = objJsonUtil.getMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllAggregations);

			for(FlagOptions_ProductDetails flag : FlagOptions_ProductDetails.values()) {
				if(mapSKUDataOfDifferentFlags.get(flag)==null) {
					sFlagKeyInResponse = null;
					switch(flag) {
					case AVAILABLE_IN_BUNDLE:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.COMPONENTPRODUCTS))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_COMPONENT;
						break;
					case BULK_FREIGHT:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_BULKFREIGHT;
						break;
					case BUNDLE:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.BUNDLEAVAILABLE))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_BUNDLE;
						break;
					case CLEARANCE:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_CLEARANCE))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_CLEARANCE;
						break;
					case DIRECTSHIP:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.DIRECTSHIP))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_DIRECTSHIP;
						break;
					case DISCONTINUED:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.DISCONTINUED))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_DISCONTINUED;
						break;
					case DOWNLOADABLE:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.DOWNLOAD))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_ESD;
						break;
					case END_USER:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_ENDUSER))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_ENDUSER;
						break;
					case FREE_ITEM:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.SHIPALONG))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_FREEITEM;
						break;
					case HEAVY_WEIGHT:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.HEAVYWEIGHT))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_HEAVYWEIGHT;
						break;
					case INSTOCK:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.INSTOCK))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_INSTOCK;
						break;
					case INSTOCK_OR_ORDER:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_INSTOCKORORDER;
						break;
					case LICENCE_PRODUCT:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_LICENCEPRODUCTS;
						break;
					case LTL:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_LTL))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_LTL;					
						break;	
					case NEW:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_NEW))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_NEW;					
						break;
					case NO_RETURNS:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_NORETURNS;					
						break;
					case PROMOTION:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.AGGREGATION_PROMOTION))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_PROMOTION2;					
						break;
					case QUANTITY_BREAK:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.AGGREGATION_QTYBREAK))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_QUANTITYBREAK;					
						break;
					case REFURBISHED:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.REFURBISHED))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_REFURBISHED;					
						break;
					case RETURN_LIMITATIONS:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_RETURNLIMITATIONS;					
						break;
					case WARRANTY_PRODUCT:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.WARRANTYPRODUCTS))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_WARRANTY;					
						break;
					case WEB_ONLY_PRICE:
						if(isDocCountInAggregationGreaterThanZeroForGivenFlag(hmAggregationsInfo, ProductSearchElasticAPIConstants.WEBONLYPRICE))
							sFlagKeyInResponse = ProductSearchElasticAPIConstants.FLAG_WEBONLYPRICE;
						break;
					default:
						break;
					}
					if(sFlagKeyInResponse != null) {
						mapResponseSourceData = rehitRequestForGivenFlagIfNotRetrivedInFirstCallAndStoreResponse(sFlagKeyInResponse);						
						sSKUNumber = mapResponseSourceData.get(ProductSearchElasticAPIConstants.MATERIAL);
						mapSKUDataOfDifferentFlags.put(flag, sSKUNumber);
						switch(flag) {
						case AVAILABLE_IN_BUNDLE:
							if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
								mapPartOfBundleData = new HashMap<String, String>();
								mapPartOfBundleData.put(sSKUNumber, mapResponseSourceData.get(ProductSearchElasticAPIConstants.PARTOFBUNDLE));
							}
							break;
						case BUNDLE:
							if(lstOfCountriesAvailableInPIM.contains(sCountry))
								getComponentDataForBundleSKU(sSKUNumber);
							break;
						case CLEARANCE:
							if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
								mapClearanceFlagData = new HashMap<String, HashMap<String, String>>();
								HashMap<String, String> mapObj = new HashMap<String, String>();
								for(String item : ProductSearchElasticAPIConstants.attributeArrForClearanceFlag)
									mapObj.put(item, mapResponseSourceData.get(item));
								mapClearanceFlagData.put(sSKUNumber, mapObj);
							}
							break;
						case FREE_ITEM:
							if(lstOfCountriesAvailableInPIM.contains(sCountry)) {
								mapFreeItemData = new HashMap<String, HashMap<String, String>>();
								HashMap<String, String> mapObj = new HashMap<String, String>();
								for(String item : ProductSearchElasticAPIConstants.attributeArrForFreeItem)
									mapObj.put(item, mapResponseSourceData.get(item));
								mapFreeItemData.put(sSKUNumber, mapObj);
							}
							break;
						default:
							break;
						}
						if(!mapContentsOfProductDetailsData.containsKey(sSKUNumber) && !lstOfCountriesAvailableInPIM.contains(sCountry))
							captureBasicContentsOfProductDetails(sSKUNumber, mapResponseSourceData);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in the function retryToCaptureDataIfNotRetrivedInFirstCall()", e);
			e.printStackTrace();
		}
	}

	private void captureBasicContentsOfProductDetails(String sSKUNumber, HashMap<String, String> mapResponseData) {
		Object responseData = null;
		HashMap<String, String> mapContentsOfProductDetails = new HashMap<String, String>();
		try {
			for(String sBasicData : ProductSearchElasticAPIConstants.attributeArrForProductDetails) {
				responseData = mapResponseData.get(sBasicData);
				if(mapResponseData.containsKey(sBasicData) && null != responseData)					
					mapContentsOfProductDetails.put(sBasicData, responseData.toString());
			}
			mapContentsOfProductDetailsData.put(sSKUNumber, mapContentsOfProductDetails);
		} catch(Exception e) {
			logger.error("Error in the function captureBasicContentsOfProductDetailsAPI()", e);
			e.printStackTrace();
		}
	}

	private boolean isDocCountInAggregationGreaterThanZeroForGivenFlag(HashMap<Object, Object> hmAllAggregations, String sFlagValueInAggregation) {
		String doccounttext = null;
		try {
			doccounttext = hmAllAggregations.get(sFlagValueInAggregation) != null ? hmAllAggregations.get(sFlagValueInAggregation).toString():"0";
			if(!doccounttext.equals("0"))
				doccounttext = doccounttext.indexOf(",")==-1?doccounttext.substring(doccounttext.indexOf("=")+1,doccounttext.indexOf("}")) : doccounttext.substring(doccounttext.indexOf("=")+1,doccounttext.indexOf(","));
		} catch (Exception e) {
			logger.error("Error in the function isDocCountInAggregationGreaterThanZeroForGivenFlag()", e);
			e.printStackTrace();
		} 
		return Integer.parseInt(doccounttext)>0;
	}

	private HashMap<String, String> rehitRequestForGivenFlagIfNotRetrivedInFirstCallAndStoreResponse(String productStatus){
		JsonUtil objJsonUtilFlag=null;
		HashMap<String, String> mapResponseData = null;
		List<HashMap<String, String>> lstSourcesResponseData = null;
		try {
			HashMap<String, String> hmForSpecificFlag = AppUtil.getMapWithBlankDataWhichIsNotPresentInTestDataExcel(ProductSearchElasticAPITest.sFileName,mapReqDataForTemplate,ProductSearchElasticAPITest.isMultiSet);
			hmForSpecificFlag.put(ProductSearchElasticAPIConstants.EXCELHEADER_PRODUCTSTATUS, productStatus);
			hmForSpecificFlag.put(ProductSearchElasticAPIConstants.EXCELHEADER_KEYWORD, "<<Blank>>");
			String sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSearchElasticAPITest.sFileName, hmForSpecificFlag, ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.TestDataFile);
			objJsonUtilFlag= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"",mapReqDataForTemplate);
			objJsonUtilFlag.postRequest();
			lstSourcesResponseData = objJsonUtilFlag.getListofHashMapFromJSONResponse(ProductSearchElasticAPIConstants.xpathAllSources);
			sJsonResponse = objJsonUtilFlag.getResponse().asString();
			if(lstSourcesResponseData.size()>0)
				if(lstSourcesResponseData.get(0).get(productStatus).equals("true") || lstSourcesResponseData.get(0).get(productStatus).equals("Y"))
					mapResponseData = lstSourcesResponseData.get(0);
		} catch (Exception e) {
			logger.error("Error in the function captureDataForFlagsIfNotRetrievedInFirstCall", e);
			e.printStackTrace();
		}
		return mapResponseData;		
	}
		
	private Map<String, List<String>> getComponentDataForBundleSKU(String pSKU) {
		mapComponentData = new HashMap<String, List<String>>();
		try {
			String xpathRule = "$..hits[*]._source[?(@.material=='"+pSKU+"')].kits[*].component";
			mapComponentData.put(pSKU, JsonPath.read(sJsonResponse, xpathRule));
		} catch(Exception e) {
			logger.error("Error in the function getComponentDataForBundleSKU", e);
			e.printStackTrace();
		}
		return mapComponentData;
	}

	public  Multimap<String, String> getTechSpecData(String pSKU, ExecuteSQLscript objES) {
		Multimap<String, String> mapOfTechSpecData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sTechSpecQuery = "SELECT  ATTRIBUTENAME, ATTRIBUTEVALUE FROM UDRP.ATTRIBUTE_"+sCountryCode+" WHERE MATERIAL = '"+pSKU+"'";	
			mapOfTechSpecData = objES.executrScriptToCaptureMultimapData(sTechSpecQuery);
			
		} catch (Exception e) {
			logger.error("Error in the function getTechSpecData", e);
			e.printStackTrace();
		}
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return mapOfTechSpecData;
	}

	public List<Map<String, String>> getImageData(String pSKU, ExecuteSQLscript objES) {		
		List<Map<String, String>> lstOfImageData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sTechSpecQuery = "SELECT imagelow, imagemid, imagehigh, description FROM UDRP.gallery WHERE MATERIAL = '"+pSKU+"' AND IMAGETYPE = 1 AND COUNTRY = '"+sCountryCode+"'";
			lstOfImageData = objES.executrScript(sTechSpecQuery);
			
		} catch (Exception e) {
			logger.error("Error in the function getImageData", e);
			e.printStackTrace();
		}
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return lstOfImageData;
	}

	public List<Map<String, String>> getRichMediaData(String pSKU, ExecuteSQLscript objES) {		
		List<Map<String, String>> lstOfRichMediaData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sTechSpecQuery = "SELECT imagelow, imagemid, imagehigh, description FROM UDRP.gallery WHERE MATERIAL = '"+pSKU+"' AND IMAGETYPE = 2 AND COUNTRY = '"+sCountryCode+"'";
			lstOfRichMediaData = objES.executrScript(sTechSpecQuery);
			
		}catch (Exception e) {
			logger.error("Error in the function getRichMediaData", e);
			e.printStackTrace();
		} 
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return lstOfRichMediaData;
	}

	public Map<String, String> getMarketingInformation(String pSKU, ExecuteSQLscript objES) {		
		Map<String, String> mapOfMarketInfoData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sTechSpecQuery = "SELECT ATTRIBUTENAME, ATTRIBUTEVALUE FROM UDRP.MARKETING_ATTRIBUTE WHERE MATERIAL = '"+pSKU+"' AND COUNTRY = '"+sCountryCode+"'";
			mapOfMarketInfoData = objES.executrScriptToCaptureTwoColumnPairData(sTechSpecQuery);
			
		} catch (Exception e) {
			logger.error("Error in the function getMarketingInformation", e);
			e.printStackTrace();
		}
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return mapOfMarketInfoData;
	}
	
	public Map<String, Set<String>> getAccessoriesSKU(String pSKU, String sServiceType, ExecuteSQLscript objES) {		
		Map<String, Set<String>> mapOfAccessoriesData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sAccessoriesQuery = "SELECT childmaterial FROM MATERIALS B "
					+ "join UDRP.CROSSSELL_"+sCountryCode+" A on (A.MATERIAL = B.MATERIAL ) "
					+ "WHERE B.WEBVISIBLEFLAG='WebVisibleFlag' and A.SERVICETYPE='"+sServiceType+"' AND B.MATERIAL = '"+pSKU+"'";
			mapOfAccessoriesData = objES.executeScriptToCaptureSetOfData(sAccessoriesQuery);
			
		} catch (Exception e) {
			logger.error("Error in the function getAccessoriesSKU", e);
			e.printStackTrace();
		}
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return mapOfAccessoriesData;
	}
	
	public Map<String, Set<String>> getWarrantiesSKU(String pSKU, ExecuteSQLscript objES) {
		Map<String, Set<String>> mapOfWarrantiesData = null;
		String sCountryCode = Util.getCountryCodeForDatabase(sCountry);
		try {
//			objES = new ExecuteSQLscript(ConnectToDatabase.HERMES, "GDWS");
			
			String sWarrantiesQuery = "SELECT childmaterial FROM MATERIALS B "
					+ "join UDRP.CROSSSELL_"+sCountryCode+" A on (A.MATERIAL = B.MATERIAL ) "
					+ "WHERE B.WEBVISIBLEFLAG='WebVisibleFlag' and A.SERVICETYPE='C' AND B.MATERIAL = '"+pSKU+"'";;
			mapOfWarrantiesData = objES.executeScriptToCaptureSetOfData(sWarrantiesQuery);
			
		} catch (Exception e) {
			logger.error("Error in the function getWarrantiesSKU", e);
			e.printStackTrace();
		}
//		finally {
//			objES.getDatabaseConnect().closeConnection();
//		}
		return mapOfWarrantiesData;
	}
	// Getter Section: Here will have all public methods to fetch the data from TestDataUtil

	public EnumMap<FlagOptions_ProductDetails, String> getSKUDataOfDifferentFlags() {
		return mapSKUDataOfDifferentFlags;
	}

	public Map<String, HashMap<String, String>> getBasicContentsProductDetailsData() {
		return mapContentsOfProductDetailsData;
	}
	
	public Map<String, List<String>> getComponentDataForBundleSKU() {
		return mapComponentData;
	}
	
	public Map<String, String> getSubMaterialData() {
		return mapSubMaterialData;
	}
	
	public Map<String, HashMap<String, String>> getFreeItemData() {
		return mapFreeItemData;
	}
	
	public Map<String, HashMap<String, String>> getClearanceFlagData() {
		return mapClearanceFlagData;
	}
	
	public Map<String, String> getPartOfBundleSKUForComponentSKU() {
		return mapPartOfBundleData;
	}
	
	public Map<String, String> getSimilarProductsData() {
		return mapSimilarProductsData;
	}
	
	public Map<String, String> getPromoEndDateforSpecialPricingSKU() {
		return mapPromotionDataForSpecialPricing;
	}
}
