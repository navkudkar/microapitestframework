package com.im.imonline.api.cisco.validation;

public class AcquireEstimateAPIExpectedData 
{
	
	String sNationCode;

	public AcquireEstimateAPIExpectedData(String sCountryCode) {
		this.sNationCode =sCountryCode;
	}
	public String PriceListCountryCode() 
	{
		String sDefault="GLGB";
		switch(sNationCode) 
		{
		case "US" :	
			return "GLGB" ;
		case "CA" :
			return "GLCA";
		default : return sDefault;
		}
	}
	
	public String CurrencyCode() 
	{
		String sDefault="GBP";
		switch(sNationCode) 
		{
		case "US" :	
			return "GBP" ;
		case "CA" :
			return "CAD";
		default : return sDefault;
		}
	}
	
	public String PriceListDescription() 
	{
		String sDefault="Global United Kingdom Price List in Pounds Sterling";
		switch(sNationCode) 
		{
		case "US" :	
			return "Global United Kingdom Price List in Pounds Sterling" ;
		case "CA" :
			return "Global Canada Price List in CANADIAN Dollars";
		default : return sDefault;
		}
	}
	
	
}
