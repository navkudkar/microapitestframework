package com.im.imonline.api.cisco.validation;

import java.util.HashMap;

import com.im.api.core.common.Constants;
public class ListEstimateAPIExpectedData 
{
	
	String sNationCode;
	public ListEstimateAPIExpectedData(String sCountryCode) {
		this.sNationCode =sCountryCode;
	}
	public String OwnerName(HashMap<String, String> configHMap) 
	{
		String sDefault="";
		switch(sNationCode) 
		{
		case "US" :	
			
			String sGetURLUS = configHMap.get(Constants.EXCELHEADER_TokenReqBody);
			String sOwnerNameUS = sGetURLUS.substring(sGetURLUS.indexOf("username=")+9, sGetURLUS.indexOf("&password="));
			if(sOwnerNameUS.contains("@"))
			sOwnerNameUS = sOwnerNameUS.substring(0, sOwnerNameUS.indexOf("@"));
			return sOwnerNameUS;
	
		case "CA":
			
			String sGetURLCA = configHMap.get(Constants.EXCELHEADER_TokenReqBody);
			String sOwnerNameCA = sGetURLCA.substring(sGetURLCA.indexOf("username=")+9, sGetURLCA.indexOf("&password="));
			if(sOwnerNameCA.contains("@"))
			sOwnerNameCA = sOwnerNameCA.substring(0, sOwnerNameCA.indexOf("@"));
			return sOwnerNameCA;
		
		default : return sDefault;
		}
	}
	
	public String EstimateName() 
	{
		String sDefault="TEST-2470967-2002";
		switch(sNationCode) 
		{
		case "US" :	
			return "TEST-2470967-2002" ;
		case "CA" :
			return "Example 3";
		default : return sDefault;
		}
	}
	
	public String EstimateID() 
	{
		String sDefault="RD112311884BE";
		switch(sNationCode) 
		{
		case "US" :	
			return "RD112311884BE" ;
		case "CA" :
			return "UI94387500EW";
		default : return sDefault;
		}
	}
	
	public String CountryCode() 
	{
		String sDefault="GLUS";
		switch(sNationCode) 
		{
		case "US" :	
			return "GLUS" ;
		case "CA" :
			return "GLCA";
		default : return sDefault;
		}
	}
	
}
