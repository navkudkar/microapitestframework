package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.RenewalCCWDownloadInvoiceServiceAPIConstants;
import com.im.imonline.api.tests.RenewalCCWDownloadInvoiceServiceAPITest;

public class RenewalCCWDownloadInvoiceServiceAPIActionBucket {

	Logger logger = Logger.getLogger("RenewalICCWServicesAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public RenewalCCWDownloadInvoiceServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void validateRenewalCCWInvoiceServicesResponse() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWDownloadInvoiceServiceAPITest.sFileName, hmTestData, RenewalCCWDownloadInvoiceServiceAPITest.isMultiSet, RenewalCCWDownloadInvoiceServiceAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpQuoteName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathQuotecreated);
			String sExpectedExpQuoteName= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPQUOTECREATEDNAME);
			objAPIAssertion.assertEquals(sActualExpQuoteName, sExpectedExpQuoteName, " Verify  Quote Created Name ");

			String sActualExpInvoiceTotoalAmount = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathInvoiceTotoalAmount);
			String sExpectedExpInvoiceTotoalAmount= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPINVOICETOTALAMOUNT);
			objAPIAssertion.assertEquals(sActualExpInvoiceTotoalAmount, sExpectedExpInvoiceTotoalAmount, " Verify invoice Total Amount ");

			String sActualExpQuoteCreateDate = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathquoteCreationDate);
			String sExpectedExpQuoteCreateDate= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPQUOTECREATIONDATE);
			objAPIAssertion.assertEquals(sActualExpQuoteCreateDate, sExpectedExpQuoteCreateDate, " Verify Quote create Date ");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathstatus);
			String sExpectedExpStatus= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify Status ");

		}catch(Exception e) {
			String sErrMessage="FAIL: validateRenewalCCWInvoiceServicesResponse() method of RenewalCCWDownloadInvoiceServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWDownloadInvoiceServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });


		}

	}
	public void validateinvalidQuoteId() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWDownloadInvoiceServiceAPITest.sFileName, hmTestData, RenewalCCWDownloadInvoiceServiceAPITest.isMultiSet, RenewalCCWDownloadInvoiceServiceAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);


			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");


			String sActualExpInvalidQuoteidStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathInvaldidQuoteIdStatus);
			String sExpectedExpInvalidQuoteidStatus = hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPINVALIDSTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidQuoteidStatus, sExpectedExpInvalidQuoteidStatus, " verify for Status ");

			String sActualExpInvalidResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathInvaldidResponseStatus);
			String sExpectedExpInvalidResponseStatus = hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPINVALIDRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidResponseStatus, sExpectedExpInvalidResponseStatus, " verify for Response Status ");

			String sActualExpInvalidResponseMessageStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathInvaldidResponseMessageStatus);
			String sExpectedExpInvalidResponseMessageStatus = hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPINVALIDRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidResponseMessageStatus, sExpectedExpInvalidResponseMessageStatus, " verify for ResponseMessage Status ");

		}catch(Exception e) {
			String sErrMessage="FAIL: validateinvalidQuoteId() method of RenewalCCWDownloadInvoiceServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWDownloadInvoiceServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void validateinvoiceLines() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWDownloadInvoiceServiceAPITest.sFileName, hmTestData, RenewalCCWDownloadInvoiceServiceAPITest.isMultiSet, RenewalCCWDownloadInvoiceServiceAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);

			CommonValidation cValidation = new CommonValidation(objAPIAssertion);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathstatus);
			String sExpectedExpStatus= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify Status ");

			String sActualExpTotalLines = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewalCCWDownloadInvoiceServiceAPIConstants.xpathTotalLines);
			String sExpectedTotalLines= hmTestData.get(RenewalCCWDownloadInvoiceServiceAPIConstants.EXPTOTALLINES);
			objAPIAssertion.assertEquals(sActualExpTotalLines, sExpectedTotalLines, " Verify Total Lines ");


			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, RenewalCCWDownloadInvoiceServiceAPITest.TestDataFile);

			for (int i = 0; i<multisetDataDrivenMap.size(); i++) {


				List<HashMap<String, String>> lstOfMapData_QBDetails = objJsonUtil.getListofHashMapFromJSONResponse("invoiceLines");


				cValidation.validateSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapData_QBDetails,new String[]{"serviceLevelDescription", "totalListPrice", "quantity","serviceOrderingSKU","serviceLevel","unitPrice","invoiceDate"}, "QuoteId");
				System.out.println(multisetDataDrivenMap.get(i));
			}


		}catch(Exception e) {
			String sErrMessage="FAIL: validateinvoiceLines() method of RenewalCCWDownloadInvoiceServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWDownloadInvoiceServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}
}




