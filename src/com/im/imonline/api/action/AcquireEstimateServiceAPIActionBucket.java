package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.cisco.validation.AcquireEstimateAPIExpectedData;
import com.im.imonline.api.constants.AcquireEstimateServicesAPIConstants;
import com.im.imonline.api.tests.AcquireEstimateServicesAPITest;

public class AcquireEstimateServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("AcquireEstimateServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	

	
	public AcquireEstimateServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		
	}
	
	public void ValidateAcquireEstimateWithManditoryFields() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(AcquireEstimateServicesAPITest.sFileName, hmTestData, AcquireEstimateServicesAPITest.isMultiSet, AcquireEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sCountryCode = hmConfig.get(Constants.ExcelHeaderRunConfig);
				AcquireEstimateAPIExpectedData ExpectedData = new AcquireEstimateAPIExpectedData(sCountryCode);
			
				List<String> sActualPriceListCountrycode= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathPriceListCountryCode);					
				String sExpectedPriceListCountrycode  = ExpectedData.PriceListCountryCode();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualPriceListCountrycode, sExpectedPriceListCountrycode);
				objAPIAssertion.assertTrue(allMatch, "Validation For Price List Country code["+sExpectedPriceListCountrycode+"]");
			
				allMatch=false;
				List<String> sActualPriceListDescription= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathPriceListDescription);					
				String sExpectedPriceListDescription = ExpectedData.PriceListDescription();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualPriceListDescription, sExpectedPriceListDescription);
				objAPIAssertion.assertTrue(allMatch, "Validation For Price List Description["+sExpectedPriceListDescription+"]");
					
				allMatch=false;
				List<String> sActualNameEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathNameEstimate);					
				String sExpectedNameEstimate = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATENAME);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualNameEstimate, sExpectedNameEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Name["+sExpectedNameEstimate+"]");
					
				allMatch=false;
				List<String> sActualEstimateID= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathIDEstimate);					
				String sExpectedEstimateID = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATEID);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateID, sExpectedEstimateID);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate ID["+sExpectedEstimateID+"]");
				
				allMatch=false;
				List<String> sActualStatusEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathStatusEstimate);					
				String sExpectedStatusEstimate = AcquireEstimateServicesAPIConstants.Expected_Valid_Status;
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusEstimate, sExpectedStatusEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusEstimate+"]");
			
				allMatch=false;
				List<String> sActualEndUserName= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathEndUserName);					
				String sExpectedEndUserName = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPENDUSERNAME);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEndUserName, sExpectedEndUserName);
				objAPIAssertion.assertTrue(allMatch, "Validation For End User Name["+sExpectedEndUserName+"]");
			
		
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateAcquireEstimateWithManditoryFields() method of AcquireEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void ValidateLineItemWithOutTerm() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(AcquireEstimateServicesAPITest.sFileName, hmTestData, AcquireEstimateServicesAPITest.isMultiSet, AcquireEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				String sCountryCode = hmConfig.get(Constants.ExcelHeaderRunConfig);
				AcquireEstimateAPIExpectedData ExpectedData = new AcquireEstimateAPIExpectedData(sCountryCode);
			
				allMatch=false;
				List<String> sActualNameEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathNameEstimate);					
				String sExpectedNameEstimate = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATENAME);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualNameEstimate, sExpectedNameEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Name["+sExpectedNameEstimate+"]");
					
				allMatch=false;
				List<String> sActualEstimateID= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathIDEstimate);					
				String sExpectedEstimateID = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATEID);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateID, sExpectedEstimateID);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate ID["+sExpectedEstimateID+"]");
				
				allMatch=false;
				List<String> sActualStatusEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathStatusEstimate);					
				String sExpectedStatusEstimate =AcquireEstimateServicesAPIConstants.Expected_Valid_Status;
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusEstimate, sExpectedStatusEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusEstimate+"]");
			
				
				//For Line Item Details
				
				allMatch=false;
				List<String> sActualCCWLineNumber= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathCCWLineNumber);					
				String sExpectedCCWLineNumber = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPCCWLINENUMBER);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualCCWLineNumber, sExpectedCCWLineNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation For CCW Line Number["+sExpectedCCWLineNumber+"]");
			
				allMatch=false;
				List<String> sActualLineItem= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathLineItem);					
				String sExpectedLineItem = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLINEITEM);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualLineItem, sExpectedLineItem);
				objAPIAssertion.assertTrue(allMatch, "Validation For Line Item["+sExpectedLineItem+"]");
			
				allMatch=false;
				List<String> sActualLineItemDescription= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathLineItemDescription);					
				String sExpectedLineItemDescription = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLINEITEMDESCRIPTION);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualLineItemDescription, sExpectedLineItemDescription);
				objAPIAssertion.assertTrue(allMatch, "Validation For Line Item Description["+sExpectedLineItemDescription+"]");
			
				allMatch=false;
				List<String> sActualLineItemQuantity= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathLineItemQuantity);					
				String sExpectedLineItemQuantity = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLINEITEMQUANTITY);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualLineItemQuantity, sExpectedLineItemQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation For Line Item Quantity["+sExpectedLineItemQuantity+"]");
			
				allMatch=false;
				List<String> sActualListPrice= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathListPrice);					
				String sExpectedListPrice = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLISTPRICE);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualListPrice, sExpectedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation For List Price["+sExpectedListPrice+"]");
			
				allMatch=false;
				List<String> sActualCurrencyCode= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathCurrencyCode);					
				String sExpectedCurrencyCode  = ExpectedData.CurrencyCode();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualCurrencyCode, sExpectedCurrencyCode);
				objAPIAssertion.assertTrue(allMatch, "Validation For Currency Code["+sExpectedCurrencyCode+"]");
			
				allMatch=false;
				List<String> sActualExtendedListPrice= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathExtendedListPrice);					
				String sExpectedExtendedListPrice = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPEXTENDEDLISTPRICE);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualExtendedListPrice, sExpectedExtendedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation For Extended List Price["+sExpectedExtendedListPrice+"]");
			
				allMatch=false;
				List<String> sActualTotalAmount= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathTotalAmount);					
				String sExpectedTotalAmount = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPTOTALAMOUNT);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualTotalAmount, sExpectedTotalAmount);
				objAPIAssertion.assertTrue(allMatch, "Validation For Total Amount["+sExpectedTotalAmount+"]");
			
				allMatch=false;
				List<String> sActualDiscount= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathDiscount);					
				String sExpectedDiscount = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPDISCOUNT);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualDiscount, sExpectedDiscount);
				objAPIAssertion.assertTrue(allMatch, "Validation For Discount["+sExpectedDiscount+"]");
	
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateLineItemWithOutTerm() method of AcquireEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	

	public void ValidateLineItemWithTerm() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(AcquireEstimateServicesAPITest.sFileName, hmTestData, AcquireEstimateServicesAPITest.isMultiSet, AcquireEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				String sCountryCode = hmConfig.get(Constants.ExcelHeaderRunConfig);
				AcquireEstimateAPIExpectedData ExpectedData = new AcquireEstimateAPIExpectedData(sCountryCode);
			
				allMatch=false;
				List<String> sActualNameEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathNameEstimate);					
				String sExpectedNameEstimate = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATENAME);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualNameEstimate, sExpectedNameEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Name["+sExpectedNameEstimate+"]");
					
				allMatch=false;
				List<String> sActualEstimateID= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathIDEstimate);					
				String sExpectedEstimateID = hmTestData.get(AcquireEstimateServicesAPIConstants.ESTIMATEID);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateID, sExpectedEstimateID);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate ID["+sExpectedEstimateID+"]");
				
				allMatch=false;
				List<String> sActualStatusEstimate= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathStatusEstimate);					
				String sExpectedStatusEstimate = AcquireEstimateServicesAPIConstants.Expected_Valid_Status;
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusEstimate, sExpectedStatusEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusEstimate+"]");
			
				
				//For Line Item Details
				
				allMatch=false;
				List<String> sActualCCWLineNumber= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathCCWLineNumber);					
				String sExpectedCCWLineNumber = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPCCWLINENUMBER);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualCCWLineNumber, sExpectedCCWLineNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation For CCW Line Number["+sExpectedCCWLineNumber+"]");
			
				allMatch=false;
				List<String> sActualLineItem= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathLineItem);					
				String sExpectedLineItem = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLINEITEM);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualLineItem, sExpectedLineItem);
				objAPIAssertion.assertTrue(allMatch, "Validation For Line Item["+sExpectedLineItem+"]");
			
				allMatch=false;
				List<String> sActualLineItemQuantity= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathLineItemQuantity);					
				String sExpectedLineItemQuantity = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLINEITEMQUANTITY);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualLineItemQuantity, sExpectedLineItemQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation For Line Item Quantity["+sExpectedLineItemQuantity+"]");
			
				allMatch=false;
				List<String> sActualListPrice= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathListPrice);					
				String sExpectedListPrice = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPLISTPRICE);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualListPrice, sExpectedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation For List Price["+sExpectedListPrice+"]");
			
				allMatch=false;
				List<String> sActualCurrencyCode= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathCurrencyCode);					
				String sExpectedCurrencyCode  = ExpectedData.CurrencyCode();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualCurrencyCode, sExpectedCurrencyCode);
				objAPIAssertion.assertTrue(allMatch, "Validation For Currency Code["+sExpectedCurrencyCode+"]");
			
				allMatch=false;
				List<String> sActualExtendedListPrice= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathExtendedListPrice);					
				String sExpectedExtendedListPrice = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPEXTENDEDLISTPRICE);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualExtendedListPrice, sExpectedExtendedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation For Extended List Price["+sExpectedExtendedListPrice+"]");
			
				allMatch=false;
				List<String> sActualTotalAmount= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathTotalAmount);					
				String sExpectedTotalAmount = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPTOTALAMOUNT);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualTotalAmount, sExpectedTotalAmount);
				objAPIAssertion.assertTrue(allMatch, "Validation For Total Amount["+sExpectedTotalAmount+"]");
			
				allMatch=false;
				List<String> sActualDiscount= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathDiscount);					
				String sExpectedDiscount = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPDISCOUNT);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualDiscount, sExpectedDiscount);
				objAPIAssertion.assertTrue(allMatch, "Validation For Discount["+sExpectedDiscount+"]");
				
				allMatch=false;
				List<String> sActualTypeOfService= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathTypeOfService);					
				String sExpectedTypeOfService = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPTYPEOFSERVICE);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualTypeOfService, sExpectedTypeOfService);
				objAPIAssertion.assertTrue(allMatch, "Validation For Service Type ["+sExpectedTypeOfService+"]");
				
				allMatch=false;
				List<String> sActualExpServiceTerm= objJsonUtil.getListofStringsFromJSONResponse(AcquireEstimateServicesAPIConstants.xpathServiceTerm);					
				String sExpectedExpServiceTerm = hmTestData.get(AcquireEstimateServicesAPIConstants.EXPSERVICETERM);
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualExpServiceTerm, sExpectedExpServiceTerm);
				objAPIAssertion.assertTrue(allMatch, "Validation For Service Term ["+sExpectedExpServiceTerm+"]");
	
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateLineItemWithTerm() method of AcquireEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void ValidateLineItemWithBlankEstimateNameAndID() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(AcquireEstimateServicesAPITest.sFileName, hmTestData, AcquireEstimateServicesAPITest.isMultiSet, AcquireEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sGetRespStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireEstimateServicesAPIConstants.xpathRespStatus);
				String sActualRespStatus = sGetRespStatus.replaceAll("\\[", "").replaceAll("\\]","");
				String sExpectedRespStatus  = AcquireEstimateServicesAPIConstants.EXP_Error;
				objAPIAssertion.assertEquals(sActualRespStatus, sExpectedRespStatus, " verify Status code ");
								
			
				String sGetDescription = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireEstimateServicesAPIConstants.xpathDescription);
				String sActualDescription = sGetDescription.replaceAll("\\[", "").replaceAll("\\]","");
				String sExpectedDescription  = AcquireEstimateServicesAPIConstants.Exp_ErrorDescription;
				objAPIAssertion.assertEquals(sActualDescription, sExpectedDescription, " verify Status Description ");
				
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateLineItemWithBlankEstimateNameAndID() method of AcquireEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
			
}

