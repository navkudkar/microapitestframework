package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.InvoiceSearchAPIValidation;
import com.im.imonline.api.business.InvoiceSearchRequestDataConfig;
import com.im.imonline.api.constants.InvoiceSearchAPIConstants;
import com.im.imonline.api.tests.InvoiceSearchAPITest;


public class InvoiceSearchAPIActionBucket {
	Logger logger = Logger.getLogger("InvoiceSearchElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	InvoiceSearchRequestDataConfig objConfig = null;
	InvoiceSearchAPIValidation objVal= null;


	public InvoiceSearchAPIActionBucket(APIDriver pAPIDriver, InvoiceSearchRequestDataConfig pObjConfig) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=pObjConfig;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new InvoiceSearchAPIValidation(objAPIAssertion,objConfig);
	}

	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(InvoiceSearchAPITest.sFileName,hmTestData,InvoiceSearchAPITest.isMultiSet,
					InvoiceSearchAPITest.TestDataFile);
			objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
			objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();
			//System.out.println(jsonRes);
			objVal.performValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of InvoiceSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			InvoiceSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),hmTestData.get(Constants.ExcelHeaderRunConfig),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(InvoiceSearchAPIConstants.INVOICEBEGINING) });
		}

	}

}
