package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;



public class ProductSearchElasticAPIActionBucket_Old<E> {
	Logger logger = Logger.getLogger("ProductSearchElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 


	public ProductSearchElasticAPIActionBucket_Old(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig ;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	

	
	private JsonUtil hitTheRequest() throws Exception {
		JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSearchElasticAPITest.sFileName,hmTestData,ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.TestDataFile);
			logger.info("Request body after logic :"+sRequestBody);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);		
			objJSONUtil.postRequest();
		}
		catch(Exception e) {
			String sErrMessage="FAIL: hitTheRequest() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			e.printStackTrace();
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

		return objJSONUtil;

	}
	
	
	private void performResKeywordTextValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<E> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathKeywordText);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_KEYWORD_TEXT);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'Did you mean'- text check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResKeywordTextValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}
	
	private void performResCatDescValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathCatDesc);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT1DESC);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sExpected.equals(sActual);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'Category description' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResCatDescValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	
	private void performResSubCatDescValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathSubCatDesc);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT2DESC);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.equals(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'Sub-Category description' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResSubCatDescValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
	}
	
	private void performResVendorNameValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathVendorName);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_VENDORNAME);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.equals(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'Vendor Name' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResVendorNameValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	
	private void performResInStockStatusValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathInStockStatus);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_INSTOCK);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.equals(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'In stock Status' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResInStockStatusValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	

	private void performResIsEsdProductStatusValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathIsEsdProductStatus);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_ISESDPRODUCT);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.equals(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'IsESDProduct Status' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResIsEsdProductStatusValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	

	private void performResProdTypeCat3Validation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathprodTypeCatDesc);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT3DESC);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.equals(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'cat3Desc/ProductType requested' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResProdTypeCat3Validation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
	}
	
	private void performResLongDesc1ContentValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			String sActual= objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathLongDesc);	
			String sExpected =  hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_LONGDESC_CONTENT);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=sActual.contains(sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Response 'LongDesc1' check passed as expetced was ["+sExpected+ "] and actual was "+sActual);
			}
		catch(Exception e) {
			String sErrMessage="FAIL: performResLongDesc1ContentValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
		
	}	


	private void performResSortingValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<String> sActual= objJSONUtil.getListofStringsFromJSONResponse(ProductSearchElasticAPIConstants.xpathDealerPrice);	
			String sortOrder=hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION);
			List<String> sExpected= Util.getSortedListInRequiredOrder(sActual, sortOrder);
			allMatch=sActual.equals(sExpected);
			
			String sortingNeeded=hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTBY);
			if (!(sortingNeeded.equalsIgnoreCase("")||sortingNeeded.equalsIgnoreCase(Constants.BLANKVALUE))) 
				objAPIAssertion.assertTrue(allMatch, "Validation of Response Sorting based on Dealer Price. Actual : ["+sActual.toString()+ "] and Expected :["+sExpected.toString()+ "]");
			else
				objAPIAssertion.assertTrue(true, "Response is sorted as expected based on Dealer Price. Actual : ["+sActual.toString()+ "] and Expected :["+sExpected.toString()+ "]");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResSortingValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}	
		
	}

	
	public void performValidationForAvailableValues() throws Exception {
		JsonUtil objJSONUtil = new JsonUtil(sRequestBody, hmTestData, hmConfig);
		objJSONUtil=hitTheRequest();
		
		String jsonRes=objJSONUtil.getResponse().asString();
		
		
		try {
				
		String sActualResponseCode = objJSONUtil.getStatusCode()+"";		
		objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_KEYWORD_TEXT).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_KEYWORD_TEXT).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResKeywordTextValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT1DESC).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT1DESC).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResCatDescValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT2DESC).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT2DESC).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResSubCatDescValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_VENDORNAME).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_VENDORNAME).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResVendorNameValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_INSTOCK).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_INSTOCK).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResInStockStatusValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_ISESDPRODUCT).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_ISESDPRODUCT).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResIsEsdProductStatusValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT3DESC).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_CAT3DESC).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResProdTypeCat3Validation(objJSONUtil);
	
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_LONGDESC_CONTENT).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_LONGDESC_CONTENT).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResLongDesc1ContentValidation(objJSONUtil);
		
		if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResSortingValidation(objJSONUtil);
		
	}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForAvailableValues() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}
			
	}



	public void performValidationForViewAlloption() throws Exception {
		JsonUtil objJSONUtil = new JsonUtil(sRequestBody, hmTestData, hmConfig);
		objJSONUtil=hitTheRequest();
		
		String jsonRes=objJSONUtil.getResponse().asString();
		
		 try {
		
		String sActualResponseCode = objJSONUtil.getStatusCode()+"";		
		objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		
		objAPIAssertion.assertHardTrue((objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)<=6),"Response received within 6 secs i.e. in "+objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)+" secs");
		
		String prodCount=objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING);
		objAPIAssertion.assertHardTrue(Integer.parseInt(prodCount)>0, "No of products retrieved is greater than 0 : "+prodCount);
		
		String totalCount=objJSONUtil.getActualValueFromJSONResponseWithoutModify(ProductSearchElasticAPIConstants.xpathTotalHits);
		objAPIAssertion.assertHardTrue(Integer.parseInt(totalCount)>0, "No of total hits is greater than 0 : "+totalCount);
		
		 }catch(Exception e) {
				String sErrMessage="FAIL: performValidationForAvailableValues() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
				logger.fatal(sErrMessage);       
				objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
				String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
				ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}
	}



	public void performValidationForSortingoption() throws Exception {

		JsonUtil objJSONUtil = new JsonUtil(sRequestBody, hmTestData, hmConfig);
		objJSONUtil=hitTheRequest();

		String jsonRes=objJSONUtil.getResponse().asString();
		
		try {
			
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			objAPIAssertion.assertHardTrue((objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)<=6),"Response received within 6 secs i.e. in "+objJSONUtil.getResponse().timeIn(TimeUnit.SECONDS)+" secs");

			if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_KEYWORD_TEXT).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_RES_KEYWORD_TEXT).equalsIgnoreCase(Constants.BLANKVALUE)))
				performResKeywordTextValidation(objJSONUtil);
			
			if(!(hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION).equalsIgnoreCase("")||hmTestData.get(ProductSearchElasticAPIConstants.EXCELHEADER_SORTDIRECTION).equalsIgnoreCase(Constants.BLANKVALUE)))
				performResSortingValidation(objJSONUtil);
			
		}catch(Exception e) {
			String sErrMessage="FAIL: performValidationForSortingoption() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}
	}
	
	public void performResponseCodeValidation() throws Exception {
		JsonUtil objJSONUtil = new JsonUtil(sRequestBody, hmTestData, hmConfig);
		objJSONUtil=hitTheRequest();
		String jsonRes=objJSONUtil.getResponse().asString();
		try {
			String sActualResponseCode = objJSONUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
		}catch(Exception e) {
			String sErrMessage="FAIL: performResponseCodeValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}
	}

}
