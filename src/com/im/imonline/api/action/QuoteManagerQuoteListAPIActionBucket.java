package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.QuoteManagerQuoteListAPIValidation;
import com.im.imonline.api.business.QuoteManagerQuoteListRequestDataConfig;
import com.im.imonline.api.tests.QuoteManagerQuoteListAPITest;


public class QuoteManagerQuoteListAPIActionBucket {
	Logger logger = Logger.getLogger("QuoteManagerQuoteListAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	QuoteManagerQuoteListRequestDataConfig objConfig = null;
	QuoteManagerQuoteListAPIValidation objVal= null;


	public QuoteManagerQuoteListAPIActionBucket(APIDriver pAPIDriver, QuoteManagerQuoteListRequestDataConfig pObjConfig) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=pObjConfig;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new QuoteManagerQuoteListAPIValidation(objAPIAssertion,objConfig);
	}

	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
      			sRequestBody = AppUtil.getRequestBodyForRestRequest(QuoteManagerQuoteListAPITest.sFileName,hmTestData,QuoteManagerQuoteListAPITest.isMultiSet,
					QuoteManagerQuoteListAPITest.TestDataFile);
			objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
 			objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();		
			objVal.performValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of QuoteManagerQuoteListAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			QuoteManagerQuoteListAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),hmTestData.get(Constants.ExcelHeaderRunConfig),sTestCaseName, sRequestBody,jsonRes });
		}

	}
	
	public void performResponseNegativeValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
	  			sRequestBody = AppUtil.getRequestBodyForRestRequest(QuoteManagerQuoteListAPITest.sFileName,hmTestData,QuoteManagerQuoteListAPITest.isMultiSet,
					QuoteManagerQuoteListAPITest.TestDataFile);
			objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
				objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();
			System.out.println(jsonRes);
			objVal.performNegativeValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of QuoteManagerQuoteListAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			QuoteManagerQuoteListAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),hmTestData.get(Constants.ExcelHeaderRunConfig),sTestCaseName, sRequestBody,jsonRes });
		}

	}

}



