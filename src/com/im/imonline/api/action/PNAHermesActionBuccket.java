package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.PNAHermisAPIConstant;
import com.im.imonline.api.tests.PNAHermesAPITest;

public class PNAHermesActionBuccket {
	Logger logger = Logger.getLogger("PNAHermesActionBuccket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public PNAHermesActionBuccket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validatePrice(String [] priceTypeToBeValidate) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;	
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(PNAHermesAPITest.sFileName, hmTestData, PNAHermesAPITest.isMultiSet, PNAHermesAPITest.TestDataFile);			
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJSONUtil.postRequest();
			objAPIAssertion.assertHardEquals(String.valueOf(objJSONUtil.getStatusCode()), "200", "Status response expected [200] Actual ["+String.valueOf(objJSONUtil.getStatusCode())+"]");

			CommonValidation cValidation = new CommonValidation(objAPIAssertion);
			List<HashMap<String, String>> lstOfMapData = objJSONUtil.getListofHashMapFromJSONResponse(PNAHermisAPIConstant.XPATH_DETAILS);

			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, PNAHermesAPITest.TestDataFile);
			//cValidation.validateMultiSetDetailsForREST(hmTestData, lstOfMapData, priceTypeToBeValidate, PNAHermesAPITest.TestDataFile);					
			cValidation.validateMultiSetDetailsForREST(multisetDataDrivenMap, lstOfMapData, priceTypeToBeValidate);

		}
		catch(Exception e) {
			String sErrMessage="FAIL: validatePrice() method of PNAHermesActionBuccket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PNAHermesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });
		}
	}

}
