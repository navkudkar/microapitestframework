package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.MultiSkuPriceandStockAPIConstants;
import com.im.imonline.api.tests.MultiSkuPriceandStockServicesAPITest;

public class MultiSkuPriceandStockServicesAPIActionBucket {

	Logger logger = Logger.getLogger("MultiSkuPriceandStockServicesAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public MultiSkuPriceandStockServicesAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void validateMultiSkuPriceandStockResponse() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {

			sRequestBody = AppUtil.getRequestBodyForRestRequest(MultiSkuPriceandStockServicesAPITest.sFileName, hmTestData, MultiSkuPriceandStockServicesAPITest.isMultiSet, MultiSkuPriceandStockServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " Verify  Expected Response Status");

			String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathStatusCode);
			String sExpectedExpStatusCode= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Expected Status code ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseMessage);
			String sExpectedExpResponseMessage= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " Verify Expected Response Message ");



		}catch(Exception e) {
			String sErrMessage="FAIL: validateMultiSkuPriceandStockResponse() method of MultiSkuPriceandStockServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			MultiSkuPriceandStockServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void validatePriceandStockResponseDetails() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {


			sRequestBody = AppUtil.getRequestBodyForRestRequest(MultiSkuPriceandStockServicesAPITest.sFileName, hmTestData, MultiSkuPriceandStockServicesAPITest.isMultiSet, MultiSkuPriceandStockServicesAPITest.TestDataFile);
			ObjectMapper mapper = new ObjectMapper();
		    Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " Verify  Expected Response Status");

			String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathStatusCode);
			String sExpectedExpStatusCode= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Expected Status code ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseMessage);
			String sExpectedExpResponseMessage= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " Verify Expected Response Message ");

			String sActualExpItemStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathItemStatus);
			String sExpectedExpItemStatus= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPITEAMSTATUS);
			objAPIAssertion.assertEquals(sActualExpItemStatus, sExpectedExpItemStatus, " Verify Iteam Status ");

			String sActualExpItemStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathItemStatusCode);
			String sExpectedExpItemStatusCode= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPITEAMSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpItemStatusCode, sExpectedExpItemStatusCode, " Verify Iteam Status code ");

			String sActualExpStatusMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathStatusMessage);
			String sExpectedExpStatusMessage= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPSTATUSMESSAGE);
			objAPIAssertion.assertEquals(sActualExpStatusMessage, sExpectedExpStatusMessage, " Verify Status Message ");

			String sActualExpIngrampartNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathIngramPartNumber);
			String sExpectedExpIngrampartNumber= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPINGRAMPARTNUMBER);
			objAPIAssertion.assertEquals(sActualExpIngrampartNumber, sExpectedExpIngrampartNumber, " Verify Ingrampart Number ");

			String sActualExpMpn= objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathmanfacturerpartnumber);
			String sExpectedExpMpn= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPMPN);
			objAPIAssertion.assertEquals(sActualExpMpn, sExpectedExpMpn, " Verify MPN Number");

			String sActualExpUpc = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathupc);
			String sExpectedExpUpc= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPUPC);
			objAPIAssertion.assertEquals(sActualExpUpc, sExpectedExpUpc, " Verify UPC Number ");

			String sActualExpQuantity = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathquantity);
			String sExpectedExpQuantity= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPQUANTITY);
			objAPIAssertion.assertEquals(sActualExpQuantity, sExpectedExpQuantity, " Verify Quantity ");

			String sActualExpRetailPrice = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpatretailprice);
			String sExpectedExpRetailPrice= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRETAILPRICE);
			objAPIAssertion.assertEquals(sActualExpRetailPrice, sExpectedExpRetailPrice, " Verify RetailPrice ");




		}catch(Exception e) {
			String sErrMessage="FAIL: validatePriceandStockResponseDetails() method of MultiSkuPriceandStockServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			MultiSkuPriceandStockServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void validateWarehousedetails() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(MultiSkuPriceandStockServicesAPITest.sFileName, hmTestData, MultiSkuPriceandStockServicesAPITest.isMultiSet, MultiSkuPriceandStockServicesAPITest.TestDataFile);
			ObjectMapper mapper = new ObjectMapper();
		    Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();
			CommonValidation cValidation = new CommonValidation(objAPIAssertion);
			
			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " Verify  Expected Response Status");

			String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathStatusCode);
			String sExpectedExpStatusCode= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Expected Status code ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseMessage);
			String sExpectedExpResponseMessage= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " Verify Expected Response Message ");



			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, MultiSkuPriceandStockServicesAPITest.TestDataFile);

			for (int i = 0; i<multisetDataDrivenMap.size(); i++) {

				List<HashMap<String, String>> lstOfMapData_QBDetails = objJsonUtil.getListofHashMapFromJSONResponse("serviceresponse.priceandstockresponse.details.warehousedetails[0]");

				cValidation.validateSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapData_QBDetails,new String[]{"warehouseid", "warehousedescription",}, "customernumber");
				System.out.println(multisetDataDrivenMap.get(i));
			}


		}catch(Exception e) {
			String sErrMessage="FAIL: validateWarehousedetails() method of MultiSkuPriceandStockServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			MultiSkuPriceandStockServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void validateWithoutCustomernumber() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {

			sRequestBody = AppUtil.getRequestBodyForRestRequest(MultiSkuPriceandStockServicesAPITest.sFileName, hmTestData, MultiSkuPriceandStockServicesAPITest.isMultiSet, MultiSkuPriceandStockServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "400", "Status Code Validation");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " Verify  Expected Response Status");

			String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathStatusCode);
			String sExpectedExpStatusCode= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Expected Status code ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(MultiSkuPriceandStockAPIConstants.xpathResponseMessage);
			String sExpectedExpResponseMessage= hmTestData.get(MultiSkuPriceandStockAPIConstants.EXPRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " Verify Expected Response Message ");


		}catch(Exception e) {
			String sErrMessage="FAIL: validateWithoutCustomernumber() method of MultiSkuPriceandStockServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			MultiSkuPriceandStockServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}



}




