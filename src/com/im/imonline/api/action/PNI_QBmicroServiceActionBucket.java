package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.SpecialPriceMicroserviceAPIValidation;
//import com.im.imonline.api.constants.PNI_QBmicroserviceAPIConstants;
import com.im.imonline.api.constants.PNImicroserviceAPIConstants;
import com.im.imonline.api.tests.PNI_QBmicroServiceAPITest;

public class PNI_QBmicroServiceActionBucket {
	Logger logger = Logger.getLogger("PNImicroServiceActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;

	public PNI_QBmicroServiceActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	//CheckResponseStatus-[Special Pricing Flags, Price & Discount] Validation
	public void validatePNI_QBMicroServiceResponse(String[] pArrQuantityDiscountBreakDetailsToVerify) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;

		CommonValidation cValidation = new CommonValidation(objAPIAssertion);
		SpecialPriceMicroserviceAPIValidation QBValidation = new SpecialPriceMicroserviceAPIValidation(objAPIAssertion);
		
		//Dynamic X-Path
		String sDynamicXPATH_QB_VD_FLAG_INDICATOR = null;
		String sDynamicXPATH_QB_FLAG_DETAILS = null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(PNI_QBmicroServiceAPITest.sFileName, hmTestData, PNI_QBmicroServiceAPITest.isMultiSet, PNI_QBmicroServiceAPITest.TestDataFile);			
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJSONUtil.postRequest();
			//System.out.println(" Response: \n\n"+objJSONUtil.getResponse().asString());
			cValidation.verifyEndptResponseStatus(objJSONUtil, "s");

			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, PNI_QBmicroServiceAPITest.TestDataFile);

			for (int i = 0; i<multisetDataDrivenMap.size(); i++) {
				
				//Dynamic XPATH - For QB VD Discount Details
				sDynamicXPATH_QB_VD_FLAG_INDICATOR = "quantityBreaksSummaryList[" + i + "].hasQuantityBreaksAvailable";
				String fActual = objJSONUtil.getActualValueFromJSONResponseWithoutModify(sDynamicXPATH_QB_VD_FLAG_INDICATOR);
				
				objAPIAssertion.setExtentInfo("MultiSKU Iteration ["+ (i+1) +"] **********");
				
				//If [hasQuantityBreaksAvailable = True; then validate]
				if (fActual == "true")
				{
					//Dynamic XPATH - For QB VD Discount Details
					sDynamicXPATH_QB_FLAG_DETAILS = "quantityBreaksSummaryList[" + i + "].quantityBreaksSummary";

					List<HashMap<String, String>> lstOfMapData_QBDetails = objJSONUtil.getListofHashMapFromJSONResponse(sDynamicXPATH_QB_FLAG_DETAILS);
					//cValidation.validateSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapData_QBDetails,pArrQuantityDiscountBreakDetailsToVerify, PNI_QBmicroserviceAPIConstants.INGRAMPART_NUMBER);
					QBValidation.validateACOPSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapData_QBDetails,pArrQuantityDiscountBreakDetailsToVerify, PNImicroserviceAPIConstants.INGRAMPART_NUMBER);
				}
				else objAPIAssertion.setExtentWarning("For SKU [" + multisetDataDrivenMap.get(i).get(PNImicroserviceAPIConstants.INGRAMPART_NUMBER) + "], KEY[hasQuantityBreaksAvailable] Verification: ");
			}

		}

		catch(Exception e) {
			String sErrMessage="FAIL: validatePNI_QBMicroServiceResponse() method of PNI_QBmicroServiceActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PNI_QBmicroServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });

		}

	}
}
