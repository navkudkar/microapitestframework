package com.im.imonline.api.action;

import java.io.IOException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.TypeaheadSearchElasticAPIValidation;
import com.im.imonline.api.business.TypeaheadSearchRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;
import com.im.imonline.api.tests.TypeaheadSearchElasticAPITest;


public class TypeaheadSearchElasticAPIActionBucket {
	Logger logger = Logger.getLogger("TypeaheadSearchElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	TypeaheadSearchRequestDataConfig objConfig = null;
	TypeaheadSearchElasticAPIValidation objVal= null;
	 

	public TypeaheadSearchElasticAPIActionBucket(APIDriver pAPIDriver, TypeaheadSearchRequestDataConfig objConfig2) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=objConfig2;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new TypeaheadSearchElasticAPIValidation(objAPIAssertion,objConfig);
	}
	
	/**
	 * @author Vinita Lakhwani
	 * performResponseValidation
	 * Method to be used for validation of typeahead Search API Response
	 */
	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		sRequestBody="";
		try {
			getTypeaheadReqBody(); 
			logger.info(sRequestBody);
			objJSONUtil= new JsonUtil("\r"+sRequestBody+"\r\n", hmTestData);		
			objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();
			//logger.info(jsonRes);
			objVal.performValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of TypeaheadSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}

	}

	private void getTypeaheadReqBody() throws Exception {
		try {
			String[] queryFieldArr= {"vendorname","cat1desc","cat2desc","cat3desc","tatier1","tatier2","tatier3","tatier4"};
			for(String queryField : queryFieldArr)
			{
			hmTestData.put(ProductSearchElasticAPIConstants.QUERYFIELD, queryField);
			sRequestBody=sRequestBody+"{}\n"+AppUtil.getRequestBodyForRestRequest(TypeaheadSearchElasticAPITest.sFileName,hmTestData,TypeaheadSearchElasticAPITest.isMultiSet, TypeaheadSearchElasticAPITest.TestDataFile)+"\n";
			}
		} catch (IOException e) {
			String sErrMessage="FAIL: getTypeaheadReqBody() method of TypeaheadSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}

}
