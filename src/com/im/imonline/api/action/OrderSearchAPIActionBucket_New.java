package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.OrderSearchAPIValidation;
import com.im.imonline.api.business.OrderSearchRequestDataConfig;
import com.im.imonline.api.constants.OrderSearchAPIConstants_New;
import com.im.imonline.api.tests.OrderSearchAPITest_New;


public class OrderSearchAPIActionBucket_New {
	Logger logger = Logger.getLogger("OrderSearchElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	OrderSearchRequestDataConfig objConfig = null;
	OrderSearchAPIValidation objVal= null;
	 

	public OrderSearchAPIActionBucket_New(APIDriver pAPIDriver, OrderSearchRequestDataConfig pObjConfig) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=pObjConfig;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new OrderSearchAPIValidation(objAPIAssertion,objConfig);
	}
	
	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest_New.sFileName,hmTestData,OrderSearchAPITest_New.isMultiSet,OrderSearchAPITest_New.TestDataFile);
			objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
			objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();
			//System.out.println(jsonRes);
			objVal.performValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of InvoiceSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest_New.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),hmTestData.get(Constants.ExcelHeaderRunConfig),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants_New.INVOICEBEGINING) });
		}

	}

}
