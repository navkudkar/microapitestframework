package com.im.imonline.api.action;


import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.CoustmerCreateServicesAPIConstants;
import com.im.imonline.api.tests.CoustmerCreateServicesAPITest;



public class CoustmerCreateServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("CoustmerCreateServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	private static final String Expected_Valid_Status= "S";
	private static final String Expected_ErrorStatus= "E";
	private static final String Expected_BlankCountryCodeErrorMsg= "country should be provided and not be null";
	private static final String Expected_PostalCodeErrorMsg= "POSTAL CODE MISSING";
	private static final String Expected_BlankPartyNameErrorMsg= "party name should be provided and not be null in locale: en-US";

	public CoustmerCreateServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void ValidateManditoryFieldsForCoustmerCreateService() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CoustmerCreateServicesAPITest.sFileName, hmTestData, CoustmerCreateServicesAPITest.isMultiSet, CoustmerCreateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathStatus);
				String sExpectedStatus = Expected_Valid_Status;
				objAPIAssertion.assertEquals(sActualStatus, sExpectedStatus, " Verify Status ");
				
				String sActualCountryCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathCountryCode);
				String sExpectedCountryCode = hmTestData.get(CoustmerCreateServicesAPIConstants.COUNTRY);
				objAPIAssertion.assertEquals(sActualCountryCode, sExpectedCountryCode, " Verify country ");
				
				String sActualPartyName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathPartyName);
				String sExpectedPartyName = hmTestData.get(CoustmerCreateServicesAPIConstants.PARTYNAME);
				objAPIAssertion.assertEquals(sActualPartyName, sExpectedPartyName, " Verify Party Name ");
				
				String sActualAddress = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathAddressLine1);
				String sExpectedAddress = hmTestData.get(CoustmerCreateServicesAPIConstants.ADDRESSLINE1);
				objAPIAssertion.assertEquals(sActualAddress, sExpectedAddress, " Verify Address ");
				
				String sActualCity = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathAddressLine1);
				String sExpectedCity = hmTestData.get(CoustmerCreateServicesAPIConstants.ADDRESSLINE1);
				objAPIAssertion.assertEquals(sActualCity, sExpectedCity, " Verify City ");
				
				String sActualState = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathState);
				String sExpectedState = hmTestData.get(CoustmerCreateServicesAPIConstants.STATE);
				objAPIAssertion.assertEquals(sActualState, sExpectedState, " Verify State ");
			
				String sActualPostalCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathPostalCode);
				String sExpectedPostalCode = hmTestData.get(CoustmerCreateServicesAPIConstants.POSTALCODE);
				objAPIAssertion.assertEquals(sActualPostalCode, sExpectedPostalCode, " Verify Postal Code ");
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateManditoryFieldsForCoustmerCreateService() method of CoustmerCreateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CoustmerCreateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void ValidateBlankCountryCodeForCoustmerCreateService() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CoustmerCreateServicesAPITest.sFileName, hmTestData, CoustmerCreateServicesAPITest.isMultiSet, CoustmerCreateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualErrorStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathStatus);
				String sExpectedErrorStatus = Expected_ErrorStatus;
				objAPIAssertion.assertEquals(sActualErrorStatus, sExpectedErrorStatus, " Verify Error Status ");
				
				String sActualErrorMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathMessage);
				String sExpectedErrorMessage= Expected_BlankCountryCodeErrorMsg;
				objAPIAssertion.assertEquals(sActualErrorMessage, sExpectedErrorMessage, " Verify Error Message ");
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateBlankCountryCodeForCoustmerCreateService() method of CoustmerCreateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CoustmerCreateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void ValidateBlankPostalCodeForCoustmerCreateService() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CoustmerCreateServicesAPITest.sFileName, hmTestData, CoustmerCreateServicesAPITest.isMultiSet, CoustmerCreateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualErrorMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathPostalCodeMessage);
				String sExpectedErrorMessage= Expected_PostalCodeErrorMsg;
				objAPIAssertion.assertEquals(sActualErrorMessage, sExpectedErrorMessage, " Verify Error Message for Postal code");
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateBlankCountryCodeForCoustmerCreateService() method of CoustmerCreateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CoustmerCreateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	
	public void ValidateBlankPartyNameForCoustmerCreateService() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CoustmerCreateServicesAPITest.sFileName, hmTestData, CoustmerCreateServicesAPITest.isMultiSet, CoustmerCreateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				

				String sActualErrorStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathStatus);
				String sExpectedErrorStatus = Expected_ErrorStatus;
				objAPIAssertion.assertEquals(sActualErrorStatus, sExpectedErrorStatus, " Verify Error Status ");
				
				String sActualErrorMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CoustmerCreateServicesAPIConstants.xpathMessage);
				String sExpectedErrorMessage= Expected_BlankPartyNameErrorMsg;
				objAPIAssertion.assertEquals(sActualErrorMessage, sExpectedErrorMessage, " Verify Error Message ");
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateBlankCountryCodeForCoustmerCreateService() method of CoustmerCreateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CoustmerCreateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
}

