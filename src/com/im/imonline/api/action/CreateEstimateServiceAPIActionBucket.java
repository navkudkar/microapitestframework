package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.CreateEstimateServicesAPIConstants;
import com.im.imonline.api.tests.CreateEstimateServicesAPITest;

//import com.sun.xml.bind.v2.schemagen.xmlschema.List;

public class CreateEstimateServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("CreateEstimateServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	private static final String Expected_Valid_Status= "Success";
	private static final String Expected_Status= "Error";
	private static final String Expected_EstimateIDTypeCode= "Estimate ID";
	private static final String Expected_ProductMsg= "ProductIdentifier is absent for line number";

	public CreateEstimateServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	public void ValidateEstimateCreatedAndCheckInlistEstimate() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		boolean allMatch  =false;
	
		try {
				sRequestBody=AppUtil.getRequestBodyForRestRequest(CreateEstimateServicesAPITest.sFileName, hmTestData, CreateEstimateServicesAPITest.isMultiSet, CreateEstimateServicesAPITest.TestDataFile);
				//sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CreateEstimateServicesAPITest.sFileName, hmTestData, CreateEstimateServicesAPITest.isMultiSet, CreateEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sGetActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathStatusCode);
				String sActualExpStatus = sGetActualExpStatus.replaceAll("\\[", "").replaceAll("\\]","");
				String sExpectedExpStatus = Expected_Valid_Status;
				objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, "Create Estimate Status ");
				
			
				List<String> sActualEstimateIDTypeCode= objJsonUtil.getListofStringsFromJSONResponse(CreateEstimateServicesAPIConstants.xpathEstimateIDTypeCode);					
				String sExpectedEstimateIDTypeCode= Expected_EstimateIDTypeCode;		
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateIDTypeCode, sExpectedEstimateIDTypeCode);
				objAPIAssertion.assertHardTrue(allMatch, "Validation For Estimate Id Avalibility in Create Estimate API["+sExpectedEstimateIDTypeCode+"]");
				
				//Get Estimate Id to verify in List Estimate API .
				String sGetExpectedEstimateId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathEstimateIDTypeCodeValue);
				String sExpectedEstimateId = sGetExpectedEstimateId.replaceAll("\\[","").replaceAll("\\]", "");
				
				//Get Estimate Name to verify in List Estimate API .
				List<ArrayList<String>> sGetEstimateName= objJsonUtil.getListofStringsFromJSONResponse(CreateEstimateServicesAPIConstants.xpathEstimateNameTypeCodeValue);					
				ArrayList<String> sGetFirstEstimateName = sGetEstimateName.get(0);
				ArrayList<String> sGetValues = new ArrayList<>();
				for(String sValues:sGetFirstEstimateName)
				{
					sGetValues.add(sValues);
				}
				String sExpectedEstimateName   = sGetValues.get(1);
				
				//Get Estimate Valid Status and verify in List Estimate 
				String sGetExpectedEstimateValid = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathEstimateValid);
				String sExpectedEstimateValid = sGetExpectedEstimateValid.replaceAll("\\[","").replaceAll("\\]", "");
				
				objJsonUtil.postRequestNextAPI(sToken);
				
				String sGetAPIStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathListEstimateStatus);
				String APIStatus = sGetAPIStatus.replaceAll("\\[", "").replaceAll("\\]", "");
				objAPIAssertion.assertHardTrue(APIStatus.equals(Expected_Valid_Status), " Verify List Estimate Status In List Estimate API["+APIStatus+"]");

				List<E> sGetActualListEstimateValid= objJsonUtil.getListofStringsFromJSONResponse(CreateEstimateServicesAPIConstants.xpathListEstimateValid);					
				String sGetFirstEstimateValid = String.valueOf(sGetActualListEstimateValid.get(0));
				String sActualListEstimateValid = sGetFirstEstimateValid.replaceAll("\\[", "").replaceAll("\\]", "");
				objAPIAssertion.assertEquals(sActualListEstimateValid, sExpectedEstimateValid, "Valid Estimate status in List Estimate API");

				
				List<String> sActualEstimateId= objJsonUtil.getListofStringsFromJSONResponse(CreateEstimateServicesAPIConstants.xpathEstimateIDOfListEstimate);					
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateId, sExpectedEstimateId);
				objAPIAssertion.assertTrue(allMatch, "Verifiy EstimateID in List Estimate API["+sExpectedEstimateId+"]");
				
				List<E> sGetActualListEstimateName= objJsonUtil.getListofStringsFromJSONResponse(CreateEstimateServicesAPIConstants.xpathEstimateNameInListEstimate);					
				String sGetFirstEstimateNameInList = String.valueOf(sGetActualListEstimateName.get(0));
				String sActualListEstimateName = sGetFirstEstimateNameInList.replaceAll("\\[", "").replaceAll("\\]", "");
				objAPIAssertion.assertEquals(sActualListEstimateName, sExpectedEstimateName, "List Estimate Name In List Estimate API");
				
					
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateEstimateCreatedAndCheckInlistEstimate() method of CreateEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CreateEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	

	public void ValidateCreateEstimateServiceForProductLineMissing() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {
				sRequestBody=AppUtil.getRequestBodyForRestRequest(CreateEstimateServicesAPITest.sFileName, hmTestData, CreateEstimateServicesAPITest.isMultiSet, CreateEstimateServicesAPITest.TestDataFile);
				//sRequestBody = AppUtil.getRequestBodyForSOAPRequest(CreateEstimateServicesAPITest.sFileName, hmTestData, CreateEstimateServicesAPITest.isMultiSet, CreateEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sGetActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathStatusCode);
				String sActualExpStatus = sGetActualExpStatus.replaceAll("\\[", "").replaceAll("\\]","");
				String sExpectedExpStatus = Expected_Status;
				objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, "Create Estimate Status ");
				
			
				String sGetActualProductInfoMsg = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CreateEstimateServicesAPIConstants.xpathProductLineMissMessage);
				String sActualProductInfoMsg = sGetActualProductInfoMsg.replaceAll("\\[", "").replaceAll("\\]","");
				String sGetLineNumber = hmTestData.get(CreateEstimateServicesAPIConstants.LINENUMBERID);
				String sExpectedProductMsg = Expected_ProductMsg.concat(" "+sGetLineNumber);
				objAPIAssertion.assertEquals(sActualProductInfoMsg, sExpectedProductMsg, "Product Line Missing ");
					
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateEstimateCreatedAndCheckInlistEstimateForInvalidStatus() method of CreateEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CreateEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
}

