package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;

import io.restassured.RestAssured;
import io.restassured.response.Response;



public class OrderStatusAPIActionBucket<E> {
	Logger logger = Logger.getLogger("OrderStatusAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	private Response objResponse = null;


	public OrderStatusAPIActionBucket(HashMap<String, String> phmTestData, APIDriver pAPIDriver) {
		hmTestData=phmTestData;	
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	
	public void performResponseValidation(String endPointUrlNameKey) throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			String sResponseBody=null;		

			RestAssured.baseURI = hmTestData.get(endPointUrlNameKey);
			objResponse = RestAssured.given()
					.when().get(RestAssured.baseURI)
					.then()
					.extract()
					.response();		
						
			if(objResponse==null || objResponse.getStatusCode()!=200) {
				throw new Exception("Null values in GET Response for LOGIN BACKOFFICE URL in the function generateBackofficeLoginRequestVerificationToken");
			}else {
				objAPIAssertion.assertTrue(objResponse.getStatusCode()==200, "Response API Status Code should be 200" );
				sResponseBody = objResponse.getBody().asString();
			}
			objAPIAssertion.assertTrue(true, "Get Response Successfully Response :  + "+sResponseBody);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of InvoiceSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		

	}
	
	

}
