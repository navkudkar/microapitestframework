package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.RenewallCCWServicesAPIConstants;
import com.im.imonline.api.tests.RenewalCCWServicesAPITest;

public class RenewalICCWServicesAPIActionBucket {

	Logger logger = Logger.getLogger("RenewalICCWServicesAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public RenewalICCWServicesAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void USValidateRenewalCCWServicesResponse() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWServicesAPITest.sFileName, hmTestData, RenewalCCWServicesAPITest.isMultiSet, RenewalCCWServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathQuoteiDstatus);
			String sExpectedExpStatus = hmTestData.get(RenewallCCWServicesAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify  Expected Status code ");


			String sActualExpQuoteName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathQuotecreated);
			String sExpectedExpQuoteName= hmTestData.get(RenewallCCWServicesAPIConstants.EXPQUOTECREATEDNAME);
			objAPIAssertion.assertEquals(sActualExpQuoteName, sExpectedExpQuoteName, " Verify  Quote Created Name ");

			String sActualExpInvoiceTotoalAmount = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathInvoiceTotoalAmount);
			String sExpectedExpInvoiceTotoalAmount= hmTestData.get(RenewallCCWServicesAPIConstants.EXPINVOICETOTALAMOUNT);
			objAPIAssertion.assertEquals(sActualExpInvoiceTotoalAmount, sExpectedExpInvoiceTotoalAmount, " Verify invoice Total Amount ");

			String sActualExpQuoteCreateDate = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathquoteCreationDate);
			String sExpectedExpQuoteCreateDate= hmTestData.get(RenewallCCWServicesAPIConstants.EXPQUOTECREATIONDATE);
			objAPIAssertion.assertEquals(sActualExpQuoteCreateDate, sExpectedExpQuoteCreateDate, " Verify Quote create Date ");

		}catch(Exception e) {
			String sErrMessage="FAIL: VerifyRenewalCCWServiceForQuoteID() method of RenewalICCWServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void validateResponseForQuoteRequset() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWServicesAPITest.sFileName, hmTestData, RenewalCCWServicesAPITest.isMultiSet, RenewalCCWServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);


			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");


			String sActualExpInvalidQuoteidStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathInvaldidQuoteIdStatus);
			String sExpectedExpInvalidQuoteidStatus = hmTestData.get(RenewallCCWServicesAPIConstants.EXPINVALIDSTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidQuoteidStatus, sExpectedExpInvalidQuoteidStatus, " verify for Status ");

			String sActualExpInvalidResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathInvaldidResponseStatus);
			String sExpectedExpInvalidResponseStatus = hmTestData.get(RenewallCCWServicesAPIConstants.EXPINVALIDRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidResponseStatus, sExpectedExpInvalidResponseStatus, " verify for Response Status ");

			String sActualExpInvalidResponseMessageStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathInvaldidResponseMessageStatus);
			String sExpectedExpInvalidResponseMessageStatus = hmTestData.get(RenewallCCWServicesAPIConstants.EXPINVALIDRESPONSEMESSAGESTATUS);
			objAPIAssertion.assertEquals(sActualExpInvalidResponseMessageStatus, sExpectedExpInvalidResponseMessageStatus, " verify for ResponseMessage Status ");

		}catch(Exception e) {
			String sErrMessage="FAIL: USValidateinvalidQuoteId() method of RenewalICCWServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

	public void USValidatepriceProtectionEndDate() throws Exception {
		// TODO Auto-generated method stub
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch=false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(RenewalCCWServicesAPITest.sFileName, hmTestData, RenewalCCWServicesAPITest.isMultiSet, RenewalCCWServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);

			CommonValidation cValidation = new CommonValidation(objAPIAssertion);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathQuoteiDstatus);
			String sExpectedExpStatus = hmTestData.get(RenewallCCWServicesAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify  Expected Status code ");

			String sActualExpdatepriceProtectionEndDate = objJsonUtil.getActualValueFromJSONResponseWithoutModify(RenewallCCWServicesAPIConstants.xpathpriceProtectionEndDate);
			String sExpectedExpdatepriceProtectionEndDate = hmTestData.get(RenewallCCWServicesAPIConstants.EXPPRICEPROTECTIONENDDATE);
			objAPIAssertion.assertEquals(sActualExpdatepriceProtectionEndDate, sExpectedExpdatepriceProtectionEndDate, " Verify price ProtectionEndDate ");

			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, RenewalCCWServicesAPITest.TestDataFile);

			for (int i = 0; i<multisetDataDrivenMap.size(); i++) {


				List<HashMap<String, String>> lstOfMapData_QBDetails = objJsonUtil.getListofHashMapFromJSONResponse("invoiceSummaries");

				cValidation.validateSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapData_QBDetails,new String[]{"serviceOrderingSKU", "amount", "quantity"}, "QuoteId");
			}




		}catch(Exception e) {
			String sErrMessage="FAIL: USValidatepriceProtectionEndDate() method of RenewalICCWServicesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			RenewalCCWServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}

	}

}




