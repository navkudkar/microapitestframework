package com.im.imonline.api.action;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.BackofficeMerchandisingAPIValidation;
import com.im.imonline.api.backoffice.business.BackOfficeCreateRule;
import com.im.imonline.api.backoffice.business.BackOfficeRuleCreateRegister;
import com.im.imonline.api.backoffice.business.BackofficeMerchandisingRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.testdata.util.BackofficeMerchandisingTestDataUtil;
import com.im.imonline.api.tests.BackofficeMerchandisingRuleAPITest;


public class BackofficeMerchandisingAPIActionBucket {
	Logger logger = Logger.getLogger("BackofficeMerchandisingAPIActionBucket"); 
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	BackofficeMerchandisingTestDataUtil objConfig = null;	
	BackOfficeRuleCreateRegister objBkRegister = null;
	 

	public BackofficeMerchandisingAPIActionBucket(APIDriver pAPIDriver, BackOfficeRuleCreateRegister objBkReg, HashMap<String, String> pExcelHMap ) throws Exception {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objBkRegister = objBkReg;	
		hmConfig=pExcelHMap;		
	}	
	
	/**
	 * @author Psave
	 * createRuleAndPerformResponseValidation
	 * Method to be used for create backoffice rule using http request and verify rule using response validation
	 */
	public void createRuleAndPerformResponseValidation() throws Exception {			
		BackOfficeCreateRule objBkCreateRule = null;
		boolean sSkipFlag = false;
		boolean sDeleteFlag = false;
		try {
			String sCountry = objBkRegister.getCountry();
			String sToken = objBkRegister.getCreateRuleRequestVerificationToken();
			Map<String,String> objMaap = objBkRegister.getCreateRuleRequestCookies();
			sSkipFlag = validateRuleCreationData();
			objAPIAssertion.assertHardTrue(sToken!=null, "Backoffice Rule Creation - Request Verification Token Created with ID : "+sToken);
			if(objMaap==null)
				objAPIAssertion.assertHardTrue(objMaap!=null, "Backoffice Rule Creation - Cookies Required For Rule Generation is Getting as NULL");			
			if(!sSkipFlag) {
				objAPIAssertion.setExtentInfo("BackOffice Rule Creation Started For Rule Name : "+objBkRegister.getFormDataRequest().get("Name"));
				objBkCreateRule = new BackOfficeCreateRule(sCountry, sToken,objMaap, objAPIAssertion );
				objBkCreateRule.createBackOfficeRule(objBkRegister.getFormDataRequest());
				String sRuleId = objBkCreateRule.getRuleID();
				objAPIAssertion.assertHardTrue(objBkCreateRule.getFlagRuleCreated() && sRuleId!=null, "Backoffice Rule Created. Rule Name : "+objBkRegister.getFormDataRequest().get("Name"));
				objBkRegister.setRuleID(sRuleId);
				performResponseValidation();
				sDeleteFlag = objBkCreateRule.deleteBackOfficeRule(objBkRegister.getFormDataRequest());//Delete rule 					
			}
		}catch(Exception e) {
			String sErrMessage="FAIL: createRuleAndPerformResponseValidation() method of BackofficeMerchandisingAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			if(!sSkipFlag && objBkCreateRule.getFlagRuleCreated()) {
				if(!sDeleteFlag)				
					objAPIAssertion.assertTrue(objBkCreateRule.deleteBackOfficeRule(objBkRegister.getFormDataRequest()) , "Backoffice Rule Deletion");
				else
					objAPIAssertion.assertTrue(sDeleteFlag , "Backoffice Rule Deletion");
				
			}
			
		}

	}	
	
	private void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		String sLocationArr[]  = null;
		HashMap<String, String> hmTestData = null;
		try {
			sLocationArr = objBkRegister.getFieldsLevelFlag() ? objBkRegister.getFieldsValueAsLocation().split(";") : objBkRegister.getLocation().split(";");
			int requCnt=1;
			for(String sLocation : sLocationArr) {
				BackofficeMerchandisingRequestDataConfig objBkDataConfig = new BackofficeMerchandisingRequestDataConfig(objBkRegister, hmConfig);
				hmTestData = objBkDataConfig.generatRequestMap(sLocation);
				sRequestBody = AppUtil.getRequestBodyForRestRequest(BackofficeMerchandisingRuleAPITest.sFileName,hmTestData,BackofficeMerchandisingRuleAPITest.isMultiSet, BackofficeMerchandisingRuleAPITest.BackofficeMerchandisingTestDataFile);
				objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData, hmConfig);		
				objJSONUtil.postRequest();
				objAPIAssertion.setExtentInfo("Response Validation for "+requCnt+" Request where Location as : ' "+ sLocation+" '");
				objAPIAssertion.assertHardTrue(objJSONUtil.getStatusCode()==200 , "Response Validation Status Code : "+objJSONUtil.getStatusCode());
				BackofficeMerchandisingAPIValidation objVal=new BackofficeMerchandisingAPIValidation(objAPIAssertion,objBkRegister,hmConfig);
				objVal.performRuleValidation(objJSONUtil);
				requCnt++;
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of BackofficeMerchandisingAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		/*finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			BackofficeMerchandisingRuleAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes });
		}*/

	}
	
	private boolean validateRuleCreationData() throws Exception {			
		boolean SflagSkip = false;
		try {
			if(objBkRegister.getFlagForProductSpecificTypePlacement() && objBkRegister.getProductSelectionValues().contains(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				objAPIAssertion.setExtentSkip(" Product Selection Data not Capture. For Product Selection Type : "+Arrays.asList(objBkRegister.getProductSelectionArr()));
				SflagSkip = true;
			}else if(objBkRegister.getLocation().equals(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				objAPIAssertion.setExtentSkip(" Location Data not Capture. For Location Type : "+Arrays.asList(objBkRegister.getLocationSelectionArr()));
				SflagSkip = true;
			}else if(objBkRegister.getLocation().contains(ProductSearchElasticAPIConstants.NULLCONSTANT)) {
				objAPIAssertion.setExtentSkip(" Location Data not Capture. For Location Type : "+Arrays.asList(objBkRegister.getLocationSelectionArr())+
						". Getting Dynamic Location value as Null in the response : ' "+objBkRegister.getLocation()+" '");
				SflagSkip = true;
			}else if(objBkRegister.getFieldsLevelFlag() && objBkRegister.getFieldsValue().contains(ProductSearchElasticAPIConstants.NULLCONSTANT)){
				objAPIAssertion.setExtentSkip(" Fields value Data not Capture. For Fields Value Type : "+Arrays.asList(objBkRegister.getFieldsValueAsLocation()));
				SflagSkip = true;
			}else {			
				objAPIAssertion.setExtentInfo("All Elastic Data Capture Successfully");
			}
			
		}
		catch(Exception e) {
			String sErrMessage="FAIL: validateRuleCreationData() method of BackofficeMerchandisingAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		
		return SflagSkip;
	}
	
	public void deleteAllRules() throws Exception {			
		BackOfficeCreateRule objBkCreateRule = null;
		try {
			String sCountry = objBkRegister.getCountry();
			String sToken = objBkRegister.getCreateRuleRequestVerificationToken();
			Map<String,String> objMaap = objBkRegister.getCreateRuleRequestCookies();		
			objAPIAssertion.assertHardTrue(sToken!=null, "Backoffice Rule Creation - Request Verification Token Created with ID : "+sToken);
			if(objMaap==null)
				objAPIAssertion.assertHardTrue(objMaap!=null, "Backoffice Rule Creation - Cookies Required For Rule Generation is Getting as NULL");			
			objBkCreateRule = new BackOfficeCreateRule(sCountry, sToken,objMaap, objAPIAssertion );
			objBkCreateRule.deleteAllTheRules();			
		}catch(Exception e) {
			String sErrMessage="FAIL: createRuleAndPerformResponseValidation() method of BackofficeMerchandisingAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
			
		

	}	

}
