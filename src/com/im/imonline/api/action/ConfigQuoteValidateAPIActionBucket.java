package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.ConfigQuoteValidateServicesAPIConstants;
import com.im.imonline.api.tests.ConfigQuoteValidateServicesAPITest;

public class ConfigQuoteValidateAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("ConfigQuoteValidateAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public ConfigQuoteValidateAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validateManditoryFieldsForQuoteValidateService() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(ConfigQuoteValidateServicesAPITest.sFileName, hmTestData, ConfigQuoteValidateServicesAPITest.isMultiSet, ConfigQuoteValidateServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);
			CommonValidation cValidation = new CommonValidation(objAPIAssertion);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(ConfigQuoteValidateServicesAPIConstants.xpathStatusCode);
			String sExpectedExpStatusCode = hmTestData.get(ConfigQuoteValidateServicesAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(ConfigQuoteValidateServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = hmTestData.get(ConfigQuoteValidateServicesAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify Status  ");	

			List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, ConfigQuoteValidateServicesAPITest.TestDataFile);

			List<HashMap<String, String>> lstMapActData = new ArrayList<>();
			for (int i = 0; i<multisetDataDrivenMap.size(); i++) {
				HashMap<String,String> mapOfTestData = new HashMap<>();
				
				String sXpath = "serviceresponse.message.AcknowledgeConfiguration.ConfigurationLine.ItemName["+i+"]";
				String sActualItemNummber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(sXpath);
				mapOfTestData.put("ItemName", sActualItemNummber);
				System.out.println(mapOfTestData);
				lstMapActData.add(mapOfTestData);			
			}
			
			cValidation.validateMultiSetDetailsForREST(multisetDataDrivenMap, lstMapActData, new String[]{"ItemName"});

		}
		catch(Exception e) {
			String sErrMessage="FAIL: ValidateManditoryFieldsForQuoteValidateService() method of ConfigQuoteValidateAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ConfigQuoteValidateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}



	public void validateWithoutItemName() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(ConfigQuoteValidateServicesAPITest.sFileName, hmTestData,ConfigQuoteValidateServicesAPITest.isMultiSet, ConfigQuoteValidateServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpMessageCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(ConfigQuoteValidateServicesAPIConstants.xpathMessageCode);
			String sExpectedExpMessageCode = hmTestData.get(ConfigQuoteValidateServicesAPIConstants.EXPMESSAGECODE);
			objAPIAssertion.assertEquals(sActualExpMessageCode, sExpectedExpMessageCode, " Verify Message code ");

			String sActualExpDescription = objJsonUtil.getActualValueFromJSONResponseWithoutModify(ConfigQuoteValidateServicesAPIConstants.xpathDescription);
			String sExpectedExpDescription = hmTestData.get(ConfigQuoteValidateServicesAPIConstants.EXPDESCRIPTION);
			objAPIAssertion.assertEquals(sActualExpDescription, sExpectedExpDescription, " Verify Description Message ");


		}
		catch(Exception e) {
			String sErrMessage="FAIL: ValidateManditoryFieldsForQuoteValidateService() method of ConfigQuoteValidateAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ConfigQuoteValidateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}

}

