package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.GetPartnerAddressServiceAPIConstants;
import com.im.imonline.api.tests.GetPartnerAddressServiceAPITest;


public class GetPartnerAddressServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("GetPartnerAddressServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public GetPartnerAddressServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validateGetPartnerAddressService() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(GetPartnerAddressServiceAPITest.sFileName, hmTestData, GetPartnerAddressServiceAPITest.isMultiSet, GetPartnerAddressServiceAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();
			
			String sToken = objJsonUtil.getAccessToken("access_token");
			objJsonUtil.postRequest(sToken);
			
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);
			
			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpSiteId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathsiteId);
			String sExpectedExpSiteId  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPSITEID);
			objAPIAssertion.assertEquals(sActualExpSiteId, sExpectedExpSiteId, " verify Site Id ");
			
			String sActualExpSiteName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathsiteName);
			String sExpectedExpSiteName  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPSITENAME);
			objAPIAssertion.assertEquals(sActualExpSiteName, sExpectedExpSiteName, " verify Site Name");
			
			String sActualExpStreetAddress = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathstreetAddress1);
			String sExpectedExpStreetAddress  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPSTREETADDRESS1);
			objAPIAssertion.assertEquals(sActualExpStreetAddress, sExpectedExpStreetAddress, " verify StreetAddress ");
			
			String sActualExpCountry = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathcountry);
			String sExpectedExpCountry  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPCOUNTRY);
			objAPIAssertion.assertEquals(sActualExpCountry, sExpectedExpCountry, " verify Country ");
			
			String sActualExpZipcode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathzipCode);
			String sExpectedExpZipcode  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPZIPCODE);
			objAPIAssertion.assertEquals(sActualExpZipcode, sExpectedExpZipcode, " verify ZIP Code");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetPartnerAddressServiceAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus  = hmTestData.get(GetPartnerAddressServiceAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Site Id ");

			
	
		}
		catch(Exception e) {
			String sErrMessage="FAIL: validateGetPartnerAddressService() method of GetPartnerAddressServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			GetPartnerAddressServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}

	
}

