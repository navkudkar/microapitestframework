package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.DartDetailsServicesForCanadaAPIConstants;
import com.im.imonline.api.tests.DartDetailsServicesForCanadaAPITest;

public class DartDetailsServiceForCanadaAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("DartDetailsServiceForCanadaAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public DartDetailsServiceForCanadaAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	public void CanadaValidateDartDetailsServicesResponse() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null; boolean allMatch=false;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForCanadaAPITest.sFileName, hmTestData, DartDetailsServicesForCanadaAPITest.isMultiSet, DartDetailsServicesForCanadaAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathStatus);
				String sExpectedExpStatus = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUS);
				objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");
			
				String sActualExpDealId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathDealId);
				String sExpectedExpDealId = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPDEALID);
				objAPIAssertion.assertEquals(sActualExpDealId, sExpectedExpDealId, " Verify Deal Id ");
				
				String sActualAutorizationNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathAuthorizationNumber);
				String sExpectedAutorizationNumber = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.AUTHORIZATIONNUMBER);
				objAPIAssertion.assertEquals(sActualAutorizationNumber , sExpectedAutorizationNumber, "Verify Autorization Number");
				
				String sActualCustomerName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathCustomerName);
				String sExpectedCustomerName = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPCUSTOMERNAME);
				objAPIAssertion.assertEquals(sActualCustomerName , sExpectedCustomerName, "Verify Customer Name ");
				
				String sActualBranchCustNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathBranchCustNumber);
				String sExpectedBranchCustNumber = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPBRANCHCUSTNUMBER);
				objAPIAssertion.assertEquals(sActualBranchCustNumber , sExpectedBranchCustNumber, "Verify Branch Number");
				
				//productInformation check starts here
				
				List<String> sActualManufacturePartNumber= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathManufacturePartNumber);					
				String sExpectedManufacturePartNumber = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPMANUFACTUREPARTNUMBER);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualManufacturePartNumber, sExpectedManufacturePartNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation of Manufacturing Part Number["+sExpectedManufacturePartNumber+"]");
				
				//Sku mapping
				allMatch =false;
				List<String> sActualSku= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathsku);					
				String sExpectedSku = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSKU);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualSku, sExpectedSku);
				objAPIAssertion.assertTrue(allMatch, "Validation of Sku["+sExpectedSku+"]");
				
				//Approved Quantity
				allMatch =false;
				List<String> sActualApprovedQuantity= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathApprovedQuantity);					
				String sExpectedApprovedQuantity = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPAPPROVEDQUANTITY);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualApprovedQuantity, sExpectedApprovedQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation of Approved Quantity["+sExpectedApprovedQuantity+"]");
				
				//Remaing quantity
				allMatch =false;
				List<String> sActualRemainingQuantity= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathRemainingQuantity);					
				String sExpectedRemainingQuantity = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPREMAININGQUANTITY);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualRemainingQuantity, sExpectedRemainingQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation of Remaining Quantity["+sExpectedRemainingQuantity+"]");
						
				//List Price
				allMatch =false;
				List<String> sActualListPrice= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathListPrice);					
				String sExpectedListPrice = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPLISTPRICE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualListPrice, sExpectedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation of List Price["+sExpectedListPrice+"]");
					
				//Distidiscount percentage
				allMatch =false;
				List<String> sActualDistiDiscountPercentage= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathDistiDiscountPercentage);					
				String sExpectedDistiDiscountPercentage = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPDISTIDISCOUNTPERCENTAGE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualDistiDiscountPercentage, sExpectedDistiDiscountPercentage);
				objAPIAssertion.assertTrue(allMatch, "Validation of Distidiscount Percentage["+sExpectedDistiDiscountPercentage+"]");
				
				
				//Extended List Price 
				allMatch =false;
				List<String> sActualExtendedListPrice= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathExtendedListPrice);					
				String sExpectedExtendedListPrice = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPEXTENDEDLISTPRICE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualExtendedListPrice, sExpectedExtendedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation of Extended List Price["+sExpectedExtendedListPrice+"]");
			
				//Acop Number
				allMatch =false;
				List<String> sActualAcopNumber= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathAcopNumber);					
				String sExpectedAcopNumber = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPACOPNUMBER);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualAcopNumber, sExpectedAcopNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation of Acop Number["+sExpectedAcopNumber+"]");
			
				//Acop ID
				allMatch =false;
				List<String> sActualAcopeuid= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathAcopeuid);					
				String sExpectedAcopeuid = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPACOPEUID);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualAcopeuid, sExpectedAcopeuid);
				objAPIAssertion.assertTrue(allMatch, "Validation of AcopID["+sExpectedAcopeuid+"]");
			
				//Zero Claim
				allMatch =false;
				List<String> sActualZeroClaim= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForCanadaAPIConstants.xpathZeroClaim);					
				String sExpectedZeroClaim = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPZEROCLAIM);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualZeroClaim, sExpectedZeroClaim);
				objAPIAssertion.assertTrue(allMatch, "Validation of Zero Claim["+sExpectedZeroClaim+"]");
		
		}
		catch(Exception e) {
			String sErrMessage="FAIL: CanadaValidateDartDetailsServicesResponse() method of DartDetailsServiceForCanadaAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForCanadaAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}
	
	public void CanadaInvalidDartDetails() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForCanadaAPITest.sFileName, hmTestData, DartDetailsServicesForCanadaAPITest.isMultiSet, DartDetailsServicesForCanadaAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: CanadaInvalidDartDetails() method of DartDetailsServiceForCanadaAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForCanadaAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void CanadaDartDetailsForBlankCountryCode() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForCanadaAPITest.sFileName, hmTestData, DartDetailsServicesForCanadaAPITest.isMultiSet, DartDetailsServicesForCanadaAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: CanadaDartDetailsForBlankCountryCode() method of DartDetailsServiceForCanadaAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForCanadaAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	

	public void CanadaDartDetailsForDifferentResellerAccount() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForCanadaAPITest.sFileName, hmTestData, DartDetailsServicesForCanadaAPITest.isMultiSet, DartDetailsServicesForCanadaAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: CanadaDartDetailsForDifferentResellerAccount() method of DartDetailsServiceForCanadaAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForCanadaAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}
	

	public void CanadaDartDetailsForExpiredDart() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForCanadaAPITest.sFileName, hmTestData, DartDetailsServicesForCanadaAPITest.isMultiSet, DartDetailsServicesForCanadaAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForCanadaAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForCanadaAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: CanadaDartDetailsForExpiredDart() method of DartDetailsServiceForCanadaAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForCanadaAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}


}
