package com.im.imonline.api.action;


import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.SearchAPIConstants;
import com.im.imonline.api.tests.OrderSearchAPITest;
import com.im.imonline.api.tests.SearchAPITest;

public class SearchAPIActionBucket {
	Logger logger = Logger.getLogger("SearchAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	APIAssertion objAPIAssertion = null;
	HashMap<String, String> hmConfig = null;
	

	public SearchAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void performStatusCodeValidation() throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(SearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, SearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);

			objJSONUtil.postRequest();
			objAPIAssertion.assertEquals(objJSONUtil.getStatusCode()+"", "200", "Status Code Validation");
			
		}catch(Exception e) {
			String sErrMessage="FAIL: performStatusCodeValidation() method of SearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			SearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });
		}

	}



	public void performDelearPriceValidation() throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(SearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, SearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);

			objJSONUtil.postRequest();
			String sActual=objJSONUtil.getActualValueFromJSONResponse(SearchAPIConstants.xpathDealerPrice);
			String sExpected = hmTestData.get(SearchAPIConstants.DEALERPRICE);
			objAPIAssertion.assertEquals(sActual, sExpected, "Dealer Price Validation ");

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performDelearPriceValidation() method of SearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			SearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });
		}

	}


	public void performTatier1Validation() throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(SearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, SearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);

			objJSONUtil.postRequest();
			String sActual=objJSONUtil.getActualValueFromJSONResponse(SearchAPIConstants.xpathTatier1);
			String sExpected = hmTestData.get(SearchAPIConstants.TATIER1);
			objAPIAssertion.assertEquals(sActual, sExpected, "TaTier1 Validation");

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performTatier1Validation() method of SearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			SearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });
		}

	}
		
	
		
	}//End of class
