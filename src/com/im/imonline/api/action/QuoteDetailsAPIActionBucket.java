package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.QuoteDetailsServicesAPIConstants;
import com.im.imonline.api.tests.QuoteDetailsServicesAPITest;

public class QuoteDetailsAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("QuoteDetailsAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public QuoteDetailsAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validateMandatoryFieldsForQuoteDetails() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(QuoteDetailsServicesAPITest.sFileName, hmTestData, QuoteDetailsServicesAPITest.isMultiSet, QuoteDetailsServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();			
			
			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathStatusCode);
			String sExpectedExpStatus = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Response Status  ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathResponsemessage);
			String sExpectedExpResponseMessage = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPRESPONSEMESSAGE);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " verify Response Message   ");

			String sActualExpQuoteNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathQuoteNumber);
			String sExpectedExpQuoteNumber = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPQUOTENUMBER);
			objAPIAssertion.assertEquals(sActualExpQuoteNumber, sExpectedExpQuoteNumber, " verify QuoteNumber  ");

			String sActualExpAccountNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathAccount);
			String sExpectedExpAccountNumber = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPACCOUNT);
			objAPIAssertion.assertEquals(sActualExpAccountNumber, sExpectedExpAccountNumber, " verify Customer Account Number");
			
			String sActualExpQuotePrice = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathQuotePrice);
			String sExpectedExpQuotePrice = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPQUOTEPRICE);
			objAPIAssertion.assertEquals(sActualExpQuotePrice, sExpectedExpQuotePrice, " verify QuotePrice");
			
	
		}catch(Exception e) {
			String sErrMessage="FAIL: validateMandatoryFieldsForQuoteDetails() method of QuoteDetailsAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			QuoteDetailsServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}

	public void validateBlankQuotenumber() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
		sRequestBody = AppUtil.getRequestBodyForRestRequest(QuoteDetailsServicesAPITest.sFileName, hmTestData, QuoteDetailsServicesAPITest.isMultiSet, QuoteDetailsServicesAPITest.TestDataFile);			
		objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
		objJsonUtil.postRequest();


		String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
		objAPIAssertion.assertHardEquals(sActualResponseCode, "400", "Status Code Validation");

		String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathStatusCode);
		String sExpectedExpStatus = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPSTATUSCODE);
		objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");

		String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathResponseStatus);
		String sExpectedExpResponseStatus = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPRESPONSESTATUS);
		objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Response Status  ");

		String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathResponsemessage);
		String sExpectedExpResponseMessage = hmTestData.get(QuoteDetailsServicesAPIConstants.EXPRESPONSEMESSAGE);
		objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " verify Response Message  ");
		
		
		}
		
		
		catch(Exception e) {
			String sErrMessage="FAIL: ValidateBlankQuotenumber() method of QuoteCreateandProductlinesAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			QuoteDetailsServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}


}

