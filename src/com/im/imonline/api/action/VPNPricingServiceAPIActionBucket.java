package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.VPNPricingAPIValidation;
import com.im.imonline.api.constants.VPNPricingServicesAPIConstants;
import com.im.imonline.api.tests.VPNPricingServicesAPITest;



public class VPNPricingServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("VPNPricingServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;

	public VPNPricingServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void ValidateVPNPricingWithManditoryFields() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		try {

			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORVALID;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");

		
			String sGetVendorQuantities = hmTestData.get(VPNPricingServicesAPIConstants.VENDPARTANDQUANTITY);				
			String[] sVendorQuantitiesList = sGetVendorQuantities.split(",");

			//Validation for No of record Found
			int iRecordCount = sVendorQuantitiesList.length;
			String sExpectedRecordCount  = Integer.toString(iRecordCount);
			String sActualRecordCount = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathRecordCount);
			objAPIAssertion.assertEquals(sActualRecordCount, sExpectedRecordCount, "Validation For Record Count");

			//Verification for Quantities

			List<String> sVendorQuantiti = new ArrayList<>();
			for(String  sVendorQuanti: sVendorQuantitiesList)
			{
				String sNew = sVendorQuanti.substring(sVendorQuanti.indexOf("~")+1);
				sVendorQuantiti.add(sNew);	
			}

			List<String> sGetResVendorQuantiti = objJsonUtil.getListofStringsFromJSONResponse(VPNPricingServicesAPIConstants.xpathQuantities);
			objAPIAssertion.assertHardTrue(sVendorQuantiti.size()==sGetResVendorQuantiti.size(),"Checking of count for Quantities matches");
			int max = sGetResVendorQuantiti.size() > sVendorQuantiti.size() ? sGetResVendorQuantiti.size() : sVendorQuantiti.size();
			for(int i = 0; i < max; i++) 
			{
				String sActualVendorQuantities = String.valueOf(sGetResVendorQuantiti.get(i));
				String sExpectedVendorQuantities = String.valueOf(sVendorQuantiti.get(i));
				
				objAPIAssertion.assertHardTrue(sActualVendorQuantities.equals(sExpectedVendorQuantities), "Verification for Quantity["+sExpectedVendorQuantities.toString().trim()+"]");
			}	

			//Verification for vpnCategory

			String sGetVPNCategory = hmTestData.get(VPNPricingServicesAPIConstants.VPNCATEGORY);				
			String[] sVPNCategory = sGetVPNCategory.split(",");

			List<String> sVPNCatagoryList = new ArrayList<>();
			for(String  sSepVPNCategory: sVPNCategory)
			{
				sVPNCatagoryList.add(sSepVPNCategory);	
			}

			List<String> sResVPNCategory = objJsonUtil.getListofStringsFromJSONResponse(VPNPricingServicesAPIConstants.xpathVPNCatagory);

			objAPIAssertion.assertHardTrue(sVPNCatagoryList.size()==sResVPNCategory.size(),"Checking for VPn catagory List Equals");
			int max1 = sResVPNCategory.size() > sVPNCatagoryList.size() ? sResVPNCategory.size() : sVPNCatagoryList.size();
			for(int i = 0; i < max1; i++) 
			{
				String sActualVPNCatagorey = String.valueOf(sResVPNCategory.get(i));
				String sExpectedVPNCatagorey = String.valueOf(sVPNCatagoryList.get(i));
				objAPIAssertion.assertHardTrue(sActualVPNCatagorey.equals(sExpectedVPNCatagorey), "Verification for VPN Category["+sExpectedVPNCatagorey.toString().trim()+"]");
				
			}
			
			// Verification for Vendor Part Number
			String sGetVendorPartNumber = hmTestData.get(VPNPricingServicesAPIConstants.VENDPARTANDQUANTITY);				
			String[] sVendorPartNumberList = sGetVendorPartNumber.split(",");

			List<String> sVendorPartNumber = new ArrayList<>();
			for(String  sVendorPartNum: sVendorPartNumberList)
			{
				String sNew = sVendorPartNum.substring(0,sVendorPartNum.indexOf("~"));
				sVendorPartNumber.add(sNew);	
			}
			List<String> sGetResVendorPartNumber = objJsonUtil.getListofStringsFromJSONResponse(VPNPricingServicesAPIConstants.xpathVendorPartNumber);
			objAPIAssertion.assertHardTrue(sVendorPartNumber.size()==sGetResVendorPartNumber.size(),"Checking forVendor Part Quantities Equals");

			int max2 = sGetResVendorPartNumber.size() > sVendorPartNumber.size() ? sGetResVendorPartNumber.size() : sVendorPartNumber.size();
			for(int i = 0; i < max2; i++) 
			{
				String sActualVendorPartNumber = String.valueOf(sGetResVendorPartNumber.get(i));
				String sExpectedVendorPartNumber = String.valueOf(sVendorPartNumber.get(i));
				objAPIAssertion.assertHardTrue(sActualVendorPartNumber.equals(sExpectedVendorPartNumber), "Verification for Vendor Part Number["+sExpectedVendorPartNumber.toString().trim()+"]");	
			}

			//Verify Disti Discount Response .

			List<String> sResDistiDiscount = objJsonUtil.getListofStringsFromJSONResponse(VPNPricingServicesAPIConstants.xpathresellerDiscountPercent);
			int iValue = sResDistiDiscount.size() ;
			for(int i = 0; i < iValue; i++) 
			{
				String sADistiDiscount = String.valueOf(sResDistiDiscount.get(i));
				double iDiscount = Double.parseDouble(sADistiDiscount);
				double iDiscountCont = iDiscount-3;
				String sActualDistiDiscount = Double.toString(iDiscountCont);
				objAPIAssertion.assertHardTrue(sActualDistiDiscount!=null, "Verification for Reseller Disti Discount["+sActualDistiDiscount.toString().trim()+"]");
			}

			//Verify List Price  Response .

			String sGetListPrice = hmTestData.get(VPNPricingServicesAPIConstants.LISTPRICE);				
			String[] sListPrice = sGetListPrice.split(",");

			List<String> sListPriceList = new ArrayList<>();
			for(String  sSepListPrice: sListPrice)
			{
				sListPriceList.add(sSepListPrice);	
			}
			List<String> sResListPrice = objJsonUtil.getListofStringsFromJSONResponse(VPNPricingServicesAPIConstants.xpathlistPrice);

			objAPIAssertion.assertHardTrue(sListPriceList.size()==sResListPrice.size(),"Checking for List Price Matches");

			int max4 = sResListPrice.size() > sListPriceList.size() ? sResListPrice.size() : sListPriceList.size();
			for(int i = 0; i < max4; i++) 
			{
				String sActualListPrice = String.valueOf(sResListPrice.get(i));
				String sExpectedListPrice = String.valueOf(sListPriceList.get(i));
				objAPIAssertion.assertHardTrue(sActualListPrice.equals(sExpectedListPrice), "Verification for List Price["+sExpectedListPrice.toString().trim()+"]");	
			}
		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingWithManditoryFields() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void ValidateVPNPricingByPassingBlankCustomerID() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;

		try {

			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORERROR;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");


			VPNPricingAPIValidation objVPNPricingValidation = new VPNPricingAPIValidation(hmTestData,objAPIAssertion);
			List<HashMap<String, String>>lstGetVPNRes = objJsonUtil.getListofHashMapFromJSONResponse(VPNPricingServicesAPIConstants.xpathError);
			objVPNPricingValidation.ErrorCheckForVPNPricing(lstGetVPNRes);								

		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingByPassingBlankCustomerID() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void ValidateVPNPricingByPassingBlankVPNCategory() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;

		try {

			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORERROR;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");



			VPNPricingAPIValidation objVPNPricingValidation = new VPNPricingAPIValidation(hmTestData,objAPIAssertion);
			List<HashMap<String, String>>lstGetVPNRes = objJsonUtil.getListofHashMapFromJSONResponse(VPNPricingServicesAPIConstants.xpathError);
			objVPNPricingValidation.ErrorCheckForVPNPricing(lstGetVPNRes);

		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingByPassingBlankVPNCategory() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void ValidateVPNPricingByPassingBlankListPrice() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;

		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORERROR;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");



			VPNPricingAPIValidation objVPNPricingValidation = new VPNPricingAPIValidation(hmTestData,objAPIAssertion);
			List<HashMap<String, String>>lstGetVPNRes = objJsonUtil.getListofHashMapFromJSONResponse(VPNPricingServicesAPIConstants.xpathError);
			objVPNPricingValidation.ErrorCheckForVPNPricing(lstGetVPNRes);

		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingByPassingBlankListPrice() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void ValidateVPNPricingByPassingBlankDistiDiscount() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;

		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORERROR;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");


			VPNPricingAPIValidation objVPNPricingValidation = new VPNPricingAPIValidation(hmTestData,objAPIAssertion);
			List<HashMap<String, String>>lstGetVPNRes = objJsonUtil.getListofHashMapFromJSONResponse(VPNPricingServicesAPIConstants.xpathError);
			objVPNPricingValidation.ErrorCheckForVPNPricing(lstGetVPNRes);

		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingByPassingBlankDistiDiscount() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}


	public void ValidateVPNPricingByPassingBlankVendorPartNoandQty() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;

		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(VPNPricingServicesAPITest.sFileName, hmTestData, VPNPricingServicesAPITest.isMultiSet, VPNPricingServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(VPNPricingServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus = VPNPricingServicesAPIConstants.EXPSTATUSFORERROR;
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");


			VPNPricingAPIValidation objVPNPricingValidation = new VPNPricingAPIValidation(hmTestData,objAPIAssertion);
			List<HashMap<String, String>>lstGetVPNRes = objJsonUtil.getListofHashMapFromJSONResponse(VPNPricingServicesAPIConstants.xpathError);
			objVPNPricingValidation.ErrorCheckForVPNPricing(lstGetVPNRes);


		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateVPNPricingByPassingBlankVendorPartNoandQty() method of VPNPricingServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			VPNPricingServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
}

