package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.XMLUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.ProductLookUpAPIConstants;
import com.im.imonline.api.tests.OrderSearchAPITest;
import com.im.imonline.api.tests.ProductLookUpAPITest;


public class ProductLookUpAPIActionBucket {
	Logger logger = Logger.getLogger("ProductLookUpAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	APIAssertion objAPIAssertion = null;
	HashMap<String, String> hmConfig = null;
	

	public ProductLookUpAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void performStatusCodeValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);
			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);
			objXMLUtil.postRequest();
			objAPIAssertion.assertEquals(objXMLUtil.getStatusCode()+"","200","Status Code Validation");						
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performStatusCodeValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}

	}


	public void performCredentialValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);

			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);	
			objXMLUtil.postRequest();	

			String sActualFaultString = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagFaultString);
			String sExpectedFaultString = hmTestData.get(ProductLookUpAPIConstants.FAULTSTRING);
			objAPIAssertion.assertEquals(sActualFaultString,sExpectedFaultString,"Fault String Validation");

			String sActualFaultCode = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagFaultCode);
			String sExpectedFaultCode = hmTestData.get(ProductLookUpAPIConstants.FAULTCODE);
			objAPIAssertion.assertEquals(sActualFaultCode, sExpectedFaultCode, "Fault Code Validation");

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performCredentialValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}	
	}
	
	public void performCompanyCodeValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);

			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);	
			objXMLUtil.postRequest();	
			String sActualFaultString = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagFaultString);
			String sExpectedFaultString = hmTestData.get(ProductLookUpAPIConstants.FAULTSTRING);

			if(!(sActualFaultString.trim().equalsIgnoreCase("")))//Code to validate Invalid Company Code 
				objAPIAssertion.assertEquals(sActualFaultString,sExpectedFaultString,"Fault String for Invalid Company Code");
			else
			{ //Code to validate Valid Company Code
				String sActualReturnMessage = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagReturnMessage);
				String sExpectedReturnMessage = hmTestData.get(ProductLookUpAPIConstants.RETURNMESSAGE);
				objAPIAssertion.assertEquals(sActualReturnMessage,sExpectedReturnMessage, "Return Message for Valid Company Code");
				objAPIAssertion.assertEquals(objXMLUtil.getStatusCode()+"", "200", "Status Code for Valid Company Code");
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performCompanyCodeValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}		
	}


	public void performCustomerNumberValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
		    sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);

			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);	
			objXMLUtil.postRequest();	

			String sActualFaultString = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagReturnMessage);
			String sExpectedFaultString = hmTestData.get(ProductLookUpAPIConstants.RETURNMESSAGE);
			objAPIAssertion.assertEquals(sActualFaultString,sExpectedFaultString,"Return Message for Cutomer Number Validation");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performCustomerNumberValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}	
	}
	

	public void performIngramPartNumberValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);

			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);	
			objXMLUtil.postRequest();	

			//Return Message Validation
			String sActualFaultString = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagReturnMessage);
			String sExpectedFaultString = hmTestData.get(ProductLookUpAPIConstants.RETURNMESSAGE);
			objAPIAssertion.assertEquals(sActualFaultString,sExpectedFaultString,"Return Message for Ingram Part Number Validation");

			//UPC Code validation for corresponding Part Number
			String sActualUPCValue = objXMLUtil.getXMLNodeValByXPath(ProductLookUpAPIConstants.xpathUPCCode);
			String sExpectedUPCValue = hmTestData.get(ProductLookUpAPIConstants.UPCCODE);
			objAPIAssertion.assertEquals(sActualUPCValue,sExpectedUPCValue,"UPC Code for Given Ingram Part Number");		
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performIngramPartNumberValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}	
	}


	public void performMFGPartNumberValidation() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ProductLookUpAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, ProductLookUpAPITest.TestDataFile);

			objXMLUtil =  new XMLUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);	
			objXMLUtil.postRequest();	
			//Return Message Validation
			String sActualFaultString = objXMLUtil.getNodeValueByTagName(ProductLookUpAPIConstants.tagReturnMessage);
			String sExpectedFaultString = hmTestData.get(ProductLookUpAPIConstants.RETURNMESSAGE);
			objAPIAssertion.assertEquals(sActualFaultString,sExpectedFaultString,"Return Message for MFG Part Number Validation");

			//MFG Part Number validation for in Part Number Tag
			String sActualUPCValue = objXMLUtil.getXMLNodeValByXPath(ProductLookUpAPIConstants.xpathMFGPartNum);
			String sExpectedUPCValue = hmTestData.get(ProductLookUpAPIConstants.MFGPartNum);
			objAPIAssertion.assertEquals(sActualUPCValue,sExpectedUPCValue,"MFG Part Number in Part Number of response");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performMFGPartNumberValidation() method of ProductLookUpAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductLookUpAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });			
		}		
	}

	
		
	}// End of Class
