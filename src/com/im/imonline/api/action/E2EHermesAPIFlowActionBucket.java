package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.HermesPnAAPIValidation;
import com.im.api.validation.OrderCreateAPIValidation;
import com.im.api.validation.OrderDeleteAPIValidation;
import com.im.api.validation.OrderDetailsAPIValidation;
import com.im.api.validation.OrderModifyAPIValidation;
import com.im.api.validation.OrderSimulateAPIValidation;
import com.im.imonline.api.business.APIEnumerations.CarrierOptions;
import com.im.imonline.api.business.APIEnumerations.OrderProcessOptionsForHeldOrder;
import com.im.imonline.api.business.APIEnumerations.ShipToAndBillToAddressOptions;
import com.im.imonline.api.business.HermesE2EFlowRequestConfig;
import com.im.imonline.api.constants.HermesAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;


public class E2EHermesAPIFlowActionBucket {
	Logger logger = Logger.getLogger("E2EHermesAPIFlowActionBucket"); 
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	HermesE2EFlowRequestConfig objReqConfig=null;
	HermesPnAAPIValidation objPnAVal=null;
	OrderSimulateAPIValidation objOrderQVal=null; 
	OrderCreateAPIValidation objOrderCrVal=null;
	OrderDetailsAPIValidation objOrderDetVal=null;
	OrderDeleteAPIValidation objOrderDelVal=null;
	OrderModifyAPIValidation objOrderModVal=null;

	public E2EHermesAPIFlowActionBucket(HermesE2EFlowRequestConfig objConfig2) {
		this.objReqConfig = objConfig2;
		this.objAPIAssertion = ToolAPI.getAPIAssertion(objConfig2.getObjAPIDriver());
		this.objPnAVal=new HermesPnAAPIValidation(objAPIAssertion, objConfig2);
		this.objOrderQVal=new OrderSimulateAPIValidation(objAPIAssertion, objConfig2);
		this.objOrderCrVal=new OrderCreateAPIValidation(objAPIAssertion, objConfig2);
		this.objOrderDetVal=new OrderDetailsAPIValidation(objAPIAssertion, objConfig2);
		this.objOrderDelVal=new OrderDeleteAPIValidation(objAPIAssertion, objConfig2);
		this.objOrderModVal=new OrderModifyAPIValidation(objAPIAssertion, objConfig2);
	}

	/**
	 * @author Vinita Lakhwani
	 * This method takes care of the entire Hermes Automation FLow 
	 * apart of Hold Order scenarios
	 * Hermes API Automation
	 */
	public void submitOrderAndVerifyOrderDetails() throws Exception {
		try {
			objReqConfig.setPnAReqParameters();

			if(objReqConfig.isDataCaptured())	// used to check if the data is captured for a particular product type - INSTOCK, ESD, DIRECTSHIP... 
			{
				JsonUtil objJSONUtilPnA = hitConfigReqAsPerMultisetParam(true);
				objPnAVal.performValidationforGivenConfig(objJSONUtilPnA);			//validate P&A Response
				objReqConfig.setOrderSimulateReqParameters(objJSONUtilPnA);			//use P&A response to set some fields in OrderSimultate request
				if(objReqConfig.isDataCaptured())	// used to check if the data captured has stock in multiple warehouse for warehouse split
				{
					JsonUtil objJSONUtilOrdrSim = hitConfigReqAsPerMultisetParam(true);
					//TODO: VL: Add Retry logic
					String resStatusField=objJSONUtilOrdrSim.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode");
					int i=0;
					while(i<3 && !(resStatusField.equals("200")))
						{
						objAPIAssertion.setExtentInfo("Attempt no :"+i+1+" for Order Simulate");
						objJSONUtilOrdrSim = hitConfigReqAsPerMultisetParam(true);
						resStatusField=objJSONUtilOrdrSim.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode");
						i++;
						}
					objOrderQVal.performValidationforGivenConfig(objJSONUtilOrdrSim);	//validate OrderSimulate Response
					objReqConfig.setOrderCreateReqParameters(objJSONUtilOrdrSim);		//use OrderSimulate response to set some fields in OrderCreate request
					if(objReqConfig.isDataCaptured())	// used to check if the data captured has different carriers available for selecting non-default carrier
					{
						JsonUtil objJSONUtilOrCreate = hitConfigReqAsPerMultisetParam(true);
						objOrderCrVal.performValidationforGivenConfig(objJSONUtilOrCreate);		//validate OrderCreate Response
						List<Object> lstLinessResponseData=objJSONUtilOrCreate.getResponseFromJayway(objJSONUtilOrCreate,"$..lines");
						List<HashMap<String, Object>> lstLinesResData= (List<HashMap<String, Object>>) lstLinessResponseData.get(0);  //fetch Order Lines from Order create response to use each line order created in Order Details one by one
						
						JsonUtil objJSONUtilOrDets; 
						for(HashMap<String, Object> lines : lstLinesResData)
						{
							objReqConfig.setOrderDetailsReqParameters(lines,objJSONUtilOrCreate);	//use each line to create each OrderDetails Request
							objJSONUtilOrDets=hitConfigReqAsPerMultisetParam(false);
							//TODO: VL: Add Retry logic
							int j=0;
							while(j<3 && !("200".equals(objJSONUtilOrDets.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode"))))
							{
								objAPIAssertion.setExtentInfo("Attempt no :"+j+1+" for Order Details");
								objJSONUtilOrDets = hitConfigReqAsPerMultisetParam(false);
								j++;
							}
							
							objOrderDetVal.performValidationforGivenConfig(objJSONUtilOrDets); //validate each OrderDetails Response
						}
					}
					else
						objAPIAssertion.setExtentSkip("Data not available for non default delivery service");
				}
				else
					objAPIAssertion.setExtentSkip("Data not captured for Split Warehouse flow");
			}
			else
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
		} catch (Exception e) {
			String sErrMessage="FAIL: submitOrderAndVerifyOrderDetails() method of E2EHermesAPIFlowActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	
	private JsonUtil hitConfigReqAsPerMultisetParam(boolean isMultiset) {
		String sRequestBody= new String();
		HashMap<String, String> hmTestData =objReqConfig.getHmTestData();
		List<List<HashMap<String, String>>> ListMapMultipleSetOfSKUData = objReqConfig.getListMapMultipleSetOfSKUData();
		JsonUtil objJSONUtil = null;
		try {
			if(isMultiset)
				sRequestBody = AppUtil.getRequestBodyForMultiSetRestRequestWithTestDataUtil(hmTestData.get(HermesAPIConstants.APITEMPLATE),hmTestData,ListMapMultipleSetOfSKUData);
			else
				sRequestBody = AppUtil.getRequestBodyForRestRequest(hmTestData.get(HermesAPIConstants.APITEMPLATE),hmTestData,false, ProductSearchElasticAPITest.TestDataFile);
			objJSONUtil  = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
			logger.info("Request body after logic :"+objJSONUtil.getsRequestBody());
			objAPIAssertion.setExtentInfo("Request body after logic :"+objJSONUtil.getsRequestBody());
			
			objJSONUtil.postRequest();
			objAPIAssertion.setExtentInfo("The Response status code is: "+objJSONUtil.getStatusCode());
			String jsonRes=objJSONUtil.getResponse().asString();
			logger.info(jsonRes);
			if(!objJSONUtil.getActualValueFromJSONResponseWithoutModify("serviceresponse.responsepreamble.statuscode").equals("200"))
				objAPIAssertion.setExtentInfo("The failed response is: "+"\""+jsonRes+"\"");			
		}
		catch(Exception e) {
			String sErrMessage="FAIL: hitConfigReqAsPerMultisetParam() method of E2EHermesAPIFlowActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);
		}
		return objJSONUtil;			
	}


	/**
	 * @author Vinita Lakhwani
	 * This method takes care of the entire Hermes Automation FLow 
	 * for Hold Order scenarios
	 * Hermes API Automation
	 */
	public void holdOrderAndProcessPerOrderProcessOptionForHeldOrder() throws Exception {
		try {
			objReqConfig.setPnAReqParameters();

			if(objReqConfig.isDataCaptured())	// used to check if the data is captured for a particular product type - INSTOCK, ESD, DIRECTSHIP...
			{
				JsonUtil objJSONUtilPnA = hitConfigReqAsPerMultisetParam(true);
				objPnAVal.performValidationforGivenConfig(objJSONUtilPnA);
				objReqConfig.setOrderSimulateReqParameters(objJSONUtilPnA);

				if(objReqConfig.isDataCaptured())	// used to check if the data captured has stock in multiple warehouse for warehouse split
				{
					JsonUtil objJSONUtilOrdrSim = hitConfigReqAsPerMultisetParam(true);
					objOrderQVal.performValidationforGivenConfig(objJSONUtilOrdrSim);
					objReqConfig.setOrderCreateReqParameters(objJSONUtilOrdrSim);

					if(objReqConfig.isDataCaptured())	// used to check if the data captured has different carriers available for selecting non-default carrier
					{
						JsonUtil objJSONUtilOrCreate = hitConfigReqAsPerMultisetParam(true);
						objOrderCrVal.performValidationforGivenConfig(objJSONUtilOrCreate);

						List<Object> lstLinessResponseData=objJSONUtilOrCreate.getResponseFromJayway(objJSONUtilOrCreate,"$..lines");
						List<HashMap<String, Object>> lstLinesResData= (List<HashMap<String, Object>>) lstLinessResponseData.get(0);
						for(HashMap<String, Object> lines : lstLinesResData)
						{
							objReqConfig.setOrderDetailsReqParameters(lines,objJSONUtilOrCreate);
							JsonUtil objJSONUtilOrDetails=hitConfigReqAsPerMultisetParam(false);
							objOrderDetVal.performValidationforGivenConfig(objJSONUtilOrDetails);

							//after validating the held order details,cancel/release/release with modification
							//if(!objReqConfig.getHermesRegisterForTestCase().getScenarioForHoldOrder().equals("NONE"))
								switch(objReqConfig.getHermesRegisterForTestCase().getScenarioForHoldOrder())
								{
								case "CANCEL":
									//hit orderdelete for held order
									objReqConfig.setOrderDeleteReqParameters(objJSONUtilOrDetails);
									JsonUtil objJSONUtilOrDelete=hitConfigReqAsPerMultisetParam(false);
									objOrderDelVal.performValidationforGivenConfig(objJSONUtilOrDelete);
									break;

								case "RELEASEWITHOUTMODIFY":
									//hit order create to release order without modification and validate newly created order details
									objReqConfig.getHermesRegisterForTestCase().setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.NONE);
									objReqConfig.setOrderCreateReqParameters(objJSONUtilOrdrSim);	
									if(objReqConfig.isDataCaptured())
									{
										JsonUtil objJSONUtilOrCreate1 = hitConfigReqAsPerMultisetParam(true);
										objOrderCrVal.performValidationforGivenConfig(objJSONUtilOrCreate1);
										List<Object> lstLinessResponseData1=objJSONUtilOrCreate.getResponseFromJayway(objJSONUtilOrCreate1,"$..lines");
										List<HashMap<String, Object>> lstLinesResData1= (List<HashMap<String, Object>>) lstLinessResponseData1.get(0);
										for(HashMap<String, Object> lines1 : lstLinesResData1)
										{
											objReqConfig.setOrderDetailsReqParameters(lines1,objJSONUtilOrCreate1);
											JsonUtil objJSONUtilOrDetails1=hitConfigReqAsPerMultisetParam(false);
											objOrderDetVal.performValidationforGivenConfig(objJSONUtilOrDetails1);
										}
									}
									break;

								case "MODIFYSHIPPINGADDRESSANDRELEASE":
									//hit order modify with modification, create order and validate order details
									objReqConfig.getHermesRegisterForTestCase().setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.NONE);
									objReqConfig.getHermesRegisterForTestCase().setShipToAndBillToAddressType(ShipToAndBillToAddressOptions.SELECT_DIFFERENT_SHIPTO);
									objReqConfig.setOrderModifyReqParameters(objJSONUtilOrDetails);
									JsonUtil objJSONUtilOrModify=hitConfigReqAsPerMultisetParam(true);
									objOrderModVal.performValidationforGivenConfig(objJSONUtilOrModify);
									runOrderSimulateToOrderDetailsJourney(objJSONUtilOrModify);
									break;

								case "MODIFYCARRIERANDRELEASE":
									//hit order modify with modification, create order and validate order details
									objReqConfig.getHermesRegisterForTestCase().setScenarioForHoldOrder(OrderProcessOptionsForHeldOrder.NONE);
									objReqConfig.getHermesRegisterForTestCase().setDeliveryService(CarrierOptions.DIFFCHOICE);
									objReqConfig.setOrderModifyReqParameters(objJSONUtilOrDetails);
									JsonUtil objJSONUtilOrModify1=hitConfigReqAsPerMultisetParam(true);
									objOrderModVal.performValidationforGivenConfig(objJSONUtilOrModify1);
									runOrderSimulateToOrderDetailsJourney(objJSONUtilOrModify1);
									break;

								}
						}
					}
				}
			}
			else
				objAPIAssertion.setExtentSkip("Data not captured for the test case.");
		} catch (Exception e) {
			String sErrMessage="FAIL: holdOrderAndProcessPerOrderProcessOptionForHeldOrder() method of E2EHermesAPIFlowActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			logger.info("");
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
	}


	private void runOrderSimulateToOrderDetailsJourney(JsonUtil objJSONUtilOrModify) throws Exception {
		//rehit OrderSimulate for held Orders and Run the journey until Order Details
		objReqConfig.updateHashmapInfoForOrderSimulateRehit(objJSONUtilOrModify);
		JsonUtil objJSONUtilOrdrSim = hitConfigReqAsPerMultisetParam(true);
		objOrderQVal.performValidationforGivenConfig(objJSONUtilOrdrSim);
		objReqConfig.setOrderCreateReqParameters(objJSONUtilOrdrSim);
		if(objReqConfig.isDataCaptured())
		{
			JsonUtil objJSONUtilOrCreate = hitConfigReqAsPerMultisetParam(true);
			objOrderCrVal.performValidationforGivenConfig(objJSONUtilOrCreate);

			List<Object> lstLinessResponseData=objJSONUtilOrCreate.getResponseFromJayway(objJSONUtilOrCreate,"$..lines");
			List<HashMap<String, Object>> lstLinesResData= (List<HashMap<String, Object>>) lstLinessResponseData.get(0);
			for(HashMap<String, Object> lines : lstLinesResData)
			{
				objReqConfig.setOrderDetailsReqParameters(lines,objJSONUtilOrCreate);
				JsonUtil objJSONUtilOrDetails=hitConfigReqAsPerMultisetParam(false);
				objOrderDetVal.performValidationforGivenConfig(objJSONUtilOrDetails);
			}
		}
	}
}
