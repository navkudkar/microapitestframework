package com.im.imonline.api.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.DateTimeUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.OrderSearchAPIConstants;
import com.im.imonline.api.tests.OrderSearchAPITest;



public class OrderSearchAPIActionBucket<E> {
	Logger logger = Logger.getLogger("OrderSearchAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 


	public OrderSearchAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig ;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	public void performValidationForAvailableValues() throws Exception {
		JsonUtil objJSONUtil = new JsonUtil(sRequestBody, hmTestData, hmConfig);
		objJSONUtil=hitTheRequest();

		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ENDUSERPONUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ENDUSERPONUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performEndUserPONumValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERORDERNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERORDERNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performResellerPONumValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performCustomerNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INGRAMPARTNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INGRAMPARTNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performSKUnumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORPARTNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORPARTNUMBER).equalsIgnoreCase(Constants.BLANKVALUE))) 
			performVPNNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SERIALNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SERIALNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performSerialNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_UPCNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_UPCNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performUPCNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INVOICENUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INVOICENUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performInvoiceNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATEFROMDATE).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATEFROMDATE).equalsIgnoreCase(Constants.BLANKVALUE)||
				hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATETODATE).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATETODATE).equalsIgnoreCase(Constants.BLANKVALUE)))
			performOrderCreateDateValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATEFROMDATE).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATEFROMDATE).equalsIgnoreCase(Constants.BLANKVALUE)||
				hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATETODATE).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATETODATE).equalsIgnoreCase(Constants.BLANKVALUE)))
			performShipDateDateValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERSTATUSNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERSTATUSNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performOrderStatusValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORNAME).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORNAME).equalsIgnoreCase(Constants.BLANKVALUE)))
			performVendorNameValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERPARTNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERPARTNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performCustomerPartNumberValidation(objJSONUtil);
		if(!(hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SPECIALBIDNUMBER).equalsIgnoreCase("")||hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SPECIALBIDNUMBER).equalsIgnoreCase(Constants.BLANKVALUE)))
			performSpecialBidNumberValidation(objJSONUtil);
	}



	private JsonUtil hitTheRequest() throws Exception {
		JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, OrderSearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);		
			objJSONUtil.postRequest();
		}
		catch(Exception e) {
			String sErrMessage="FAIL: hitTheRequest() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}

		return objJSONUtil;

	}
	private void performOrderCreateDateValidation(JsonUtil objJSONUtil ) throws Exception	{
		boolean allMatch=false;

		try {
			SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
			String sExpectedFromDate =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATEFROMDATE);
			Date dOrderFromDate= sdf.parse(sExpectedFromDate);
			String sdayBefore=	DateTimeUtil.getDateMinusNoOfDays(dOrderFromDate, 1);
			Date dDayBeforeFromDate= sdf.parse(sdayBefore);

			String sExpectedToDate =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERCREATETODATE);
			Date dOrderToDate= sdf.parse(sExpectedToDate);	
			Date dDayAftertoDate= sdf.parse(DateTimeUtil.getDatePlusNoOfDays(dOrderToDate, 1));

			//System.out.println("From date is "+sExpectedFromDate+"  but changes to "+dDayBeforeFromDate.toString()+" \n to datee is "+sExpectedToDate+"and chnages to "+dDayAftertoDate.toString());
			List<E> sActualOrderCreateDate= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.ORDERCREATEDATE);	
			List<Date> dActualOrderCreateDate= new ArrayList<Date>();
			for(E date: sActualOrderCreateDate) {
				try {
					dActualOrderCreateDate.add(sdf.parse(date.toString()));
				}
				catch (ParseException e) {
					e.printStackTrace();
				}

			}
			allMatch=DateTimeUtil.verifyGivenDateFallsBetween2Dates(dActualOrderCreateDate, dDayBeforeFromDate, dDayAftertoDate);
			objAPIAssertion.assertTrue(allMatch, "Order Date PO number check for [ from:"+ sExpectedFromDate+" to Date:"+sExpectedToDate+"] ");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performOrderCreateDateValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performShipDateDateValidation(JsonUtil objJSONUtil ) throws Exception	{
		boolean allMatch=false;
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sFromDate =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATEFROMDATE);
			Date dFromDate= simpleDateFormat.parse(sFromDate);
			Date dDayBeforeFromDate=simpleDateFormat.parse(DateTimeUtil.getDateMinusNoOfDays(dFromDate, 1));

			String sToDate =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SHIPDATETODATE);
			Date dToDate= simpleDateFormat.parse(sToDate);
			Date dDayAfterToDate=simpleDateFormat.parse(DateTimeUtil.getDatePlusNoOfDays(dToDate, 1));

			List<E> sActualShipDateDate= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.SHIPDATE);	
			List<Date> dActualShipDateDate= new ArrayList<Date>();
			for(E date: sActualShipDateDate) {
				try {
					String sDate=date.toString();
					String newDate=sDate.substring(1, sDate.length()-1);
					dActualShipDateDate.add(simpleDateFormat.parse(newDate));
				}
				catch (ParseException e) {
					e.printStackTrace();
				}

			}
			allMatch=DateTimeUtil.verifyGivenDateFallsBetween2Dates(dActualShipDateDate, dDayBeforeFromDate, dDayAfterToDate);
			objAPIAssertion.assertTrue(allMatch, "Ship Date PO number check for [ from:"+ sFromDate+" to Date:"+sToDate+"] ");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performShipDateDateValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performVendorNameValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<E> sActual= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.VENDORNAME);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORNAME);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;

			objAPIAssertion.assertTrue(allMatch, "Vendor check passed as expetced was ["+sExpected+ "] and actual was"+sActual);

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performVendorNameValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performSpecialBidNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<E> sActual= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.SPECIALBIDNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SPECIALBIDNUMBER);
			allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			objAPIAssertion.assertTrue(allMatch, "Bid number check passed as expetced was ["+sExpected+ "] and actual was"+sActual);

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performSpecialBidNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performCustomerPartNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<E> sActual= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.CPN);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERPARTNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;

			objAPIAssertion.assertTrue(allMatch, "CustomerPart number check passed as expetced was ["+sExpected+ "] and actual was"+sActual);

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performCustomerPartNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performIMOrderNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List<E> sActual= objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.IMORDERNUM);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;

			objAPIAssertion.assertTrue(allMatch, "IM Order number check passed as expetced was ["+sExpected+ "] and actual was"+sActual);

		}
		catch(Exception e) {
			String sErrMessage="FAIL: performIMOrderNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performEndUserPONumValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.ENDUSERPONUM);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ENDUSERPONUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE)))
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;

			objAPIAssertion.assertTrue(allMatch, "End User PO number checked for ["+ sExpected+"]and actual is "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performEndUserPONumValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performResellerPONumValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch=false;
		try {
			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.CUSTOMERORDERNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERORDERNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Reseller PO number check for ["+ sExpected+"] and actual is "+sActual);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResellerPONumValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(),objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performCustomerNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch = false;
		try {
			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.CUSTOMERNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_CUSTOMERNUMBER);
			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Customer number checked for ["+ sExpected+"] and result appeared as ["+sActual+"]");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performCustomerNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performSKUnumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch = false;
		try {

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.SKU);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INGRAMPARTNUMBER);
			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "SKU number checked for ["+ sExpected+"] and result appeared as ["+sActual+"] and also the count of order is "+objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING));
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performSKUnumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performVPNNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch = false;
		try {
			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.VPN);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_VENDORPARTNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "VPN number checked for ["+ sExpected+"] and result appeared as ["+sActual+"]");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performVPNNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performSerialNumberValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch = false;
		try {

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.SERIALNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_SERIALNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "SERIAL number checked for ["+ sExpected+"] and result appeared as ["+sActual+"]");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performSerialNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

	private void performOrderStatusValidation(JsonUtil objJSONUtil) throws Exception {
		boolean allMatch = false;
		try {

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.ORDERSTATUSNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_ORDERSTATUSNUMBER);

			allMatch=true;
			String exepected=sExpected.replace("\"", "");
			for(E strActual : sActual) {
				String s=strActual.toString();
				if(!(exepected.contains(s))) {
					allMatch=false;
					break;
				}}

			objAPIAssertion.assertTrue(allMatch, "status checked for ["+ sExpected+"] and result appeared as ["+sActual+"]");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performOrderStatusValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performUPCNumberValidation(JsonUtil objJSONUtil) throws Exception {

		boolean allMatch = false;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, OrderSearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);		
			objJSONUtil.postRequest();

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.UPCNUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_UPCNUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "UPC number checked for ["+ sExpected+"] and result appeared as ["+sActual+"] and also the count of order is "+objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING));
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performUPCNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}
	private void performInvoiceNumberValidation(JsonUtil obJsonUtil) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		boolean allMatch = false;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(OrderSearchAPITest.sFileName,hmTestData,OrderSearchAPITest.isMultiSet, OrderSearchAPITest.TestDataFile);
			objJSONUtil = new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData,hmConfig);		
			objJSONUtil.postRequest();

			List <E> sActual = objJSONUtil.getListofStringsFromJSONResponse(OrderSearchAPIConstants.INVOICENUMBER);	
			String sExpected =  hmTestData.get(OrderSearchAPIConstants.EXCELHEADER_INVOICENUMBER);

			if (!(sExpected.equalsIgnoreCase("")||sExpected.equalsIgnoreCase(Constants.BLANKVALUE))) 
				allMatch=Util.compareListValuesWithExpectedValue(sActual, sExpected);
			else
				allMatch = true;
			objAPIAssertion.assertTrue(allMatch, "Invoice number checked for ["+ sExpected+"] and result appeared as ["+sActual+"] and also the count of order is "+objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING));
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performInvoiceNumberValidation() method of OrderSearchAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderSearchAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString(), objJSONUtil.countTheOccuranceOfSpecifiedField(OrderSearchAPIConstants.ORDERBEGINING) });
		}
	}

}
