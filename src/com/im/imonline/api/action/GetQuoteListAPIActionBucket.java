package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.GetQuoteListServicesAPIConstants;
import com.im.imonline.api.constants.QuoteDetailsServicesAPIConstants;
import com.im.imonline.api.tests.GetQuoteListServicesAPITest;

public class GetQuoteListAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("GetQuoteListAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public GetQuoteListAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validateMandatoryFieldsForGetQuoteList() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(GetQuoteListServicesAPITest.sFileName, hmTestData, GetQuoteListServicesAPITest.isMultiSet, GetQuoteListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();
			
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);
			
			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathStatusCode);
			String sExpectedExpStatus = hmTestData.get(GetQuoteListServicesAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code is 200 ");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(QuoteDetailsServicesAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(GetQuoteListServicesAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Response Status  ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathResponsemessage);
			String sExpectedExpResponseMessage = hmTestData.get(GetQuoteListServicesAPIConstants.EXPRESPONSEMESSAGE);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " verify Response Message   ");

			String sActualExpQuoteNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathQuoteNumber);
			String sExpectedExpQuoteNumber = hmTestData.get(GetQuoteListServicesAPIConstants.EXPQUOTENUMBER);
			objAPIAssertion.assertEquals(sActualExpQuoteNumber, sExpectedExpQuoteNumber, " verify QuoteNumber  ");

			String sActualExpAccountNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathAccount);
			String sExpectedExpAccountNumber = hmTestData.get(GetQuoteListServicesAPIConstants.EXPACCOUNT);
			objAPIAssertion.assertEquals(sActualExpAccountNumber, sExpectedExpAccountNumber, " verify Customer Account Number");
			
			String sActualExpQuoteName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.XpathQuoteName);
			String sExpectedExpQuoteName = hmTestData.get(GetQuoteListServicesAPIConstants.EXPQUOTENAME);
			objAPIAssertion.assertEquals(sActualExpQuoteName, sExpectedExpQuoteName, " verify Quote Name");
			
			String sActualExpQuoteStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathStatus);
			String sExpectedExpQuoteStatus = hmTestData.get(GetQuoteListServicesAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpQuoteStatus, sExpectedExpQuoteStatus, " verify Quote Status ");
			
			String sActualExpQuoteStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathStatusReason);
			String sExpectedExpQuoteStatusReason = hmTestData.get(GetQuoteListServicesAPIConstants.EXPSTATUSREASON);
			objAPIAssertion.assertEquals(sActualExpQuoteStatusReason, sExpectedExpQuoteStatusReason, " verify Quote StatusReason ");

			String sActualExpDartNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathDartNumbar);
			String sExpectedExpDartNumber = hmTestData.get(GetQuoteListServicesAPIConstants.EXPDARTNUMBER);
			objAPIAssertion.assertEquals(sActualExpDartNumber, sExpectedExpDartNumber, " verify Dart Number ");

			String sActualExpEstimateId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathEstimateId);
			String sExpectedExpEstimateId = hmTestData.get(GetQuoteListServicesAPIConstants.EXPESTIMATEID);
			objAPIAssertion.assertEquals(sActualExpEstimateId, sExpectedExpEstimateId, " verify EstimateID  ");

			String sActualExpDealId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathDealId);
			String sExpectedExpDealId = hmTestData.get(GetQuoteListServicesAPIConstants.EXPDEALID);
			objAPIAssertion.assertEquals(sActualExpDealId, sExpectedExpDealId, " verify DealID  ");
		}
		catch(Exception e) {
			String sErrMessage="FAIL: ValidateManditoryFieldsForGetQuoteList() method of GetQuoteListAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			GetQuoteListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}

	public void validateBlankCustomerNumber() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(GetQuoteListServicesAPITest.sFileName, hmTestData, GetQuoteListServicesAPITest.isMultiSet, GetQuoteListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();


			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "400", "Status Code Validation");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathStatusCode);
			String sExpectedExpStatus = hmTestData.get(GetQuoteListServicesAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(GetQuoteListServicesAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Response Status  ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(GetQuoteListServicesAPIConstants.xpathResponsemessage);
			String sExpectedExpResponseMessage = hmTestData.get(GetQuoteListServicesAPIConstants.EXPRESPONSEMESSAGE);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " verify Response Message  ");


		}


		catch(Exception e) {
			String sErrMessage="FAIL: ValidateBlankCustomerNumber() method of GetQuoteListAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			GetQuoteListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}


}

