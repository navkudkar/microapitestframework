package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.DartListServicesAPIConstants;
import com.im.imonline.api.tests.DartListServicesAPITest;

//import com.sun.xml.bind.v2.schemagen.xmlschema.List;

public class DartListServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("DartListServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public DartListServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void ValidateDartListServiceResponseForManditoryFields() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			List<HashMap<String, String>> sDartListCount = objJsonUtil.getListofHashMapFromJSONResponse(DartListServicesAPIConstants.xpathDartListCount);
			int DartListCount = sDartListCount.size();
			String sActualDartListCount  = Integer.toString(DartListCount);
			String sExpectedDartListCount = hmTestData.get(DartListServicesAPIConstants.EXPDARTLISTCOUNT);
			objAPIAssertion.assertEquals(sActualDartListCount, sExpectedDartListCount, "Validation of Dart List Count");

			String sActualExpNumberOfResults = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartListServicesAPIConstants.xpathNumberOfResult);
			String sExpectedExpNumberOfResults = hmTestData.get(DartListServicesAPIConstants.EXPNUMBEROFRESULTS);
			objAPIAssertion.assertEquals(sActualExpNumberOfResults, sExpectedExpNumberOfResults, " Verify the Number of Result Display in Per Page ");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartListServicesAPIConstants.xpathStatus);
			String sExpectedExpStatus= hmTestData.get(DartListServicesAPIConstants.EXPSTATUS);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " Verify Dart List Status ");


		}catch(Exception e) {
			String sErrMessage="FAIL: ValidateDartListServiceResponseForManditoryFields() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void DartListServiceForDartExpirationDateBasedOnSorting(boolean isAscending) throws Exception {
		String sRequestBody=null,sSorting=null; JsonUtil objJsonUtil=null;
		try
		{
			sSorting = (isAscending)?"Ascending":"Descending";
			
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			List<String> sGetExpirationDate= objJsonUtil.getListofStringsFromJSONResponse(DartListServicesAPIConstants.xpathDartExpiration);	
			List<String> sExpiryDateWothoutTime = new ArrayList<>();
			for(String  sExpDate: sGetExpirationDate)
			{
				String sNew = sExpDate.substring(0,sExpDate.indexOf(" "));
				sExpiryDateWothoutTime.add(sNew);
			}
			
			List<String> lstExpirAscOrder= Util.getSortedDate(sExpiryDateWothoutTime, hmTestData.get("CountryCode"),isAscending);		
			objAPIAssertion.assertTrue(sExpiryDateWothoutTime.equals(lstExpirAscOrder), "Validation for DartExpirationDate In ["+sSorting+"] order");
			
		}catch(Exception e) {

			String sErrMessage="FAIL: DartListServiceForDartExpirationDateAscending() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void DartListServiceForDartExpirationDateDescending() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");


			List<String> sGetExpirationDateDesc= objJsonUtil.getListofStringsFromJSONResponse(DartListServicesAPIConstants.xpathDartExpiration);	
			List<String> sExpiryDateWothoutTime = new ArrayList<>();
			for(String  sExpDateDesc: sGetExpirationDateDesc)
			{
				String sNewDesc = sExpDateDesc.substring(0, sExpDateDesc.indexOf(" "));
				sExpiryDateWothoutTime.add(sNewDesc);
			}

			List<String> lstExpirDescOrder= Util.getSortedDate(sExpiryDateWothoutTime, hmTestData.get("CountryCode"), false);				
			objAPIAssertion.assertTrue(sExpiryDateWothoutTime.equals(lstExpirDescOrder), "Validation for DartExpirationDate In Descending order");

		}catch(Exception e) {
			String sErrMessage="FAIL: DartListServiceForDartExpirationDateDescending() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void DartListServiceForEndUserName() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch = false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			List<String> sActualEndUserName= objJsonUtil.getListofStringsFromJSONResponse(DartListServicesAPIConstants.xpathEndUserName);					
			String sExpectedEndUserName = hmTestData.get(DartListServicesAPIConstants.EXPENDUSERNAME);

			allMatch= Util.compareListOfValuesWithExpectedValue(sActualEndUserName, sExpectedEndUserName);
			objAPIAssertion.assertTrue(allMatch, "Verify End User Name["+sExpectedEndUserName+"]");


		}catch(Exception e) {
			String sErrMessage="FAIL: DartListServiceForEndUserName() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void DartListServiceForBlankCustomerNumber() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartListServicesAPIConstants.xpathStatusReason);
			String sExpectedExpStatusReason= hmTestData.get(DartListServicesAPIConstants.EXPSTATUSREASON);
			objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reasons for Blank Customer number ");

		}catch(Exception e) {
			String sErrMessage="FAIL: DartListServiceForBlankCustomerNumber() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void DartListServiceForDealId() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;boolean allMatch = false;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(sRequestBody, Object.class);
			sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			allMatch =false;
			List<String> sActualDealId= objJsonUtil.getListofStringsFromJSONResponse(DartListServicesAPIConstants.xpathDealId);					
			String sExpectedDealId = hmTestData.get(DartListServicesAPIConstants.EXPDEALID);

			allMatch= Util.compareListOfValuesWithExpectedValue(sActualDealId, sExpectedDealId);
			objAPIAssertion.assertTrue(allMatch, "Verify End User Name["+sExpectedDealId+"]");

		}catch(Exception e) {
			String sErrMessage="FAIL: DartListServiceForDealId() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void DartListServiceForBlankCountryCode() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartListServicesAPITest.sFileName, hmTestData, DartListServicesAPITest.isMultiSet, DartListServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");

			String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartListServicesAPIConstants.xpathStatusReason);
			String sExpectedExpStatusReason= hmTestData.get(DartListServicesAPIConstants.EXPSTATUSREASON);
			objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reasons for Blank Country code ");


		}catch(Exception e) {
			String sErrMessage="FAIL: DartListServiceForBlankCountryCode() method of DartListServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartListServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
}
