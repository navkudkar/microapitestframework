package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.ProductSearchElasticAPIValidation;
import com.im.imonline.api.business.ProductSearchRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;


public class ProductSearchElasticAPIActionBucket {
	Logger logger = Logger.getLogger("ProductSearchElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	ProductSearchRequestDataConfig objConfig = null;
	ProductSearchElasticAPIValidation objVal= null;
	 

	public ProductSearchElasticAPIActionBucket(APIDriver pAPIDriver, ProductSearchRequestDataConfig pObjConfig) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=pObjConfig;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new ProductSearchElasticAPIValidation(objAPIAssertion,objConfig);
	}
	
	/**
	 * @author Vinita Lakhwani
	 * performResponseValidation
	 * Method to be used for validation of Elastic Search API Response
	 */
	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSearchElasticAPITest.sFileName,hmTestData,ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.TestDataFile);
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData);
			objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
			objJSONUtil.postRequest();
			jsonRes=objJSONUtil.getResponse().asString();
			/*logger.info("TestCase : "+Thread.currentThread().getStackTrace()[2].getMethodName()+
						"\n******* Request For ["+hmTestData.get(Constants.ExcelHeaderRunConfig)+"] *******\n"+sRequestBody+
						"\n*******Response for Country"+hmTestData.get(Constants.ExcelHeaderRunConfig)+"*******\n"+" : "+jsonRes+
						"\n+++++++++++++***********************************+++++++++++++%%%%%***");*/
			objVal.performValidationforGivenConfig(objJSONUtil);
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of ProductSearchElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ProductSearchElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}

	}

}
