package com.im.imonline.api.action;

import java.sql.SQLException;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.ProductDetailsElasticAPIValidation;
import com.im.imonline.api.business.ProductDetailsRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductDetailsElasticAPITest;
import com.im.imonline.api.tests.ProductSearchElasticAPITest;

public class ProductDetailsElasticAPIActionBucket {
	Logger logger = Logger.getLogger("ProductDetailsElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	ProductDetailsRequestDataConfig objConfig = null;
	ProductDetailsElasticAPIValidation objVal = null;
	
	public ProductDetailsElasticAPIActionBucket(APIDriver pAPIDriver, ProductDetailsRequestDataConfig pObjConfig) throws SQLException {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig = pObjConfig;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new ProductDetailsElasticAPIValidation(objAPIAssertion,objConfig);
	}
	
	public void performResponseValidation() throws Exception {
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			if(objConfig.isDataAvailable()) {
				sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductDetailsElasticAPITest.sFileName,hmTestData,ProductSearchElasticAPITest.isMultiSet, ProductSearchElasticAPITest.TestDataFile);
				objJSONUtil = new JsonUtil(sRequestBody, hmTestData);
				objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
				objJSONUtil.postRequest();
				jsonRes=objJSONUtil.getResponse().asString();
				System.out.println("******* Response For ["+hmTestData.get(Constants.ExcelHeaderRunConfig)+"] *******\n"+jsonRes);
				objVal.performValidationforGivenConfig(objJSONUtil);
			} else
				objAPIAssertion.setExtentSkip("Data is not available for ["+objConfig.getFlagName()+"] product in ["+hmTestData.get("RunConfig")+"]");
		} catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of ProductDetailsElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		} finally {
            String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());               
            if(objConfig.isDataAvailable())
                ProductDetailsElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
        }
	}
}
