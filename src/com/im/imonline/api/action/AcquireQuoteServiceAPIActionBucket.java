package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.AcquireQuoteServiceAPIConstants;
import com.im.imonline.api.tests.AcquireQuoteServicesAPITest;

public class AcquireQuoteServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("QuoteDetailsAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public AcquireQuoteServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}



	public void validateMagicKeyandNetPriceProtectionDate() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(AcquireQuoteServicesAPITest.sFileName, hmTestData, AcquireQuoteServicesAPITest.isMultiSet, AcquireQuoteServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();			
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
			
			String sActualExpReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireQuoteServiceAPIConstants.xpathReason);
			String sExpectedExpReason = hmTestData.get(AcquireQuoteServiceAPIConstants.EXPREASON);
			objAPIAssertion.assertEquals(sActualExpReason, sExpectedExpReason, " verify Reason Status  ");
			
			String sActualExpMagicKey = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireQuoteServiceAPIConstants.xpathMagicKey);
			String sExpectedExpMagicKey = hmTestData.get(AcquireQuoteServiceAPIConstants.EXPMAGICKEY);
			objAPIAssertion.assertEquals(sActualExpMagicKey, sExpectedExpMagicKey, " verify MagicKey key ");

			String sActualExpNetPriceProtectionDate = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireQuoteServiceAPIConstants.xpathNetPriceProtectionDate);
			String sExpectedExpNetPriceProtectionDate = hmTestData.get(AcquireQuoteServiceAPIConstants.EXPNETPRICEPROTECTION);
			objAPIAssertion.assertEquals(sActualExpNetPriceProtectionDate, sExpectedExpNetPriceProtectionDate, " verify NetPrice ProtectionDate ");



		}catch(Exception e) {
			String sErrMessage="FAIL: validateMagicKeyandNetPriceProtectionDate() method of AcquireQuoteServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireQuoteServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}
	public void validatewithoutExpressionid() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {

			sRequestBody = AppUtil.getRequestBodyForRestRequest(AcquireQuoteServicesAPITest.sFileName, hmTestData, AcquireQuoteServicesAPITest.isMultiSet, AcquireQuoteServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();			
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);
			
			String sActualExpDescription = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireQuoteServiceAPIConstants.xpathDescription);
			String sExpectedExpDescription = hmTestData.get(AcquireQuoteServiceAPIConstants.EXPDESCRIPTION);
			objAPIAssertion.assertEquals(sActualExpDescription, sExpectedExpDescription, " verify Description ");

			String sActualExpID = objJsonUtil.getActualValueFromJSONResponseWithoutModify(AcquireQuoteServiceAPIConstants.xpathID);
			String sExpectedExpID = hmTestData.get(AcquireQuoteServiceAPIConstants.EXPID);
			objAPIAssertion.assertEquals(sActualExpID, sExpectedExpID, " verify ID ");

		}catch(Exception e) {
			String sErrMessage="FAIL: validatewithoutExpressionid() method of AcquireQuoteServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			AcquireQuoteServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}


}

