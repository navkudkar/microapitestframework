package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.cisco.validation.ListEstimateAPIExpectedData;
import com.im.imonline.api.constants.ListEstimateServicesAPIConstants;
import com.im.imonline.api.tests.ListEstimateServicesAPITest;

//import com.sun.xml.bind.v2.schemagen.xmlschema.List;

public class ListEstimateServiceAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("ListEstimateServiceAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	private static final String Expected_Valid_Status= "[VALID]";
	private static final String Expected_InValid_Status= "[INVALID]";
	private static final String Expected_StatusCode= "Success";

	
	public ListEstimateServiceAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	public void ValidateListEstimateServiceResponseForManditoryFields() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ListEstimateServicesAPITest.sFileName, hmTestData, ListEstimateServicesAPITest.isMultiSet, ListEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sCountryCode = hmConfig.get(Constants.ExcelHeaderRunConfig);
				ListEstimateAPIExpectedData ExpectedData = new ListEstimateAPIExpectedData(sCountryCode);
				
				
				List<String> sActualStatusCode= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathStatusCode);					
				String sExpectedStatusCode = Expected_StatusCode;		
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusCode, sExpectedStatusCode);
				objAPIAssertion.assertTrue(allMatch, "Validation For Status Code["+sExpectedStatusCode+"]");
				
				
				List<HashMap<String, String>> sEstimateListCount = objJsonUtil.getListofHashMapFromJSONResponse(ListEstimateServicesAPIConstants.xpathEstimateCount);
				int EstimateListCount = sEstimateListCount.size();
				String sActualsEstimateListCount  = Integer.toString(EstimateListCount);
				String sEstimateListCounts = hmTestData.get(ListEstimateServicesAPIConstants.PAGECOUNTVALUES);
				int iEstimateListCount = Integer.parseInt(sEstimateListCounts)-1;
				String sExpectedsEstimateListCount  = Integer.toString(iEstimateListCount);
				objAPIAssertion.assertEquals(sActualsEstimateListCount, sExpectedsEstimateListCount, "Validation of Estimate List count In Page");
				
				List<String> sActualStatusOfEstimate= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathStatusOfEstimate);					
				String sExpectedStatusOfEstimate = Expected_Valid_Status;		
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusOfEstimate, sExpectedStatusOfEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusOfEstimate+"]");
				
			
				List<String> sActualEstimateName= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathEstimateName);					
				String sExpectedEstimateName = ExpectedData.EstimateName();	
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateName, sExpectedEstimateName);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Name ["+sExpectedEstimateName+"]");
				
				List<String> sActualEstimateID= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathEstimateID);					
				String sExpectedEstimateID = ExpectedData.EstimateID();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateID, sExpectedEstimateID);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate ID ["+sExpectedEstimateID+"]");
			
				List<String> sActualCountryCode= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathCountryCode);					
				String sExpectedCountryCode = ExpectedData.CountryCode();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualCountryCode, sExpectedCountryCode);
				objAPIAssertion.assertTrue(allMatch, "Validation For Country Code ["+sExpectedCountryCode+"]");
				
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidateListEstimateServiceResponseForManditoryFields() method of ListEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ListEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void ValidationForInvalidStatus() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ListEstimateServicesAPITest.sFileName, hmTestData, ListEstimateServicesAPITest.isMultiSet, ListEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				List<HashMap<String, String>> sEstimateListCount = objJsonUtil.getListofHashMapFromJSONResponse(ListEstimateServicesAPIConstants.xpathEstimateCount);
				int EstimateListCount = sEstimateListCount.size();
				String sActualsEstimateListCount  = Integer.toString(EstimateListCount);
				String sEstimateListCounts = hmTestData.get(ListEstimateServicesAPIConstants.PAGECOUNTVALUES);
				int iEstimateListCount = Integer.parseInt(sEstimateListCounts)-1;
				String sExpectedsEstimateListCount  = Integer.toString(iEstimateListCount);
				objAPIAssertion.assertEquals(sActualsEstimateListCount, sExpectedsEstimateListCount, "Validation of Estimate List count In Page");
				
				
				List<String> sActualStatusOfEstimate= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathStatusOfEstimate);					
				String sExpectedStatusOfEstimate = Expected_InValid_Status;		
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusOfEstimate, sExpectedStatusOfEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusOfEstimate+"]");
				
			}catch(Exception e) {
			String sErrMessage="FAIL: ValidationForInvalidStatus() method of ListEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ListEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}

	public void SearchEstimatesWithEstimateName() throws Exception {
		String sRequestBody=null; 
		JsonUtil objJsonUtil=null;
		boolean allMatch=false;
		try {
				
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(ListEstimateServicesAPITest.sFileName, hmTestData, ListEstimateServicesAPITest.isMultiSet, ListEstimateServicesAPITest.TestDataFile);			
				ObjectMapper mapper = new ObjectMapper();
				Object json = mapper.readValue(sRequestBody, Object.class);
				sRequestBody = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			
				String sToken = objJsonUtil.getAccessToken("access_token");
				objJsonUtil.postRequest(sToken);
				
				String sCountryCode = hmConfig.get(Constants.ExcelHeaderRunConfig);
				ListEstimateAPIExpectedData ExpectedData = new ListEstimateAPIExpectedData(sCountryCode);
		
				List<String> sActualStatusOfEstimate= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathStatusOfEstimate);					
				String sExpectedStatusOfEstimate = Expected_Valid_Status;		
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualStatusOfEstimate, sExpectedStatusOfEstimate);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Status["+sExpectedStatusOfEstimate+"]");
				
				List<E> sGetListOfOwnerName= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathExpOwnerName);					
				List<String> sActualOwnerName = new ArrayList<String>();	
				for(E  sGetList: sGetListOfOwnerName)
				{
					String sGetLsitWithBrace = sGetList.toString();
					String sGetListWithoutBrace = sGetLsitWithBrace.replaceAll("\\[", "").replaceAll("\\]","");
					String sListOfOwnerName  = sGetListWithoutBrace.toUpperCase();
					sActualOwnerName.add(sListOfOwnerName);
				}
				String sExpectedOwnerName  = ExpectedData.OwnerName(hmConfig).toUpperCase();
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualOwnerName, sExpectedOwnerName);
				objAPIAssertion.assertTrue(allMatch, "Validation For Owner Name ["+sExpectedOwnerName+"]");
				
				List<String> sActualEstimateName= objJsonUtil.getListofStringsFromJSONResponse(ListEstimateServicesAPIConstants.xpathEstimateName);					
				String sExpectedEstimateName = ExpectedData.EstimateName();	
				allMatch= Util.compareListOfValuesWithExpectedValueMoreThanOne(sActualEstimateName, sExpectedEstimateName);
				objAPIAssertion.assertTrue(allMatch, "Validation For Estimate Name ["+sExpectedEstimateName+"]");
				
			
			}catch(Exception e) {
			String sErrMessage="FAIL: SearchEstimatesWithEstimateName() method of ListEstimateServiceAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			ListEstimateServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
}

