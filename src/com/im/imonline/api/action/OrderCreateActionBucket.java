package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;
import org.w3c.dom.NodeList;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.XMLUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.business.OrderCreateAPIUtility;
import com.im.imonline.api.constants.OrderCreateAPIConstant;
import com.im.imonline.api.tests.OrderCreateAPITest;

public class OrderCreateActionBucket {
	Logger logger = Logger.getLogger("OrderCreateActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public OrderCreateActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}


	public void createAndValidateStockOrder(String [] sArrayToVerify) throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;

		try {
			sRequestBody = AppUtil.getRequestBodyForSOAPRequest(OrderCreateAPITest.sFileName, hmTestData, OrderCreateAPITest.isMultiSet, OrderCreateAPITest.TestDataFile);			
			objXMLUtil = new XMLUtil(sRequestBody, hmTestData,hmConfig);
			objXMLUtil.postRequest();
	
			objAPIAssertion.assertHardEquals(String.valueOf(objXMLUtil.getStatusCode()), "200", "Status response expected [200] Actual ["+String.valueOf(objXMLUtil.getStatusCode())+"]");
			NodeList listNodeToBeVerified = objXMLUtil.getListOfNodeForTagName(OrderCreateAPIConstant.Line);			
						
			CommonValidation cValidation = new CommonValidation(objAPIAssertion);
			List<HashMap<String, String>> lstOfTestDataFromResponse = OrderCreateAPIUtility.getValidDataResponse(listNodeToBeVerified);
			cValidation.validateMultiSetDetails(hmTestData, lstOfTestDataFromResponse, sArrayToVerify, OrderCreateAPITest.TestDataFile);
		
		}
		catch(Exception e) {
			String sErrMessage="FAIL: createAndValidateStockOrder() method of OrderCreateActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);		
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			OrderCreateAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });
		}
	}
	
	
	

}
