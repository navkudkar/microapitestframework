package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.DartDetailsServicesForUSAPIConstants;
import com.im.imonline.api.tests.DartDetailsServicesForUSAPITest;

public class DartDetailsServiceForUSAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("DartDetailsServiceForUSAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public DartDetailsServiceForUSAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	public void USValidateDartDetailsServicesResponse() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null; boolean allMatch=false;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForUSAPITest.sFileName, hmTestData, DartDetailsServicesForUSAPITest.isMultiSet, DartDetailsServicesForUSAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathStatus);
				String sExpectedExpStatus = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUS);
				objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code ");
			
				String sActualExpDealId = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathDealId);
				String sExpectedExpDealId = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPDEALID);
				objAPIAssertion.assertEquals(sActualExpDealId, sExpectedExpDealId, " Verify Deal Id ");
				
				String sActualAutorizationNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathAuthorizationNumber);
				String sExpectedAutorizationNumber = hmTestData.get(DartDetailsServicesForUSAPIConstants.AUTHORIZATIONNUMBER);
				objAPIAssertion.assertEquals(sActualAutorizationNumber , sExpectedAutorizationNumber, "Verify Autorization Number");
				
				String sActualCustomerName = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathCustomerName);
				String sExpectedCustomerName = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPCUSTOMERNAME);
				objAPIAssertion.assertEquals(sActualCustomerName , sExpectedCustomerName, "Verify Customer Name ");
				
				String sActualBranchCustNumber = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathBranchCustNumber);
				String sExpectedBranchCustNumber = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPBRANCHCUSTNUMBER);
				objAPIAssertion.assertEquals(sActualBranchCustNumber , sExpectedBranchCustNumber, "Verify Branch Number");
				
				//productInformation check starts here	 
				List<String> sActualManufacturePartNumber= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathManufacturePartNumber);					
				String sExpectedManufacturePartNumber = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPMANUFACTUREPARTNUMBER);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualManufacturePartNumber, sExpectedManufacturePartNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation of Manufacturing Part Number["+sExpectedManufacturePartNumber+"]");
				
				//Sku mapping
				allMatch =false;
				List<String> sActualSku= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathsku);					
				String sExpectedSku = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSKU);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualSku, sExpectedSku);
				objAPIAssertion.assertTrue(allMatch, "Validation of Sku["+sExpectedSku+"]");
				
				//Approved Quantity
				allMatch =false;
				List<String> sActualApprovedQuantity= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathApprovedQuantity);					
				String sExpectedApprovedQuantity = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPAPPROVEDQUANTITY);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualApprovedQuantity, sExpectedApprovedQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation of Approved Quantity["+sExpectedApprovedQuantity+"]");
				
				//Remaing quantity
				allMatch =false;
				List<String> sActualRemainingQuantity= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathRemainingQuantity);					
				String sExpectedRemainingQuantity = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPREMAININGQUANTITY);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualRemainingQuantity, sExpectedRemainingQuantity);
				objAPIAssertion.assertTrue(allMatch, "Validation of Remaining Quantity["+sExpectedRemainingQuantity+"]");
						
				//List Price
				allMatch =false;
				List<String> sActualListPrice= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathListPrice);					
				String sExpectedListPrice = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPLISTPRICE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualListPrice, sExpectedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation of List Price["+sExpectedListPrice+"]");
					
				//Distidiscount percentage
				allMatch =false;
				List<String> sActualDistiDiscountPercentage= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathDistiDiscountPercentage);					
				String sExpectedDistiDiscountPercentage = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPDISTIDISCOUNTPERCENTAGE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualDistiDiscountPercentage, sExpectedDistiDiscountPercentage);
				objAPIAssertion.assertTrue(allMatch, "Validation of Distidiscount Percentage["+sExpectedDistiDiscountPercentage+"]");
				
				
				//Extended List Price 
				allMatch =false;
				List<String> sActualExtendedListPrice= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathExtendedListPrice);					
				String sExpectedExtendedListPrice = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPEXTENDEDLISTPRICE);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualExtendedListPrice, sExpectedExtendedListPrice);
				objAPIAssertion.assertTrue(allMatch, "Validation of Extended List Price["+sExpectedExtendedListPrice+"]");
			
				//Acop Number
				allMatch =false;
				List<String> sActualAcopNumber= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathAcopNumber);					
				String sExpectedAcopNumber = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPACOPNUMBER);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualAcopNumber, sExpectedAcopNumber);
				objAPIAssertion.assertTrue(allMatch, "Validation of Acop Number["+sExpectedAcopNumber+"]");
			
				//Acop ID
				allMatch =false;
				List<String> sActualAcopeuid= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathAcopeuid);					
				String sExpectedAcopeuid = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPACOPEUID);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualAcopeuid, sExpectedAcopeuid);
				objAPIAssertion.assertTrue(allMatch, "Validation of AcopID["+sExpectedAcopeuid+"]");
			
				//Zero Claim
				allMatch =false;
				List<String> sActualZeroClaim= objJsonUtil.getListofStringsFromJSONResponse(DartDetailsServicesForUSAPIConstants.xpathZeroClaim);					
				String sExpectedZeroClaim = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPZEROCLAIM);
				
				allMatch= Util.compareListOfValuesWithExpectedValue(sActualZeroClaim, sExpectedZeroClaim);
				objAPIAssertion.assertTrue(allMatch, "Validation of Zero Claim["+sExpectedZeroClaim+"]");		
		}catch(Exception e) {
			String sErrMessage="FAIL: USValidateDartDetailsServicesResponse() method of DartDetailsServiceForUSAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForUSAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void USInvalidDartDetails() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null; 
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForUSAPITest.sFileName, hmTestData, DartDetailsServicesForUSAPITest.isMultiSet, DartDetailsServicesForUSAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: USInvalidDartDetails() method of DartDetailsServiceForUSAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForUSAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });

		}
	}
	
	public void USDartDetailsForBlankCountryCode() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null; 
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForUSAPITest.sFileName, hmTestData, DartDetailsServicesForUSAPITest.isMultiSet, DartDetailsServicesForUSAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
				
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");		
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: USDartDetailsForBlankCountryCode() method of DartDetailsServiceForUSAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForUSAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}
	
	public void USDartDetailsForDifferentResellerAccount() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null; 
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForUSAPITest.sFileName, hmTestData, DartDetailsServicesForUSAPITest.isMultiSet, DartDetailsServicesForUSAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
						
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: USDartDetailsForDifferentResellerAccount() method of DartDetailsServiceForUSAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForUSAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}
	
	
	public void USDartDetailsForExpiredDart() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
				sRequestBody = AppUtil.getRequestBodyForSOAPRequest(DartDetailsServicesForUSAPITest.sFileName, hmTestData, DartDetailsServicesForUSAPITest.isMultiSet, DartDetailsServicesForUSAPITest.TestDataFile);			
				objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
				objJsonUtil.postRequest();
		
				String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
				
				String sActualExpInvalidDartStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathInvalidDartStatus);
				String sExpectedExpInvalidDartStatus = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPINVALIDDARTSTATUS);
				objAPIAssertion.assertEquals(sActualExpInvalidDartStatus, sExpectedExpInvalidDartStatus, " verify for Status ");
			
				
				String sActualExpStatusReason = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusReason);
				String sExpectedExpStatusReason = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSREASON);
				objAPIAssertion.assertEquals(sActualExpStatusReason, sExpectedExpStatusReason, " Verify Status Reason ");
				
				String sActualExpStatusCode = objJsonUtil.getActualValueFromJSONResponseWithoutModify(DartDetailsServicesForUSAPIConstants.xpathstatusCode);
				String sExpectedExpStatusCode = hmTestData.get(DartDetailsServicesForUSAPIConstants.EXPSTATUSCODE);
				objAPIAssertion.assertEquals(sActualExpStatusCode, sExpectedExpStatusCode, " Verify Status code ");
				
		}catch(Exception e) {
			String sErrMessage="FAIL: USDartDetailsForExpiredDart() method of DartDetailsServiceForUSAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			DartDetailsServicesForUSAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}

}
