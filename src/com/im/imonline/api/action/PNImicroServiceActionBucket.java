package com.im.imonline.api.action;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.SpecialPriceMicroserviceAPIValidation;
import com.im.imonline.api.constants.PNImicroserviceAPIConstants;
import com.im.imonline.api.tests.PNImicroServiceAPITest;

public class PNImicroServiceActionBucket {
	Logger logger = Logger.getLogger("PNImicroServiceActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;

	public PNImicroServiceActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	//CheckResponseStatus-[API healthCheckup] Validation
	private boolean validatePNIMicroService_AvailabilitySTATUS(JsonUtil pObjJSONUtil) throws Exception {

		boolean blnErrorStatus = true;
		try 
		{
			pObjJSONUtil.postRequest();
			//Get the Response values in a List variable - lstOfMapData
			String strAPI_Status = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(PNImicroserviceAPIConstants.XPATH_MICROSERVICE_STATUS);
			String strAPI_StatusCode = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(PNImicroserviceAPIConstants.XPATH_MICROSERVICE_STATUS_Code);
			String strAPI_ErrorCode = pObjJSONUtil.getActualValueFromJSONResponseWithoutModify(PNImicroserviceAPIConstants.XPATH_MICROSERVICE_ErrorMSG);

			if (!(strAPI_StatusCode.equalsIgnoreCase("S"))) {
				ExtentTestManager.log(Status.FAIL, "API Response Status: ["+strAPI_Status +"]", hmTestData.get(Constants.COUNTRY));
				ExtentTestManager.log(Status.FAIL, "Status Code is: ["+strAPI_StatusCode +"]", hmTestData.get(Constants.COUNTRY));
				ExtentTestManager.log(Status.FAIL, "Error Message Displayed as: ["+strAPI_ErrorCode +"]", hmTestData.get(Constants.COUNTRY));
				blnErrorStatus = false;
			}
		}catch(Exception e)	{
			String sErrMessage="FAIL: validatePNIMicroService_AvailabilitySTATUS() method of PNImicroServiceActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
			blnErrorStatus = false;
		}
		return blnErrorStatus;
	}


	//CheckResponseStatus-[Special Pricing Flags, Price & Discount] Validation
	public void validatePNIMicroServiceResponse(String[] pArrSpcPriceFlagValuesToVerify, String[] pArrSpcPriceDiscountDetailsToVerify) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(PNImicroServiceAPITest.sFileName, hmTestData, PNImicroServiceAPITest.isMultiSet, PNImicroServiceAPITest.TestDataFile);			
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			//API Health code needs to be implemented here.
			boolean blnAPIStatus = validatePNIMicroService_AvailabilitySTATUS(objJSONUtil);

			if (blnAPIStatus) 
			{
				//Get the Response values in a List variable - lstOfMapData [For Flags]
				List<HashMap<String, String>> lstOfMapData = objJSONUtil.getListofHashMapFromJSONResponse(PNImicroserviceAPIConstants.XPATH_SPCPRICE_FLAG_DETAILS);
				SpecialPriceMicroserviceAPIValidation SPValidation = new SpecialPriceMicroserviceAPIValidation(objAPIAssertion);

				List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, PNImicroServiceAPITest.TestDataFile);
				SPValidation.validatePNISPResponse(multisetDataDrivenMap, lstOfMapData, pArrSpcPriceFlagValuesToVerify);

				//Get the Response values in a List variable - lstOfMapData [For Discount Details]
				List<HashMap<String, String>> lstOfMapDataDiscDetails = objJSONUtil.getListofHashMapFromJSONResponse(PNImicroserviceAPIConstants.XPATH_SPCPRICE_MESSAGE_DETAILS);
				SPValidation.validatePNISPResponse(multisetDataDrivenMap, lstOfMapDataDiscDetails, pArrSpcPriceDiscountDetailsToVerify);
			}
		}

		catch(Exception e) {
			String sErrMessage="FAIL: validatePNIMicroServiceResponse() method of PNImicroServiceActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PNImicroServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });

		}

	}

	//CheckResponseStatus-[Special Pricing Flags, Price & Discount] Validation
	public void validatePNIMicroServiceResponse_withAllACOPs(String[] pArrSpcPriceFlagValuesToVerify, String[] pArrSpcPriceDiscountDetailsToVerify, String[] pArrAllACOPsSpcPriceDiscountDetailsToVerify) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		//Dynamic X-Path
		String sDynamicXPATH_SP_AllACOP_DETAILS = null;

		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(PNImicroServiceAPITest.sFileName, hmTestData, PNImicroServiceAPITest.isMultiSet, PNImicroServiceAPITest.TestDataFile);			
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			//API Health code needs to be implemented here.
			boolean blnAPIStatus = validatePNIMicroService_AvailabilitySTATUS(objJSONUtil);

			if (blnAPIStatus) 
			{
				//Get the Response values in a List variable - lstOfMapData [For Flags]
				List<HashMap<String, String>> lstOfMapData = objJSONUtil.getListofHashMapFromJSONResponse(PNImicroserviceAPIConstants.XPATH_SPCPRICE_FLAG_DETAILS);
				SpecialPriceMicroserviceAPIValidation SPValidation = new SpecialPriceMicroserviceAPIValidation(objAPIAssertion);

				List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, PNImicroServiceAPITest.TestDataFile);
				SPValidation.validatePNISPResponse(multisetDataDrivenMap, lstOfMapData, pArrSpcPriceFlagValuesToVerify);

				//Get the Response values in a List variable - lstOfMapData [For Discount Details]
				List<HashMap<String, String>> lstOfMapDataDiscDetails = objJSONUtil.getListofHashMapFromJSONResponse(PNImicroserviceAPIConstants.XPATH_SPCPRICE_MESSAGE_DETAILS);
				SPValidation.validatePNISPResponse(multisetDataDrivenMap, lstOfMapDataDiscDetails, pArrSpcPriceDiscountDetailsToVerify);

				objAPIAssertion.setExtentInfo("\n<<<-- Individual ACOP Discount Details - as Below -->>>\n");

				for (int i = 0; i<multisetDataDrivenMap.size(); i++) {

					//Dynamic XPATH change
					sDynamicXPATH_SP_AllACOP_DETAILS = "specialPrcingSummaryList[" + i + "].specialPricingDiscountsList";
					List<HashMap<String, String>> lstOfMapDataDiscDetails_AllACOPSindividual_fromXPath = objJSONUtil.getListofHashMapFromJSONResponse(sDynamicXPATH_SP_AllACOP_DETAILS);
					if(! hmTestData.get(Constants.ExcelHeaderRunConfig).equalsIgnoreCase("US"))

					{
						objAPIAssertion.assertTrue(lstOfMapDataDiscDetails_AllACOPSindividual_fromXPath==null, "No SpecialPricing Summary List available");

					}

					else {

						SPValidation.validateIndividualACOPdetailsSingleToMultipleSetRequest(multisetDataDrivenMap.get(i), lstOfMapDataDiscDetails_AllACOPSindividual_fromXPath,pArrAllACOPsSpcPriceDiscountDetailsToVerify, hmTestData.get(Constants.SKU_NUMBER));
					}
				} 
			}
			//ELSE
            else objAPIAssertion.assertTrue(blnAPIStatus==true, "Special Price Response PASS/FAIL validation");
		}

		catch(Exception e) {
			String sErrMessage="FAIL: validatePNIMicroServiceResponse() method of PNImicroServiceActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PNImicroServiceAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });

		}

	}
}
