package com.im.imonline.api.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.aventstack.extentreports.Status;
import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.utility.extentreport.ExtentTestManager;
import com.im.api.core.validation.CommonValidation;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.PieThon_PSDConstants;
import com.im.imonline.api.tests.PieThon_PSDAPITest;

public class PieThon_PSDActionBucket {
	Logger logger = Logger.getLogger("PNImicroServiceActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;

	public PieThon_PSDActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	

	//CheckResponseStatus-[API healthCheckup] Validation
	private boolean validate_PieThon_NewJSONResponseAvailabilitySTATUS(JsonUtil pObjJSONUtil, HashMap<String, String> hSingleSetData, String sTestDataFilenm) throws Exception {

		boolean blnErrorStatus = true;
		List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hSingleSetData, sTestDataFilenm);
		
		try 
		{
			pObjJSONUtil.postRequest();
			String sActualResponseCode = pObjJSONUtil.getStatusCode()+"";

			ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+==+=[Input Payload]+=+=+=+=+=+=+=+=+=++=+==+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));

			if (!(sActualResponseCode.equalsIgnoreCase("200"))) {
				ExtentTestManager.log(Status.INFO, "Service Input Payload Hit: DONE", hmTestData.get(Constants.COUNTRY));
				ExtentTestManager.log(Status.FAIL, "API Response Code Fetched: ["+ sActualResponseCode +"]", hmTestData.get(Constants.COUNTRY));
				blnErrorStatus = false;
			}
			else {
				/*
				//Input - Payload Reporting
				for(int i=0; i<multisetDataDrivenMap.size(); i++)
				{
					String sCountry = multisetDataDrivenMap.get(i).get(Constants.ExcelHeaderRunConfig);

					if (!(i==0)) {
						ExtentTestManager.log(Status.INFO, "-----------------------------------------------------", sCountry);
					}

				ExtentTestManager.log(Status.INFO, "Running for Multi set Test Data SR.NO ["+multisetDataDrivenMap.get(i).get(Constants.EXCELHEADER_SRNO) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [AP_STUS_CD]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_AP_STUS_CD) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [ASN_RU_VEND_NBR]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_ASN_RU_VEND_NBR) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [BOH_QTY]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_BOH_QTY) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [BR_NBR]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_BR_NBR) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [BYR_NBR]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_BYR_NBR) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [CNSG_ACCT_PO_FLG]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_CNSG_ACCT_PO_FLG) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [EDI_SEND_FLG]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_EDI_SEND_FLG) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [ETA_SRC_CD]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_ETA_SRC_CD) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [FOB_CD]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_FOB_CD) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [IMD_VIA_CD]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_IMD_VIA_CD) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [INV_UOM_CD]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_INV_UOM_CD) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [PART_NBR]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_PART_NBR) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [PRTY_FLG]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_PRTY_FLG) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [PURCHASE_ORDER_DAY]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_PURCHASE_ORDER_DAY) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [PURCHASE_ORDER_TIME]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_PURCHASE_ORDER_TIME) +"]", sCountry);
					ExtentTestManager.log(Status.INFO, "Input  [TERM_ID]: "+multisetDataDrivenMap.get(i).get(PieThon_ReplenishmentConstants.INPUT_TERM_ID) +"]", sCountry);
					
				}
				*/
			}


		}catch(Exception e)	{
			String sErrMessage="FAIL: validate_PieThon_NewJSONResponseAvailabilitySTATUS() method of PieThon_NewJSONreqResActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
			blnErrorStatus = false;
		}
		return blnErrorStatus;	}


	//CheckResponseStatus
	public void validate_PieThon_PSD(String[] parrPredictedvaluesInResponseToVerify) throws Exception {
		String sRequestBody=null; JsonUtil objJSONUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(PieThon_PSDAPITest.sFileName, hmTestData, PieThon_PSDAPITest.isMultiSet, PieThon_PSDAPITest.TestDataFile);			
			objJSONUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);

			CommonValidation cValidation = new CommonValidation(objAPIAssertion);
			//API Health code needs to be implemented here.
			boolean blnAPIStatus = validate_PieThon_NewJSONResponseAvailabilitySTATUS(objJSONUtil, hmTestData, PieThon_PSDAPITest.TestDataFile);
			System.out.println(" ");
			
			if (blnAPIStatus) 
			{
				ExtentTestManager.log(Status.INFO, "", hmConfig.get(Constants.ExcelHeaderRunConfig));
				ExtentTestManager.log(Status.INFO, "=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+[Prediction Validation Below]=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+", hmConfig.get(Constants.ExcelHeaderRunConfig));
				
				String sActualResponseCode = objJSONUtil.getStatusCode()+"";
				ExtentTestManager.log(Status.INFO, "Service Input Payload Hit: DONE", hmConfig.get(Constants.ExcelHeaderRunConfig));
				objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status response expected [200], Actual ["+sActualResponseCode+"]");
				
				//Get the Response values in a List variable - lstOfMapData [For all the predicted values]
				
				//List<HashMap<String, String>> lstOfMapData = null;//objJSONUtil.getListofHashMapFromJSONResponse(PieThon_ReplenishmentConstants.XPATH_PieThone_ResponseROOT);
				List<HashMap<String, String>> lstOfMapData = objJSONUtil.getListofHashMapFromJSONResponse(PieThon_PSDConstants.XPATH_PieThone_ResponseROOT);
				List<String> lstOfPSDValues =	objJSONUtil.getListofStringsFromJSONResponse(PieThon_PSDConstants.XPATH_PieThone_ResponseROOT);
				List<HashMap<String, String>> lstExponenToNumber = new ArrayList<HashMap<String, String>>();
									
				for(HashMap<String, String> strResMap1: lstOfMapData) {
					HashMap<String, String> mapConvertedRes = new HashMap<>();
					for (Map.Entry mapElement : strResMap1.entrySet()) { 
						String key = mapElement.getKey().toString(); 
						
						if (key.equalsIgnoreCase("PSD")) {
							String value = Util.geStringofExponentialToNumber(mapElement.getValue().toString());
							mapConvertedRes.put(key, value);         
				            System.out.println(key + " : " + value);
				            break;
						}
						 
			        } 
					lstExponenToNumber.add(mapConvertedRes);				
				}
				
				List<HashMap<String,String>> multisetDataDrivenMap = AppUtil.getMultiSetDataAsPerCategory(hmTestData, PieThon_PSDAPITest.TestDataFile);
				cValidation.validateMultiSetDetailsForREST(multisetDataDrivenMap, lstExponenToNumber, parrPredictedvaluesInResponseToVerify);
			}
		}

		catch(Exception e) {
			String sErrMessage="FAIL: validate_PieThon_Replenishment() method of PieThon_NewJSONreqResActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PieThon_PSDAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJSONUtil.getResponse().asString() });

		}

	}
}
