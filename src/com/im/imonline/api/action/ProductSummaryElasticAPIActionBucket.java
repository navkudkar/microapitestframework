package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.api.validation.ProductSummaryElasticAPIValidation;
import com.im.imonline.api.business.ProductSummaryRequestDataConfig;
import com.im.imonline.api.constants.ProductSearchElasticAPIConstants;
import com.im.imonline.api.tests.ProductSummaryElasticAPITest;

public class ProductSummaryElasticAPIActionBucket {
	Logger logger = Logger.getLogger("ProductSummaryElasticAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;
	String sRequestBody=null; 
	ProductSummaryRequestDataConfig objConfig = null;
	ProductSummaryElasticAPIValidation objVal= null;
	 

	public ProductSummaryElasticAPIActionBucket(APIDriver pAPIDriver, ProductSummaryRequestDataConfig objConfig2) {
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
		objConfig=objConfig2;
		hmTestData=new HashMap<String, String>();
		hmTestData=objConfig.gethmTestData();
		objVal=new ProductSummaryElasticAPIValidation(objAPIAssertion,objConfig);
	}
	
	
	public void performResponseValidation() throws Exception {			
		JsonUtil objJSONUtil=null;
		String jsonRes=null;
		try {
			if(objConfig.isDataCaptured())
			{
				sRequestBody = AppUtil.getRequestBodyForRestRequest(ProductSummaryElasticAPITest.sFileName,hmTestData,ProductSummaryElasticAPITest.isMultiSet, ProductSummaryElasticAPITest.TestDataFile);
				objJSONUtil = new JsonUtil(sRequestBody, hmTestData);
				objJSONUtil= new JsonUtil("\r\n"+sRequestBody+"\r\n"+"", hmTestData);		
				objJSONUtil.postRequest();
				jsonRes=objJSONUtil.getResponse().asString();
				logger.info("TestCase : "+Thread.currentThread().getStackTrace()[2].getMethodName()+
						"\n******* Request For ["+hmTestData.get(Constants.ExcelHeaderRunConfig)+"] *******\n"+sRequestBody+
						"\n*******Response for Country"+hmTestData.get(Constants.ExcelHeaderRunConfig)+"*******\n"+" : "+jsonRes+
						"\n******************************************************************************************************");
				objVal.performValidationforGivenConfig(objJSONUtil);
			}
			else
			{
				//objAPIAssertion.assertTrue(true, "Skipping this case");
				objAPIAssertion.setExtentSkip("Data not captured for the test case");
			}
		}
		catch(Exception e) {
			String sErrMessage="FAIL: performResponseValidation() method of ProductSummaryElasticAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}
		finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			if(objConfig.isDataCaptured())
				ProductSummaryElasticAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,jsonRes,objJSONUtil.countTheOccuranceOfSpecifiedField(ProductSearchElasticAPIConstants.PRODUCTBEGINING) });
		}

	}

}
