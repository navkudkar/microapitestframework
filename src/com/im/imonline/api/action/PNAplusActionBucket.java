package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.XMLUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.PNAPlusAPIConstant;
import com.im.imonline.api.tests.PNAplusTest;

public class PNAplusActionBucket {
	Logger logger = Logger.getLogger("PNAplusActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public PNAplusActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) {
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}
	
	public void validateSpecialPricingFlag() throws Exception {
		String sRequestBody=null; XMLUtil objXMLUtil=null;
		try {
		sRequestBody = AppUtil.getRequestBodyForSOAPRequest(PNAplusTest.sFileName, hmTestData, PNAplusTest.isMultiSet, PNAplusTest.TestDataFile);			
		objXMLUtil = new XMLUtil(sRequestBody, hmTestData,hmConfig);
		objXMLUtil.postRequest();
		
		String sActualResponseCode = objXMLUtil.getStatusCode()+"";		
		objAPIAssertion.assertHardEquals(sActualResponseCode, "200", "Status Code Validation");
	
		String sActual = objXMLUtil.getNodeValueByTagName(PNAPlusAPIConstant.COLUMNID);
		String sExpected = hmTestData.get(PNAPlusAPIConstant.COLUMNID);		
		objAPIAssertion.assertEquals(sActual, sExpected, "special price flag validation");
				
		}
		catch(Exception e) {
			String sErrMessage="FAIL: validateSpecialPricingFlag() method of PNAplusActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			PNAplusTest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objXMLUtil.getResponse().asString() });

		}
	}

}
