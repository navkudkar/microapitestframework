package com.im.imonline.api.action;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.im.api.core.business.AppUtil;
import com.im.api.core.business.JsonUtil;
import com.im.api.core.common.Constants;
import com.im.api.core.common.Util;
import com.im.api.core.wrapper.APIAssertion;
import com.im.api.core.wrapper.APIDriver;
import com.im.api.core.wrapper.ToolAPI;
import com.im.imonline.api.constants.CloneQuoteServicesAPIConstants;
import com.im.imonline.api.tests.CloneQuoteServicesAPITest;

public class CloneQuoteAPIActionBucket<E> 
{
	Logger logger = Logger.getLogger("CloneQuoteAPIActionBucket"); 
	HashMap<String, String> hmTestData = null;
	HashMap<String, String> hmConfig = null;
	APIAssertion objAPIAssertion = null;


	public CloneQuoteAPIActionBucket(HashMap<String, String> phmTestData, HashMap<String, String> phmConfig,APIDriver pAPIDriver) 
	{
		hmTestData=phmTestData;
		hmConfig=phmConfig;
		objAPIAssertion = ToolAPI.getAPIAssertion(pAPIDriver);
	}

	public void validateManditoryFieldsForCloneQuoteDetails() throws Exception {
		String sRequestBody=null; JsonUtil objJsonUtil=null;
		try {
			sRequestBody = AppUtil.getRequestBodyForRestRequest(CloneQuoteServicesAPITest.sFileName, hmTestData, CloneQuoteServicesAPITest.isMultiSet, CloneQuoteServicesAPITest.TestDataFile);			
			objJsonUtil = new JsonUtil(sRequestBody, hmTestData,hmConfig);
			objJsonUtil.postRequest();
			
			String str =objJsonUtil.getResponse().asString();
			System.out.println(str);

			String sActualResponseCode = objJsonUtil.getStatusCode()+"";		
			objAPIAssertion.assertHardEquals(sActualResponseCode, hmTestData.get(CloneQuoteServicesAPIConstants.EXPSTATUSCODE), "Status Code Validation");

			String sActualExpStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CloneQuoteServicesAPIConstants.xpathStatusCode);
			String sExpectedExpStatus = hmTestData.get(CloneQuoteServicesAPIConstants.EXPSTATUSCODE);
			objAPIAssertion.assertEquals(sActualExpStatus, sExpectedExpStatus, " verify Status code in in the response ");

			String sActualExpResponseStatus = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CloneQuoteServicesAPIConstants.xpathResponseStatus);
			String sExpectedExpResponseStatus = hmTestData.get(CloneQuoteServicesAPIConstants.EXPRESPONSESTATUS);
			objAPIAssertion.assertEquals(sActualExpResponseStatus, sExpectedExpResponseStatus, " verify Response Status ");

			String sActualExpResponseMessage = objJsonUtil.getActualValueFromJSONResponseWithoutModify(CloneQuoteServicesAPIConstants.xpathResponsemessage);
			String sExpectedExpResponseMessage = hmTestData.get(CloneQuoteServicesAPIConstants.EXPRESPONSEMESSAGE);
			objAPIAssertion.assertEquals(sActualExpResponseMessage, sExpectedExpResponseMessage, " verify Response Message  ");
			


		}
		catch(Exception e) {
			String sErrMessage="FAIL: ValidateManditoryFieldsForCloneQuoteDetails() method of CloneQuoteAPIActionBucket: "+Util.getExceptionDesc(e);
			logger.fatal(sErrMessage);       
			objAPIAssertion.setHardAssert_TCFailsAndStops(sErrMessage,e);
		}finally {
			String sTestCaseName=(String) Reporter.getCurrentTestResult().getTestContext().getAttribute(AppUtil.METHOD+Thread.currentThread().getId());				
			CloneQuoteServicesAPITest.lstResultSet.add(new Object[] {hmTestData.get(Constants.EXCELHEADER_SRNO),sTestCaseName, sRequestBody,objJsonUtil.getResponse().asString() });
		}
	}




}

