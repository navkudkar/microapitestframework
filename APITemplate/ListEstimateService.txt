{
  "GetQuote" : {
    "ApplicationArea" : {
      "Sender" : {
        "LogicalID" : {
          "value" : "12345567"
        },
        "ComponentID" : {
          "value" : "B2B-3.0"
        },
        "ReferenceID" : {
          "value" : "reg id"
        }
      },
      "CreationDateTime" : "0001-01-01T00:00:00"
    },
    "DataArea" : {
      "Get" : {
        "Expression" : [ {
          "expressionLanguage" : "FromDate",
          "value" : "2019-07-23T09:31:51Z"
        }, {
          "expressionLanguage" : "ToDate",
          "value" : ""
        }, {
          "expressionLanguage" : "Estimate Name",
          "value" : "@NameDartEstimateValues"
        }, {
          "expressionLanguage" : "SortBy",
          "value" : "LastModificationDateTime"
        }, {
          "expressionLanguage" : "SortOrder",
          "value" : "SortOrderValue"
        }, {
          "expressionLanguage" : "StartIndex",
          "value" : "1"
        }, {
          "expressionLanguage" : "EndIndex",
          "value" : "@PageCountValues"
        } ]
      }
    }
  }
}