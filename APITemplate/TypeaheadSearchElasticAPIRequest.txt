{
    "id": "@id",
    "params": {
    	"query_field": "@query_field",
        "query_string": "@query_string",
        "pageStart": "@pageStart",
        "pageSize": "@pageSize",
        "resellersid": [
            "@resellersid"
        ],
        "customerKey": "@customerKey",
        "keywords": "@keywords",
        "webvisibleflag":@webvisibleflag,
        "authindex": "@authindex",
        "negativeauthindex": "@negativeauthindex",
        "custpromoindex": "@custpromoindex",
        "sapauthorization":"@sapauthorization",
        "branch": "@branch"
    }
}